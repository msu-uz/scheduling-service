import os
import random
import urllib.request
from faker import Faker

from django.core.files import File
from django.core.management.base import BaseCommand
from django.conf import settings

from api.models import applicant, user, role, catalog, group, Course

fake = Faker(["ru_RU"])

GROUPS_COUNT = 4
STUDENT_IN_GROUPS = 25


class Command(BaseCommand):
    def handle(self, **options):
        if not settings.DEBUG:
            print("can't be run in non-debug mode")
            return

        print("Generating testing data...")
        self.generate_groups()
        print("Data was successfully generated!")

    def generate_groups(self):
        student = role.Student.objects.all()

        for _ in range(GROUPS_COUNT):
            groups = group.StudentGroup()
            groups.name = fake.job()
            groups.department = fake.company()
            groups.course_id = random.randint(1, 4)
            groups.course_id = random.randint(1,4)
            groups.save()

            for module in groups.course.modulecourse_set.all():
                msg = group.ModuleStudentGroup()
                msg.group = groups
                msg.module_course = module
                msg.teacher_id = random.randint(role.Teacher.objects.first().id, role.Teacher.objects.last().id)
                msg.save()

            for i in range(STUDENT_IN_GROUPS):
                group_forming = group.StudentGroupForming()
                group_forming.group = groups
                group_forming.student = student.get(id=random.randint(student.first().id, student.last().id))
                group_forming.save()

