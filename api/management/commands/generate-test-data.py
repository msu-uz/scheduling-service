import os
import random
import urllib.request

from django.conf import settings
from django.core.files import File
from django.core.management.base import BaseCommand
from faker import Faker

from api.models import applicant, user, role, TaskType
from api.models import discipline, CourseType, group, Type, Difficulty, Topic, Task, Answer, Teacher

fake = Faker(["ru_RU"])
fake_en = Faker()

COURSE_COUNT = 4
MODULE_COURSE = 5
WEEK = 4
TOPICS_PER_WEEK = 5
DISCIPLINE = 5
TOPICS_PER_DISCIPLINE = 10
CONTROL_PER_DISCIPLINE = 8
CONTROL_TOPICS_PER_CONTROL = 5
DIFFICULTIES = 3
TYPES = 6
TASKS_PER_TOPICS_TYPE = 20
ITEMS_COUNT = 100
TEACHER_COUNT = 100
GROUPS_COUNT = 4
STUDENT_IN_GROUPS = 25


class Command(BaseCommand):
    def handle(self, **options):
        if not settings.DEBUG:
            print("can't be run in non-debug mode")
            return

        print("Generating testing data...")

        # self.generate_admin()
        self.generate_course_type()
        self.generate_applicants()
        self.generate_discipline()
        # self.genereate_tasks()
        self.generate_courses()
        # self.generate_groups()
        print("Data was successfully generated!")

    def generate_admin(self):
        u = user.User()
        u.username = "admin"
        u.set_password("admin")
        u.is_staff = True
        u.is_superuser = True
        u.save()

    def genereate_tasks(self):
        if TaskType.objects.all().count() == 0:
            types = [
                TaskType(name={"ru": "Казус", "uz": "Case"}),
                TaskType(name={"ru": "Тест", "uz": "Test"})
            ]
            TaskType.objects.bulk_create(types)
        if Difficulty.objects.all().count() == 0:
            difficulties = [
                Difficulty(name={"ru": "Легкая сложность", "uz": "Easy difficulty"}),
                Difficulty(name={"ru": "Средняя сложность", "uz": "Medium difficulty"}),
                Difficulty(name={"ru": "Тяжелая сложность", "uz": "Hard difficulty"}),
            ]
            Difficulty.objects.bulk_create(difficulties)
            print(f"Generated : Difficulties - {Difficulty.objects.all().count()}")
        elif Difficulty.objects.all().count() == 1:
            if Difficulty.objects.filter(id=1, name={"ru": "Легкая сложность", "uz": "Easy difficulty"}) is not None:
                difficulties = [
                    Difficulty(name={"ru": "Средняя сложность", "uz": "Medium difficulty"}),
                    Difficulty(name={"ru": "Тяжелая сложность", "uz": "Hard difficulty"}),
                ]
                Difficulty.objects.bulk_create(difficulties)
                print(f"Generated : Difficulties - {Difficulty.objects.all().count()}")
            elif Difficulty.objects.filter(id=1, name={"ru": "Средняя сложность", "uz": "Medium difficulty"}) is not None:
                difficulties = [
                    Difficulty(name={"ru": "Легкая сложность", "uz": "Easy difficulty"}),
                    Difficulty(name={"ru": "Тяжелая сложность", "uz": "Hard difficulty"}),
                ]
                Difficulty.objects.bulk_create(difficulties)
                print(f"Generated : Difficulties - {Difficulty.objects.all().count()}")
            elif Difficulty.objects.filter(id=1, name={"ru": "Тяжелая сложность", "uz": "Hard difficulty"}) is not None:
                difficulties = [
                    Difficulty(name={"ru": "Легкая сложность", "uz": "Easy difficulty"}),
                    Difficulty(name={"ru": "Средняя сложность", "uz": "Medium difficulty"}),
                ]
                Difficulty.objects.bulk_create(difficulties)
                print(f"Generated : Difficulties - {Difficulty.objects.all().count()}")
        elif Difficulty.objects.all().count() == 2:
            if Difficulty.objects.filter(id=1, name={"ru": "Легкая сложность", "uz": "Easy difficulty"}) is not None and Difficulty.objects.filter(id=2, name={"ru": "Средняя сложность", "uz": "Medium difficulty"}) is not None:
                difficulties = [
                    Difficulty(name={"ru": "Тяжелая сложность", "uz": "Hard difficulty"})
                    ]
                Difficulty.objects.bulk_create(difficulties)
                print(f"Generated : Difficulties - {Difficulty.objects.all().count()}")
            elif Difficulty.objects.filter(id=1, name={"ru": "Легкая сложность", "uz": "Easy difficulty"}) is not None and Difficulty.objects.filter(id=2, name={"ru": "Тяжелая сложность", "uz": "Hard difficulty"}) is not None:
                difficulties = [
                    Difficulty(name={"ru": "Средняя сложность", "uz": "Medium difficulty"})
                    ]
                Difficulty.objects.bulk_create(difficulties)
                print(f"Generated : Difficulties - {Difficulty.objects.all().count()}")
            elif Difficulty.objects.filter(id=1, name={"ru": "Средняя сложность", "uz": "Medium difficulty"}) is not None and Difficulty.objects.filter(id=2, name={"ru": "Тяжелая сложность", "uz": "Hard difficulty"}) is not None:
                difficulties = [
                    Difficulty(name={"ru": "Легкая сложность", "uz": "Easy difficulty"})
                    ]
                Difficulty.objects.bulk_create(difficulties)
                print(f"Generated : Difficulties - {Difficulty.objects.all().count()}")
            elif Difficulty.objects.filter(id=1, name={"ru": "Средняя сложность", "uz": "Medium difficulty"}) is not None and Difficulty.objects.filter(id=2, name={"ru": "Легкая сложность", "uz": "Easy difficulty"}) is not None:
                difficulties = [
                    Difficulty(name={"ru": "Тяжелая сложность", "uz": "Hard difficulty"})
                    ]
                Difficulty.objects.bulk_create(difficulties)
                print(f"Generated : Difficulties - {Difficulty.objects.all().count()}")
            elif Difficulty.objects.filter(id=1, name={"ru": "Тяжелая сложность", "uz": "Hard difficulty"}) is not None and Difficulty.objects.filter(id=2, name={"ru": "Легкая сложность", "uz": "Easy difficulty"}) is not None:
                difficulties = [
                    Difficulty(name={"ru": "Средняя сложность", "uz": "Medium difficulty"})
                    ]
                Difficulty.objects.bulk_create(difficulties)
                print(f"Generated : Difficulties - {Difficulty.objects.all().count()}")
            elif Difficulty.objects.filter(id=1, name={"ru": "Тяжелая сложность", "uz": "Hard difficulty"}) is not None and Difficulty.objects.filter(id=2, name={"ru": "Средняя сложность", "uz": "Medium difficulty"}) is not None:
                difficulties = [
                    Difficulty(name={"ru": "Легкая сложность", "uz": "Easy difficulty"})
                    ]
                Difficulty.objects.bulk_create(difficulties)
                print(f"Generated : Difficulties - {Difficulty.objects.all().count()}")
        if Difficulty.objects.all().count() == 3:
            data = []
            for i in range(1, Difficulty.objects.all().count()+1):
                for j in range(1, TaskType.objects.all().count()+1):
                    types = [
                        Type(task_type_id=j, difficulty_id=i, duration_seconds=150*i*j)
                        ]
                    data += types
            Type.objects.bulk_create(data)
            print(f"Generated : Task types - {Type.objects.all().count()}")
            for i in range(1, Topic.objects.all().count()+1):
                for j in range(1, Type.objects.all().count()+1):
                    for a in range(TASKS_PER_TOPICS_TYPE):
                        tasks = [
                            Task(type_id=j, topic_id=i, creator_id=random.randint(Teacher.objects.first().id,
                                                                                  Teacher.objects.last().id),
                                 question={"ru": fake.sentence(nb_words=10), "uz": fake_en.sentence(nb_words=10)})
                        ]
                        Task.objects.bulk_create(tasks)
            print(f"Generated : Tasks - {Task.objects.all().count()}")
            for i in range(1, Task.objects.all().count()+1):
                if Task.objects.filter(id=i, type__task_type__name={"ru": "Казус", "uz": "Case"}):
                    answer = [
                        Answer(task_id=i, answer={"ru": fake.paragraphs(nb=4), "uz": fake_en.paragraphs(nb=3)},
                               is_correct=True)
                    ]
                    Answer.objects.bulk_create(answer)
                else:
                    for q in range(3):
                        answer = [
                            Answer(task_id=i, answer={"ru": fake.word(), "uz": fake_en.word()}, is_correct=False)
                        ]
                        Answer.objects.bulk_create(answer)
                    answer_2 = [
                        Answer(task_id=i, answer={"ru": fake.word(), "uz": fake_en.word()}, is_correct=True)
                    ]
                    Answer.objects.bulk_create(answer_2)

            print(f"Generated : Task answers - {Answer.objects.all().count()}")
        else:
            print("Difficulties have already been created")

    def generate_course_type(self):
        if CourseType.objects.all().count() == 0:
            types = [
                CourseType(name={'ru': 'Очное', 'uz': 'Traditional'}),
                CourseType(name={'ru': 'Дистанционное', 'uz': 'Remote'})
            ]
            CourseType.objects.bulk_create(types)
            print(f"Generated : Course type - 2")
        elif CourseType.objects.all().count() == 1:
            types = [
                CourseType(name={'ru': 'Очное', 'uz': 'Traditional'}),
            ]
            CourseType.objects.bulk_create(types)
            print(f"Generated : Course type - 1")

        else:
            print(f"Course type already exist")

    def generate_applicants(self):
        print("Downloading testing image...")
        url = "https://upload.wikimedia.org/wikipedia/commons/0/06/Korean_woman.jpg"
        testing_photo_path = os.path.join(settings.MEDIA_ROOT, "face.jpg")
        file_name = os.path.basename(testing_photo_path)
        urllib.request.urlretrieve(url, testing_photo_path)
        print("Image successfully downloaded!")
        types = [
            CourseType(name={'ru': 'Очное', 'uz': 'Traditional'}),
            CourseType(name={'ru': 'Дистанционное', 'uz': 'Remote'})
        ]
        CourseType.objects.bulk_create(types)
        for _ in range(ITEMS_COUNT):
            # generate applicants parents
            parent_info = applicant.ApplicantParentInfo()
            parent_info.full_name = fake.name()
            parent_info.birth_date = fake.date()
            parent_info.phone_number = fake.phone_number()
            parent_info.job_place = "OSG"
            parent_info.job_address = fake.address()
            parent_info.save()

            # generate applicants
            a = applicant.Applicant()
            full_name = fake.name().split(" ")
            a.first_name = full_name[0]
            a.last_name = full_name[1]
            a.second_name = full_name[2]
            a.mother_info = parent_info
            a.father_info = parent_info
            a.birth_date = fake.date()
            a.sex = applicant.Applicant.MALE
            a.passport_number = 123123123
            a.passport_address = fake.address()
            a.birth_place = fake.address()
            a.education_job = fake.job()
            a.living_address = fake.address()
            a.university_name = "harvard"
            a.specialization = fake.job()
            a.university_study_start_year = fake.date()
            a.university_study_end_year = fake.date()
            a.university_foreign_language = "english"
            a.university_study_language = "english"
            a.job_place = "Online Service Group"
            a.job_address = fake.address()
            a.war_ticket_info = "Проходил службу в воздушных войсках"
            a.save()

            a.image.save(file_name, File(open(testing_photo_path, "rb")))

            # generate applicants results
            applicant_result = applicant.ApplicantExamResults()
            applicant_result.applicant = a
            applicant_result.first_exam_points = random.randint(0, 100)
            applicant_result.second_exam_points = random.randint(0, 100)
            applicant_result.save()

            # generate user datq
            u = user.User()
            full_name = fake.name().split(" ")
            u.username = fake.profile()['username']
            u.set_password("123123d.")
            u.first_name = full_name[0]
            u.last_name = full_name[1]
            u.middle_name = full_name[2]
            u.access_level = 1  # Student level
            u.phone = fake.phone_number()
            u.save()

            u.profile_image = url

            # generate student data
            s = role.Student()
            s.applicant = a
            s.user = u
            s.course_type_id = random.randint(CourseType.objects.first().id, CourseType.objects.last().id)
            s.save()

        print(f"Generated : Applicants - {ITEMS_COUNT}")
        print(f"Generated : Students - {ITEMS_COUNT}")

        # for _ in range(TEACHER_COUNT):
        #     u = user.User()
        #     full_name = fake.name().split(" ")
        #     u.username = fake.profile()['username']
        #     u.set_password("123123d.")
        #     u.first_name = full_name[0]
        #     u.last_name = full_name[1]
        #     u.middle_name = full_name[2]
        #     u.access_level = 1  # Student level
        #     u.phone = fake.phone_number()
        #     u.save()
        #     u.profile_image = url
        #
        #     t = role.Teacher()
        #     t.user = u
        #     t.work_place = fake.address()
        #     t.teaching_load = random.randint(50, 200)
        #     t.save()
        #
        # print(f"Generated : Teachers - {TEACHER_COUNT}")

    def generate_discipline(self):
        for _ in range(DISCIPLINE):
            subject = discipline.Discipline()
            subject.name = {'ru': fake.job(), 'uz': fake_en.job()}

            subject.save()

            for i in range(TOPICS_PER_DISCIPLINE):
                topic = discipline.Topic()
                topic.discipline = subject
                topic.name = {'ru': fake.job(), 'uz': fake_en.job()}
                topic.content = {'ru': fake.sentence(nb_words=10), 'uz': fake_en.sentence(nb_words=10)}
                topic.save()
            for i in range(CONTROL_PER_DISCIPLINE):
                control = discipline.IntermediateControl()
                control.discipline = subject
                control.name = {'ru': fake.job(), 'uz': fake_en.job()}
                control.content = {'ru': fake.sentence(nb_words=10), 'uz': fake_en.sentence(nb_words=10)}
                control.save()
                for j in range(CONTROL_TOPICS_PER_CONTROL):
                    ict = discipline.IntermediateControlTopics()
                    ict.intermediate_control_id = control.id
                    ict.topic_id = random.randint(discipline.Topic.objects.first().id, discipline.Topic.objects.last().id)
                    ict.save()

        print(f"Generated : Discipline - {DISCIPLINE}")
        print(f"Generated : Topics - {DISCIPLINE * TOPICS_PER_DISCIPLINE}")
        print(f"Generated : Controls - {DISCIPLINE * CONTROL_PER_DISCIPLINE}")

    def generate_courses(self):
        for _ in range(COURSE_COUNT):
            course = discipline.Course()
            course.name = {'ru': fake.job(), 'uz': fake_en.job()}
            course.duration = random.randint(1, 50)
            course.anons = {'ru': fake.sentence(nb_words=10), 'uz': fake_en.sentence(nb_words=10)}
            course.regulation = {'ru': fake.paragraphs(nb=3), 'uz': fake_en.paragraphs(nb=3)}
            course.course_type_id = random.randint(CourseType.objects.first().id, CourseType.objects.last().id)
            course.save()

            for i in range(MODULE_COURSE):
                module = discipline.ModuleCourse()
                module.course = course
                module.discipline_id = random.randint(discipline.Discipline.objects.first().id,
                                                      discipline.Discipline.objects.last().id)
                module.lecture_duration = random.randint(10, 100)
                module.practice_duration = random.randint(10, 100)
                module.distantly_duration = random.randint(10, 100)
                module.save()

                for j in range(WEEK):
                    weeks = discipline.Week()
                    weeks.name = {'ru': fake.job(), 'uz': fake_en.job()}
                    weeks.module_course = module
                    weeks.save()

                    for c in range(TOPICS_PER_WEEK):
                        topic = discipline.ModuleWeekLessonDay()
                        topic.lesson_id = random.randint(discipline.Topic.objects.first().id,
                                                         discipline.Topic.objects.last().id)
                        topic.question_count = random.randint(0, 20)
                        topic.save()
                    control = discipline.ModuleWeekControlDay()
                    control.intermediate_control_id = random.randint(discipline.IntermediateControl.objects.first().id,
                                                                     discipline.IntermediateControl.objects.last().id)

                    mc = discipline.ModuleWeekType()
                    mc.week_day = weeks
                    topic = discipline.ModuleWeekLessonDay()
                    topic.lesson_id = random.randint(discipline.Topic.objects.first().id,
                                                     discipline.Topic.objects.last().id)
                    # topic.week_day = weeks
                    topic.question_count = random.randint(0, 20)
                    topic.deadline_duration = random.randint(0, 5)
                    topic.save()
                    mc.lesson = topic
                    mc.save()
                    control = discipline.ModuleWeekControlDay()
                    control.intermediate_control_id = random.randint(discipline.IntermediateControl.objects.first().id,
                                                                     discipline.IntermediateControl.objects.last().id)
                    # control.week_day = weeks
                    mc = discipline.ModuleWeekType()
                    mc.week_day = weeks

                    control.easy_question_count = random.randint(0, 20)
                    control.medium_question_count = random.randint(0, 20)
                    control.hard_question_count = random.randint(0, 20)
                    control.deadline_duration = random.randint(0, 5)
                    control.save()
                    mc.int_control = control
                    mc.save()

        print(f"Generated : Courses - {COURSE_COUNT}")
        print(f"Generated : Module course - {COURSE_COUNT * MODULE_COURSE}")
        print(f"Generated : Week - {COURSE_COUNT * MODULE_COURSE * WEEK}")

    def generate_groups(self):
        student = role.Student.objects.all()

        for _ in range(GROUPS_COUNT):
            groups = group.StudentGroup()
            groups.name = fake.job()
            groups.course_id = random.randint(discipline.Course.objects.first().id, discipline.Course.objects.last().id)
            groups.save()

            for module in groups.course.modulecourse_set.all():
                msg = group.ModuleStudentGroup()
                msg.group = groups
                msg.module_course = module
                msg.teacher_id = random.randint(role.Teacher.objects.first().id, role.Teacher.objects.last().id)
                msg.save()

            for i in range(STUDENT_IN_GROUPS):
                group_forming = group.StudentGroupForming()
                group_forming.group = groups
                group_forming.student = student.get(id=random.randint(student.first().id, student.last().id))
                group_forming.save()

        print(f"Generated : STUDENT GROUP - {GROUPS_COUNT} each has {STUDENT_IN_GROUPS} students")
