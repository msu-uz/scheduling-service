import os
import random
import urllib.request

from faker import Faker

from django.core.files import File
from django.core.management.base import BaseCommand
from django.conf import settings
from rest_framework.utils import json

from api.models import discipline

fake = Faker(["ru_RU"])
fake_en = Faker()

COURSE_COUNT = 4
MODULE_COURSE = 5
WEEK = 4
TOPICS_PER_WEEK = 5
DISCIPLINE = 5
TOPICS_PER_DISCIPLINE = 40
CONTROL_PER_DISCIPLINE = 8


class Command(BaseCommand):
    def handle(self, **options):
        if not settings.DEBUG:
            print("can't be run in non-debug mode")
            return

        print("Generating testing data...")
        self.generate_courses()
        print("Data was successfully generated!")

    def generate_courses(self):
        for _ in range(DISCIPLINE):
            subject = discipline.Discipline()
            subject.name = fake_en.json(data_columns={'ru': 'job', 'uz': 'job'}, num_rows=1)
            subject.name = fake.job()
            subject.save()

            for i in range(TOPICS_PER_DISCIPLINE):
                topic = discipline.Topic()
                topic.discipline = subject
                topic.name = {'ru': fake.job(), 'en': fake_en.job()}
                topic.content = {'ru': fake.sentence(nb_words=10), 'en': fake_en.sentence(nb_words=10)}
                topic.save()
            for i in range(CONTROL_PER_DISCIPLINE):
                control = discipline.IntermediateControl()
                control.discipline = subject
                control.name = {'ru': fake.job(), 'en': fake_en.job()}
                control.content = {'ru': fake.sentence(nb_words=10), 'en': fake_en.sentence(nb_words=10)}
                control.save()

        for _ in range(COURSE_COUNT):
            course = discipline.Course()
            course.name = {'ru': fake.job(), 'en': fake_en.job()}
            course.duration = random.randint(1, 50)
            course.anons = {'ru': fake.sentence(nb_words=10), 'en': fake_en.sentence(nb_words=10)}
            course.regulation = {'ru': fake.paragraphs(nb=3), 'en': fake_en.paragraphs(nb=3)}
            course.course_type_id = random.randint(1, 2)
            course.save()

            for i in range(MODULE_COURSE):
                module = discipline.ModuleCourse()
                module.course = course
                module.discipline_id = random.randint(discipline.Discipline.objects.first().id,
                                                      discipline.Discipline.objects.last().id)
                module.lecture_duration = random.randint(10, 100)
                module.practice_duration = random.randint(10, 100)
                module.distantly_duration = random.randint(10, 100)
                module.save()

                for j in range(WEEK):
                    weeks = discipline.Week()
                    weeks.name = {'ru': fake.job(), 'en': fake_en.job()}
                    weeks.module_course = module
                    weeks.save()

                    for c in range(TOPICS_PER_WEEK):
                        topic = discipline.ModuleWeekLessonDay()
                        topic.lesson_id = random.randint(discipline.Topic.objects.first().id,
                                                         discipline.Topic.objects.last().id)
                        topic.question_count = random.randint(0, 30)
                        topic.save()
                    control = discipline.ModuleWeekControlDay()
                    control.intermediate_control_id = random.randint(discipline.IntermediateControl.objects.first().id,
                                                                     discipline.IntermediateControl.objects.last().id)

                    control.week_day = weeks
                    mc = discipline.ModuleWeekType()
                    mc.week_day = weeks
                    topic = discipline.ModuleWeekLessonDay()
                    topic.lesson_id = random.randint(discipline.Topic.objects.first().id,
                                                     discipline.Topic.objects.last().id)
                    # topic.week_day = weeks
                    topic.question_count = random.randint(0, 30)
                    topic.save()
                    mc.lesson = topic
                    mc.save()
                    mc = discipline.ModuleWeekType()
                    mc.week_day = weeks
                    topic = discipline.ModuleWeekLessonDay()
                    topic.lesson_id = random.randint(discipline.Topic.objects.first().id,
                                                     discipline.Topic.objects.last().id)
                    # topic.week_day = weeks
                    topic.question_count = random.randint(0, 30)
                    topic.save()
                    mc.lesson = topic
                    mc.save()
                    control = discipline.ModuleWeekControlDay()
                    control.intermediate_control_id = random.randint(discipline.IntermediateControl.objects.first().id,
                                                                     discipline.IntermediateControl.objects.last().id)
                    # control.week_day = weeks
                    mc = discipline.ModuleWeekType()
                    mc.week_day = weeks

                    control.easy_question_count = random.randint(0, 30)
                    control.medium_question_count = random.randint(0, 30)
                    control.hard_question_count = random.randint(0, 30)
                    control.save()
                    mc.int_control = control
                    mc.save()
