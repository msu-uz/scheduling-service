from django.contrib.auth import get_user_model
from api.models.catalog import Category
from api.models.library import LibraryBook, TypeOfBook
import os
import random
import datetime
import urllib.request

from faker import Faker

from django.core.files import File
from django.core.management.base import BaseCommand
from django.conf import settings

fake = Faker(["ru_RU"])


User = get_user_model()


start_date = datetime.date(1900, 1, 1)
end_date = datetime.date(2020, 2, 1)

time_between_dates = end_date - start_date
days_between_dates = time_between_dates.days


class Command(BaseCommand):
    def handle(self, **options):
        if not settings.DEBUG:
            print("can't be run in non-debug mode")
            return

        print("Generating librarybooks...")
        self.generate_librarybooks()
        print("Data was successfully generated!")

    def random_date(self):
        random_number_of_days = random.randrange(days_between_dates)
        return start_date + datetime.timedelta(days=random_number_of_days)

    def generate_librarybooks(self):
        for i in range(40):
            print(f"Downloading test image #{i}...")
            url = "https://picsum.photos/400"
            testing_photo_path = os.path.join(settings.MEDIA_ROOT, "bookpic.jpg")
            file_name = os.path.basename(testing_photo_path)
            urllib.request.urlretrieve(url, testing_photo_path)

            lb = LibraryBook(
                title={"ru": fake.sentence(nb_words=4), "uz": fake.sentence(nb_words=3)},
                description={"ru": fake.sentence(nb_words=12), "uz": fake.sentence(nb_words=12)},
                author=fake.sentence(nb_words=2),
                category=Category.objects.all().order_by("?").first(),
                publish_date=self.random_date(),
                page_count=random.randint(15, 500),
                publisher=User.objects.all().order_by("?").first(),
                book_type=TypeOfBook.objects.order_by("?").first()
            )

            lb.image.save(file_name, File(open(testing_photo_path, "rb")))
            lb.file.save(file_name, File(open(testing_photo_path, "rb")))

            lb.save()
