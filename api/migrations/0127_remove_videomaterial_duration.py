# Generated by Django 3.1.2 on 2021-01-29 13:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0126_auto_20210128_1729'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='videomaterial',
            name='duration',
        ),
    ]
