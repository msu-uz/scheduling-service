# Generated by Django 3.1.2 on 2020-11-28 12:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0063_retake_passed'),
    ]

    operations = [
        migrations.AlterField(
            model_name='retake',
            name='description',
            field=models.TextField(),
        ),
    ]
