# Generated by Django 3.1.2 on 2020-11-23 06:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0046_auto_20201117_2228'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentgroup',
            name='course',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='api.course'),
        ),
        migrations.CreateModel(
            name='ModuleStudentGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('group', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='api.studentgroup')),
                ('module_course', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='api.modulecourse')),
                ('teacher', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='api.teacher')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
