# Generated by Django 3.1.2 on 2020-11-24 12:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0050_notification_notificationtemplate_pivotnotification_pivotnotificationtemplate'),
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('difficulty', models.IntegerField(choices=[(0, 'Легкая сложность'), (1, 'Средняя сложность'), (2, 'Тяжелая сложность')], default=0, verbose_name='Сложность')),
                ('is_test', models.BooleanField(default=False, verbose_name='Является ли тестом?')),
                ('question', models.JSONField(verbose_name='Задание')),
                ('status', models.BooleanField(default=False, verbose_name='Активен ли?')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.teacher', verbose_name='Автор')),
                ('task_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.tasktype', verbose_name='Тип задания')),
                ('topic', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.topic', verbose_name='Топик')),
            ],
            options={
                'db_table': 'task_task_type',
            },
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('answer', models.JSONField(verbose_name='Ответ')),
                ('is_correct', models.IntegerField(choices=[(0, 'Неправильно'), (1, 'Правильно'), (2, 'В ожидании')], default=2, verbose_name='Правилен ли ответ?')),
                ('student_answer', models.BooleanField(default=False, verbose_name='Прада или нет?')),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.task', verbose_name='Задание')),
            ],
            options={
                'db_table': 'test_task',
            },
        ),
        migrations.CreateModel(
            name='TaskRightAnswer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('right_answer', models.JSONField(verbose_name='Правильный ответ')),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.task', verbose_name='Задание')),
            ],
            options={
                'db_table': 'task_right_answer',
            },
        ),
    ]
