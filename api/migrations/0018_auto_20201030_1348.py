# Generated by Django 3.1.2 on 2020-10-30 13:48

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0017_auto_20201030_1148'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applicantresultslistvote',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
