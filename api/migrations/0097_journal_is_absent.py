# Generated by Django 3.1.2 on 2020-12-17 13:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0096_auto_20201217_1612'),
    ]

    operations = [
        migrations.AddField(
            model_name='journal',
            name='is_absent',
            field=models.BooleanField(default=False),
        ),
    ]
