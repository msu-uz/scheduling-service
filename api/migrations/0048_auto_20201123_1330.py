# Generated by Django 3.1.2 on 2020-11-23 08:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0047_auto_20201123_1137'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='name',
            field=models.JSONField(max_length=255),
        ),
    ]
