# Generated by Django 3.1.2 on 2020-10-29 05:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0014_applicant_qualification'),
    ]

    operations = [
        migrations.CreateModel(
            name='ApplicantParentInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('full_name', models.CharField(max_length=250)),
                ('birth_date', models.DateField()),
                ('birth_place', models.CharField(max_length=100)),
                ('job_place', models.CharField(max_length=100)),
                ('phone_number', models.CharField(max_length=20)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='applicant',
            name='father_info',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='father_info', to='api.applicantparentinfo'),
        ),
        migrations.AddField(
            model_name='applicant',
            name='mother_info',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='mother_info', to='api.applicantparentinfo'),
        ),
    ]
