# Generated by Django 3.1.2 on 2020-12-16 07:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0093_auto_20201215_1256'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hometaskanswerreview',
            name='is_checked',
        ),
    ]
