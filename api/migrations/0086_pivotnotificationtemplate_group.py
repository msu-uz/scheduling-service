# Generated by Django 3.1.2 on 2020-12-10 11:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0085_auto_20201210_1427'),
    ]

    operations = [
        migrations.AddField(
            model_name='pivotnotificationtemplate',
            name='group',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.studentgroupforming'),
        ),
    ]
