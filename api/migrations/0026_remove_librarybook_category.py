# Generated by Django 3.1.2 on 2020-11-02 11:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0025_auto_20201102_1124'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='librarybook',
            name='category',
        ),
    ]
