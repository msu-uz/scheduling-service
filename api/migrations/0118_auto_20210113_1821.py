# Generated by Django 3.1.2 on 2021-01-13 13:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0117_auto_20210112_1810'),
    ]

    operations = [
        migrations.AddField(
            model_name='teacher',
            name='can_check_test',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='teacher',
            name='can_create_test',
            field=models.BooleanField(default=False),
        ),
    ]
