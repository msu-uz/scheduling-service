# Generated by Django 3.1.2 on 2021-01-04 07:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0107_auto_20201223_1119'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskanswerreview',
            name='comment',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='taskanswerreviewcollection',
            name='total_mark',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
    ]
