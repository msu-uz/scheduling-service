from django.urls import path, include

from drf_yasg2.views import get_schema_view
from drf_yasg2 import openapi

from rest_framework import permissions


schema_view = get_schema_view(
   openapi.Info(
      title="RES API",
      default_version='v1',
      description="API for project RES",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="loki@sentry.uz"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('auth/', include("api.v1.auth.urls")),
    path('admission_board/', include("api.v1.admission_board.urls")),
    path('attendance/', include("api.v1.attendance.urls")),
    path('catalog/', include('api.v1.catalog.urls')),
    path('discipline/', include('api.v1.discipline.urls')),
    path('education-office/', include('api.v1.groups.urls')),
    path('library/', include('api.v1.library.urls')),
    path('schedule/', include('api.v1.schedule.urls')),
    path('statement/', include('api.v1.statement.urls')),

    path('notification/', include('api.v1.notification.urls')),
    path('feedback/', include('api.v1.feedback.urls')),
    path('teacher/', include('api.v1.teacher.urls')),
    path('load/', include('api.v1.load.urls')),
    path('student/', include('api.v1.student.urls')),
    path('admin/', include('api.v1.admin.urls')),
    path('task/', include('api.v1.task.urls')),
    path('resources/', include('api.v1.resources.urls')),
]
