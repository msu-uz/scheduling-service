import mptt
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.auth.admin import UserAdmin
from mptt.admin import MPTTModelAdmin

from .models import User
from .models.department import Department
from .models.role import AdmissionBoardUnit
from .models.group import Student, Teacher, StudentGroup, StudentGroupForming, ModuleStudentGroup
from .models.library import LibraryBook, LibraryShare, TypeOfBook, VideoMaterial, ModuleOfVideo, VideoRecording, \
    LessonMaterial
from .models.applicant import Applicant, ApplicantReview, ApplicantExamResults, ApplicantParentInfo, \
    ApplicantsResultsList
from .models.catalog import Category, LibraryElementCategory, LibraryElementType, CatalogLanguage, CourseType, TaskType, \
    Position, Organization, Nationality, Language
from .models.region import District, City

from .models.discipline import Topic, Course, Discipline, Section, ModuleCourse, Week, IntermediateControlTopics, \
    IntermediateControl, ModuleWeekLessonDay, ModuleWeekControlDay, FinalControl, FinalControlTopics, \
    ModuleWeekFinalControlDay
from .models.schedule import *
from .models.statement import *
from .models.resources import *
from .models.discipline import Topic, Course, Discipline, Section, ModuleCourse, Week, IntermediateControlTopics, \
    IntermediateControl, ModuleWeekLessonDay, ModuleWeekControlDay, FinalControl, FinalControlTopics, \
    ModuleWeekFinalControlDay, ModuleWeekType

from api.v1.admission_board.froms import AdmissionBoardUnitForm
from .models.discipline import Topic, Course, Discipline, Section, ModuleCourse, Week, IntermediateControlTopics, \
    IntermediateControl, ModuleWeekLessonDay, ModuleWeekControlDay, FinalControl, FinalControlTopics, \
    ModuleWeekFinalControlDay
from .models.task import *
from .models.feedback import CourseFeedback, CourseFeedbackAnswer, CourseFeedbackComment, CourseFeedbackQuestion, \
    ModuleCourseFeedback, ModuleCourseFeedbackAnswer, \
    ModuleCourseFeedbackQuestion
from .models.schedule import LessonEvent, LessonEventInterval
from .models.resources import Resource, ResourcesType
from .models.notification import Notification, NotificationTemplate, PivotNotification
from .models.employee import Employee, EmployeeRelative


class AdmissionBoardUnitAdmin(admin.ModelAdmin):
    form = AdmissionBoardUnitForm


class CustomUserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + ((None, {
        "fields": (
            "profile_image",
            "middle_name",
            "phone",
            "access_level",
        )
    }),)
    add_fieldsets = UserAdmin.add_fieldsets + ((None, {
        "fields": (
            "profile_image",
            "middle_name",
            "phone",
            "access_level",
        )
    }),)


admin.site.register(User, CustomUserAdmin)


class StudentAdmin(admin.ModelAdmin):
    def group(self, obj):
        return obj.student_groups.last()

    def username(self, obj):
        return obj.user.username

    list_display = ["id", "username", "employee", "applicant", "group", "is_student"]
    search_fields = ["user__username"]
    list_filter = ["is_student"]
    ordering = ["-created_at"]


admin.site.register(Student, StudentAdmin)


class StudentGroupAdmin(admin.ModelAdmin):

    def get_course(self, obj: StudentGroup):
        return f"{obj.course.id} - {obj.course.name.get('ru')}"

    list_display = ["id", "name", "get_course", "is_completed"]


admin.site.register(StudentGroup, StudentGroupAdmin)
admin.site.register(ModuleStudentGroup)

admin.site.register(StudentGroupForming)
admin.site.register(AdmissionBoardUnit, AdmissionBoardUnitAdmin)

admin.site.register(Teacher)
admin.site.register(Applicant)
admin.site.register(ApplicantsResultsList)
admin.site.register(ApplicantReview)
admin.site.register(LibraryBook)
admin.site.register(LibraryShare)
admin.site.register(TypeOfBook)
admin.site.register(VideoMaterial)
admin.site.register(ApplicantExamResults)
admin.site.register(ApplicantParentInfo)

admin.site.register(Category)
admin.site.register(LibraryElementCategory, MPTTModelAdmin)
admin.site.register(LibraryElementType)
admin.site.register(CatalogLanguage)
admin.site.register(CourseType)
admin.site.register(TaskType)
admin.site.register(Position)
admin.site.register(Organization)
admin.site.register(Nationality)
admin.site.register(Language)
admin.site.register(City)
admin.site.register(District)


# ------------------------------
class TopicInline(admin.TabularInline):
    model = Topic


admin.site.register(Topic)


# -------------------------------
class DisciplineAdmin(admin.ModelAdmin):
    model = Discipline
    inlines = (TopicInline,)


admin.site.register(Discipline, DisciplineAdmin)

# ------------------------------
admin.site.register(Section)
admin.site.register(Course)
admin.site.register(ModuleCourse)
admin.site.register(Week)
admin.site.register(IntermediateControl)
admin.site.register(FinalControl)
admin.site.register(FinalControlTopics)
admin.site.register(IntermediateControlTopics)
admin.site.register(ModuleWeekControlDay)
admin.site.register(ModuleWeekLessonDay)
admin.site.register(ModuleWeekFinalControlDay)


class JournalAdmin(admin.ModelAdmin):
    def lesson_event_date(self, obj: Journal):
        if obj.event:
            return f"{obj.event.date}"

    def lesson_event_interval(self, obj):
        if obj.event:
            if obj.event.interval:
                return f"{obj.event.interval.start_time} - {obj.event.interval.end_time}"

    def lesson_event(self, obj):
        if obj.event:
            return f"{obj.event.id}"

    def msg(self, obj: Journal):
        if obj.module_student_group:
            return f"{obj.module_student_group.id} - {next(iter(obj.module_student_group.module_course.discipline.name.values()))}"

    list_display = ["student", "msg", "lesson_event", "lesson_event_date", "lesson_event_interval",
                    "score",
                    "is_absent"]


admin.site.register(Journal, JournalAdmin)
admin.site.register(Retake)
admin.site.register(QualitativeAssessment)
admin.site.register(Event)
admin.site.register(EventType)


class LessonEventAdmin(admin.ModelAdmin):
    def module_course(self, obj):
        if obj.module_student_group:
            return obj.module_student_group.module_course

    list_display = ["id", "module_student_group", "date", "lesson", "module_course"]


admin.site.register(LessonEvent, LessonEventAdmin)
admin.site.register(LessonEventInterval)
admin.site.register(SpecialEvent)
admin.site.register(SpecialEventType)

# Resource


@admin.register(Resource)
class ResourceAdmin(ModelAdmin):
    list_display = ("id", "name", "type")
    list_filter = ("type",)


admin.site.register(ResourcesType)


# HomeTask


@admin.register(HomeTask)
class HomeTaskAdmin(ModelAdmin):
    list_display = ("id", "title", "creator", "module_course")
    list_filter = ("creator", "module_course__discipline")


@admin.register(HomeTaskReceiver)
class HomeTaskReceiverAdmin(ModelAdmin):
    list_display = ("id", "title", "home_task", "receiver", "start_date", "end_date")
    list_filter = ("home_task__creator",)

    def title(self, obj):
        return obj.home_task.title


@admin.register(HomeTaskAnswer)
class HomeTaskAnswerAdmin(ModelAdmin):
    list_display = ("id", "title", "receiver", "task", "created_at")
    list_filter = ("task__home_task__creator",)

    def receiver(self, obj):
        return obj.task.receiver


@admin.register(HomeTaskAnswerReview)
class HomeTaskAnswerReviewAdmin(ModelAdmin):
    list_display = ("id", "title", "receiver", "home_task_answer", "mark", "created_at")

    def title(self, obj):
        return obj.home_task_answer.task.home_task.title

    def receiver(self, obj):
        return obj.home_task_answer.task.receiver


admin.site.register(HomeTaskAnswerFiles)
admin.site.register(HomeTaskFiles)


# Feedback


@admin.register(CourseFeedbackQuestion)
class CourseFeedbackQuestionAdmin(ModelAdmin):
    list_display = ("__str__", "id", "course")
    list_filter = ("course",)


@admin.register(CourseFeedbackAnswer)
class CourseFeedbackAnswerAdmin(ModelAdmin):
    list_display = ("__str__", "id", "course_feedback_question")


@admin.register(CourseFeedback)
class CourseFeedbackAdmin(ModelAdmin):
    list_display = ("__str__", "id", "student", "course_feedback_answer")


@admin.register(ModuleCourseFeedbackQuestion)
class ModuleCourseFeedbackQuestionAdmin(ModelAdmin):
    list_display = ("__str__", "id", "module_course")


@admin.register(ModuleCourseFeedbackAnswer)
class ModuleCourseFeedbackAnswerAdmin(ModelAdmin):
    list_display = ("__str__", "id", "module_course_feedback_question")


@admin.register(ModuleCourseFeedback)
class ModuleCourseFeedbackAdmin(ModelAdmin):
    list_display = ("__str__", "id", "student", "module_course_feedback_answer")


@admin.register(CourseFeedbackComment)
class CourseFeedbackCommentAdmin(ModelAdmin):
    list_display = ("__str__", "id", "course", "student")


admin.site.register(Department)


class ModuleWeekTypeAdmin(admin.ModelAdmin):
    def mwt(self, obj: ModuleWeekType):
        return next(iter(obj.name.values()))

    list_display = ["id", "mwt"]


admin.site.register(ModuleWeekType, ModuleWeekTypeAdmin)
admin.site.register(Task)
admin.site.register(TaskReview)
admin.site.register(TaskHistory)


class TaskCollectionAdmin(admin.ModelAdmin):
    def module_course(self, obj: TaskCollection):
        if obj.module_week_type:
            return obj.module_week_type.week_day.module_course

    def topics(self, obj: TaskCollection):
        if obj.get_topics:
            return obj.get_topics.all().values_list("id", flat=True)

    list_display = ["id", "module_week_type", "is_active", "parent", "teacher", "module_course", "topics"]


admin.site.register(TaskCollection, TaskCollectionAdmin)


class TaskCollectionReceiversAdmin(admin.ModelAdmin):
    def answer(self, obj: TaskCollectionReceiver):
        return obj.taskanswercollection

    list_display = ["id", "task_collection", "student", "start_date", "end_date", "is_active", "answer"]


admin.site.register(TaskCollectionReceiver, TaskCollectionReceiversAdmin)
admin.site.register(Type)
admin.site.register(Difficulty)


class AnswerAdmin(admin.ModelAdmin):
    list_display = ["id", "is_correct", "task"]


admin.site.register(Answer, AnswerAdmin)
admin.site.register(TaskAnswer)
admin.site.register(TaskAnswerReview)
admin.site.register(TaskAnswerCollection)
admin.site.register(TaskAnswerReviewCollection)

admin.site.register(NotificationTemplate)
admin.site.register(Notification)
admin.site.register(PivotNotification)

admin.site.register(Employee)
admin.site.register(EmployeeRelative)

admin.site.register(ModuleOfVideo)
admin.site.register(VideoRecording)
admin.site.register(LessonMaterial)
