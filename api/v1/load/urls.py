from api.v1.load import views
from django.urls import path

urlpatterns = [
    path("today/", views.TodayLoadListView.as_view()),
    path("month/", views.MonthLoadListView.as_view()),
    path("year/", views.YearLoadListView.as_view()),
    path("period/", views.PeriodLoadListView.as_view()),
]
