from datetime import date, datetime

from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from api.models.role import Teacher
from api.v1.load.serializers import (
    TeacherTodaySerializer,
    TeacherMonthSerializer,
    TeacherPeriodSerializer,
    TeacherYearSerializer,
)


class TodayLoadListView(ListAPIView):
    serializer_class = TeacherTodaySerializer
    queryset = Teacher.objects.filter(
        modulestudentgroup__lessonevent__date=datetime.today()
    ).distinct()
    permission_classes = (IsAuthenticated,)


class MonthLoadListView(ListAPIView):
    serializer_class = TeacherMonthSerializer
    queryset = Teacher.objects.filter(
        modulestudentgroup__lessonevent__date__month=datetime.today().month
    ).distinct()
    permission_classes = (IsAuthenticated,)


class YearLoadListView(ListAPIView):
    serializer_class = TeacherYearSerializer
    queryset = Teacher.objects.filter(
        modulestudentgroup__lessonevent__date__gte=datetime.today()
    ).distinct()


class PeriodLoadListView(ListAPIView):
    serializer_class = TeacherPeriodSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        date_start = self.request.GET.get("date_start")
        date_end = self.request.GET.get("date_end")

        if date_start and date_end:
            start = date.fromisoformat(date_start)
            end = date.fromisoformat(date_end)

            queryset = Teacher.objects.filter(
                modulestudentgroup__lessonevent__date__gte=start,
                modulestudentgroup__lessonevent__date__lte=end,
            ).distinct()
        else:
            queryset = Teacher.objects.none()

        return queryset
