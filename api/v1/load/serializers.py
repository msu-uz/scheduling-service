from datetime import datetime

from api.models import LessonEvent
from api.models.role import Teacher
from rest_framework.serializers import ModelSerializer
from django.db.models import Q


class TeacherTodaySerializer(ModelSerializer):
    class Meta:
        model = Teacher
        fields = ("id", "user", "employee", "teaching_load")

    def to_representation(self, instance):
        response = super().to_representation(instance)

        plan_lesson_events = LessonEvent.objects.filter(
            module_student_group__teacher=instance, date=datetime.today()
        ).count()

        done_lesson_events = LessonEvent.objects.filter(
            module_student_group__teacher=instance,
            date=datetime.today(),
            interval__end_time__lte=datetime.now().time(),
        ).count()

        left_lesson_events = plan_lesson_events - done_lesson_events

        response["plan_hours"] = plan_lesson_events
        response["done_hours"] = done_lesson_events
        response["left_hours"] = left_lesson_events
        response["first_name"] = instance.employee.first_name
        response["last_name"] = instance.employee.last_name
        response["middle_name"] = instance.employee.second_name
        response["image"] = instance.employee.image

        return response


class TeacherMonthSerializer(ModelSerializer):
    class Meta:
        model = Teacher
        fields = ("id", "user", "employee", "teaching_load")

    def to_representation(self, instance):
        response = super().to_representation(instance)

        plan_lesson_events = LessonEvent.objects.filter(
            module_student_group__teacher=instance,
            date__month=datetime.today().month,
        ).count()

        done_lesson_events = LessonEvent.objects.filter(
            Q(
                module_student_group__teacher=instance,
                date__month=datetime.today().month,
            )
            & (
                Q(date__lt=datetime.today())
                | Q(
                    date=datetime.today(),
                    interval__end_time__lte=datetime.now().time(),
                )
            )
        ).count()

        left_lesson_events = plan_lesson_events - done_lesson_events

        response["plan_hours"] = plan_lesson_events
        response["done_hours"] = done_lesson_events
        response["left_hours"] = left_lesson_events
        response["first_name"] = instance.employee.first_name
        response["last_name"] = instance.employee.last_name
        response["middle_name"] = instance.employee.second_name
        response["image"] = instance.employee.image

        return response


class TeacherYearSerializer(ModelSerializer):
    class Meta:
        model = Teacher
        fields = ("id", "user", "employee", "teaching_load")

    def to_representation(self, instance):
        response = super().to_representation(instance)

        left_lesson_events = LessonEvent.objects.filter(
            Q(module_student_group__teacher=instance)
            & (
                Q(
                    date__gt=datetime.today(),
                )
                | Q(
                    date=datetime.today(),
                    interval__start_time__gte=datetime.now().time(),
                )
            )
        ).count()

        response["left_hours"] = left_lesson_events

        response["first_name"] = instance.employee.first_name
        response["last_name"] = instance.employee.last_name
        response["middle_name"] = instance.employee.second_name
        response["image"] = instance.employee.image

        return response


class TeacherPeriodSerializer(ModelSerializer):
    class Meta:
        model = Teacher
        fields = ("id", "user", "employee", "teaching_load")

    def to_representation(self, instance):
        response = super().to_representation(instance)

        request = self.context["request"]
        date_start = request.GET.get("date_start")
        date_end = request.GET.get("date_end")

        plan_lesson_events = LessonEvent.objects.filter(
            module_student_group__teacher=instance,
            date__gte=date_start,
            date__lte=date_end,
        ).count()

        done_lesson_events = LessonEvent.objects.filter(
            Q(
                module_student_group__teacher=instance,
                date__gte=date_start,
                date__lte=date_end,
            )
            & (
                Q(date__lt=datetime.today())
                | Q(
                    date=datetime.today(),
                    interval__end_time__lte=datetime.now().time(),
                )
            )
        ).count()

        left_lesson_events = plan_lesson_events - done_lesson_events

        response["plan_hours"] = plan_lesson_events
        response["done_hours"] = done_lesson_events
        response["left_hours"] = left_lesson_events
        response["first_name"] = instance.employee.first_name
        response["last_name"] = instance.employee.last_name
        response["middle_name"] = instance.employee.second_name
        response["image"] = instance.employee.image

        return response
