from django.urls import path
from api.v1.attendance import views

urlpatterns = [
    path('group/list/', views.GroupListAPIView.as_view()),
    path('group/<int:id>/', views.GroupRetrieveAPIView.as_view()),
    path('course/list/', views.CourseListAPIView.as_view()),
]