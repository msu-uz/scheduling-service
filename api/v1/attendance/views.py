from django_filters import FilterSet, AllValuesMultipleFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, filters
from api.models import Journal, Student, LessonEvent
from api.v1.attendance.serializers import *
from api.v1.permissions import EducationOfficeUnitPermission


class CourseFilter(FilterSet):
    course = AllValuesMultipleFilter(field_name='course')

    class Meta:
        model = StudentGroup
        fields = ['course']


class GroupListAPIView(generics.ListAPIView):
    queryset = StudentGroup.objects.filter(is_completed=False)
    serializer_class = GroupListSerializer
    permission_classes = [EducationOfficeUnitPermission, ]
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_class = CourseFilter
    search_fields = ['name']


class GroupRetrieveAPIView(generics.RetrieveAPIView):
    queryset = StudentGroup.objects.all()
    serializer_class = GroupDetailSerializer
    permission_classes = [EducationOfficeUnitPermission, ]
    lookup_url_kwarg = 'id'


class CourseListAPIView(generics.ListAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseListSerializer
    permission_classes = [EducationOfficeUnitPermission, ]
