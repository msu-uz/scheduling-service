from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from api.models import Journal, Student, StudentGroup, LessonEvent, Course, CourseType, Applicant, Employee


class JournalSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Journal
        fields = [
            'id',
            'module_student_group',
            'student',
            'is_absent',
            'event'

        ]


class CourseTypeSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = CourseType
        fields = [
            'id',
            'name'
        ]


class EventSerializer(serializers.ModelSerializer):
    interval_number = serializers.IntegerField(source='interval.number')

    class Meta:
        model = LessonEvent
        fields = [
            'id',
            'module_student_group',
            'date',
            'interval_number',
        ]


class CourseShortSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()
    course_type = CourseTypeSerializer()

    class Meta:
        model = Course
        fields = [
            'id',
            'name',
            'course_type'
        ]


class CourseListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = Course
        fields = [
            'id',
            'name'
        ]


class StatementStudentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    is_student = serializers.BooleanField()
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        if Applicant.objects.filter(student=obj.id).exists():
            return self.context['request'].build_absolute_uri(obj.applicant.image.url)
        elif Employee.objects.filter(student=obj.id).exists():
            return obj.employee.image

    class Meta:
        model = Student
        fields = (
            'id',
            'image',
            'is_student',
            'created_at',
            'updated_at'
        )

    def to_representation(self, instance):
        response = super(StatementStudentSerializer, self).to_representation(instance)
        response['first_name'] = instance.user.first_name
        response['middle_name'] = instance.user.middle_name
        response['last_name'] = instance.user.last_name

        events = instance.journal_set.all()
        if self.context['request'].GET.get('start_date') and self.context['request'].GET.get('end_date'):
            start_date = self.context['request'].GET.get('start_date')
            end_date = self.context['request'].GET.get('end_date')
            events = instance.journal_set.all().filter(event__date__gte=start_date, event__date__lte=end_date)
        response['journal'] = JournalSerializer(events, many=True).data
        return response


class GroupDetailSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    students = serializers.SerializerMethodField()
    events = serializers.SerializerMethodField()

    def get_students(self, obj):
        queryset = obj.get_students()
        return StatementStudentSerializer(queryset, many=True, context=self.context).data

    def get_events(self, obj):
        queryset = LessonEvent.objects.filter(module_student_group__group__uuid=obj.uuid,
                                              module_student_group__group__is_completed=False).order_by('date',
                                                                                                        'interval__number',
                                                                                                        )
        if self.context['request'].GET.get('start_date') and self.context['request'].GET.get('end_date') and queryset:
            start_date = self.context['request'].GET.get('start_date')
            end_date = self.context['request'].GET.get('end_date')
            queryset = queryset.filter(date__gte=start_date, date__lte=end_date).distinct()
        return EventSerializer(queryset, many=True).data

    class Meta:
        model = StudentGroup
        fields = [
            'id',
            'name',
            'department',
            'course',
            'students',
            'events',
            'created_at',
            'updated_at',
        ]

    def to_representation(self, instance):
        response = super(GroupDetailSerializer, self).to_representation(instance)
        response['course'] = CourseShortSerializer(instance.course).data
        return response


class GroupListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = StudentGroup
        fields = [
            'id',
            'name',
            'department',
            'course',
            'created_at',
            'updated_at',
        ]

    def to_representation(self, instance):
        response = super(GroupListSerializer, self).to_representation(instance)
        student = StudentGroup.objects.get(id=instance.id)
        response['course'] = CourseShortSerializer(instance.course).data
        response['count'] = student.studentgroupforming_set.count()
        return response
