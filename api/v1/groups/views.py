from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView, DestroyAPIView, \
    RetrieveAPIView
from rest_framework.response import Response
from rest_framework import filters
from .serializers import (StudentSerializer, GroupListSerializer,
                          TeacherSerializer, TeacherUserCreateUpdateSerializer, TeacherDetailSerializer,
                          GroupCreateSerializer, StudentDetailSerializer, DepartmentSerializer, CitySerializer,
                          DistrictSerializer, OrganizationSerializer, PositionSerializer,
                          ModuleStudentGroupDetailSerializer, JournalControlSerializer, DisciplineShortSerializer)
from api.models.group import Student, StudentGroup, StudentGroupForming, ModuleStudentGroup
from api.v1.permissions import EducationOfficeUnitPermission
from api.models.role import Teacher
from api.models.user import User
from api.models import Applicant, CourseType, Department, City, District, Organization, Position, ModuleCourse, \
    Discipline
from django_filters.rest_framework import DjangoFilterBackend


# Student Group

class DisciplineListAPIView(ListAPIView):
    queryset = Discipline.objects.all()
    permission_classes = [EducationOfficeUnitPermission]
    serializer_class = DisciplineShortSerializer


class GroupListView(ListAPIView):
    serializer_class = GroupListSerializer
    # permission_classes = [EducationOfficeUnitPermission]
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filterset_fields = ["course"]
    search_fields = ['name']
    queryset = StudentGroup.objects.filter(is_completed=False).prefetch_related('course', 'department',
                                                                                'studentgroupforming_set')


class GroupCreateView(CreateAPIView):
    serializer_class = GroupCreateSerializer
    permission_classes = [EducationOfficeUnitPermission]


class GroupDetailView(RetrieveAPIView):
    queryset = StudentGroup.objects.all()
    permission_classes = [EducationOfficeUnitPermission]
    serializer_class = GroupCreateSerializer
    lookup_url_kwarg = 'group_id'


class GroupRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    queryset = StudentGroup.objects.all()
    permission_classes = [EducationOfficeUnitPermission]
    serializer_class = GroupCreateSerializer
    lookup_url_kwarg = 'group_id'


class GroupDetailStudentView(ListAPIView):
    serializer_class = StudentSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = 'group_id'
    filter_backends = (filters.SearchFilter,)
    search_fields = ['user__first_name', 'user__middle_name', 'user__last_name']

    def get_queryset(self):
        queryset = StudentGroup.objects.get(id=self.kwargs['group_id']).get_students()
        return queryset


class GroupStudentDetailView(RetrieveAPIView):
    serializer_class = StudentDetailSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = 'student_id'

    def get_queryset(self):
        queryset = Student.objects.filter(studentgroupforming__group_id=self.kwargs['group_id'])
        return queryset


class GroupStudentDeleteView(DestroyAPIView):
    serializer_class = StudentDetailSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = 'student_id'
    lookup_field = 'student'

    def destroy(self, request, *args, **kwargs):
        try:
            obj = StudentGroupForming.objects.get(group_id=self.kwargs['group_id'],
                                                  student_id=self.kwargs['student_id'])
            self.perform_destroy(obj)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)


class StudentListView(ListAPIView):
    serializer_class = StudentSerializer
    permission_classes = [EducationOfficeUnitPermission]
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['user__first_name', 'user__middle_name', 'user__last_name']
    filterset_fields = {'employee__living_place__city',
                        'employee__living_place',
                        'employee__organization',
                        'employee__position',
                        }

    def get_queryset(self):
        students = Student.objects.all()
        is_applicant = self.request.query_params.get('is_applicant', None)
        is_employee = self.request.query_params.get('is_employee', None)
        last_qualification_date = self.request.query_params.get('last_qualification_date', None)
        if is_employee:
            students = students.filter(employee__isnull=False)
            if last_qualification_date:
                students = students.filter(employee__training_date_to__lte=last_qualification_date)
        elif is_applicant:
            students = students.filter(applicant__isnull=False)

        return students


class StudentPerformanceAPIView(RetrieveAPIView):
    queryset = Student.objects.all()
    serializer_class = ModuleStudentGroupDetailSerializer
    lookup_url_kwarg = 'student_id'


class JournalControlListAPIView(ListAPIView):
    serializer_class = JournalControlSerializer

    # permission_classes = [EducationOfficeUnitPermission, ]

    def get_queryset(self):
        group = StudentGroup.objects.get(id=self.kwargs.get('group_id'))
        students = group.get_students()
        student = Student.objects.get(id=self.kwargs.get('student_id'))
        if students.filter(id=student.id).exists():
            msg = ModuleStudentGroup.objects.filter(group=group, group__is_completed=False)
            return msg


# ------------------------------------ Teacher ----------------------------------

class TeacherListView(ListAPIView):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer
    permission_classes = [EducationOfficeUnitPermission]
    filter_backends = [filters.SearchFilter, ]
    search_fields = ['user__first_name', 'user__middle_name', 'user__last_name']


class TeacherCreateView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = TeacherUserCreateUpdateSerializer
    permission_classes = [EducationOfficeUnitPermission]


class TeacherDetailView(RetrieveAPIView):
    serializer_class = TeacherDetailSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = 'teacher_id'

    def get_queryset(self):
        queryset = Teacher.objects.filter(id=self.kwargs['teacher_id'])
        return queryset


class TeacherRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    serializer_class = TeacherUserCreateUpdateSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = 'teacher_id'
    lookup_field = 'teacher'

    def get_queryset(self):
        queryset = User.objects.all()
        return queryset


class DepartmentListView(ListAPIView):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    permission_classes = [EducationOfficeUnitPermission]


# ---------------REGION, DISTRICT, ORGANIZATION, POSITION ------------


class CityViewList(ListAPIView):
    """
    Region = City не имеет значение как называть, Модель = Сити, но в ТЗ = Регион
    """
    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = [EducationOfficeUnitPermission]


class DistrictListView(ListAPIView):
    serializer_class = DistrictSerializer
    queryset = District.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['city']
    permission_classes = [EducationOfficeUnitPermission]


class OrganizationListView(ListAPIView):
    serializer_class = OrganizationSerializer
    queryset = Organization.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['district']
    permission_classes = [EducationOfficeUnitPermission]


class PositionListView(ListAPIView):
    serializer_class = PositionSerializer
    queryset = Position.objects.all()
    permission_classes = [EducationOfficeUnitPermission]
