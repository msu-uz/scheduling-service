#STUDENT FILTER 

## City (region)
### /city/list - GET 
 Список регионов (для использования в фильтрации)
 
```json
[
    {
      "id": 1,
      "name": "Tashkent"
    },
    {
      "id": 2,
      "name": "Samarqand"
    },
    {
      "id": 3,
      "name": "Bukhara"
    }
  ]
```

## District list с фильтрацией по городу
### /district/list?city={id:city}  - GET
Query Params:   
city - идентификатор региона (города) 

```json
 [
    {
      "id": 1,
      "name": "Yunusabad",
      "city": 1
    },
    {
      "id": 2,
      "name": "Mirzo-Ulugbek",
      "city": 1
    }
  ]
```

## Organization list c фильтрацией по району (district)
### /organization/list?district={id:district} - GET
Query Params:   
district - идентификатор района
```json
[
    {
      "id": 2,
      "name": "Samarqand-1-MVD",
      "district": 3
    }
  ]
```

## Position list 
### /position/list/ - GET
```json
[
    {
      "id": 1,
      "name": "MVD"
    },
    {
      "id": 2,
      "name": "Stajer"
    }
  ]
```

## Student list with filter
### /student/list?is_employee=true&employee__living_place__city=2&employee__living_place=3&employee__position=2&last_qualification_date=2020-12-30
Query params:   
Существует два варианта:  
1. Для applicant фильтры не нужны и выводится просто список
2. Для employee верны все фильтры  
Очень важно писать название параметров полностью и без ошибок.
Обратить внимание на подчеркивания.

is_applicant (boolean) -  Если true то вариант 1 
is_employee (boolean) - Если true то вариант 2 
employee__living_place__city - идентификатор региона (города)
employee__living_place - индетификатор районов (зависит от конкретного региона)
employee__organization - идентификатор организаций (зависит от районов)
employee__position - идентификатор должности 
last_qualification_date - выбирается дата, согласно которой отбираются кадры, прошедшие курсы повышения квалификации ранее указанной даты.

```json
 [
    {
      "id": 102,
      "image": "http://127.0.0.1:8000/media/profile_images/matrix-neo-man-white-512.png",
      "created_at": "2020-12-21T11:39:29.378934+05:00",
      "updated_at": "2020-12-21T11:39:29.378975+05:00",
      "first_name": "",
      "middle_name": "Lol",
      "last_name": "",
      "education_job": "Stajer",
      "living_address": "Samarqand-1",
      "checked": false
    },
    {
      "id": 100,
      "image": null,
      "created_at": "2020-12-19T17:11:48.773704+05:00",
      "updated_at": "2020-12-21T12:02:48.982502+05:00",
      "first_name": "Фомичев",
      "middle_name": "Аверьянович",
      "last_name": "Агап",
      "education_job": "Stajer",
      "living_address": "Samarqand-1",
      "checked": false
    }
  ]
```