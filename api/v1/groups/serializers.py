from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.validators import UniqueTogetherValidator

from api.models import Applicant, Course, Employee, LessonEvent, Journal, ModuleCourse, Discipline
from api.models import Applicant, Course, City, District, Organization, Position
from api.models.group import StudentGroup, Student, StudentGroupForming, Teacher, ModuleStudentGroup, Department
from api.models.statement import QualitativeAssessment, Retake
from api.models.user import User
from datetime import datetime, timezone

from api.v1.admission_board.serializers import ParentSerializer


class DisciplineShortSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Discipline
        fields = (
            'id',
            'name'
        )


class TeacherUserCreateUpdateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='teacher.id', read_only=True)
    profile_image = serializers.SerializerMethodField()
    disciplines = serializers.PrimaryKeyRelatedField(source='teacher.disciplines', queryset=Discipline.objects.all(),
                                                     many=True)
    can_create_test = serializers.BooleanField(source='teacher.can_create_test')
    can_check_test = serializers.BooleanField(source='teacher.can_check_test')
    teaching_load = serializers.IntegerField(source='teacher.teaching_load', read_only=True)

    def get_profile_image(self, obj):
        if Employee.objects.filter(teacher=obj.id).exists():
            return obj.employee.image

    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'middle_name',
            'last_name',
            'disciplines',
            'teaching_load',
            'access_level',
            'username',
            'profile_image',
            'password',
            'email',
            'phone',
            'is_active',
            'can_create_test',
            'can_check_test'
        )
        extra_kwargs = {
            'password': {'write_only': True},
            'access_level': {'read_only': True}
        }

    # def create(self, validated_data):
    #     password = validated_data.pop('password')
    #     teacher = validated_data.pop('teacher')
    #     user = User.objects.create(**validated_data)
    #     user.set_password(password)
    #     user.access_level = User.TEACHER_LEVEL
    #     user.save()
    #     Teacher.objects.create(user=user)
    #     return user
    def to_representation(self, instance):
        response = super(TeacherUserCreateUpdateSerializer, self).to_representation(instance)
        response['disciplines'] = DisciplineShortSerializer(instance.teacher.disciplines, many=True).data
        if instance.teacher.employee.organization:
            response['work_place'] = instance.teacher.employee.organization.name.get('ru')
        else:
            response['work_place'] = instance.teacher.employee.organization
        if instance.teacher.employee.position:
            response['position'] = instance.teacher.employee.position.name.get('ru')
        else:
            response['position'] = instance.teacher.employee.position
        return response

    def update(self, instance, validated_data):
        instance.updated_at = datetime.now()
        if validated_data.get('password'):
            password = validated_data.pop('password')
            instance.set_password(password)
        if validated_data.get('teacher'):
            teacher = validated_data.pop('teacher')
            if 'disciplines' in teacher:
                instance.teacher.disciplines.set(teacher['disciplines'])
            if 'can_check_test' in teacher:
                instance.teacher.can_check_test = teacher['can_check_test']
            if 'can_create_test' in teacher:
                instance.teacher.can_create_test = teacher['can_create_test']
            instance.teacher.save()
        instance.save()
        return super(TeacherUserCreateUpdateSerializer, self).update(instance, validated_data)


class StudentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        if Applicant.objects.filter(student=obj.id).exists():
            return self.context['request'].build_absolute_uri(obj.applicant.image.url)
        elif Employee.objects.filter(student=obj.id).exists():
            return obj.employee.image

    class Meta:
        model = Student
        fields = (
            'id',
            'image',
            'created_at',
            'updated_at'
        )

    def to_representation(self, instance):
        response = super(StudentSerializer, self).to_representation(instance)
        response['first_name'] = instance.user.first_name
        response['middle_name'] = instance.user.middle_name
        response['last_name'] = instance.user.last_name
        response['education_job'] = instance.get_education_job()
        response['living_address'] = instance.get_address()
        response['checked'] = False
        return response


class StudentDetailSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        if Applicant.objects.filter(student=obj.id).exists():
            return self.context['request'].build_absolute_uri(obj.applicant.image.url)
        elif Employee.objects.filter(student=obj.id).exists():
            return obj.employee.image

    class Meta:
        model = Student
        fields = [
            'id',
            'image'
        ]

    def to_representation(self, instance):
        response = super(StudentDetailSerializer, self).to_representation(instance)
        if Applicant.objects.filter(student=instance.id).exists():
            response['first_name'] = instance.applicant.first_name
            response['second_name'] = instance.applicant.second_name
            response['last_name'] = instance.applicant.last_name
            response['sex'] = instance.applicant.get_sex_display()
            response['passport_number'] = instance.applicant.passport_number
            response['birth_place'] = instance.applicant.birth_place
            response['birth_date'] = instance.applicant.birth_date
            response['passport_address'] = instance.applicant.passport_address
            response['education_job'] = instance.applicant.education_job
            response['living_address'] = instance.applicant.living_address
            response['university_name'] = instance.applicant.university_name
            response['university_study_start_year'] = instance.applicant.university_study_start_year
            response['university_study_end_year'] = instance.applicant.university_study_end_year
            response['university_foreign_language'] = instance.applicant.university_foreign_language
            response['university_study_language'] = instance.applicant.university_study_language
            response['specialization'] = instance.applicant.specialization
            response['job_place'] = instance.applicant.job_place
            response['job_address'] = instance.applicant.job_address
            response['war_ticket_info'] = instance.applicant.war_ticket_info
            response['qualification'] = instance.applicant.qualification
            response['mother_info'] = ParentSerializer(instance.applicant.mother_info).data
            response['father_info'] = ParentSerializer(instance.applicant.father_info).data
        elif Employee.objects.filter(student=instance.id).exists():
            response['first_name'] = instance.employee.first_name
            response['second_name'] = instance.employee.second_name
            response['last_name'] = instance.employee.last_name
            response['sex'] = instance.employee.get_sex_display()
            response['passport_number'] = instance.employee.passport_number
            response['birth_place'] = instance.employee.passport_address.name.get('ru')
            response['birth_date'] = instance.employee.birth_date
            response['passport_address'] = instance.employee.passport_address.name.get('ru')
            if instance.employee.position:
                response['education_job'] = instance.employee.position.name.get('ru')
            else:
                response['education_job'] = instance.employee.position
            response['living_address'] = instance.employee.living_place.name.get('ru')
            response['university_name'] = instance.employee.university_name
            response['university_study_start_year'] = instance.employee.university_study_start_date
            response['university_study_end_year'] = instance.employee.university_study_end_date
            response['university_foreign_language'] = None
            response['university_study_language'] = None
            response['specialization'] = instance.employee.specialization
            if instance.employee.organization:
                response['job_place'] = instance.employee.organization.name.get('ru')
            else:
                response['job_place'] = instance.employee.organization
            if instance.employee.organization:
                response['job_address'] = instance.employee.organization.district.name.get('ru')
            else:
                response['job_address'] = instance.employee.organization
            response['war_ticket_info'] = None
            response['qualification'] = instance.employee.qualification
            response['mother_info'] = None  # TODO: copy from Ekadr EmployeeRelative & make Serializer
            response['father_info'] = None

        return response


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = ('id', 'created_at', 'updated_at')

    def to_representation(self, instance):
        response = super().to_representation(instance)

        response['first_name'] = instance.employee.first_name
        response['second_name'] = instance.employee.second_name
        response['last_name'] = instance.employee.last_name
        response['image'] = instance.employee.image
        if instance.employee.position:
            response['position'] = instance.employee.position.name.get('ru')
        else:
            response['position'] = instance.employee.position
        return response


class TeacherDetailSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    access_level = serializers.CharField(source='user.get_access_level_display')
    disciplines = DisciplineShortSerializer(many=True)

    class Meta:
        model = Teacher
        fields = (
            'id',
            'teaching_load',
            'disciplines',
            'access_level',
            'created_at',
            'updated_at',
            'can_create_test',
            'can_check_test'
        )

    def to_representation(self, instance):
        response = super(TeacherDetailSerializer, self).to_representation(instance)
        response['image'] = instance.employee.image
        response['first_name'] = instance.user.first_name
        response['middle_name'] = instance.user.middle_name
        response['last_name'] = instance.user.last_name
        response['username'] = instance.user.username
        response['email'] = instance.user.email
        response['phone'] = instance.user.phone
        response['is_active'] = instance.user.is_active
        if instance.employee.organization:
            response['work_place'] = instance.employee.organization.name.get('ru')
        else:
            response['work_place'] = instance.employee.organization
        if instance.employee.position:
            response['position'] = instance.employee.position.name.get('ru')
        else:
            response['position'] = instance.employee.position
        return response


class DepartmentSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Department
        fields = [
            'id',
            'name'
        ]


class GroupListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = StudentGroup
        fields = [
            'id',
            'name',
            'department',
            'course',
            'created_at',
            'updated_at',

        ]

    def to_representation(self, instance):
        response = super(GroupListSerializer, self).to_representation(instance)
        student = StudentGroup.objects.get(id=instance.id)
        response['course'] = ''
        if instance.course:
            response['course'] = instance.course.name
        response['department'] = {}
        if instance.department:
            response['department'] = DepartmentSerializer(instance.department).data
        response['count'] = StudentGroup.get_students(instance).count()
        return response


class GroupCreateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    students = serializers.PrimaryKeyRelatedField(many=True, queryset=Student.objects.all(),
                                                  source="get_students")
    course = serializers.PrimaryKeyRelatedField(queryset=Course.objects.all(), required=False, allow_null=True)

    class Meta:
        model = StudentGroup
        fields = [
            'id',
            'name',
            'department',
            'students',
            'created_at',
            'updated_at',
            'course'
        ]
        extra_kwargs = {
            'students': {'required': False}
        }

    def to_representation(self, instance):
        response = super(GroupCreateSerializer, self).to_representation(instance)
        response['students'] = StudentSerializer(StudentGroup.get_students(instance), many=True,
                                                 context=self.context).data
        return response

    def create(self, validated_data):
        group = StudentGroup.objects.create(name=validated_data['name'], department=validated_data['department'])
        group.save()
        if validated_data.get('get_students'):
            students = validated_data.pop('get_students')
            for student in students:
                group_forming = StudentGroupForming.objects.create(group_id=group.id, student_id=student.id)
                group_forming.save()
        return group

    # //////////////////////////////////////UPDATE METHOD///////////////////////////////////////
    # Если у группы не было приявзки к курсу то обновит(поле курс изначально null), иначе создает новый обьект на основе
    # этого instance, но с запрашиваемым курсом

    def update(self, instance, validated_data):
        print(validated_data)
        instance.updated_at = datetime.now()
        if validated_data.get('name'):
            instance.name = validated_data['name']
            instance.save()
        if validated_data.get('department'):
            instance.department = validated_data['department']
            instance.save()
        if validated_data.get('get_students'):
            student_groups = StudentGroupForming.objects.filter(group_id=instance.id)
            student_groups.delete()
            students = validated_data.pop('get_students')
            for student in students:
                group_forming = StudentGroupForming.objects.create(group_id=instance.id, student=student)
                group_forming.save()
            instance.save()
        if validated_data.get('course'):
            course = validated_data['course']
            if not instance.course:
                instance.course = course
                instance.save()
            elif instance.course:
                instance.is_completed = True
                instance.save()
                new_instance = StudentGroup.objects.create(name=instance.name, uuid=instance.uuid, course=course,
                                                           department=instance.department)
                new_instance.save()
                return new_instance
        return instance


# ---------------REGION, DISTRICT, ORGANIZATION ------------

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name',)


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = ('id', 'name', 'city')


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ('id', 'name', 'district')


class PositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = ('id', 'name')


# ------------------------- STUDENT PERFORMANCE -------------------------------
class EventSerializer(serializers.ModelSerializer):
    interval_number = serializers.IntegerField(source='interval.number')

    class Meta:
        model = LessonEvent
        fields = [
            'id',
            'module_student_group',
            'date',
            'interval_number',
        ]


class ModuleCourseListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = ModuleCourse
        fields = [
            'id',
        ]

    def to_representation(self, instance):
        response = super(ModuleCourseListSerializer, self).to_representation(instance)
        response['name'] = Discipline.objects.get(id=instance.discipline_id).name
        student = Student.objects.get(id=self.context['view'].kwargs.get('student_id'))
        group = StudentGroup.objects.get(id=self.context['view'].kwargs.get('group_id'))
        events = student.journal_set.all()
        msg = ModuleStudentGroup.objects.filter(module_course=instance, group=group).first()
        if msg and events:
            events = student.journal_set.all().filter(module_student_group_id=msg.id)

        if self.context['request'].GET.get('start_date') and self.context['request'].GET.get('end_date'):
            start_date = self.context['request'].GET.get('start_date')
            end_date = self.context['request'].GET.get('end_date')
            events = events.filter(event__date__gte=start_date, event__date__lte=end_date)
        response['journal'] = JournalSerializer(events, many=True).data
        journal = student.journal_set.all().filter(event__lesson__lesson__isnull=True, score__lte=55,
                                                   student_id=student.id)
        if msg is not None and journal:
            journal = student.journal_set.all().filter(event__lesson__lesson__isnull=True, score__lte=55,
                                                       student_id=student.id, module_student_group=msg)
        response['failed_control'] = JournalSerializer(journal, many=True).data
        retake = Retake.objects.filter(retake_event__student_id=student.id)
        if msg is not None and retake:
            retake = Retake.objects.filter(retake_event__student_id=student.id, retake_event__module_student_group=msg)
        response['retake_event'] = RetakeListSerializer(retake, many=True).data
        response['checked'] = False
        return response


class RetakeListSerializer(serializers.ModelSerializer):
    passed = serializers.BooleanField(read_only=True)

    class Meta:
        model = Retake
        fields = [
            'id',
            'retake_event',
            'score',
            'passed',
            'date',
            'description',
            'start_time',
            'end_time'
        ]

    def to_representation(self, instance):
        response = super(RetakeListSerializer, self).to_representation(instance)
        response['retake_event'] = JournalSerializer(instance.retake_event).data
        return response


class QASerializer(serializers.ModelSerializer):
    class Meta:
        model = QualitativeAssessment
        fields = [
            'id',
            'name'
        ]


class JournalSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Journal
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(
                queryset=Journal.objects.all(),
                fields=['student', 'event']
            )
        ]

    def to_representation(self, instance):
        response = super(JournalSerializer, self).to_representation(instance)
        scores = QualitativeAssessment.objects.get(min_score__lte=instance.score,
                                                   max_score__gte=instance.score)
        response['qa'] = QASerializer(scores).data
        return response


class StatementStudentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    is_student = serializers.BooleanField()
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        if Applicant.objects.filter(student=obj.id).exists():
            return self.context['request'].build_absolute_uri(obj.applicant.image.url)
        elif Employee.objects.filter(student=obj.id).exists():
            return obj.employee.image

    class Meta:
        model = Student
        fields = (
            'id',
            'image',
            'is_student',
            'created_at',
            'updated_at'
        )

    def to_representation(self, instance):
        response = super(StatementStudentSerializer, self).to_representation(instance)
        response['first_name'] = instance.user.first_name
        response['middle_name'] = instance.user.middle_name
        response['last_name'] = instance.user.last_name
        return response


class ModuleStudentGroupDetailSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    student = serializers.SerializerMethodField()
    events = serializers.SerializerMethodField()
    module_courses = serializers.SerializerMethodField()

    def get_student(self, obj):
        return StatementStudentSerializer(obj, context=self.context).data

    def get_module_courses(self, obj):
        group = StudentGroup.objects.get(id=self.context['view'].kwargs.get('group_id'))
        students = group.get_students()
        student = Student.objects.get(id=self.context['view'].kwargs.get('student_id'))
        if students.filter(id=student.id).exists():
            msg = ModuleStudentGroup.objects.filter(group=group, group__is_completed=False)
            queryset = ModuleCourse.objects.filter(modulestudentgroup__in=msg)
            return ModuleCourseListSerializer(queryset, many=True, context=self.context).data
        else:
            raise ValidationError('No such student in this group')

    def get_events(self, obj):
        group = StudentGroup.objects.get(id=self.context['view'].kwargs.get('group_id'))
        students = group.get_students()
        student = Student.objects.get(id=self.context['view'].kwargs.get('student_id'))
        if students.filter(id=student.id).exists():
            msg = ModuleStudentGroup.objects.filter(group=group)
            queryset = LessonEvent.objects.filter(module_student_group__in=msg).order_by('date', 'interval__number')
            if self.context['request'].GET.get('start_date') and self.context['request'].GET.get('end_date'):
                start_date = self.context['request'].GET.get('start_date')
                end_date = self.context['request'].GET.get('end_date')
                queryset = queryset.filter(date__gte=start_date, date__lte=end_date).distinct()
            if self.context['request'].GET.get('only_score') and queryset:
                only_score = self.context['request'].GET.get('only_score', None)
                if only_score:
                    queryset = queryset.filter(journal__score__isnull=False).distinct()
            return EventSerializer(queryset, many=True).data
        else:
            raise ValidationError('No such student in this group')

    class Meta:
        model = Student
        fields = [
            'id',
            'module_courses',
            'student',
            'events',
        ]


class JournalControlSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = ModuleStudentGroup
        fields = (
            'id',
        )

    def to_representation(self, instance):
        date = datetime.today().date()
        student = Student.objects.get(id=self.context['view'].kwargs.get('student_id'))
        response = super(JournalControlSerializer, self).to_representation(instance)
        response['name'] = Discipline.objects.get(id=instance.module_course.discipline_id).name
        response['current_score'] = Journal.get_lesson_score(instance, date, student, instance)
        if response['current_score'] is not None:
            scores = QualitativeAssessment.objects.get(min_score__lte=response['current_score'],
                                                       max_score__gte=response['current_score'])
            response['current_score_qa'] = QASerializer(scores).data
        response['intermediate_control'] = Journal.get_int_score(self=instance,
                                                                 date=date,
                                                                 student=student,
                                                                 msg=instance.id)
        if response['intermediate_control'] is not None:
            scores = QualitativeAssessment.objects.get(min_score__lte=response['intermediate_control'],
                                                       max_score__gte=response['intermediate_control'])
            response['intermediate_control_qa'] = QASerializer(scores).data
        response['final_control'] = Journal.get_final_score(self=instance,
                                                            date=date,
                                                            student=student,
                                                            msg=instance.id)
        if response['final_control'] is not None:
            scores = QualitativeAssessment.objects.get(min_score__lte=response['final_control'],
                                                       max_score__gte=response['final_control'])
            response['final_control_qa'] = QASerializer(scores).data
        response['checked'] = False
        return response
