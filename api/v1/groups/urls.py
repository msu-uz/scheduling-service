from django.urls import path
from api.v1.groups import views

urlpatterns = [
    path('student-group/list/', views.GroupListView.as_view()),
    path('student-group/create/', views.GroupCreateView.as_view()),
    path('student-group/<int:group_id>/', views.GroupDetailView.as_view()),
    path('student-group/<int:group_id>/edit/', views.GroupRetrieveUpdateDestroyView.as_view()),
    path('student-group/<int:group_id>/student/', views.GroupDetailStudentView.as_view()),
    path('student-group/<int:group_id>/student/<int:student_id>/', views.GroupStudentDetailView.as_view()),
    path('student-group/<int:group_id>/student/<int:student_id>/delete/', views.GroupStudentDeleteView.as_view()),
    path('student-group/<int:group_id>/student/<int:student_id>/performance/', views.StudentPerformanceAPIView.as_view()),          # TODO: Filter events by score existing
    path('student-group/<int:group_id>/student/<int:student_id>/performance/control/', views.JournalControlListAPIView.as_view()),  # TODO: Control statement
    path('student/list/', views.StudentListView.as_view()),

    path('teacher/list/', views.TeacherListView.as_view()),
    path('teacher/create/', views.TeacherCreateView.as_view()),
    path('teacher/<int:teacher_id>/', views.TeacherDetailView.as_view()),
    path('teacher/<int:teacher_id>/edit/', views.TeacherRetrieveUpdateDestroyView.as_view()),

    path('department/list/', views.DepartmentListView.as_view()),
    path('discipline/list/',  views.DisciplineListAPIView.as_view()),

    path('city/list/', views.CityViewList.as_view()),  # CITY = REGION
    path('district/list/', views.DistrictListView.as_view()),
    path('organization/list/', views.OrganizationListView.as_view()),
    path('position/list/', views.PositionListView.as_view())
]
