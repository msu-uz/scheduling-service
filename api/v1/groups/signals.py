from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver
from api.models import StudentGroup, Course, ModuleStudentGroup


# /////////////////////////CREATE MODULE STUDENT GROUP///////////////////////////
# При привязке объекта из StudentGroup к объекту из Course с вызовом метода save(), создаются объекты ModuleStudentGroup
# для каждого модуля курса из course.modulecourse_set c тем номером объекта из StudentGroup
# @receiver(pre_save, sender=StudentGroup)
# def create_msg(sender, instance, **kwargs):
#     if instance.course:
#         course = Course.objects.get(id=instance.course_id)
#         for module in course.modulecourse_set.all():
#             module_student_group = ModuleStudentGroup.objects.create(group_id=instance.id, module_course=module)
#             module_student_group.save()
