from rest_framework_simplejwt.serializers import TokenRefreshSerializer, TokenObtainPairSerializer

from api.models import Student, Applicant


class RefreshTokenSerializer(TokenRefreshSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token["access_level"] = user.access_level
        return token


class TokenObtainPairSerializer(TokenObtainPairSerializer):
    def get_token(self, user):
        token = super().get_token(user)
        token["access_level"] = user.access_level
        token["last_name"] = user.last_name
        token["middle_name"] = user.middle_name
        token["first_name"] = user.first_name

        if hasattr(user, 'student'):
            if user.student.applicant:
                token["profile_image"] = self.context['request'].build_absolute_uri(user.student.applicant.image.url)
            elif user.student.employee:
                token["profile_image"] = user.student.employee.image
        elif hasattr(user, 'teacher'):
            if user.teacher.employee:
                token["profile_image"] = user.teacher.employee.image
        elif user.profile_image:
            token["profile_image"] = self.context['request'].build_absolute_uri(user.profile_image.url)
        token["username"] = user.username
        return token
