import json
import urllib
import requests

from django.http import HttpResponse, JsonResponse
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView
from loguru import logger

from api.models.user import User
from . import serializers


ONE_ID_BASE_URL = 'https://sso.gov.uz:8443/sso/oauth/Authorization.do'


class OneIDHandlerAPIView(APIView):
    def get(self, request):
        one_id_code = request.GET.get('code')
        if not one_id_code:
            return JsonResponse({"message": "one id code was not recieved"})

        get_token_params = {
            'code': one_id_code,
            'client_id': settings.ONE_ID_CONFIG["client_id"],
            'client_secret': settings.ONE_ID_CONFIG["secret"],
            'grant_type': 'one_authorization_code',
        }

        urlparams = urllib.parse.urlencode(get_token_params)
        get_token_url = ONE_ID_BASE_URL + "?" + urlparams

        r = requests.post(get_token_url)
        access_token = json.loads(r.text).get("access_token")
        if not access_token:
            logger.debug(f"no access token, one id rsp: {r.text}")
            return JsonResponse({"message": "authentication failed"})

        get_info_params = {
            'access_token': access_token,
            'client_id': settings.ONE_ID_CONFIG["client_id"],
            'client_secret': settings.ONE_ID_CONFIG["secret"],
            'scope': settings.ONE_ID_CONFIG["scope"],
            'grant_type': 'one_access_token_identify',
        }

        urlparams = urllib.parse.urlencode(get_info_params)
        get_info_url = ONE_ID_BASE_URL + "?" + urlparams

        r = requests.post(get_info_url)
        user_info = json.loads(r.text)
        user_email = user_info.get("email")
        if not user_email:
            logger.debug(f"no user info, one id rsp: {r.text}")
            return JsonResponse({"message": "authentication failed"}, status=401)

        user = User.objects.filter(email=user_email).first()
        if not user:
            logger.debug(f"can't login a user with email {user_info['email']}")
            return JsonResponse({"message": "access denied"}, status=403)

        refresh = RefreshToken.for_user(user)
        refresh["access_level"] = user.access_level

        return Response({
            "access": str(refresh.access_token),
            "refresh": str(refresh),
        })


class RefreshTokenView(TokenRefreshView):
    serializer_class = serializers.RefreshTokenSerializer


class ObtainTokenPairView(TokenObtainPairView):
    serializer_class = serializers.TokenObtainPairSerializer
