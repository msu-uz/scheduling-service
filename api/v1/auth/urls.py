from django.urls import path
from . import views


urlpatterns = [
    path('oauth/one-id/', views.OneIDHandlerAPIView.as_view()),
    path('refresh/', views.RefreshTokenView.as_view()),
    path('access/', views.ObtainTokenPairView.as_view())
]
