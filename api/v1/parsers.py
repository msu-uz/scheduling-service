from rest_framework import parsers
import json


class MultipartJsonParser(parsers.MultiPartParser):
    """
    Парсер для использования во View, когда в multipart-form передаётся JSON в
    виде строки.

    Во View пишется:
    parser_classes = (JSONParser, MultipartJsonParser)

    Источник:
    https://stackoverflow.com/questions/61161227/uploading-multiple-images-and-nested-json-using-multipart-form-data-in-django-re
    """
    def parse(self, stream, media_type=None, parser_context=None):
        result = super().parse(stream, media_type=media_type, parser_context=parser_context)
        data = {}

        for key, value in result.data.items():
            if type(value) != str:
                data[key] = value
                continue
            if '{' in value or "[" in value:
                try:
                    data[key] = json.loads(value)
                except ValueError:
                    data[key] = value
            else:
                data[key] = value
        return parsers.DataAndFiles(data, result.files)
