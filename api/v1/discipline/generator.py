from django.db.models import Count, F
from django.shortcuts import get_object_or_404
from django.db.models.query_utils import Q

from api.models.task import *
import random


def action_delete(task_collection_id,
                  chosen_questions):
    task_collection = get_object_or_404(TaskCollection, id=task_collection_id)
    if chosen_questions and isinstance(chosen_questions, list):
        task_collection.tasks.remove(*chosen_questions)

    return task_collection


def action_replace(task_collection_id, chosen_questions):
    task_collection = get_object_or_404(TaskCollection, id=task_collection_id)
    excluded_question = task_collection.tasks.all()
    task_collection = action_delete(task_collection_id, chosen_questions)
    replace_question = Task.objects.filter(id__in=chosen_questions)
    argument = group_by(replace_question)
    module_week_type = task_collection.module_week_type_id
    task_collection = add_task(module_week_type, None, argument, task_collection_id, excluded_question)
    return task_collection


def action_add(task_collection_id, chosen_questions):
    task_collection = get_object_or_404(TaskCollection, id=task_collection_id)
    if chosen_questions and isinstance(chosen_questions, list):
        task_collection.tasks.add(*chosen_questions)


def random_generate(topics,
                    arguments,
                    exclude_query):
    task_list = []
    for argument in arguments:
        topic = argument.get('topic')
        task_type = argument.get('task_type')
        difficulty = argument.get('difficulty')
        question_count = argument.get('question_count')
        if topics:
            query = Q(topic_id__in=topics)
        if topic:
            if isinstance(topic, list):
                query = Q(topic_id__in=topic)
            elif isinstance(topic, int):
                query = Q(topic_id=topic)
        if not topics and not topic:
            query = Q()  # TODO: refactor
        if task_type:
            query &= Q(type__task_type=task_type)
        if difficulty:
            query &= Q(type__difficulty=difficulty)
        if exclude_query:
            task_id = Task.objects.exclude(exclude_query).filter(query).values_list('id', flat=True)
        else:
            task_id = Task.objects.filter(query).values_list('id', flat=True)
        task_id = random.sample(list(task_id), k=question_count if len(task_id) > question_count else len(task_id))
        for task in task_id:
            if task not in task_list:
                task_list.append(task)
    return task_list


def group_by(task_set):
    group = task_set.annotate(task_type=F('type__task_type'),
                              difficulty=F('type__difficulty')).values('topic', 'task_type',
                                                                       'difficulty').annotate(
        question_count=Count('type'))
    return group


def add_task(module_week_type,
             topics,
             arguments,
             task_collection_id,
             excluded_question):
    task_list = []
    task_collection = get_object_or_404(TaskCollection, id=task_collection_id)
    task_set = task_collection.tasks.all()
    if excluded_question:
        exclude_query = Q(id__in=excluded_question)
    else:
        exclude_query = Q(id__in=task_set)
    if arguments:
        task_list = random_generate(topics, arguments, exclude_query)

    else:
        arguments = group_by(task_set)
        task_list = random_generate(topics, arguments, exclude_query)
    tasks = Task.objects.filter(id__in=task_list)
    new_tasks = task_set.union(tasks)
    task_collection.tasks.set(new_tasks)
    return task_collection


def regenerate(module_week_type,
               topics,
               arguments,
               task_collection_id):
    task_collection = get_object_or_404(TaskCollection, id=task_collection_id)
    if arguments:
        task_list = random_generate(topics, arguments, None)
        task_collection.tasks.set(task_list)

    else:
        task_set = task_collection.tasks.all()
        arguments = group_by(task_set)
        if arguments:
            task_list = random_generate(topics, arguments, None)
            task_collection.tasks.set(task_list)
    return task_collection


def easy_generate(module_week_type,
                  topic_id,
                  question_count,
                  difficulty,
                  deadline,
                  task_collection_id,
                  change_tasks):
    task_ids = Task.objects.filter(topic_id__in=topic_id)
    if task_collection_id:
        task_collection = get_object_or_404(TaskCollection, id=task_collection_id)
        tasks = task_collection.tasks.all()
        question_count = [len(change_tasks)]
        if difficulty is None:
            task_ids = Task.objects.filter(topic_id__in=topic_id).exclude(id__in=tasks).values_list('id', flat=True)
            task_ids = Task.objects.filter(id__in=random.sample(list(task_ids),
                                                                k=question_count[0] if len(task_ids) > question_count[
                                                                    0] else len(task_ids)))
            tasks = tasks.exclude(id__in=change_tasks)
            new_tasks = tasks.union(task_ids)
            task_collection.tasks.set(new_tasks)
        else:
            data = []
            for i in difficulty:
                task_ids = task_ids.filter(type__difficulty_id=i).values_list('id', flat=True)
                task_ids = random.sample(list(task_ids),
                                         k=question_count[i - 1] if len(task_ids) > question_count[i - 1] else len(
                                             task_ids))
                data.extend(task_ids)
            tasks = tasks.exclude(id__in=change_tasks)
            new_tasks = tasks.union(*data)
            task_collection.tasks.set(new_tasks)

        return task_collection
    else:
        if difficulty is None:
            task_ids = task_ids.all().values_list('id', flat=True)
            task_ids = random.sample(list(task_ids),
                                     k=question_count[0] if len(task_ids) > question_count[0] else len(task_ids))
            t = TaskCollection.objects.create(module_week_type=module_week_type, deadline_duration=deadline)
            t.tasks.set(task_ids)
        else:
            data = []
            for i in difficulty:
                task_ids = task_ids.filter(type__difficulty_id=i).values_list('id', flat=True)
                task_ids = random.sample(list(task_ids),
                                         k=question_count[i - 1] if len(task_ids) > question_count[i - 1] else len(
                                             task_ids))
                data.extend(task_ids)
            t = TaskCollection.objects.create(module_week_type=module_week_type, deadline_duration=deadline)
            t.tasks.set(data)
            return t


def randomize_set(task_id):
    qs = Task.objects.filter(taskcollection=task_id).order_by('?').prefetch_related('answer_set')
    return qs
