from django.db.models.signals import post_save
from django.dispatch import receiver
from api.models.task import *
from api.models.discipline import *
from api.v1.discipline.generator import easy_generate, randomize_set


@receiver(post_save, sender=ModuleWeekType)
def task_set_create(sender, instance, **kwargs):
    if instance.lesson:
        question_count = [instance.lesson.question_count]
        topic = [instance.lesson.lesson.id]
        deadline = instance.lesson.deadline_duration
        easy_generate(instance, topic, question_count, difficulty=None, deadline=deadline)
    if instance.int_control:
        question_count = [
            instance.int_control.easy_question_count,
            instance.int_control.medium_question_count,
            instance.int_control.hard_question_count
        ]
        deadline = instance.int_control.deadline_duration
        int_control_topic = IntermediateControlTopics.objects.filter(
            intermediate_control_id=instance.int_control.intermediate_control).values_list('topic_id', flat=True)
        easy_generate(instance, list(int_control_topic), question_count, [1, 2, 3], deadline)

    if instance.fin_control:
        question_count = [
            instance.fin_control.easy_question_count,
            instance.fin_control.medium_question_count,
            instance.fin_control.hard_question_count
        ]
        deadline = instance.fin_control.deadline_duration
        fin_control_topic = FinalControlTopics.objects.filter(
            final_control_id=instance.fin_control.final_control).values_list('topic_id', flat=True)
        print(fin_control_topic)
        easy_generate(instance, list(fin_control_topic), question_count, [1, 2, 3], deadline)
