from django.urls import path

from api.v1.discipline import views

urlpatterns = [
    path('course/list/', views.CourseListAPIView.as_view()),
    path('list/', views.DisciplineListAPIView.as_view()),
    path('section/list/', views.SectionListAPIView.as_view()),
    path('topic/list/', views.TopicListAPIView.as_view()),
    path('int_control/list/', views.IntControlListAPIView.as_view()),
    path('fin_control/list/', views.FinControlListAPIView.as_view()),

    path('course/<int:course_id>/', views.CourseDetailAPIView.as_view()),
    path('course/<int:course_id>/modulecourse/list/', views.ModuleCourseListAPIView.as_view()),
    path('course/<int:course_id>/modulecourse/create/', views.ModuleCourseCreateAPIView.as_view()),
    path('course/<int:course_id>/modulecourse/<int:id>/', views.ModuleCourseRetrieveUpdateDestroyAPIView.as_view()),
    path('modulecourse/<int:id>/edit/', views.ModuleCourseRetrieveUpdateDestroyAPIView.as_view()),
    path('modulecourse/<int:id>/', views.ModuleCourseDetailAPIView.as_view()),
    path('modulecourse/<int:id>/week/list/', views.WeekListAPIView.as_view()),
    path('modulecourse/<int:id>/week/create/', views.WeekCreateAPIView.as_view()),
    path('week/<int:week_id>/', views.WeekRetrieveUpdateDestroyAPIView.as_view()),
    path('week/<int:week_id>/topic/list/', views.ModuleWeekLessonDayListAPIView.as_view()),
    path('week/<int:week_id>/topic/<int:id>/', views.ModuleWeekLessonDayDetailAPIView.as_view()),
    path('week/<int:week_id>/int_control/<int:id>/', views.ModuleWeekControlDayDetailAPIView.as_view()),
    path('week/<int:week_id>/fin_control/<int:id>/', views.ModuleWeekFinalControlDayDetailAPIView.as_view()),
    path('week/<int:week_id>/int_control/<int:id>/add_topic/', views.IntermediateControlTopicsUpdateView.as_view()),
    path('week/<int:week_id>/fin_control/<int:id>/add_topic/', views.FinalControlTopicsUpdateView.as_view()),

    path('course/create/', views.CourseCreateAPIView.as_view()),
    path('create/', views.DisciplineCreateAPIView.as_view()),
    path('section/create/', views.SectionCreateAPIView.as_view()),
    path('topic/create/', views.TopicCreateAPIView.as_view()),
    path('int_control/create/', views.IntControlCreateAPIView.as_view()),
    path('fin_control/create/', views.FinControlCreateAPIView.as_view()),

    path('update/<int:id>/', views.DisciplineUpdateAPIView.as_view()),
    path('section/update/<int:id>/', views.SectionUpdateAPIView.as_view()),
    path('topic/update/<int:id>/', views.TopicUpdateAPIView.as_view()),
    path('int_control/update/<int:id>/', views.IntControlUpdateAPIView.as_view()),
    path('fin_control/update/<int:id>/', views.FinControlUpdateAPIView.as_view()),

    path('delete/<int:id>/', views.DisciplineDeleteAPIView.as_view()),
    path('section/delete/<int:id>/', views.SectionDeleteAPIView.as_view()),
    path('topic/delete/<int:id>/', views.TopicDeleteAPIView.as_view()),
    path('int_control/delete/<int:id>/', views.IntControlDeleteAPIView.as_view()),
    path('fin_control/delete/<int:id>/', views.FinControlDeleteAPIView.as_view()),
    path('course_type/list/', views.CourseTypeListView.as_view()),
    path('department/list/', views.DepartmentListView.as_view()),
]
