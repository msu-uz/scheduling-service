from datetime import datetime

from django.db.models import Q
from rest_framework import serializers
from api.models import Topic, Discipline, Section, Course, ModuleCourse, Week, ModuleWeekControlDay, \
    ModuleWeekLessonDay, CourseType, Department
from api.models.discipline import ModuleWeekFinalControlDay, FinalControlTopics, FinalControl, \
    IntermediateControlTopics, IntermediateControl, ModuleWeekType


class DepartmentSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Department
        fields = [
            'id',
            'name'
        ]


class CourseTypeSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = CourseType
        fields = [
            'id',
            'name'
        ]


class TopicShortSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Topic
        fields = (
            'id',
            'name',

        )


class DisciplineShortSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Discipline
        fields = (
            'id',
            'name',
            'department'
        )

    def to_representation(self, instance):
        response = super(DisciplineShortSerializer, self).to_representation(instance)
        response['department'] = DepartmentSerializer(instance.department).data
        return response


class SectionShortSerializer(serializers.ModelSerializer):
    discipline = DisciplineShortSerializer(read_only=True)

    class Meta:
        model = Section
        fields = (
            'name',
            'discipline',
        )


class TopicListSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()
    content = serializers.JSONField()

    class Meta:
        model = Topic
        fields = [
            'id',
            'name',
            'content',
            'discipline'
        ]

    def to_representation(self, instance):
        response = super(TopicListSerializer, self).to_representation(instance)
        response['discipline'] = DisciplineShortSerializer(instance.discipline).data
        return response

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == "name":
                for lang in set(value):
                    instance.name[lang] = value[lang]
                    instance.save()
            elif attr == "content":
                for lang in set(value):
                    instance.content[lang] = value[lang]
                    instance.save()
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance


class FinalControlShortSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = FinalControl
        fields = [
            'id',
            'name'
        ]


class FinalControlListSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()
    content = serializers.JSONField()

    class Meta:
        model = FinalControl
        fields = '__all__'

    def to_representation(self, instance):
        response = super(FinalControlListSerializer, self).to_representation(instance)
        response['discipline'] = DisciplineShortSerializer(instance.discipline).data
        return response

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == "name":
                for lang in set(value):
                    instance.name[lang] = value[lang]
                    instance.save()
            if attr == "content":
                for lang in set(value):
                    instance.content[lang] = value[lang]
                    instance.save()
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance


class IntermediateControlShortSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = IntermediateControl
        fields = [
            'id',
            'name'
        ]


class IntermediateControlListSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()
    content = serializers.JSONField()

    class Meta:
        model = IntermediateControl
        fields = '__all__'

    def to_representation(self, instance):
        response = super(IntermediateControlListSerializer, self).to_representation(instance)
        response['discipline'] = DisciplineShortSerializer(instance.discipline).data
        return response

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == "name":
                for lang in set(value):
                    instance.name[lang] = value[lang]
                    instance.save()
            if attr == "content":
                for lang in set(value):
                    instance.content[lang] = value[lang]
                    instance.save()
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance


class SectionListSerializer(serializers.ModelSerializer):
    discipline_id = serializers.IntegerField(write_only=True)
    discipline = DisciplineShortSerializer(read_only=True)
    topic_set = TopicShortSerializer(many=True, read_only=True)

    class Meta:
        model = Section
        fields = (
            'name',
            'discipline_id',
            'discipline',
            'topic_set',
            'created_at',
            'updated_at'
        )


class DisciplineListSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Discipline
        fields = (
            'id',
            'name',
            'department',
            # 'created_at',
            # 'updated_at'
        )

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == "name":
                for lang in set(value):
                    instance.name[lang] = value[lang]
                    instance.save()
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance

    def to_representation(self, instance):
        response = super(DisciplineListSerializer, self).to_representation(instance)
        response['department'] = DepartmentSerializer(instance.department).data
        return response


class CourseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()
    anons = serializers.JSONField()
    regulation = serializers.JSONField()

    class Meta:
        model = Course
        fields = '__all__'

    def to_representation(self, instance):
        response = super(CourseSerializer, self).to_representation(instance)
        response['course_type'] = CourseTypeSerializer(instance.course_type).data
        return response


class CourseShortSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = Course
        fields = [
            'id',
            'name'
        ]


class CourseDetailSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()
    anons = serializers.JSONField()
    regulation = serializers.JSONField()

    class Meta:
        model = Course
        fields = '__all__'

    def to_representation(self, instance):
        response = super(CourseDetailSerializer, self).to_representation(instance)
        response['course_type'] = CourseTypeSerializer(instance.course_type).data
        response['module_course'] = ModuleCourseListSerializer(instance.modulecourse_set.all(), many=True).data
        return response

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == "name":
                for lang in set(value):
                    instance.name[lang] = value[lang]
                    instance.save()
            if attr == "anons":
                for lang in set(value):
                    instance.anons[lang] = value[lang]
                    instance.save()
            if attr == "regulation":
                for lang in set(value):
                    instance.regulation[lang] = value[lang]
                    instance.save()
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance


class ModuleCourseListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = ModuleCourse
        fields = '__all__'

    def to_representation(self, instance):
        response = super(ModuleCourseListSerializer, self).to_representation(instance)
        response['course'] = CourseShortSerializer(instance.course).data
        response['discipline'] = DisciplineShortSerializer(instance.discipline).data
        # response['weeks'] = WeekSerializer(instance.week_set.all(), many=True).data
        return response


class ModuleCourseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = ModuleCourse
        fields = '__all__'
        extra_kwargs = {
            'course': {'required': False}
        }

    def to_representation(self, instance):
        response = super(ModuleCourseSerializer, self).to_representation(instance)
        response['course'] = CourseShortSerializer(instance.course).data
        response['discipline'] = DisciplineShortSerializer(instance.discipline).data
        response['weeks'] = WeekSerializer(instance.week_set.all(), many=True).data
        return response

    def create(self, validated_data):
        course = self.context.get('view').kwargs.get('course_id')
        validated_data['course'] = Course.objects.get(id=course)
        return super(ModuleCourseSerializer, self).create(validated_data)


class ModuleWeekControlDaySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    moduleweektype_id = serializers.IntegerField(source='moduleweektype.id', required=False)

    class Meta:
        model = ModuleWeekControlDay
        fields = '__all__'

    def to_representation(self, instance):
        response = super(ModuleWeekControlDaySerializer, self).to_representation(instance)
        response['intermediate_control'] = IntermediateControlShortSerializer(instance.intermediate_control).data
        response[
            'total_question_count'] = instance.easy_question_count + instance.medium_question_count + instance.hard_question_count
        return response


class ModuleWeekLessonDaySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    moduleweektype_id = serializers.IntegerField(source='moduleweektype.id', required=False)

    class Meta:
        model = ModuleWeekLessonDay
        fields = [
            'id',
            'lesson',
            'moduleweektype_id',
            'question_count',
            'deadline_duration',
        ]
    def to_representation(self, instance):
        print(instance)
        response = super(ModuleWeekLessonDaySerializer, self).to_representation(instance)
        response['lesson'] = TopicShortSerializer(instance.lesson).data
        return response


class ModuleWeekFinalControlDaySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    moduleweektype_id = serializers.IntegerField(source='moduleweektype.id', required=False)

    class Meta:
        model = ModuleWeekFinalControlDay
        fields = '__all__'

    def to_representation(self, instance):
        response = super(ModuleWeekFinalControlDaySerializer, self).to_representation(instance)
        response['final_control'] = FinalControlShortSerializer(instance.final_control).data
        response[
            'total_question_count'] = instance.easy_question_count + instance.medium_question_count + instance.hard_question_count
        return response


class FinalControlTopicSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    topics = serializers.PrimaryKeyRelatedField(queryset=Topic.objects.all(), source='get_topics', many=True)

    class Meta:
        model = FinalControl
        fields = '__all__'

    def to_representation(self, instance):
        response = super(FinalControlTopicSerializer, self).to_representation(instance)
        response['topics'] = TopicListSerializer(FinalControl.get_topics(instance), many=True).data
        return response

    def update(self, instance, validated_data):
        if validated_data['get_topics']:
            topics = FinalControlTopics.objects.filter(final_control_id=instance.id)
            topics.delete()
            topics = validated_data.pop('get_topics')
            for topic in topics:
                final_control_forming = FinalControlTopics.objects.create(final_control_id=instance.id, topic=topic)
                final_control_forming.save()
            instance.save()
        return instance


class IntermediateControlTopicSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    topics = serializers.PrimaryKeyRelatedField(queryset=Topic.objects.all(), source='get_topics', many=True)

    class Meta:
        model = IntermediateControl
        fields = '__all__'

    def to_representation(self, instance):
        response = super(IntermediateControlTopicSerializer, self).to_representation(instance)
        response['topics'] = TopicListSerializer(IntermediateControl.get_topics(instance), many=True).data
        return response

    def update(self, instance, validated_data):
        if validated_data.get('get_topics'):
            topics = IntermediateControlTopics.objects.filter(intermediate_control=instance.id)
            topics.delete()
            topics = validated_data.pop('get_topics')
            for topic in topics:
                final_control_forming = IntermediateControlTopics.objects.create(intermediate_control_id=instance.id,
                                                                                 topic=topic)
                final_control_forming.save()
            instance.save()
        return super(IntermediateControlTopicSerializer, self).update(instance, validated_data)


class ModuleWeekTypeSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    lesson = ModuleWeekLessonDaySerializer(many=True)
    int_control = ModuleWeekControlDaySerializer(many=True)
    fin_control = ModuleWeekFinalControlDaySerializer(many=True)

    class Meta:
        model = ModuleWeekType
        fields = '__all__'


class WeekSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()
    lesson = ModuleWeekLessonDaySerializer(many=True, required=False)
    int_control = ModuleWeekControlDaySerializer(many=True, required=False)
    fin_control = ModuleWeekFinalControlDaySerializer(many=True, required=False)

    class Meta:
        model = Week
        fields = '__all__'
        extra_kwargs = {
            'module_course': {'required': False}
        }

    def to_representation(self, instance):
        response = super(WeekSerializer, self).to_representation(instance)
        lesson = ModuleWeekLessonDay.objects.filter(moduleweektype__week_day=instance.id, lesson__isnull=False)
        int_control = ModuleWeekControlDay.objects.filter(moduleweektype__week_day=instance.id,
                                                          intermediate_control__content__isnull=False)
        fin_control = ModuleWeekFinalControlDay.objects.filter(moduleweektype__week_day=instance.id,
                                                               final_control__content__isnull=False)
        response['lesson'] = ModuleWeekLessonDaySerializer(lesson, many=True).data
        response['int_control'] = ModuleWeekControlDaySerializer(int_control, many=True).data
        response['fin_control'] = ModuleWeekFinalControlDaySerializer(fin_control, many=True).data
        return response

    def create(self, validated_data):
        kwargs = self.context.get('view').kwargs.get('id')
        week = Week.objects.create(name=validated_data['name'], module_course_id=kwargs)
        if validated_data.get('lesson'):
            lesson = validated_data['lesson']
            mwl = ModuleWeekLessonDaySerializer(many=True).create(lesson)
            for data in mwl:
                print(data)
                mwt = ModuleWeekType.objects.create(lesson=data, week_day=week)
                mwt.save()
        if validated_data.get('int_control'):
            int_control = validated_data['int_control']
            print(int_control)
            mwc = ModuleWeekControlDaySerializer(many=True).create(int_control)
            for data in mwc:
                mwt = ModuleWeekType.objects.create(int_control=data, week_day=week)
                mwt.save()
        if validated_data.get('fin_control'):
            fin_control = validated_data['fin_control']
            mwc = ModuleWeekFinalControlDaySerializer(many=True).create(fin_control)
            for data in mwc:
                mwt = ModuleWeekType.objects.create(fin_control=data, week_day=week)
                mwt.save()
        week.save()
        return week

    def update(self, instance, validated_data):
        instance.updated_at = datetime.now()
        for attr, value in validated_data.items():
            if attr == "name":
                for lang in set(value):
                    instance.name[lang] = value[lang]
                    instance.save()
        ls = validated_data.get('lesson')
        ic = validated_data.get('int_control')
        fc = validated_data.get('fin_control')
        if ls:
            for lesson in ls:
                if lesson.get('moduleweektype'):
                    mwt_id = lesson.pop('moduleweektype').get('id')
                    if lesson:
                        ModuleWeekLessonDay.objects.filter(moduleweektype=mwt_id).update(**lesson)
                    else:
                        ModuleWeekType.objects.filter(id=mwt_id).delete()

                else:
                    mwl = ModuleWeekLessonDay.objects.create(**lesson)
                    ModuleWeekType.objects.create(week_day=instance, lesson=mwl)

        if ic:
            for control in ic:
                if control.get('moduleweektype'):
                    mwt_id = control.pop('moduleweektype').get('id')
                    if control:
                        ModuleWeekControlDay.objects.filter(moduleweektype=mwt_id).update(**control)
                    else:
                        ModuleWeekType.objects.filter(id=mwt_id).delete()

                else:
                    mwl = ModuleWeekControlDay.objects.create(**control)
                    ModuleWeekType.objects.create(week_day=instance, int_control=mwl)
        if fc:

            for control in fc:
                if control.get('moduleweektype'):
                    mwt_id = control.pop('moduleweektype').get('id')
                    if control:
                        ModuleWeekFinalControlDay.objects.filter(moduleweektype=mwt_id).update(**control)
                    else:
                        ModuleWeekType.objects.filter(id=mwt_id).delete()

                else:
                    mwl = ModuleWeekFinalControlDay.objects.create(**control)
                    ModuleWeekType.objects.create(week_day=instance, fin_control=mwl)

        instance.save()
        return instance
