from django.db.models.fields.json import KeyTransform
from django_filters import FilterSet, ModelMultipleChoiceFilter, CharFilter, ChoiceFilter, ModelChoiceFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, mixins, status, filters
from rest_framework.permissions import AND, AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from api.models.discipline import ModuleWeekFinalControlDay, FinalControlTopics, FinalControl, IntermediateControl, \
    ModuleWeekType
from api.v1.discipline.serializers import (TopicListSerializer, SectionListSerializer, DisciplineListSerializer,
                                           CourseSerializer, ModuleCourseSerializer,
                                           WeekSerializer, ModuleWeekControlDaySerializer,
                                           ModuleWeekLessonDaySerializer, CourseDetailSerializer,
                                           ModuleWeekFinalControlDaySerializer, FinalControlTopicSerializer,
                                           IntermediateControlTopicSerializer, FinalControlListSerializer,
                                           IntermediateControlListSerializer, ModuleCourseListSerializer,
                                           ModuleWeekTypeSerializer, CourseTypeSerializer, DepartmentSerializer)
from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView, DestroyAPIView, \
    RetrieveUpdateDestroyAPIView, RetrieveAPIView, ListCreateAPIView, RetrieveUpdateAPIView
from api.models import Topic, Discipline, Section, Course, ModuleCourse, Week, ModuleWeekControlDay, \
    ModuleWeekLessonDay, CourseType, Department
from api.v1.permissions import EducationOfficeUnitPermission, AdministratorPermission
from api.v1.permissions import EducationOfficeUnitPermission


class DisciplineListAPIView(ListAPIView):
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission ]
    serializer_class = DisciplineListSerializer

    def get_queryset(self):
        qs = Discipline.objects.all()
        id = self.request.GET.get('id')
        if id:
            qs = qs.filter(id=id)
        return qs


class SectionListAPIView(ListAPIView):
    permission_classes = [EducationOfficeUnitPermission, ]
    serializer_class = SectionListSerializer

    def get_queryset(self):
        qs = Section.objects.all()
        id = self.request.GET.get('id')
        if id:
            qs = qs.filter(id=id)
        return qs


class TopicListAPIView(ListAPIView):
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission ]
    queryset = Topic.objects.all().order_by("-created_at")
    serializer_class = TopicListSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['discipline__modulecourse']


class DisciplineCreateAPIView(CreateAPIView):
    queryset = Discipline.objects.all()
    serializer_class = DisciplineListSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission ]


class SectionCreateAPIView(CreateAPIView):
    queryset = Section.objects.all()
    serializer_class = SectionListSerializer
    permission_classes = [EducationOfficeUnitPermission, ]


class TopicCreateAPIView(CreateAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicListSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission ]


class IntControlCreateAPIView(CreateAPIView):
    queryset = IntermediateControl.objects.all()
    serializer_class = IntermediateControlListSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission ]


class FinControlListAPIView(ListAPIView):
    queryset = FinalControl.objects.all().order_by("-created_at")
    serializer_class = FinalControlListSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission ]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['discipline__modulecourse']


class IntControlListAPIView(ListAPIView):
    queryset = IntermediateControl.objects.all().order_by("-created_at")
    serializer_class = IntermediateControlListSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission ]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['discipline__modulecourse']


class FinControlCreateAPIView(CreateAPIView):
    queryset = FinalControl.objects.all()
    serializer_class = FinalControlListSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission ]


class TopicUpdateAPIView(UpdateAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicListSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission]
    lookup_url_kwarg = 'id'


class FinControlUpdateAPIView(UpdateAPIView):
    queryset = FinalControl.objects.all()
    serializer_class = FinalControlListSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission]
    lookup_url_kwarg = 'id'


class IntControlUpdateAPIView(UpdateAPIView):
    queryset = IntermediateControl.objects.all()
    serializer_class = IntermediateControlListSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission ]
    lookup_url_kwarg = 'id'


class DisciplineUpdateAPIView(UpdateAPIView):
    queryset = Discipline.objects.all()
    serializer_class = DisciplineListSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission]
    lookup_url_kwarg = 'id'


class SectionUpdateAPIView(UpdateAPIView):
    queryset = Section.objects.all()
    serializer_class = SectionListSerializer
    permission_classes = [EducationOfficeUnitPermission, ]
    lookup_url_kwarg = 'id'


class DisciplineDeleteAPIView(DestroyAPIView):
    queryset = Discipline.objects.all()
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission ]
    lookup_url_kwarg = 'id'


class SectionDeleteAPIView(DestroyAPIView):
    queryset = Section.objects.all()
    permission_classes = [EducationOfficeUnitPermission, ]
    lookup_url_kwarg = 'id'


class TopicDeleteAPIView(DestroyAPIView):
    queryset = Topic.objects.all()
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission ]
    lookup_url_kwarg = 'id'


class IntControlDeleteAPIView(DestroyAPIView):
    queryset = IntermediateControl.objects.all()
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission]
    lookup_url_kwarg = 'id'


class FinControlDeleteAPIView(DestroyAPIView):
    queryset = FinalControl.objects.all()
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission]
    lookup_url_kwarg = 'id'


class CourseListAPIView(ListAPIView):
    queryset = Course.objects.all().order_by("-created_at")
    serializer_class = CourseSerializer
    permission_classes = [EducationOfficeUnitPermission]
    filter_backends = (filters.SearchFilter,)
    search_fields = ['name']


class CourseCreateAPIView(CreateAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    permission_classes = [EducationOfficeUnitPermission]


class CourseDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseDetailSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = 'course_id'


class ModuleCourseListAPIView(ListAPIView):
    serializer_class = ModuleCourseListSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = 'course_id'
    filter_backends = (filters.SearchFilter,)
    search_fields = ['discipline__name']

    def get_queryset(self):
        queryset = ModuleCourse.objects.filter(course_id=self.kwargs['course_id'])
        return queryset


class ModuleCourseCreateAPIView(CreateAPIView):
    serializer_class = ModuleCourseSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = 'course_id'

    def get_queryset(self):
        queryset = ModuleCourse.objects.filter(course=self.kwargs['course_id'])
        return queryset


class ModuleCourseRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    queryset = ModuleCourse.objects.all()
    serializer_class = ModuleCourseListSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = ['course_id', 'id']

    def get_queryset(self):
        queryset = ModuleCourse.objects.filter(course=self.kwargs['course_id'])
        return queryset

    def get_object(self):
        obj = self.queryset.get(id=self.kwargs['id'])
        return obj


class ModuleCourseDetailAPIView(RetrieveAPIView):
    queryset = ModuleCourse.objects.all()
    serializer_class = ModuleCourseSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = 'id'


class WeekListAPIView(ListAPIView):
    serializer_class = WeekSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        queryset = Week.objects.filter(module_course_id=self.kwargs['id'])
        return queryset


class WeekCreateAPIView(CreateAPIView):
    serializer_class = WeekSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        queryset = Week.objects.filter(module_course__discipline_id=self.kwargs['id'])
        return queryset


class WeekRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Week.objects.all()
    serializer_class = WeekSerializer
    permission_classes = [EducationOfficeUnitPermission]

    lookup_url_kwarg = 'week_id'


class ModuleWeekLessonDayListAPIView(ListAPIView):
    serializer_class = ModuleWeekLessonDaySerializer
    permission_classes = [EducationOfficeUnitPermission]

    lookup_url_kwarg = 'week_id'

    def get_queryset(self):
        queryset = ModuleWeekLessonDay.objects.filter(moduleweektype__week_day=self.kwargs['week_id'])
        return queryset


class ModuleWeekControlDayListAPIView(ListAPIView):
    queryset = ModuleWeekControlDay.objects.all()
    serializer_class = ModuleWeekControlDaySerializer
    permission_classes = [EducationOfficeUnitPermission]


class ModuleWeekFinalControlDayListAPIView(ListAPIView):
    queryset = ModuleWeekControlDay.objects.all()
    serializer_class = ModuleWeekControlDaySerializer
    permission_classes = [EducationOfficeUnitPermission]


class ModuleWeekLessonDayDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ModuleWeekLessonDaySerializer
    permission_classes = [EducationOfficeUnitPermission]

    lookup_url_kwarg = 'id'

    def get_queryset(self):
        queryset = ModuleWeekLessonDay.objects.all()
        print(queryset)
        return queryset


class ModuleWeekControlDayDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ModuleWeekControlDaySerializer
    permission_classes = [EducationOfficeUnitPermission]

    # lookup_field = 'id'
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        queryset = ModuleWeekControlDay.objects.all()
        return queryset


class ModuleWeekFinalControlDayDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ModuleWeekFinalControlDaySerializer
    permission_classes = [EducationOfficeUnitPermission]

    lookup_url_kwarg = 'id'

    def get_queryset(self):
        queryset = ModuleWeekFinalControlDay.objects.all()
        return queryset


class ModuleWeekDayCreateAPIView(ListCreateAPIView):
    queryset = Week.objects.all()
    serializer_class = WeekSerializer
    lookup_url_kwarg = 'week_id'


class ModuleWeekLessonDayCreateAPIView(ListCreateAPIView):
    queryset = ModuleWeekLessonDay.objects.all()
    serializer_class = ModuleWeekLessonDaySerializer
    permission_classes = [EducationOfficeUnitPermission]

    lookup_url_kwarg = 'week_id'


class ModuleWeekControlDayCreateAPIView(CreateAPIView):
    queryset = ModuleWeekControlDay.objects.all()
    serializer_class = ModuleWeekControlDaySerializer
    permission_classes = [EducationOfficeUnitPermission]

    def create(self, request, *args, **kwargs):
        data = request.data
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class ModuleWeekFinalControlDayCreateAPIView(CreateAPIView):
    queryset = ModuleWeekFinalControlDay.objects.all()
    serializer_class = ModuleWeekFinalControlDaySerializer

    permission_classes = [EducationOfficeUnitPermission]

    def create(self, request, *args, **kwargs):
        data = request.data
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class FinalControlTopicsUpdateView(RetrieveUpdateDestroyAPIView):
    queryset = FinalControl.objects.all()
    serializer_class = FinalControlTopicSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission]
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        queryset = FinalControl.objects.all()
        return queryset


class IntermediateControlTopicsUpdateView(RetrieveUpdateDestroyAPIView):
    queryset = IntermediateControl.objects.all()
    serializer_class = IntermediateControlTopicSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission]

    lookup_url_kwarg = 'id'

    def get_queryset(self):
        queryset = IntermediateControl.objects.all()
        return queryset


class CourseTypeListView(ListAPIView):
    queryset = CourseType.objects.all()
    serializer_class = CourseTypeSerializer
    permission_classes = [EducationOfficeUnitPermission, ]

class DepartmentListView(ListAPIView):
    queryset = Department.objects.all().order_by("-created_at")
    serializer_class = DepartmentSerializer
    permission_classes = [EducationOfficeUnitPermission | AdministratorPermission]
