from api.models import Student
from api.models.applicant import Applicant
from api.v1.admin.student.serializers import (ApplicantSerializer,
                                              ApplicantSexSerializer,
                                              ApplicantShortSerializer,
                                              StudentCreateSerializer,
                                              StudentUpdateSerializer,
                                              StudentSerializer,
                                              StudentShortSerializer,
                                              StudentWithApplicantSerializer)
from api.v1.permissions import AdministratorPermission
from rest_framework.generics import (CreateAPIView,
                                     ListAPIView,
                                     RetrieveUpdateAPIView,
                                     UpdateAPIView)


class StudentCreateView(CreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    permission_classes = (AdministratorPermission,)


class StudentDetailUpdateView(RetrieveUpdateAPIView):
    serializer_class = StudentUpdateSerializer
    permission_classes = (AdministratorPermission,)
    lookup_url_kwarg = "id"
    queryset = Student.objects.all()


class StudentListView(ListAPIView):
    serializer_class = StudentShortSerializer
    permission_classes = (AdministratorPermission,)
    queryset = Student.objects.all().order_by("-created_at")


class ApplicantSexListView(ListAPIView):
    serializer_class = ApplicantSexSerializer
    permission_classes = (AdministratorPermission,)

    def get_queryset(self):
        return Applicant.SEX_CHOICES


class ApplicantListView(ListAPIView):
    serializer_class = ApplicantShortSerializer
    permission_classes = (AdministratorPermission,)
    queryset = Applicant.objects.all().order_by("-created_at")


class ApplicantCreateView(CreateAPIView):
    serializer_class = ApplicantSerializer
    permission_classes = (AdministratorPermission,)
    # parser_classes = (MultipartJsonParser, JSONParser)


class ApplicantUpdateView(UpdateAPIView):
    serializer_class = ApplicantSerializer
    permission_classes = (AdministratorPermission,)
    # parser_classes = (FileUploadParser, JSONParser, MultipartJsonParser)


class ApplicantUpdateDetailView(RetrieveUpdateAPIView):
    serializer_class = ApplicantSerializer
    permission_classes = (AdministratorPermission,)
    lookup_url_kwarg = "id"
    queryset = Applicant.objects.all()


class StudentFromApplicantCreateView(CreateAPIView):
    serializer_class = StudentCreateSerializer
    permission_classes = (AdministratorPermission,)


class StudentWithApplicantCreateView(CreateAPIView):
    serializer_class = StudentWithApplicantSerializer
