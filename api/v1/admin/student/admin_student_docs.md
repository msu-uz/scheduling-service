# Создание студента в админке

## `GET admin/student/list/`

Список студентов.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "name": "Алина Суханова Вадимовна"
    },
    {
      "id": 2,
      "name": "Фадеевич Адриан Бобылев"
    }
  ]
}
```

## `GET admin/coursetype/list/`

Список типов курсов.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Очное",
        "uz": "Traditional"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Дистанционное",
        "uz": "Remote"
      }
    }
  ]
}
```

## `GET admin/student/applicant/sex/list/`

Список полов для аппликанта.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "label": "мужской"
    },
    {
      "id": 2,
      "label": "женский"
    }
  ]
}
```

## `POST admin/student/applicant/create/`

Создание аппликанта.

### Request:

```json
{
  "image": FILE,
  "first_name": "fsf",
  "second_name": "fdsfsd",
  "last_name": "fsdfsd",
  "birth_date": "1997-10-07",
  "sex": 1,
  "passport_number": "fsdf",
  "birth_place": "Tashkent",
  "passport_address": "Tashkent",
  "living_address": "Tashkent",
  "education_job": "Kek",
  "university_name": "fdsfds",
  "specialization": "fdfsdf",
  "university_study_start_year": "2017-09-01",
  "university_study_end_year": "2021-05-25",
  "university_foreign_language": "Eng",
  "university_study_language": "Rus",
  "job_place": "OSG",
  "job_address": "Chust",
  "war_ticket_info": "Lol",
  "qualification": "Top",
  "mother_info": {
    "full_name": "fsdfsd",
    "birth_date": "1960-01-24",
    "birth_place": "Tashkent",
    "job_place": "-",
    "job_address": "Tashkent",
    "phone_number": "+998"
  },
  "father_info": {
    "full_name": "fsdfsd",
    "birth_date": "1957-08-11",
    "birth_place": "Tashkent",
    "job_place": "fsdfd",
    "job_address": "Tashkent",
    "phone_number": "+998"
  }
}
```

## `GET admin/student/applicant/list/`

Список аппликантов.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "birth_date": "1973-06-22",
      "name": "Суханова Алина Вадимовна"
    },
    {
      "id": 2,
      "birth_date": "2000-06-05",
      "name": "Адриан Фадеевич Бобылев"
    }
  ]
}
```

## `GET admin/student/applicant/<int:id>/

Детальный просмотр аппликанта.

### Response:

```json
{
  "id": 17,
  "image": "http://127.0.0.1:8000/media/face_jmyyY2N.jpg",
  "first_name": "Фомичева",
  "second_name": "Никифоровна",
  "last_name": "Валерия",
  "birth_date": "2007-12-28",
  "sex": 1,
  "passport_number": "123123123",
  "birth_place": "п. Шадринск, ул. Дружбы, д. 33 стр. 62, 865260",
  "passport_address": "ст. Видное, ш. Металлистов, д. 8 к. 4/5, 115507",
  "living_address": "п. Томари, ш. Сельское, д. 88 стр. 8, 469142",
  "education_job": "Металлург",
  "university_name": "harvard",
  "specialization": "Флорист",
  "university_study_start_year": "2017-12-10",
  "university_study_end_year": "2020-12-18",
  "university_foreign_language": "english",
  "university_study_language": "english",
  "job_place": "Online Service Group",
  "job_address": "с. Троицко-Печорск, алл. Подгорная, д. 508 стр. 7/7, 114517",
  "war_ticket_info": "Проходил службу в воздушных войсках",
  "qualification": "",
  "mother_info": {
    "id": 17,
    "full_name": "Никонов Твердислав Авдеевич",
    "birth_date": "1978-07-03",
    "birth_place": "",
    "job_place": "OSG",
    "job_address": "к. Североморск, наб. Монтажников, д. 511, 896900",
    "phone_number": "8 (407) 281-4021"
  },
  "father_info": {
    "id": 17,
    "full_name": "Никонов Твердислав Авдеевич",
    "birth_date": "1978-07-03",
    "birth_place": "",
    "job_place": "OSG",
    "job_address": "к. Североморск, наб. Монтажников, д. 511, 896900",
    "phone_number": "8 (407) 281-4021"
  }
}
```

## `PUT admin/student/applicant/<int:id>/`

Обновление аппликанта.

### Request:

```json
{
  "image": FILE,
  "first_name": "fsf",
  "second_name": "fdsfsd",
  "last_name": "fsdfsd",
  "birth_date": "1997-10-07",
  "sex": 1,
  "passport_number": "fsdf",
  "birth_place": "Tashkent",
  "passport_address": "Tashkent",
  "living_address": "Tashkent",
  "education_job": "Kek",
  "university_name": "fdsfds",
  "specialization": "fdfsdf",
  "university_study_start_year": "2017-09-01",
  "university_study_end_year": "2021-05-25",
  "university_foreign_language": "Eng",
  "university_study_language": "Rus",
  "job_place": "OSG",
  "job_address": "Chust",
  "war_ticket_info": "Lol",
  "qualification": "Top",
  "mother_info": {
    "full_name": "fsdfsd",
    "birth_date": "1960-01-24",
    "birth_place": "Tashkent",
    "job_place": "-",
    "job_address": "Tashkent",
    "phone_number": "+998"
  },
  "father_info": {
    "full_name": "fsdfsd",
    "birth_date": "1957-08-11",
    "birth_place": "Tashkent",
    "job_place": "fsdfd",
    "job_address": "Tashkent",
    "phone_number": "+998"
  }
}
```

## `POST admin/student/applicant/student/create/`

Создание студента.

### Request:

```json
{
  "username": "useruseruser",
  "password": "123123d.",
  "phone": "+998",
  "applicant": 101,
  "course_type": 1,
  "is_student": true
}
```
