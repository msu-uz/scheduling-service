import json
from json.decoder import JSONDecodeError

from django.contrib.auth import get_user_model
from django.core import exceptions
from django.db import transaction
from django.db.models.fields.files import ImageFieldFile
from rest_framework import serializers
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.serializers import ModelSerializer, Serializer

from api.models import Employee, CourseType
from api.models.applicant import Applicant, ApplicantParentInfo
from api.models.role import Student, Teacher
from api.v1.admin.ekadr_request import ekadr_request_user, create_employee

User = get_user_model()


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class StudentShortSerializer(ModelSerializer):
    class Meta:
        model = Student
        fields = ("id", "applicant_id")

    def to_representation(self, instance: Student):
        response = super().to_representation(instance)
        response.update({
            "name": str(instance),
            "image": None,
        })

        if image := instance.image:
            if isinstance(image, str):
                response["image"] = image
            elif isinstance(image, ImageFieldFile):
                response["image"] = self.context["request"].build_absolute_uri(image.url)

        return response


class EmployeeSerializer(ModelSerializer):
    class Meta:
        model = Employee
        fields = ['id', 'first_name', 'second_name', 'last_name', 'image']


class StudentSerializer(ModelSerializer):
    username = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)
    image = serializers.URLField(source='user.profile_image_url', read_only=True)
    first_name = serializers.CharField(source='user.first_name', read_only=True)
    middle_name = serializers.CharField(source='user.middle_name', read_only=True)
    last_name = serializers.CharField(source='user.last_name', read_only=True)
    kadr_id = serializers.IntegerField(write_only=True)
    email = serializers.EmailField(write_only=True, required=False)

    class Meta:
        model = Student
        fields = ('kadr_id', 'username', 'password', 'id', 'image', 'created_at', 'updated_at', 'first_name',
                  'middle_name', 'last_name')
        read_only_fields = ('id', 'image', 'created_at', 'updated_at', 'first_name', 'middle_name', 'last_name')

    def validate_kadr_id(self, ekadr_id):
        if Employee.objects.filter(ekadr_id=ekadr_id).exists():
            raise ValidationError(f'Employee with e-kard id {ekadr_id} already exists')

        ekadr_user, status_code = ekadr_request_user(ekadr_id)
        if status_code != 200:
            raise NotFound(f'User {ekadr_id} not found')

        self.employee = create_employee(ekadr_id)
        self.ekadr_user = ekadr_user
        return ekadr_id

    def validate_username(self, username):
        if User.objects.filter(username=username).exists():
            raise ValidationError(f'User with username {username} already exists')
        return username

    def create(self, validated_data):
        ekadr_user = self.ekadr_user
        username = validated_data.pop('username')
        password = validated_data.pop('password')
        email = validated_data.pop('email')

        with transaction.atomic():
            new_user = User(username=username,
                            first_name=ekadr_user['first_name']['ru'],
                            middle_name=ekadr_user['middle_name']['ru'],
                            last_name=ekadr_user['last_name']['ru'],
                            phone=ekadr_user['phone'],
                            access_level=User.STUDENT_LEVEL,
                            email=email)

            new_user.set_password(password)
            new_user.save()

            validated_data.update({
                'user': new_user,
                'employee_id': self.employee.id,
                'course_type_id': CourseType.objects.last().id,  # Дистанционное
            })

            student = super().create(validated_data)
        return student


class UserUpdateSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'phone', 'email')
        read_only_fields = ('id',)

    def update(self, instance, validated_data):
        with transaction.atomic():
            if password := validated_data.pop('password', False):
                instance.set_password(password)
            user = super().update(instance, validated_data)
        return user


class StudentUpdateSerializer(ModelSerializer):
    username = serializers.CharField(source='user.username')
    email = serializers.CharField(source='user.email')
    phone = serializers.CharField(source='user.phone')
    password = serializers.CharField(source='user.password', write_only=True)

    class Meta:
        model = Student
        fields = (
            'id',
            'username',
            'password',
            'phone',
            'email',
            'course_type',
            'applicant'
        )
        read_only_fields = ('id', 'applicant')

    def update(self, instance, validated_data):
        with transaction.atomic():
            user_data = validated_data.pop('user', dict())

            user_serializer = UserUpdateSerializer(instance=instance.user,
                                                   data=user_data,
                                                   partial=True)
            user_serializer.is_valid(raise_exception=True)
            user_serializer.save()

            student = super().update(instance, validated_data)
        return student


class ApplicantSexSerializer(Serializer):
    def to_representation(self, instance):
        return {"id": instance[0], "label": instance[1]}


class ApplicantParentInfoSerializer(ModelSerializer):
    class Meta:
        model = ApplicantParentInfo
        fields = ("id", "full_name", "birth_date", "birth_place", "job_place", "job_address", "phone_number")
        read_only_fields = ("id",)


class ApplicantSerializer(ModelSerializer):
    mother_info = serializers.CharField(required=False)
    father_info = serializers.CharField(required=False)

    class Meta:
        model = Applicant
        fields = (
            "id",
            "image",
            "first_name",
            "second_name",
            "last_name",
            "birth_date",
            "sex",
            "passport_number",
            "birth_place",
            "passport_address",
            "living_address",
            "education_job",
            "university_name",
            "specialization",
            "university_study_start_year",
            "university_study_end_year",
            "university_foreign_language",
            "university_study_language",
            "job_place",
            "job_address",
            "war_ticket_info",
            "qualification",
            "mother_info",
            "father_info",
        )
        read_only_fields = ("id",)

    def to_internal_value(self, data):
        data = super().to_internal_value(data)

        mother_str = data.get("mother_info")
        father_str = data.get("father_info")

        errors = dict()

        if mother_str:
            try:
                mother_dict = json.loads(mother_str)
            except JSONDecodeError as e:
                errors["mother_info"] = e
            else:
                data["mother_info"] = mother_dict

        if father_str:
            try:
                father_dict = json.loads(father_str)
            except JSONDecodeError as e:
                errors["father_info"] = e
            else:
                data["father_info"] = father_dict

        if errors:
            raise ValidationError(errors)

        return data

    def create(self, validated_data):
        mother_info = validated_data.get("mother_info")
        father_info = validated_data.get("father_info")

        with transaction.atomic():
            if mother_info:
                mother_serializer = ApplicantParentInfoSerializer(data=mother_info)
                mother_serializer.is_valid(raise_exception=True)
                validated_data["mother_info"] = mother_serializer.save()

            if father_info:
                father_serializer = ApplicantParentInfoSerializer(data=father_info)
                father_serializer.is_valid(raise_exception=True)
                validated_data["father_info"] = father_serializer.save()

            instance = super().create(validated_data)

        return instance

    def update(self, instance: Applicant, validated_data):
        mother_info = validated_data.get("mother_info")
        father_info = validated_data.get("father_info")

        with transaction.atomic():
            if mother_info and instance.mother_info:
                mother_serializer = ApplicantParentInfoSerializer(
                    instance=instance.mother_info,
                    data=mother_info,
                )
                mother_serializer.is_valid(raise_exception=True)
                validated_data["mother_info"] = mother_serializer.save()
            elif mother_info:
                mother_serializer = ApplicantParentInfoSerializer(data=mother_info)
                mother_serializer.is_valid(raise_exception=True)
                validated_data["mother_info"] = mother_serializer.save()

            if father_info and instance.father_info:
                father_serializer = ApplicantParentInfoSerializer(
                    instance=instance.father_info,
                    data=father_info,
                )
                father_serializer.is_valid(raise_exception=True)
                validated_data["father_info"] = father_serializer.save()
            elif father_info:
                father_serializer = ApplicantParentInfoSerializer(data=father_info)
                father_serializer.is_valid(raise_exception=True)
                validated_data["father_info"] = father_serializer.save()

            updated_instance = super().update(instance, validated_data)

        return updated_instance

    def to_representation(self, instance):
        response = super().to_representation(instance)
        if instance.mother_info:
            response["mother_info"] = ApplicantParentInfoSerializer(instance.mother_info).data
        if instance.father_info:
            response["father_info"] = ApplicantParentInfoSerializer(instance.father_info).data
        return response


class ApplicantShortSerializer(ModelSerializer):
    class Meta:
        model = Applicant
        fields = ("id", "birth_date", "image")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["name"] = str(instance)
        return response


class UserShortSerializer(ModelSerializer):
    password = serializers.CharField()

    class Meta:
        model = User
        fields = (
            "username",
            "password",
            "first_name",
            "middle_name",
            "last_name",
            "profile_image",
            "access_level",
            "phone",
        )

    def create(self, validated_data):
        password = validated_data.pop("password")
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        return user


class StudentCreateSerializer(ModelSerializer):
    username = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)
    phone = serializers.CharField(write_only=True)
    email = serializers.EmailField(write_only=True, required=False)

    class Meta:
        model = Student
        fields = (
            "id",
            "username",
            "password",
            "phone",
            "applicant",
            "course_type",
            "is_student",
            "email",
        )
        read_only_fields = ("id",)

    def create(self, validated_data):
        applicant: Applicant = validated_data["applicant"]
        user_data = {
            "username": validated_data.pop("username"),
            "password": validated_data.pop("password"),
            "first_name": applicant.first_name,
            "last_name": applicant.last_name,
            "access_level": User.STUDENT_LEVEL,
            "phone": validated_data.pop("phone"),
            "email": validated_data.pop("email", None),
        }

        if applicant.image:
            user_data["profile_image"] = applicant.image

        if applicant.second_name:
            user_data["middle_name"] = applicant.second_name

        user_serializer = UserShortSerializer(data=user_data)
        user_serializer.is_valid(raise_exception=True)

        with transaction.atomic():
            validated_data["user"] = user_serializer.save()
            student = super().create(validated_data)

        return student


class StudentWithApplicantSerializer(Serializer):
    student = StudentCreateSerializer()
    applicant = ApplicantSerializer()

    class Meta:
        fields = ('applicant', 'student')

    def create(self, validated_data):
        applicant = validated_data.pop('applicant')
        student = validated_data.pop('student')

        applicant_serializer = ApplicantSerializer(data=applicant)
        applicant_serializer.is_valid(raise_exception=True)

        with transaction.atomic():
            applicant_saved = applicant_serializer.save()
            student['applicant'] = applicant_saved.id
            student['course_type'] = student['course_type'].id
            student_serializer = StudentCreateSerializer(data=student)
            student_serializer.is_valid(raise_exception=True)
            student_saved = student_serializer.save()

        return {
            'student': student_saved,
            'applicant': applicant_saved,
        }
