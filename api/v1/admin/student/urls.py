from django.urls import path

from api.v1.admin.student.views import (ApplicantUpdateDetailView,
                                        ApplicantListView,
                                        ApplicantSexListView,
                                        StudentCreateView,
                                        StudentDetailUpdateView,
                                        ApplicantCreateView,
                                        StudentFromApplicantCreateView,
                                        StudentListView,
                                        StudentWithApplicantCreateView)

urlpatterns = [
    path("create/", StudentCreateView.as_view()),
    path("<int:id>/", StudentDetailUpdateView.as_view()),
    path("withapplicant/create/", StudentWithApplicantCreateView.as_view()),
    path("list/", StudentListView.as_view()),
    path("applicant/sex/list/", ApplicantSexListView.as_view()),
    path("applicant/create/", ApplicantCreateView.as_view()),
    path("applicant/<int:id>/", ApplicantUpdateDetailView.as_view()),
    path("applicant/list/", ApplicantListView.as_view()),
    path("applicant/student/create/", StudentFromApplicantCreateView.as_view()),
]
