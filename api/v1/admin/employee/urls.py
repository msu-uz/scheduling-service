from django.urls import path
from api.v1.admin.employee import views

urlpatterns = [
    path("list/", views.EmployeeListAPIView.as_view()),
    path("<int:id>/", views.EmployeeDetailAPIView.as_view()),
]