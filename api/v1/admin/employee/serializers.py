from rest_framework import serializers
from api.models import Employee, Student, Teacher


class EmployeeShortSerializer(serializers.ModelSerializer):
    role = serializers.SerializerMethodField()

    def get_role(self, obj):
        if Student.objects.filter(employee=obj).exists():
            return "student"
        elif Teacher.objects.filter(employee=obj).exists():
            return "teacher"
        else:
            return "no role"

    class Meta:
        model = Employee
        fields = [
            'id',
            'first_name',
            'second_name',
            'last_name',
            'role',
            'image'
        ]


class EmployeeSerializer(serializers.ModelSerializer):
    role = serializers.SerializerMethodField()

    def get_role(self, obj):
        if Student.objects.filter(employee=obj).exists():
            return "student"
        elif Teacher.objects.filter(employee=obj).exists():
            return "teacher"
        else:
            return "no role"

    class Meta:
        model = Employee
        fields = [
            "image",
            "first_name",
            "second_name",
            "last_name",
            "birth_date",
            "passport_number",
            "nationality",
            "passport_address",
            "living_place",
            "organization",
            "position",
            "training_date_from",
            "training_date_to",
            "sex",
            "phone",
            "email",
            "university_name",
            "specialization",
            "university_study_start_date",
            "university_study_end_date",
            "level",
            "language",
            "qualification",
            "mother_info",
            "father_info",
            "ekadr_id",
            "role",
        ]
