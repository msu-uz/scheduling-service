from django.db.models.query import Q
from rest_framework import generics

from api.models import Employee, Teacher, Student
from api.v1.admin.employee.serializers import (EmployeeShortSerializer,
                                               EmployeeSerializer)
from api.v1.permissions import AdministratorPermission


class EmployeeListAPIView(generics.ListAPIView):
    serializer_class = EmployeeShortSerializer
    permission_classes = [AdministratorPermission, ]

    def get_queryset(self):
        employee_type = self.request.query_params.get("type")

        query = Q()
        if employee_type == "teacher":
            teacher_employee_ids = Teacher.objects.filter(employee__isnull=False).values_list('employee', flat=True)
            query = Q(id__in=teacher_employee_ids)
        elif employee_type == "student":
            student_employee_ids = Student.objects.filter(employee__isnull=False).values_list('employee', flat=True)
            query = Q(id__in=student_employee_ids)

        return Employee.objects.filter(query).order_by("-created_at")


class EmployeeDetailAPIView(generics.RetrieveAPIView):
    serializer_class = EmployeeSerializer
    permission_classes = [AdministratorPermission, ]
    lookup_url_kwarg = "id"
    queryset = Employee.objects.all()
