from rest_framework.views import APIView
from django.http import JsonResponse

from api.models import Employee
from api.v1.admin.ekadr_request import (city_list,
                                        district_list,
                                        organization_list,
                                        position_list,
                                        ekadr_request_list,
                                        ekadr_request_user)


class EkadrList(APIView):
    def get(self, request, *args, **kwargs):
        # TODO : оптимизация
        city_list()
        district_list()
        organization_list()
        position_list()

        response, status_code = ekadr_request_list()
        existing_ids = set(Employee.objects.values_list('ekadr_id', flat=True))
        employees = response.get('results')

        if employees:
            new_employees = []
            for employee in employees:
                if employee['id'] not in existing_ids:
                    new_employees.append(employee)
            response['results'] = new_employees
            response['count'] = len(new_employees)

        return JsonResponse(response, status=status_code)


class EkadrDetailView(APIView):
    def get(self, request, *args, **kwargs):
        response, status_code = ekadr_request_user(kwargs['employee_id'])
        return JsonResponse(response, status=status_code)
