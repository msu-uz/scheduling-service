from django.urls import path
from .views import EkadrDetailView, EkadrList

urlpatterns = [
    path("list/",  EkadrList.as_view()),
    path("<int:employee_id>/", EkadrDetailView.as_view())
]
