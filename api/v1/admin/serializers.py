from api.models.catalog import CourseType
from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

User = get_user_model()


class UserUpdateSerializer(ModelSerializer):
    image = serializers.ImageField(source='profile_image')

    class Meta:
        model = User
        fields = (
            'first_name',
            'middle_name',
            'last_name',
            'image',
            'id'
        )
        read_only_fields = ('id', 'image')


class CourseTypeSerializer(ModelSerializer):
    class Meta:
        model = CourseType
        fields = ("id", "name")
