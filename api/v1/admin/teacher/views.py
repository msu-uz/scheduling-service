from rest_framework.generics import CreateAPIView

from api.models import Teacher
from api.v1.admin.teacher.serializers import TeacherSerializer
from api.v1.permissions import AdministratorPermission


class TeacherCreateView(CreateAPIView):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer
    permission_classes = [AdministratorPermission, ]
