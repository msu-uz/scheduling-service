from django.urls import path

from api.v1.admin.teacher.views import TeacherCreateView

urlpatterns = [
    path("create/", TeacherCreateView.as_view()),
]
