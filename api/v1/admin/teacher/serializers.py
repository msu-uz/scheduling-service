from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.serializers import ModelSerializer

from api.models.role import Teacher, Student
from api.v1.admin.ekadr_request import ekadr_request_user, create_employee

User = get_user_model()


class TeacherSerializer(ModelSerializer):
    kadr_id = serializers.IntegerField(write_only=True)
    username = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)

    image = serializers.URLField(source='user.profile_image_url', read_only=True)
    first_name = serializers.CharField(source='user.first_name', read_only=True)
    middle_name = serializers.CharField(source='user.middle_name', read_only=True)
    last_name = serializers.CharField(source='user.last_name', read_only=True)

    class Meta:
        model = Teacher
        fields = (
            'kadr_id',
            'username',
            'password',
            'id',
            'image',
            'created_at',
            'updated_at',
            'first_name',
            'middle_name',
            'last_name'
        )
        read_only_fields = (
            'id',
            'image',
            'created_at',
            'updated_at',
            'first_name',
            'middle_name',
            'last_name'
        )

    def validate_kadr_id(self, ekadr_id):
        teacher = Teacher.objects.filter(employee_id=ekadr_id).first()
        if teacher:
            raise ValidationError(f'User {ekadr_id} already exists')
        student = Student.objects.filter(employee_id=ekadr_id).first()
        if student:
            raise ValidationError(f'User {ekadr_id} already exists')

        ekadr_user, status_code = ekadr_request_user(ekadr_id)
        if status_code != 200:
            raise NotFound(f'User {ekadr_id} not found')
        self.employee = create_employee(ekadr_id)
        self.ekadr_user = ekadr_user
        return ekadr_id

    def validate_username(self, username):
        if User.objects.filter(username=username).exists():
            raise ValidationError(f'User with username {username} already exists')
        return username

    def create(self, validated_data):
        ekadr_user = self.ekadr_user
        ekadr_id = validated_data.pop('kadr_id')
        username = validated_data.pop('username')
        password = validated_data.pop('password')

        new_user = User.objects.create(username=username,
                                       first_name=ekadr_user['first_name']['ru'],
                                       middle_name=ekadr_user['middle_name']['ru'],
                                       last_name=ekadr_user['last_name']['ru'],
                                       phone=ekadr_user['phone'],
                                    #    profile_image=ekadr_user['photo'],
                                       access_level=User.TEACHER_LEVEL)
        new_user.set_password(password)
        new_user.save()
        validated_data['user'] = new_user
        validated_data['employee_id'] = self.employee.id
        return super().create(validated_data)
