from django.urls import path, include

from api.v1.admin.views import CourseTypeListView, UserBlockView, UserUpdateView

urlpatterns = [
    path("teacher/", include("api.v1.admin.teacher.urls")),
    path("student/", include("api.v1.admin.student.urls")),
    path("ekadr/", include("api.v1.admin.ekadr.urls")),
    path("employee/", include("api.v1.admin.employee.urls")),
    path("block/<int:user_id>", UserBlockView.as_view()),
    path('kadr-update-user-webhook/<int:kadr_id>/', UserUpdateView.as_view()),
    path("coursetype/list/", CourseTypeListView.as_view()),
]
