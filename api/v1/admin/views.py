from api.models.catalog import CourseType
from django.contrib.auth import get_user_model
from django.http import JsonResponse
from rest_framework.generics import ListAPIView, UpdateAPIView
from rest_framework.views import APIView

from api.v1.admin.serializers import CourseTypeSerializer, UserUpdateSerializer
from api.v1.permissions import AdministratorPermission

User = get_user_model()


class UserBlockView(APIView):
    permission_classes = [AdministratorPermission, ]

    def post(self, request, *args, **kwargs):
        user_id = kwargs['user_id']
        user = User.objects.filter(id=user_id).first()

        if not user:
            return JsonResponse(data={"detail": "User not found."}, status=404)
        elif user == request.user:
            return JsonResponse(data={"detail": "You can't block yourself."}, status=400)
        else:
            user.is_active = False
            user.save()
            return JsonResponse(data={"ok": True}, status=200)


class UserUpdateView(UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserUpdateSerializer
    permission_classes = [AdministratorPermission, ]
    lookup_field = 'ekadr_id'
    lookup_url_kwarg = 'kadr_id'


class CourseTypeListView(ListAPIView):
    serializer_class = CourseTypeSerializer
    permission_classes = (AdministratorPermission,)
    queryset = CourseType.objects.all()
