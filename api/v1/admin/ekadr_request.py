from datetime import datetime
from dist_learning.settings import TIME_ZONE
import pytz
import requests
from django.http import JsonResponse
from requests.adapters import HTTPAdapter
from rest_framework.exceptions import NotFound

from api.models import District, City, Organization, Position, Employee


def ekadr_request(url, headers):
    session = requests.Session()
    adapter = HTTPAdapter(max_retries=5)
    session.mount(url, adapter)
    try:
        res = session.get(url=url, timeout=30,
                          headers=headers)
    except requests.exceptions.ConnectionError as errc:
        print("Error Connecting:", errc)
        raise errc
    except requests.exceptions.Timeout as errt:
        print("Timeout Error:", errt)
        raise errt
    except requests.exceptions.RequestException as err:
        print("Else:", err)
        raise err
    else:
        return res.json(), res.status_code


def create_employee(employee_id):
    # TODO : не безопасный способ достучаться до значений, нужно обогатить условиями :)
    employee = Employee()
    response = ekadr_request_user(employee_id)
    data, status_code = response
    if status_code == 200:
        employee.id = employee_id
        employee.image = data.get('photo')
        employee.first_name = data.get('first_name')['ru']
        employee.second_name = data.get('middle_name')['ru']
        employee.last_name = data.get('last_name')['ru']
        employee.birth_date = data.get('dob')
        employee.passport_number = data.get('passport_serial')
        employee.nationality = data.get('nationality')
        employee.passport_address_id = data.get('birth_place')['id']
        employee.living_place_id = data.get('living_place')['id']
        employee.position_id = data.get('position')
        employee.sex = data.get('gender') + 1
        employee.phone = data.get('phone')
        employee.email = data.get('email')
        if data.get('university'):
            employee.university_name = data.get('employeeknowledge_set')[0]['university']['name']['ru']
        if data.get('specialization'):
            employee.specialization = data.get('employeeknowledge_set')[0]['speciality']['name']['ru']
        if data.get('employeeknowledge_set'):
            employee.university_study_start_date = data.get('employeeknowledge_set')[0]['start_date']
            employee.university_study_end_date = data.get('employeeknowledge_set')[0]['graduate_date']
            employee.level = data.get('employeeknowledge_set')[0]['level']['name']['ru']
        if data.get('qualifying_rank'):
            employee.qualification = data.get('qualifying_rank')['ru']
        # employee.training_date_from =     # TODO : не нашел
        # employee.training_date_to         # TODO : не нашел
        # employee.language                 # TODO : не нашел
        # employee.organization             # TODO : Get active work_place, надо добавить инфу в е-кадр потом чекать
        # employee.mother_info              # TODO : надо уточнить у Эргаш ака по какому ключу брать инфу
        # employee.father_info              # TODO : надо уточнить у Эргаш ака по какому ключу брать инфу
        # employee.ekadr_id
        if data.get('created_at'):
            employee.created_at = data.get('created_at')
        else:
            timezone = pytz.timezone(TIME_ZONE)
            employee.created_at = datetime.now(tz=timezone)
        if data.get('updated_at'):
            employee.updated_at = data.get('updated_at')
        else:
            timezone = pytz.timezone(TIME_ZONE)
            employee.updated_at = datetime.now(tz=timezone)
        employee.save()
    else:
        raise NotFound(f'Employee {employee_id} not found')
    return employee


def ekadr_request_list():
    return ekadr_request(url="https://ekadr.sudya.uz/api/v1/employee/list/",
                         headers={"Authorization": "Token e99cd1b670e0e219f8bf3aa161641525a5639cc6"})


def ekadr_request_user(employee_id):
    return ekadr_request(url=f'https://ekadr.sudya.uz/api/v1/employee/detail/{employee_id}',
                         headers={"Authorization": "Token e99cd1b670e0e219f8bf3aa161641525a5639cc6"})


# TODO: можно оптимизировать
def city_list():
    response = ekadr_request(url="https://ekadr.sudya.uz/api/v1/wiki/city/list/",
                             headers={"Authorization": "Token e99cd1b670e0e219f8bf3aa161641525a5639cc6"})
    data, status_code = response
    city = City()
    if status_code == 200:
        for d in data['results']:
            city.id = d.get('id')
            city.name = d.get('name')
            city.created_at = d.get('created_at')
            city.updated_at = d.get('updated_at')
            city.save()

    return response


def district_list():
    response = ekadr_request(url="https://ekadr.sudya.uz/api/v1/wiki/district/list/",
                             headers={"Authorization": "Token e99cd1b670e0e219f8bf3aa161641525a5639cc6"})
    data, status_code = response
    district = District()
    if status_code == 200:
        for d in data['results']:
            district.id = d.get('id')
            district.name = d.get('name')
            district.city_id = d.get('city')['id']
            district.created_at = d.get('created_at')
            district.updated_at = d.get('updated_at')
            district.save()

    return response


def organization_list():
    response = ekadr_request(url="https://ekadr.sudya.uz/api/v1/employee/organization/list/",
                             headers={"Authorization": "Token e99cd1b670e0e219f8bf3aa161641525a5639cc6"})
    data, status_code = response
    organization = Organization()
    if status_code == 200:
        for d in data['results']:
            organization.id = d.get('id')
            organization.name = d.get('name')
            organization.district_id = d.get('district')['id']
            organization.created_at = d.get('created_at')
            organization.updated_at = d.get('updated_at')
            organization.save()

    return response


def position_list():
    response = ekadr_request(url="https://ekadr.sudya.uz/api/v1/wiki/position/list/",
                             headers={"Authorization": "Token e99cd1b670e0e219f8bf3aa161641525a5639cc6"})
    data, status_code = response
    position = Position()
    if status_code == 200:
        for d in data['results']:
            position.id = d.get('id')
            position.name = d.get('name')
            position.created_at = d.get('created_at')
            position.updated_at = d.get('updated_at')
            position.save()

    return response
