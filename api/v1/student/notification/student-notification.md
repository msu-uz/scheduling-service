Уведомления в кабинете обучающегося 

## `GET` `/notification/list`
### список уведомлений для конкретного студента 
>RESPONSE:    
```json
[
    {
      "id": 2,
      "receiver": {
        "id": 8,
        "first_name": "Сысоева",
        "last_name": "Варвара"
      },
      "notification": 6,
      "is_read": true,
      "title": "YOU ARE PIDOR",
      "body": "AFFIRMATIVE",
      "sender": {
        "id": 11,
        "first_name": "",
        "last_name": ""
      }
    }
    
  ]
```
## `GET` `/notification/<int:id>/`
## Detail 

>RESPONSE:      
```json
{
  "id": 13,
  "receiver": {
    "id": 8,
    "first_name": "Сысоева",
    "last_name": "Варвара"
  },
  "notification": 11,
  "is_read": true,
  "title": "this is title",
  "body": "this is body",
  "sender": {
    "id": 11,
    "first_name": "",
    "last_name": ""
  }
}
```