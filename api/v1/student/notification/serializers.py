from rest_framework import serializers
from django.contrib.auth import get_user_model

from api.models import PivotNotification, NotificationTemplate

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "first_name", "last_name")


class StudentNotificationSerializer(serializers.ModelSerializer):
    """
    Serializer for PivotNotification List
    """
    receiver = UserSerializer(read_only=True)

    class Meta:
        model = PivotNotification
        fields = ('id', 'receiver', 'notification', 'is_read')

    def to_representation(self, instance: PivotNotification):
        response = super().to_representation(instance)
        response["title"] = instance.notification.title
        response["body"] = instance.notification.body
        response["is_read"] = True
        response["sender"] = UserSerializer(instance.notification.sender).data
        return response
