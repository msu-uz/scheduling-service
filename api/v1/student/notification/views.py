from rest_framework.generics import ListAPIView, RetrieveAPIView

from api.models import PivotNotification
from api.v1.permissions import StudentPermission
from .serializers import StudentNotificationSerializer


class StudentNotificationListView(ListAPIView):
    serializer_class = StudentNotificationSerializer
    permission_classes = [StudentPermission]

    def get_queryset(self):
        qs = PivotNotification.objects.filter(receiver=self.request.user).order_by("-created_at")
        return qs


class StudentNotificationDetailView(RetrieveAPIView):
    serializer_class = StudentNotificationSerializer
    permission_classes = [StudentPermission]
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        qs = PivotNotification.objects.filter(receiver=self.request.user).order_by("-created_at")
        return qs
