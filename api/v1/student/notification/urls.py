from django.urls import path
from .views import *

urlpatterns = [
    path('list/', StudentNotificationListView.as_view()),
    path('<int:id>/', StudentNotificationDetailView.as_view()),
]
