from datetime import datetime
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.response import Response

from api.models.schedule import LessonEvent
from api.v1.permissions import StudentPermission, ApplicationPermission
from api.v1.student.schedule.serializers import LessonEventSerializer, UserAllowedSerializer


class LessonEventListView(ListAPIView):
    serializer_class = LessonEventSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        start_date = self.request.query_params.get("start_date", "")
        end_date = self.request.query_params.get("end_date", "")
        student = self.request.user.student

        queryset = LessonEvent.objects.none()

        try:
            start_date = datetime.fromisoformat(start_date).date()
            end_date = datetime.fromisoformat(end_date).date()
        except ValueError:
            pass
        else:
            queryset = LessonEvent.objects.filter(module_student_group__group__studentgroupforming__student=student,
                                                  date__gte=start_date,
                                                  date__lte=end_date).distinct().order_by(
                                                      "date",
                                                      "interval__start_time")

        return queryset


class UserAllowedView(APIView):
    """
    Используется для проверки доступа студента к мероприятию.
    """

    permission_classes = (ApplicationPermission,)

    def post(self, request, *args, **kwargs):
        serializer = UserAllowedSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data)
