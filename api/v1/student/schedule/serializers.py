from rest_framework.serializers import ModelSerializer, Serializer
from rest_framework import serializers
from django.shortcuts import get_object_or_404
from api.models.role import Teacher, Student
from api.models.resources import Resource
from api.models.schedule import LessonEvent, LessonEventInterval


class TeacherSerializer(ModelSerializer):
    class Meta:
        model = Teacher
        fields = ("id",)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response.update({"name": str(instance), "image": instance.employee.image})
        return response


class LessonEventIntervalSerializer(ModelSerializer):
    class Meta:
        model = LessonEventInterval
        fields = ("id", "number", "start_time", "end_time")


class ResourceSerializer(ModelSerializer):
    class Meta:
        model = Resource
        fields = ("id", "name")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["type"] = instance.type.name
        return response


class LessonEventSerializer(ModelSerializer):
    class Meta:
        model = LessonEvent
        fields = ("id", "date")

    def to_representation(self, instance: LessonEvent):
        response = super().to_representation(instance)

        response.update({
            "module": instance.module_student_group.module_course.discipline.name,
            "interval": LessonEventIntervalSerializer(instance.interval).data,
            "topic": instance.lesson.name,
            "room": ResourceSerializer(instance.room).data,
            "teacher": TeacherSerializer(instance.teacher).data
        })

        return response


class UserAllowedSerializer(Serializer):
    user_id = serializers.IntegerField(required=True)
    event_id = serializers.IntegerField(required=True)

    def to_representation(self, instance):
        event = get_object_or_404(LessonEvent, id=instance["event_id"])
        student = get_object_or_404(Student, user_id=instance["user_id"])

        return {
            "ok": student in event.module_student_group.get_students()
        }
