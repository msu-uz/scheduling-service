# Расписание в кабинете студента

## `GET student/schedule/?start_date=2020-01-01&end_date=2020-01-31`

Список мероприятий для студента.

### Query params:

| Название   | Описание       |
| ---------- | -------------- |
| start_date | Начальная дата |
| end_date   | Конечная дата  |

### Response:

```json
{
  "results": [
    {
      "id": 2,
      "date": "2021-01-05",
      "module": {
        "ru": "Мясник",
        "uz": "Therapeutic radiographer"
      },
      "interval": {
        "id": 2,
        "number": 2,
        "start_time": "12:00:00",
        "end_time": "18:00:00"
      },
      "topic": {
        "ru": "Шлифовщик",
        "uz": "Marketing executive"
      },
      "room": {
        "id": 1,
        "name": "123",
        "type": "Аудитория"
      },
      "teacher": {
        "id": 1,
        "name": "amir",
        "image": "http://pic.jpg"
      }
    }
  ]
}
```
