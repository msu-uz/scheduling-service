from api.v1.student.schedule import views
from django.urls import path

urlpatterns = [
    path("", views.LessonEventListView.as_view()),
    path("user_allowed/", views.UserAllowedView.as_view()),
]
