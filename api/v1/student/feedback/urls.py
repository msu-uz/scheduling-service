from api.v1.student.feedback.views import (
    CourseFeedbackCommentCreateView,
    CourseFeedbackCommentListView,
    CourseFeedbackCreateView,
    CourseFeedbackQuestionList,
    CourseListView,
    ModuleCourseFeedbackCreateView,
    ModuleCourseFeedbackQuestionListView,
    ModuleCourseListView,
)
from django.urls import path

urlpatterns = [
    path("course/list/", CourseListView.as_view()),
    path("course/<int:course_id>/module/list/", ModuleCourseListView.as_view()),
    path("list/course/<int:course_id>/", CourseFeedbackQuestionList.as_view()),
    path("list/module/<int:module_id>/", ModuleCourseFeedbackQuestionListView.as_view()),
    path("create/course/", CourseFeedbackCreateView.as_view()),
    path("create/module/", ModuleCourseFeedbackCreateView.as_view()),
    path("comment/create/", CourseFeedbackCommentCreateView.as_view()),
    path("course/<int:course_id>/comment/list/", CourseFeedbackCommentListView.as_view()),
]
