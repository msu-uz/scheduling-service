from api.models.discipline import Course, ModuleCourse
from api.models.feedback import (
    CourseFeedbackComment,
    CourseFeedbackQuestion,
    ModuleCourseFeedbackQuestion,
)
from api.v1.permissions import StudentPermission
from api.v1.student.feedback.serializers import (
    CourseFeedbackCommentShortSerializer,
    CourseFeedbackCreateSerializer,
    CourseFeedbackQuestionSerializer,
    CourseFeedbackSerializer,
    CourseSerializer,
    ModuleCourseFeedbackCreateSerializer,
    ModuleCourseFeedbackQuestionSerializer,
    ModuleCourseFeedbackSerializer,
    ModuleCourseSerializer,
)
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.response import Response


class CourseListView(ListAPIView):
    """
    Список курсов для студента.
    """

    serializer_class = CourseSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        student = self.request.user.student

        return Course.objects.filter(
            modulecourse__modulestudentgroup__group__studentgroupforming__student=student
        ).distinct()


class ModuleCourseListView(ListAPIView):
    """
    Список модулей для курса.

    URL params: course_id
    """

    serializer_class = ModuleCourseSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        course_id = self.kwargs["course_id"]
        return ModuleCourse.objects.filter(course_id=course_id).select_related("discipline")


# CourseFeedback


class CourseFeedbackCreateView(CreateAPIView):
    """
    Создание фидбека по курсу.
    """

    serializer_class = CourseFeedbackCreateSerializer
    permission_classes = (StudentPermission,)

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instances = self.perform_create(serializer)
        instance_serializer = CourseFeedbackSerializer(instances, many=True, context={"request": request})
        return Response(instance_serializer.data, status=status.HTTP_201_CREATED)


class CourseFeedbackQuestionList(ListAPIView):
    """
    Список вопросов по курсу.

    URL params: course_id
    """

    serializer_class = CourseFeedbackQuestionSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        course_id = self.kwargs["course_id"]
        return CourseFeedbackQuestion.objects.filter(course_id=course_id)


# ModuleCourseFeedback


class ModuleCourseFeedbackCreateView(CreateAPIView):
    """
    Создание фидбека по модулю.
    """

    serializer_class = ModuleCourseFeedbackCreateSerializer
    permission_classes = (StudentPermission,)

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instances = self.perform_create(serializer)
        instance_serializer = ModuleCourseFeedbackSerializer(instances, many=True, context={"request": request})
        return Response(instance_serializer.data, status=status.HTTP_201_CREATED)


class ModuleCourseFeedbackQuestionListView(ListAPIView):
    """
    Список вопросов обратной связи по модулю.

    URL params: module_id
    """

    serializer_class = ModuleCourseFeedbackQuestionSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        module_id = self.kwargs["module_id"]
        return ModuleCourseFeedbackQuestion.objects.filter(
            module_course_id=module_id
        ).prefetch_related("modulecoursefeedbackanswer_set")


# CourseFeedbackComment


class CourseFeedbackCommentCreateView(CreateAPIView):
    """
    Создание комментария по курсу.
    """

    permission_classes = (StudentPermission,)
    serializer_class = CourseFeedbackCommentShortSerializer


class CourseFeedbackCommentListView(ListAPIView):
    """
    Список комментариев по курсу.

    URL params: course_id
    """

    permission_classes = (StudentPermission,)
    serializer_class = CourseFeedbackCommentShortSerializer

    def get_queryset(self):
        student = self.request.user.student
        course_id = self.kwargs["course_id"]
        return CourseFeedbackComment.objects.filter(course_id=course_id, student=student)
