from api.models.discipline import Course, ModuleCourse
from api.models.feedback import (
    CourseFeedback,
    CourseFeedbackAnswer,
    CourseFeedbackComment,
    CourseFeedbackQuestion,
    ModuleCourseFeedback,
    ModuleCourseFeedbackAnswer,
    ModuleCourseFeedbackQuestion,
)
from rest_framework import serializers
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.serializers import ModelSerializer, Serializer


class CourseSerializer(ModelSerializer):
    class Meta:
        model = Course
        fields = ("id", "name")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        student = self.context["request"].user.student
        response.update({
            "passed": CourseFeedback.objects.filter(course_feedback_answer__course_feedback_question__course=instance, student=student).exists()
        })
        return response


class ModuleCourseSerializer(ModelSerializer):
    class Meta:
        model = ModuleCourse
        fields = ("id",)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        student = self.context["request"].user.student

        response.update({
            "name": instance.discipline.name,
            "passed": ModuleCourseFeedback.objects.filter(module_course_feedback_answer__module_course_feedback_question__module_course=instance, student=student).exists()
        })

        response["name"] = instance.discipline.name
        return response


# CourseFeedback


class CourseFeedbackAnswerSerializer(ModelSerializer):
    class Meta:
        model = CourseFeedbackAnswer
        fields = ("id", "text")
        read_only_fields = ("text",)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response.update({"selected": False})
        return response


class CourseFeedbackQuestionSerializer(ModelSerializer):
    answers = CourseFeedbackAnswerSerializer(many=True, source="coursefeedbackanswer_set")

    class Meta:
        model = CourseFeedbackQuestion
        fields = ("id", "text", "answers")

    def to_representation(self, instance):
        response = super().to_representation(instance)

        student = self.context["request"].user.student

        if answer := CourseFeedbackAnswer.objects.filter(course_feedback_question=instance, coursefeedback__student=student).first():
            response.update({
                "answer": CourseFeedbackAnswerSerializer(answer).data
            })

        return response


class CourseFeedbackSerializer(ModelSerializer):
    question = CourseFeedbackQuestionSerializer(source="course_feedback_answer.course_feedback_question")
    answer = CourseFeedbackAnswerSerializer(source="course_feedback_answer")

    class Meta:
        model = CourseFeedback
        fields = ("id", "question", "answer")
        read_only_fields = ("id", "question")


class CourseFeedbackCreateSerializer(Serializer):
    answers = serializers.ListField(child=serializers.IntegerField(),
                                    allow_empty=False,
                                    write_only=True)

    def validate_answers(self, answer_ids):
        answer = CourseFeedbackAnswer.objects.filter(id=answer_ids[0]).first()
        if not answer:
            raise NotFound("Answer not found")

        course = answer.course_feedback_question.course
        answers_count = CourseFeedbackAnswer.objects.filter(id__in=answer_ids).count()
        if len(answer_ids) != answers_count:
            raise NotFound("Answer not found")

        questions_for_answers_count = (CourseFeedbackQuestion.objects.filter(
            coursefeedbackanswer__id__in=answer_ids).distinct().count())

        if answers_count != questions_for_answers_count:
            raise ValidationError("Too many answers for one question")

        course_questions_count = CourseFeedbackQuestion.objects.filter(course=course).count()
        if answers_count != course_questions_count:
            raise ValidationError("Answers for different courses or not enough answers")

        return answer_ids

    def create(self, validated_data):
        answer_ids = validated_data.pop("answers")
        student = self.context["request"].user.student

        if CourseFeedback.objects.filter(student=student, course_feedback_answer_id__in=answer_ids).exists():
            raise ValidationError("Feedback already exists")

        answers = []
        for answer_id in answer_ids:
            answers.append(CourseFeedback(student=student, course_feedback_answer_id=answer_id))

        return CourseFeedback.objects.bulk_create(answers)


# ModuleCourseFeedback


class ModuleCourseFeedbackAnswerSerializer(ModelSerializer):
    class Meta:
        model = ModuleCourseFeedbackAnswer
        fields = ("id", "text")
        read_only_fields = ("id",)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response.update({"selected": False})
        return response


class ModuleCourseFeedbackQuestionSerializer(ModelSerializer):
    """
    Используется в списке вопросов обратной связи по модулю.
    """

    answers = ModuleCourseFeedbackAnswerSerializer(many=True,
                                                   source="modulecoursefeedbackanswer_set")

    class Meta:
        model = ModuleCourseFeedbackQuestion
        fields = ("id", "text", "answers")

    def to_representation(self, instance):
        response = super().to_representation(instance)

        student = self.context["request"].user.student

        if answer := ModuleCourseFeedbackAnswer.objects.filter(module_course_feedback_question=instance, modulecoursefeedback__student=student).first():
            response.update({
                "answer": ModuleCourseFeedbackAnswerSerializer(answer).data
            })

        return response


class ModuleCourseFeedbackSerializer(ModelSerializer):
    question = ModuleCourseFeedbackQuestionSerializer(
        source="module_course_feedback_answer.module_course_feedback_question"
    )
    answer = ModuleCourseFeedbackAnswerSerializer(source="module_course_feedback_answer")

    class Meta:
        model = ModuleCourseFeedback
        fields = ("id", "question", "answer")
        read_only_fields = ("id", "question")


class ModuleCourseFeedbackCreateSerializer(Serializer):
    answers = serializers.ListField(child=serializers.IntegerField(),
                                    allow_empty=False,
                                    write_only=True)

    def validate_answers(self, answer_ids):
        answer = ModuleCourseFeedbackAnswer.objects.filter(id=answer_ids[0]).first()
        if not answer:
            raise NotFound("Answer not found")

        module_course = answer.module_course_feedback_question.module_course

        answers_count = ModuleCourseFeedbackAnswer.objects.filter(id__in=answer_ids).count()
        if len(answer_ids) != answers_count:
            raise NotFound("Answer not found")

        questions_for_answers_count = (
            ModuleCourseFeedbackQuestion.objects.filter(
                modulecoursefeedbackanswer__id__in=answer_ids
            ).distinct().count())

        if answers_count != questions_for_answers_count:
            raise ValidationError("Too many answers for one question")

        module_questions_count = ModuleCourseFeedbackQuestion.objects.filter(
            module_course=module_course).count()

        if answers_count != module_questions_count:
            raise ValidationError("Answers for different modules or not enough answers")

        return answer_ids

    def create(self, validated_data):
        answer_ids = validated_data.pop("answers")
        student = self.context["request"].user.student

        if ModuleCourseFeedback.objects.filter(student=student, module_course_feedback_answer_id__in=answer_ids).exists():
            raise ValidationError("Feedback already exists")

        answers = []
        for answer_id in answer_ids:
            answers.append(ModuleCourseFeedback(student=student,
                                                module_course_feedback_answer_id=answer_id))

        return ModuleCourseFeedback.objects.bulk_create(answers)


# CourseFeedbackComment


class CourseFeedbackCommentShortSerializer(ModelSerializer):
    class Meta:
        model = CourseFeedbackComment
        fields = ("id", "text", "course")
        read_only_fields = ("id",)

    def create(self, validated_data):
        validated_data["student"] = self.context["request"].user.student
        return super().create(validated_data)
