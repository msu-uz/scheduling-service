from django.urls import path
from .views import *

urlpatterns = [
    path("list/", CourseResultList.as_view()),
    path("summary/<int:id>/list/", CourseResultSummaryList.as_view()),
    path("modules/<int:id>/list/", ModulesSummaryList.as_view())
]
