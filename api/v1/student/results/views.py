from rest_framework.generics import ListAPIView

from api.models import Course, ModuleStudentGroup, Student
from api.v1.permissions import StudentPermission
from api.v1.student.results.serializers import CourseResultsListSerializer, CourseResultSummaryListSerializer, \
    ModulesSummaryListSerializer


class CourseResultList(ListAPIView):
    serializer_class = CourseResultsListSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        student = self.request.user.student
        qs = Course.objects.filter(studentgroup=student.student_groups.first())
        return qs


class CourseResultSummaryList(ListAPIView):
    serializer_class = CourseResultSummaryListSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        course_id = self.kwargs.get('id')
        student = self.request.user.student
        qs = Course.objects.filter(studentgroup=student.student_groups.last(), id=course_id)
        return qs


class ModulesSummaryList(ListAPIView):
    serializer_class = ModulesSummaryListSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        course_id = self.kwargs.get('id')
        student = self.request.user.student
        qs = ModuleStudentGroup.objects.filter(group=student.student_groups.last(), module_course__course=course_id)
        return qs
