# Результаты обучения студента

## First page course list
### `GET` /student/results/list/ 
```json
 [
    {
      "id": 2,
      "name": {
        "ru": "Учитель",
        "uz": "Visual merchandiser"
      },
      "progress": 100,
      "start_date": "2021-01-14",
      "end_date": "2021-01-28"
    }
  ]
```

## Second page header part 
### `GET` `student/results/summary/<int:id>/list/`

```json
 [
    {
      "id": 2,
      "name": {
        "ru": "Учитель",
        "uz": "Visual merchandiser"
      },
      "modules_count": 7,
      "lesson_count": 27,
      "total_training_time": 1246,
      "course_score": "2.14/100"
    }
  ]
```

## Second page body part
### `GET` `student/results/modules/<int:id>/list/`

```json
[
    {
      "id": 2,
      "name": {
        "ru": "Бактериолог",
        "uz": "Barista"
      },
      "current_score": [],
      "intermediate_control": [
        {
          "id": 1,
          "score": "100/100",
          "date": "2021-02-01",
          "score_qa": {
            "id": 4,
            "name": {
              "en": "Excellent",
              "ru": "Отлично",
              "uz": ""
            }
          }
        },
        {
          "id": 2,
          "score": "60/100",
          "date": "2021-02-01",
          "score_qa": {
            "id": 2,
            "name": {
              "en": "Satisfactory",
              "ru": "Удовлетворительно",
              "uz": ""
            }
          }
        }
      ],
      "final_control": null
    }
]
```