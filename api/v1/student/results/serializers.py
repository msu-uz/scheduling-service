from django.db.models import Sum
from rest_framework import serializers

from api.models.statement import QualitativeAssessment
from .additional import count_progress_percentage, modules_score_sum
from api.models import Course, ModuleWeekType, ModuleCourse, ModuleStudentGroup, Student, Discipline, Journal
from datetime import datetime


class QASerializer(serializers.ModelSerializer):
    """
    Для словесного определения балла (хорошо, отлично и т.д)
    """

    class Meta:
        model = QualitativeAssessment
        fields = [
            'id',
            'name'
        ]


class CourseResultsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ("id", "name")

    def to_representation(self, instance: Course):
        response = super(CourseResultsListSerializer, self).to_representation(instance)
        progress, first_lesson_date, last_lesson_date = count_progress_percentage(instance,
                                                                                  self.context["request"].user.student)
        response["progress"] = progress
        if first_lesson_date or last_lesson_date:
            response.update({
                "start_date": " ",
                "end_date": " "
            })
        response["start_date"] = first_lesson_date
        if progress >= 100:
            response["end_date"] = last_lesson_date
        return response


class CourseResultSummaryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('id', 'name')

    def to_representation(self, instance: Course):
        response = super(CourseResultSummaryListSerializer, self).to_representation(instance)
        response["modules_count"] = instance.modulecourse_set.all().count()
        lesson_count = ModuleWeekType.objects.filter(lesson__isnull=False,
                                                     week_day__module_course__course=instance).distinct().count()
        response['lesson_count'] = lesson_count
        modules = ModuleCourse.objects.filter(course=instance)
        if modules:
            response['total_training_time'] = (modules.aggregate(Sum('lecture_duration'))['lecture_duration__sum'] +
                                               modules.aggregate(Sum('practice_duration'))['practice_duration__sum']
                                               + modules.aggregate(Sum('distantly_duration'))['distantly_duration__sum'])

            course_score = modules_score_sum(modules, self.context["request"].user.student)
            score = course_score/instance.modulecourse_set.all().count()
        else:
            score = 0
        response["course_score"] = round(score, 2)

        return response


class ModulesSummaryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModuleStudentGroup
        fields = ('id',)

    def to_representation(self, instance: ModuleStudentGroup):
        date = datetime.today().date()
        student = self.context["request"].user.student
        response = super(ModulesSummaryListSerializer, self).to_representation(instance)
        response['name'] = instance.module_course.discipline.name
        journal = Journal.objects.filter(module_student_group=instance)
        current_score = Journal.get_all_lessons_scores(journal, student)
        response['current_score'] = JournalSerializer(current_score, many=True).data
        int_score = Journal.get_all_int_scores(journal, student)
        response['intermediate_control'] = JournalSerializer(int_score, many=True).data
        response['final_control'] = Journal.get_final_score(self=journal,
                                                            date=date,
                                                            student=student,
                                                            msg=instance.id)
        if response['final_control']:
            scores = QualitativeAssessment.objects.get(min_score__lte=response['final_control'],
                                                       max_score__gte=response['final_control'])
            response['final_control_qa'] = QASerializer(scores).data
        return response


class JournalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Journal
        fields = ('id', 'score')

    def to_representation(self, instance: Journal):
        response = super(JournalSerializer, self).to_representation(instance)
        response['score'] = round(instance.score, 2)
        response['date'] = instance.event.date
        if response['score']:
            scores = QualitativeAssessment.objects.get(min_score__lte=instance.score,
                                                       max_score__gte=instance.score)
            response['score_qa'] = QASerializer(scores).data
        return response
