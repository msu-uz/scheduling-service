from django.db.models import QuerySet
from datetime import date

from api.models import Course, Student, LessonEvent, ModuleCourse, ModuleStudentGroup


def count_progress_percentage(course: Course, student: Student) -> tuple:
    past_events_count = 0
    events = LessonEvent.objects.filter(module_student_group__module_course__course=course,
                                        module_student_group__group__studentgroupforming__student=student).distinct()
    events_counts = events.count()
    if events_counts == 0:
        events_counts = 1
    for event in events:
        if date.today() > event.date:
            past_events_count += 1

    if events:
        ordered_events = events.order_by("date")
        first_events_date = ordered_events.first().date
        last_events_date = ordered_events.last().date

    else:
        first_events_date = 0
        last_events_date = 0
    return int(past_events_count * 100 / events_counts), first_events_date, last_events_date


def modules_score_sum(modules, student):
    result_score = 0
    for module in modules:
        group = student.student_groups.first()
        if group:
            msg = ModuleStudentGroup.objects.filter(module_course=module, group=student.student_groups.first())
            if msg:
                for msg_exact in msg:
                    result_score += student.module_scores(msg_exact)

    if modules:
        return result_score
    else:
        return 0
