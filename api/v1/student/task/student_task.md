# Выполнение заданий студентом (тесты, казусы)

## `GET /student/task/module/list/`

Список модулей.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Ихтиолог",
        "uz": "Engineer, manufacturing"
      }
    },
    {
      "id": 5,
      "name": {
        "ru": "Оператор РЛС",
        "uz": "Systems analyst"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Ихтиолог",
        "uz": "Engineer, manufacturing"
      }
    }
  ]
}
```

## `GET student/task/collection/list/?module=1`

Список заданий.

### Query params:

| Name   | Description |
| ------ | ----------- |
| module | id модуля   |

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "start_date": "2021-01-01",
      "end_date": "2021-01-31",
      "event": {
        "id": 1,
        "type": "Урок",
        "title": {
          "ru": "Оперативный работник",
          "uz": "Immigration officer"
        }
      },
      "module": {
        "ru": "Ихтиолог",
        "uz": "Engineer, manufacturing"
      }
    }
  ]
}
```

## `GET student/task/collection/<int:id>/`

Детальный просмотр задания, где студент может начать решать задание. Важно, что с момента открытия этой страницы начинается отсчёт времени, расчитанного на решение задания.

### Response:

Ниже есть пояснение к response 👇

```json
{
  "id": 1,
  "start_date": "2021-01-01",
  "end_date": "2021-01-31",
  "begin_datetime": "2021-01-26T07:46:14.631595Z",
  "total_duration": 3600, // время, отведённое на решение задания
  "task_answer_collection": 3,
  "topic": {
    "ru": "Оперативный работник",
    "uz": "Immigration officer"
  },
  "module": {
    "ru": "Ихтиолог",
    "uz": "Engineer, manufacturing"
  },
  "tasks": [
    {
      "id": 1,
      "is_test": true,
      "question": {
        "ru": "Вопрос 1",
        "uz": "Vopros 1"
      },
      "answers": [
        {
          "id": 1,
          "answer": {
            "ru": "Вариант 1.1",
            "uz": "Variant 1.1"
          }
        },
        {
          "id": 2,
          "answer": {
            "ru": "Вариант 1.2",
            "uz": "Variant 1.2"
          }
        },
        {
          "id": 3,
          "answer": {
            "ru": "Вариант 1.3",
            "uz": "Variant 1.3"
          }
        }
      ],
      "answer": {
        "id": 1,
        "chosen_variant": {
          "id": 3,
          "answer": {
            "ru": "Вариант 1.3",
            "uz": "Variant 1.3"
          }
        }
      }
    },
    {
      "id": 2,
      "is_test": false,
      "question": {
        "ru": "Вопрос 2",
        "uz": "Vopros 2"
      },
      "answer": {
        "id": 3,
        "plain_answer": "Мой ответ на вопрос"
      }
    }
  ]
}
```

### Пояснение к response:

| Name                   | Description                                                                       |
| ---------------------- | --------------------------------------------------------------------------------- |
| task_answer_collection | id набора ответов, используется при создании ответа ⭐️                           |
| tasks -> answer        | Ответ на данный вопрос, если студент уже ранее ответил на этот вопрос. Иначе `{}` |
| tasks -> is_test       | Показывает, является ли это тестом                                                |
| answers                | Массив вариантов. Отсутствует, если это не тест                                   |
| begin_datetime         | Время, с которого началось решение данного задания                                |
| tasks                  | Список тестов или вопросов в данном задании                                       |
| module                 | Название предмета                                                                 |
| topic                  | Название темы                                                                     |

## `GET student/task/answer/create/`

Создание ответа на вопрос.

### Request:

```json
{
  "task_answer_collection": 3, // берётся из детального тела задания 👆
  "task": 2, // номер теста или вопроса
  "plain_answer": "Мой ответ на вопрос" // текст ответа
}
```

### Response:

```json
{
  "id": 5,
  "task_answer_collection": 3,
  "task": 2,
  "chosen_variant": null,
  "plain_answer": "Мой ответ на вопрос"
}
```

Создание ответа на тест.

### Request:

```json
{
  "task_answer_collection": 3,
  "task": 1,
  "chosen_variant": 1
}
```

### Response:

```json
{
  "id": 4,
  "task_answer_collection": 3,
  "task": 1,
  "chosen_variant": {
    "id": 1,
    "answer": {
      "ru": "Вариант 1.1",
      "uz": "Variant 1.1"
    }
  },
  "plain_answer": null
}
```

## `PATCH student/task/answer/<int:id>/`

Обновление ответа на вопрос.

### Request:

```json
{
  "plain_answer": "Мой ответ на вопрос" // текст ответа
}
```

### Response:

```json
{
  "id": 1,
  "chosen_variant": null,
  "plain_answer": "Ответ 1.1 updated"
}
```

Обновление ответа на тест.

### Request:

```json
{
  "chosen_variant": 1 // выбранный вариант
}
```

### Response:

```json
{
  "id": 4,
  "chosen_variant": {
    "id": 1,
    "answer": {
      "ru": "Вариант 1.1",
      "uz": "Variant 1.1"
    }
  },
  "plain_answer": null
}
```

# Просмотр результатов студентом

Пока не готово.
