from api.v1.student.task.views import (TaskAnswerCreateView, TaskAnswerUpdateView, TaskCollectionReceiverDetailView,
                                       TaskCollectionReceiverListView, ModuleCourseListView)
from django.urls import include, path

urlpatterns = [
    path("collection/list/", TaskCollectionReceiverListView.as_view()),
    path("collection/<int:id>/", TaskCollectionReceiverDetailView.as_view()),
    path("answer/create/", TaskAnswerCreateView.as_view()),
    path("answer/<int:id>/", TaskAnswerUpdateView.as_view()),
    path("module/list/", ModuleCourseListView.as_view()),
    path("hometask/", include("api.v1.student.task.hometask.urls")),
    path("results/", include("api.v1.student.task.results.urls")),
]
