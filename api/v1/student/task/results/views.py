from api.models.discipline import ModuleCourse
from django.db.models.query_utils import Q
from django.utils.datetime_safe import datetime

from rest_framework.generics import ListAPIView, RetrieveAPIView

from api.v1.permissions import StudentPermission
from api.models.task import TaskCollectionReceiver
from api.v1.student.task.results.serializers import (ModuleCourseSerializer, TaskCollectionReceiverSerializer,
                                                     TaskCollectionReceiverShortSerializer)


class ModuleCourseListView(ListAPIView):
    """
    Список модулей для студента.
    """

    serializer_class = ModuleCourseSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        student = self.request.user.student
        return ModuleCourse.objects.filter(modulestudentgroup__group__studentgroupforming__student=student).distinct()


class TaskCollectionReceiverListView(ListAPIView):
    """
    Список наборов заданий, по которым провален дедлайн или для которых истёк срок начала выполнения.

    Query params: module - id ModuleCourse
                  is_done - "true" или "false" или ""
                  is_reviewed - "true" или "false" или ""
    """

    serializer_class = TaskCollectionReceiverShortSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        module = self.request.query_params.get("module", None)
        is_done = self.request.query_params.get("is_done", "").lower()
        is_reviewed = self.request.query_params.get("is_reviewed", "").lower()

        student = self.request.user.student
        today = datetime.today()
        now = today.now()

        query = Q(student=student) & (Q(end_date__lt=today) | Q(taskanswercollection__deadline__lt=now))

        if module:
            try:
                module = int(module)
            except ValueError:
                pass
            else:
                query &= Q(task_collection__module_week_type__week_day__module_course_id=module)

        if is_done == "true":
            query &= Q(taskanswercollection__isnull=False)

            if is_reviewed == "true":
                query &= Q(taskanswercollection__taskanswerreviewcollection__isnull=False)
            elif is_reviewed == "false":
                query &= Q(taskanswercollection__taskanswerreviewcollection__isnull=True)

        elif is_done == "false":
            query &= Q(taskanswercollection__isnull=True)

        return TaskCollectionReceiver.objects.filter(query).distinct()


class TaskCollectionReceiverDetailView(RetrieveAPIView):
    """
    Детальный просмотр набора заданий.
    """

    serializer_class = TaskCollectionReceiverSerializer
    permission_classes = (StudentPermission,)
    lookup_url_kwarg = "id"

    def get_queryset(self):
        student = self.request.user.student
        today = datetime.today()
        now = today.now()

        query = Q(student=student) & (Q(end_date__lt=today) | Q(taskanswercollection__deadline__lt=now))

        return TaskCollectionReceiver.objects.filter(query)
