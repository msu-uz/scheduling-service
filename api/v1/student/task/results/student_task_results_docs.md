# Просмотр результатов решения заданий

## `GET /student/task/results/module/list/`

Список модулей.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Ихтиолог",
        "uz": "Engineer, manufacturing"
      }
    },
    {
      "id": 5,
      "name": {
        "ru": "Оператор РЛС",
        "uz": "Systems analyst"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Ихтиолог",
        "uz": "Engineer, manufacturing"
      }
    }
  ]
}
```

## `GET student/task/results/collection/list/?module=&is_done=&is_reviewed=`

Список наборов заданий, для которых истёк дедлайн.

### Query params:

| Название    | Описание                                   |
| ----------- | ------------------------------------------ |
| module      | id модуля                                  |
| is_done     | Выполненные (true), не выполненные (false) |
| is_reviewed | Проверенные (true), не проверенные (false) |

### Response:

```json
{
  "results": [
    {
      "id": 2,
      "start_date": "2020-12-01",
      "end_date": "2021-01-13",
      "event": {
        "id": 2,
        "type": "Урок",
        "title": {
          "ru": "Шлифовщик",
          "uz": "Marketing executive"
        }
      },
      "module": {
        "ru": "Квасник",
        "uz": "Pharmacist, community"
      },
      "review": {
        "total_mark": 5,
        "reviewer": 1,
        "created_at": "2021-01-27T14:26:47.574478+05:00"
      },
      "done_at": "2021-01-28T09:33:31Z"
    }
  ]
}
```

### Пояснение к response:

| Название              | Описание                                                 |
| --------------------- | -------------------------------------------------------- |
| done_at               | Время, когда задание было выполнено (может быть `null`)  |
| review                | Результат проверки. Может быть `{}`                      |
| module                | Предмет                                                  |
| event                 | Урок                                                     |
| start_date - end_date | Промежуток времени, когда можно начать выполнять задание |

## `GET student/task/results/collection/<int:id>/`

Детальный просмотр результата по набору заданий.

### URL params:

| Название | Описание          |
| -------- | ----------------- |
| id       | id набора заданий |

### Response:

```json
{
  "id": 2,
  "start_date": "2020-12-01",
  "end_date": "2021-01-13",
  "review": {
    "total_mark": 5,
    "reviewer": 1,
    "created_at": "2021-01-27T14:26:47.574478+05:00"
  },
  "done_at": "2021-01-28T09:33:31Z",
  "topic": {
    "ru": "Веб-программист",
    "uz": "Lecturer, higher education"
  },
  "module": {
    "ru": "Квасник",
    "uz": "Pharmacist, community"
  },
  "tasks": [
    {
      "id": 3,
      "question": {
        "ru": "Вопрос 2",
        "uz": "Vopros 2"
      },
      "is_test": false,
      "answer": {
        "id": 2,
        "plain_answer": "Развёрнутый ответ на вопрос (оставил студент)",
        "review": {
          "is_correct": false,
          "comment": "Замечания по поводу ответа на вопрос"
        }
      }
    },
    {
      "id": 4,
      "question": {
        "ru": "Тест 2",
        "uz": "Test 2"
      },
      "variants": [
        {
          "id": 6,
          "answer": {
            "ru": "Вариант 4.1",
            "uz": "Variant 4.1"
          }
        },
        {
          "id": 7,
          "answer": {
            "ru": "Вариант 4.2",
            "uz": "Variant 4.2"
          }
        },
        {
          "id": 8,
          "answer": {
            "ru": "Вариант 4.3",
            "uz": "Variant 4.3"
          }
        }
      ],
      "is_test": true,
      "answer": {
        "id": 1,
        "chosen_variant": {
          "id": 4,
          "answer": {
            "ru": "Вариант 2.3",
            "uz": "Variant 2.3"
          }
        },
        "review": {
          "is_correct": true,
          "comment": "Окей (комментарий преподавателя к конкретному заданию в наборе)"
        }
      }
    }
  ]
}
```

### Пояснение к response:

| Название                       | Описание                                                 |
| ------------------------------ | -------------------------------------------------------- |
| start_date - end_date          | Промежуток времени, когда можно начать выполнять задание |
| done_at                        | Время, когда задание было выполнено (может быть `null`)  |
| review                         | Результат проверки. Может быть `{}`                      |
| module                         | Предмет                                                  |
| event                          | Урок                                                     |
| topic                          | Тема                                                     |
| tasks                          | Список заданий в наборе                                  |
| tasks -> is_test               | Является ли тестом                                       |
| tasks -> variants              | Варианты ответа на тест                                  |
| tasks -> answer.chosen_variant | Выбранный вариант (в случае теста)                       |
| tasks -> answer.plain_answer   | Текстовый ответ на вопрос (если это не тест)             |
| tasks -> review                | Результат для конкретного задания                        |
