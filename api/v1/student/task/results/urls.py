from django.urls import path

from api.v1.student.task.results.views import (ModuleCourseListView, TaskCollectionReceiverDetailView,
                                               TaskCollectionReceiverListView)

urlpatterns = [
    path("collection/list/", TaskCollectionReceiverListView.as_view()),
    path("collection/<int:id>/", TaskCollectionReceiverDetailView.as_view()),
    path("module/list/", ModuleCourseListView.as_view()),
]
