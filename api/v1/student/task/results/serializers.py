from api.models.discipline import ModuleCourse
from api.models.schedule import LessonEvent
from rest_framework.serializers import ModelSerializer

from api.models.task import (Answer, Task, TaskAnswer, TaskAnswerReview, TaskAnswerReviewCollection,
                             TaskCollectionReceiver)


class AnswerSerializer(ModelSerializer):
    class Meta:
        model = Answer
        fields = ("id", "answer")


class TaskAnswerReviewSerializer(ModelSerializer):
    class Meta:
        model = TaskAnswerReview
        fields = ("is_correct", "comment")


class TaskAnswerReviewCollectionSerializer(ModelSerializer):
    class Meta:
        model = TaskAnswerReviewCollection
        fields = ("total_mark", "reviewer", "created_at")


class ModuleCourseSerializer(ModelSerializer):
    class Meta:
        model = ModuleCourse
        fields = ("id",)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["name"] = instance.discipline.name
        return response


class LessonEventSerializer(ModelSerializer):
    class Meta:
        model = LessonEvent
        fields = ("id",)

    def to_representation(self, instance: LessonEvent):
        response = super().to_representation(instance)

        if instance.lesson.lesson:
            response["type"] = "Урок"
        elif instance.lesson.int_control:
            response["type"] = "Промежуточный контроль"
        elif instance.lesson.fin_control:
            response["type"] = "Итоговый контроль"

        response["title"] = instance.lesson.name
        return response


class TaskCollectionReceiverShortSerializer(ModelSerializer):
    class Meta:
        model = TaskCollectionReceiver
        fields = ("id", "start_date", "end_date")

    def to_representation(self, instance: TaskCollectionReceiver):
        response = super().to_representation(instance)

        review = None
        done_at = None

        if hasattr(instance, "taskanswercollection"):
            if hasattr(instance.taskanswercollection, "taskanswerreviewcollection"):
                review = instance.taskanswercollection.taskanswerreviewcollection
            done_at = instance.taskanswercollection.deadline

        response.update({
            "event": LessonEventSerializer(instance.event).data,
            "module": instance.task_collection.module_week_type.week_day.module_course.discipline.name,
            "review": TaskAnswerReviewCollectionSerializer(review).data,
            "done_at": done_at,
        })

        return response


class TaskAnswerSerializer(ModelSerializer):
    class Meta:
        model = TaskAnswer
        fields = ("id",)

    def to_representation(self, instance: TaskAnswer):
        response = super().to_representation(instance)

        if instance.task.answer_set.count() > 1:  # is test?
            response["chosen_variant"] = AnswerSerializer(instance.chosen_variant).data
        else:
            response["plain_answer"] = instance.plain_answer

        if hasattr(instance, "taskanswerreview"):
            response["review"] = TaskAnswerReviewSerializer(instance.taskanswerreview).data

        return response


class TaskSerializer(ModelSerializer):
    class Meta:
        model = Task
        fields = ("id", "question")

    def to_representation(self, instance: Task):
        response = super().to_representation(instance)
        collection = self.context.get("task_answer_collection")

        if is_test := instance.answer_set.count() > 1:  # is test?
            response["variants"] = AnswerSerializer(instance.answer_set.all(), many=True).data

        response["is_test"] = is_test

        if collection:
            response["answer"] = TaskAnswerSerializer(
                instance.taskanswer_set.filter(task_answer_collection=collection).first()).data

        return response


class TaskCollectionReceiverSerializer(ModelSerializer):
    class Meta:
        model = TaskCollectionReceiver
        fields = ("id", "start_date", "end_date")

    def to_representation(self, instance: TaskCollectionReceiver):
        response = super().to_representation(instance)

        collection = None
        done_at = None

        if hasattr(instance, "taskanswercollection"):
            collection = instance.taskanswercollection
            done_at = collection.deadline

            if hasattr(collection, "taskanswerreviewcollection"):
                response["review"] = TaskAnswerReviewCollectionSerializer(collection.taskanswerreviewcollection).data

        response.update({
            "done_at": done_at,
            "topic": instance.task_collection.module_week_type.name,
            "module": instance.task_collection.module_week_type.week_day.module_course.discipline.name,
            "tasks": TaskSerializer(instance.task_collection.tasks,
                                    many=True,
                                    context={
                                        "task_answer_collection": collection
                                    }).data,
        })

        return response
