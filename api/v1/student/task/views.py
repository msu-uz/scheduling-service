from api.models.task import TaskAnswer
from api.models.discipline import ModuleCourse
from api.v1.permissions import StudentPermission
from api.v1.student.task.serializers import (ModuleCourseSerializer, TaskAnswerCreateSerializer,
                                             TaskAnswerUpdateSerializer, TaskCollectionReceiver,
                                             TaskCollectionReceiverSerializer, TaskCollectionReceiverShortSerializer)
from django.db.models.query_utils import Q
from django.utils.datetime_safe import datetime
from rest_framework.generics import (CreateAPIView, ListAPIView, RetrieveAPIView, UpdateAPIView)


class ModuleCourseListView(ListAPIView):
    """
    Список модулей для студента.
    """

    serializer_class = ModuleCourseSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        student = self.request.user.student
        return ModuleCourse.objects.filter(modulestudentgroup__group__studentgroupforming__student=student).distinct()


class TaskCollectionReceiverListView(ListAPIView):
    """
    Список наборов заданий, которые ещё можно начать решать и по которым не провален дедлайн.

    Query params: module - id ModuleCourse
    """

    serializer_class = TaskCollectionReceiverShortSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        module = self.request.query_params.get("module", None)
        student = self.request.user.student
        today = datetime.today()
        now = datetime.now()

        query = Q(student=student, start_date__lte=today, end_date__gte=today)
        began = Q(taskanswercollection__isnull=False, taskanswercollection__deadline__gte=now)
        not_began = Q(taskanswercollection__isnull=True)

        if module:
            query &= Q(task_collection__module_week_type__week_day__module_course_id=module)

        return TaskCollectionReceiver.objects.filter(query & (not_began | began))


class TaskCollectionReceiverDetailView(RetrieveAPIView):
    """
    Детальный просмотр набора заданий. При вызове начинается отсчёт до дедлайна.
    """

    serializer_class = TaskCollectionReceiverSerializer
    permission_classes = (StudentPermission,)
    lookup_url_kwarg = "id"

    def get_queryset(self):
        student = self.request.user.student
        today = datetime.today()
        now = datetime.now()

        query = Q(student=student, start_date__lte=today, end_date__gte=today)
        began = Q(taskanswercollection__isnull=False, taskanswercollection__deadline__gte=now)
        not_began = Q(taskanswercollection__isnull=True)

        return TaskCollectionReceiver.objects.filter(query & (not_began | began))


class TaskAnswerCreateView(CreateAPIView):
    """
    Создание ответа на задание.
    """

    serializer_class = TaskAnswerCreateSerializer
    permission_classes = (StudentPermission,)


class TaskAnswerUpdateView(UpdateAPIView):
    """
    Обновление ответа на задание.
    """

    serializer_class = TaskAnswerUpdateSerializer
    permission_classes = (StudentPermission,)
    lookup_url_kwarg = "id"
    queryset = TaskAnswer.objects.all()
