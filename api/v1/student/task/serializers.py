from api.models.discipline import ModuleCourse
from api.models.task import Answer, Task, TaskAnswer, TaskAnswerCollection, TaskCollectionReceiver, Type
from api.models.schedule import LessonEvent
from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied, ValidationError
from rest_framework.serializers import ModelSerializer
from django.utils import timezone

from datetime import timedelta


class TypeShortSerializer(ModelSerializer):
    name = serializers.CharField(source="task_type.name", read_only=True)

    class Meta:
        model = Type
        fields = ("id", "name")


class ModuleCourseSerializer(ModelSerializer):
    class Meta:
        model = ModuleCourse
        fields = ("id",)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["name"] = instance.discipline.name
        return response


class LessonEventSerializer(ModelSerializer):
    class Meta:
        model = LessonEvent
        fields = ("id",)

    def to_representation(self, instance: LessonEvent):
        response = super().to_representation(instance)

        if instance.lesson.lesson:
            response["type"] = "Урок"
        elif instance.lesson.int_control:
            response["type"] = "Промежуточный контроль"
        elif instance.lesson.fin_control:
            response["type"] = "Итоговый контроль"

        response["title"] = instance.lesson.name
        return response


class AnswerSerializer(ModelSerializer):
    class Meta:
        model = Answer
        fields = ("id", "answer")


class TaskAnswerSerializer(ModelSerializer):
    class Meta:
        model = TaskAnswer
        fields = ("id",)

    def to_representation(self, instance: TaskAnswer):
        response = super().to_representation(instance)

        if instance.task.answer_set.count() > 1:   # is test?
            response["chosen_variant"] = AnswerSerializer(instance.chosen_variant).data
        else:
            response["plain_answer"] = instance.plain_answer

        return response


class TaskSerializer(ModelSerializer):
    class Meta:
        model = Task
        fields = ("id", "question")

    def to_representation(self, instance: Task):
        response = super().to_representation(instance)
        task_answer_collection = self.context["task_answer_collection"]

        if is_test := instance.answer_set.count() > 1:  # is test?
            response["answers"] = AnswerSerializer(instance.answer_set.all(), many=True).data

        response.update({
            "is_test": is_test,
            "answer": TaskAnswerSerializer(
                TaskAnswer.objects.filter(task=instance, task_answer_collection=task_answer_collection).first()).data
        })

        return response


class TaskCollectionReceiverShortSerializer(ModelSerializer):
    class Meta:
        model = TaskCollectionReceiver
        fields = ("id", "start_date", "end_date")

    def to_representation(self, instance: TaskCollectionReceiver):
        response = super().to_representation(instance)

        begin_datetime = ""

        if hasattr(instance, "taskanswercollection"):
            begin_datetime = instance.taskanswercollection.created_at

        response.update({
            "event": LessonEventSerializer(instance.event).data,
            "module": instance.task_collection.module_week_type.week_day.module_course.discipline.name,
            "total_duration": instance.get_total_duration(),
            "begin_datetime": begin_datetime,
        })

        return response


class TaskCollectionReceiverSerializer(ModelSerializer):
    class Meta:
        model = TaskCollectionReceiver
        fields = ("id", "start_date", "end_date")

    def to_representation(self, instance: TaskCollectionReceiver):
        response = super().to_representation(instance)

        if hasattr(instance, "taskanswercollection"):
            task_answer_collection = instance.taskanswercollection
        else:
            task_answer_collection = TaskAnswerCollection.objects.create(task_collection_receiver=instance)
            task_answer_collection.deadline = task_answer_collection.created_at + timedelta(
                seconds=instance.get_total_duration())
            task_answer_collection.save()

        response.update({
            "begin_datetime": task_answer_collection.created_at,
            "total_duration": instance.get_total_duration(),
            "task_answer_collection": task_answer_collection.id,
            "topic": instance.task_collection.module_week_type.name,
            "module": instance.task_collection.module_week_type.week_day.module_course.discipline.name,
            "tasks": TaskSerializer(instance.task_collection.tasks,
                                    many=True,
                                    context={
                                        "task_answer_collection": task_answer_collection
                                    }).data,
        })

        return response


class TaskAnswerCreateSerializer(ModelSerializer):
    class Meta:
        model = TaskAnswer
        fields = ("id", "task_answer_collection", "task", "chosen_variant", "plain_answer")
        read_only_fields = ("id",)

    def validate(self, attrs):
        task = attrs["task"]
        task_answer_collection = attrs["task_answer_collection"]

        if timezone.now() > task_answer_collection.deadline:
            raise PermissionDenied("Deadline missed.")

        if task.taskanswer_set.filter(task_answer_collection=task_answer_collection).exists():
            raise ValidationError("Only one answer is allowed now.")
            # TODO: пока доступен выбор только одного ответа (изменить это везде)

        return super().validate(attrs)

    def create(self, validated_data):
        task: Task = validated_data["task"]
        is_test = task.answer_set.count() > 1

        if is_test and not validated_data.get("chosen_variant"):
            raise ValidationError("Pass chosen_variant field.")

        validated_data["is_test"] = is_test
        return super().create(validated_data)

    def to_representation(self, instance: TaskAnswer):
        response = super().to_representation(instance)

        if instance.is_test:
            response["chosen_variant"] = AnswerSerializer(instance.chosen_variant).data

        return response


class TaskAnswerUpdateSerializer(ModelSerializer):
    class Meta:
        model = TaskAnswer
        fields = ("id", "chosen_variant", "plain_answer")
        read_only_fields = ("id",)

    def update(self, instance, validated_data):
        if timezone.now() > instance.task_answer_collection.deadline:
            raise PermissionDenied("Deadline missed.")
        return super().update(instance, validated_data)

    def to_representation(self, instance: TaskAnswer):
        response = super().to_representation(instance)

        if instance.is_test:
            response["chosen_variant"] = AnswerSerializer(instance.chosen_variant).data

        return response
