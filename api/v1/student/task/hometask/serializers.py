from api.models.discipline import ModuleCourse
from api.models.role import Teacher
from api.models.task import HomeTaskAnswerReview
from api.models import (HomeTask,
                        HomeTaskAnswer,
                        HomeTaskAnswerFiles,
                        HomeTaskFiles,
                        HomeTaskReceiver)
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer


class ModuleCourseSerializer(ModelSerializer):
    class Meta:
        model = ModuleCourse
        fields = ("id",)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["name"] = instance.discipline.name
        return response


class HomeTaskFilesSerializer(ModelSerializer):
    class Meta:
        model = HomeTaskFiles
        fields = ("id", "file")


class HomeTaskAnswerFilesSerializer(ModelSerializer):
    class Meta:
        model = HomeTaskAnswerFiles
        fields = ("id", "file")


class HomeTaskSerializer(ModelSerializer):
    class Meta:
        model = HomeTask
        fields = ("id", "title", "body")

    def to_representation(self, instance):
        response = super().to_representation(instance)

        files = instance.hometaskfiles_set.all()
        response["files"] = HomeTaskFilesSerializer(files, many=True, context=self.context).data
        response["module"] = instance.module_course.discipline.name

        return response


class HomeTaskAnswerSerializer(ModelSerializer):
    files = serializers.ListField(child=serializers.FileField(allow_empty_file=False),
                                  required=False,
                                  write_only=True,
                                  allow_empty=True)

    class Meta:
        model = HomeTaskAnswer
        fields = ("id", "task", "title", "comment", "files")
        read_only_fields = ("id",)

    def create(self, validated_data):
        files = validated_data.pop("files", [])
        home_task_answer = super().create(validated_data)

        file_objects = []
        for file in files:
            file_objects.append(HomeTaskAnswerFiles(file=file, home_task=home_task_answer))

        HomeTaskAnswerFiles.objects.bulk_create(file_objects)
        return home_task_answer

    def to_representation(self, instance):
        response = super().to_representation(instance)

        files = instance.hometaskanswerfiles_set.all()
        response["files"] = HomeTaskAnswerFilesSerializer(files, many=True, context=self.context).data

        return response


class TeacherSerializer(ModelSerializer):
    class Meta:
        model = Teacher
        fields = ("name")

    def to_representation(self, instance):
        return {"name": str(instance)}


class HomeTaskAnswerReviewSerializer(ModelSerializer):
    reviewer = TeacherSerializer()

    class Meta:
        model = HomeTaskAnswerReview
        fields = ("id", "comment", "mark", "reviewer")


class HomeTaskReceiverSerializer(ModelSerializer):
    home_task = HomeTaskSerializer()

    class Meta:
        model = HomeTaskReceiver
        fields = ("id", "home_task", "start_date", "end_date")

    def to_representation(self, instance):
        response = super().to_representation(instance)

        response["answer"] = dict()
        response["review"] = dict()

        if hasattr(instance, "hometaskanswer"):
            response["answer"] = HomeTaskAnswerSerializer(instance.hometaskanswer, context=self.context).data

        if hasattr(instance, "hometaskanswer") and hasattr(instance.hometaskanswer, "hometaskanswerreview"):
            response["review"] = HomeTaskAnswerReviewSerializer(instance.hometaskanswer.hometaskanswerreview).data

        return response


class HomeTaskReceiverShortSerializer(ModelSerializer):
    class Meta:
        model = HomeTaskReceiver
        fields = ("id", "start_date", "end_date")

    def to_representation(self, instance):
        response = super().to_representation(instance)

        response["title"] = instance.home_task.title
        response["module"] = instance.home_task.module_course.discipline.name
        response["answer_date"] = ""
        response["review_date"] = ""

        if hasattr(instance, "hometaskanswer"):
            response["answer_date"] = instance.hometaskanswer.created_at.date()

        if hasattr(instance, "hometaskanswer") and hasattr(instance.hometaskanswer, "hometaskanswerreview"):
            response["review_date"] = instance.hometaskanswer.hometaskanswerreview.created_at.date()
            response["mark"] = instance.hometaskanswer.hometaskanswerreview.mark

        return response
