from api.models.discipline import ModuleCourse
from api.models.group import ModuleStudentGroup
from django.db.models.query_utils import Q
from api.models import HomeTaskReceiver
from api.v1.permissions import StudentPermission
from api.v1.student.task.hometask.serializers import (HomeTaskAnswerSerializer,
                                                      HomeTaskReceiverSerializer,
                                                      HomeTaskReceiverShortSerializer, ModuleCourseSerializer)
from rest_framework.generics import (CreateAPIView,
                                     ListAPIView,
                                     RetrieveAPIView)


class ModuleCourseListView(ListAPIView):
    """
    Список модулей для студента.
    """

    serializer_class = ModuleCourseSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        student = self.request.user.student
        return ModuleCourse.objects.filter(hometask__hometaskreceiver__receiver=student).distinct()


class HomeTaskReceiverListView(ListAPIView):
    """
    Список домашних заданий для студента.

    Query params: module
    """

    serializer_class = HomeTaskReceiverShortSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        module = self.request.query_params.get("module", None)
        done = self.request.query_params.get("done", None)
        reviewed = self.request.query_params.get("reviewed", None)

        query = Q(receiver=self.request.user.student)

        if module:
            query &= Q(home_task__module_course=module)

        if isinstance(done, str):
            if done.lower() == "true":
                query &= Q(hometaskanswer__isnull=False)
            elif done.lower() == "false":
                query &= Q(hometaskanswer__isnull=True)

            if isinstance(reviewed, str):
                if reviewed.lower() == "true":
                    query &= Q(hometaskanswer__hometaskanswerreview__isnull=False)
                elif reviewed.lower() == "false":
                    query &= Q(hometaskanswer__hometaskanswerreview__isnull=True)

        return HomeTaskReceiver.objects.filter(query).order_by("-created_at")


class HomeTaskReceiverDetailView(RetrieveAPIView):
    """
    Детальное представление домашнего задания.
    """

    serializer_class = HomeTaskReceiverSerializer
    permission_classes = (StudentPermission,)
    lookup_url_kwarg = "id"

    def get_queryset(self):
        student = self.request.user.student
        return HomeTaskReceiver.objects.filter(receiver=student)


class HomeTaskAnswerCreateView(CreateAPIView):
    """
    Создание ответа на домашнее задание.
    """

    serializer_class = HomeTaskAnswerSerializer
    permission_classes = (StudentPermission,)
