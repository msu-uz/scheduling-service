from api.v1.student.task.hometask.views import (HomeTaskAnswerCreateView,
                                                HomeTaskReceiverDetailView,
                                                HomeTaskReceiverListView, ModuleCourseListView)
from django.urls import path

urlpatterns = [
    path("module/list/", ModuleCourseListView.as_view()),
    path("list/", HomeTaskReceiverListView.as_view()),
    path("<int:id>/", HomeTaskReceiverDetailView.as_view()),
    path("answer/create/", HomeTaskAnswerCreateView.as_view()),
]
