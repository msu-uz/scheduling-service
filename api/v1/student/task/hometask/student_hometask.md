# Выполнение домашних заданий студентом

## `GET /student/task/hometask/module/list/`

Список модулей.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Ихтиолог",
        "uz": "Engineer, manufacturing"
      }
    },
    {
      "id": 5,
      "name": {
        "ru": "Оператор РЛС",
        "uz": "Systems analyst"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Ихтиолог",
        "uz": "Engineer, manufacturing"
      }
    }
  ]
}
```

## `GET /student/task/hometask/list/?module=1&done=<true,false>&reviewed=<true,false>`

Список домашних заданий.

### Query params:

| Название | Описание                                                |
| -------- | ------------------------------------------------------- |
| module   | id модуля                                               |
| done     | Только выполненные (true), только невыполнные (false)   |
| reviewed | Только проверенные (true), только невыполненные (false) |

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "start_date": "2020-11-25",
      "end_date": "2020-12-26",
      "title": "Заголовок домашнего задания",
      "module": {
        "ru": "Ихтиолог",
        "uz": "Engineer, manufacturing"
      },
      "answer_date": "2021-01-08",
      "review_date": "2021-01-21",
      "mark": 5
    }
  ]
}
```

### Response fields:

| Название    | Описание                                                                |
| ----------- | ----------------------------------------------------------------------- |
| answer_date | Дата создания ответа. Пустая строка, если ответ ещё не был дан          |
| review_date | Дата проверки преподавателем. Пустая строка, если ещё не было проверено |

---

## `GET /student/task/hometask/<int:id>/`

Детальное представление домашнего задания.

### URL parameters:

| Название | Описание             |
| -------- | -------------------- |
| id       | id домашнего задания |

### Response:

```json
{
  "id": 1,
  "home_task": {
    "id": 1,
    "title": "Заголовок домашнего задания",
    "body": "Тело домашнего задания",
    "files": [
      {
        "id": 1,
        "file": "http://127.0.0.1:8000/media/home_tasks/tasks/task.txt"
      }
    ],
    "module": {
      "ru": "Ихтиолог",
      "uz": "Engineer, manufacturing"
    }
  },
  "start_date": "2020-11-25",
  "end_date": "2020-12-26",
  "answer": {
    "id": 2,
    "task": 1,
    "title": "Решение домашнего задания 1",
    "comment": "Текст ответа на домашнее задание 1",
    "files": [
      {
        "id": 3,
        "file": "/media/home_tasks/answers/eb8ba192-6426-4fa7-a673-0051b7c63c18.png"
      },
      {
        "id": 4,
        "file": "/media/home_tasks/answers/48d68809-5ac5-451d-8374-b6cb3cd049d4.png"
      }
    ]
  },
  "review": {
    "id": 2,
    "comment": "Текст проверки домашнего задания",
    "mark": 5,
    "reviewer": {
      "name": "Амир Муллагалиев"
    }
  }
}
```

### Response fields:

| Название | Описание                           |
| -------- | ---------------------------------- |
| answer   | Объект-ответ. Может быть пустым    |
| review   | Объект-проверка. Может быть пустым |

---

## `POST /student/task/hometask/answer/create/`

Создание ответа на домашнее задание.

### Request fields (multipart form):

| Название | Описание                                            |
| -------- | --------------------------------------------------- |
| title    | Заголовок ответа на домашнее задание, необязательно |
| task     | id домашнего задания                                |
| comment  | Текст ответа на домашнее задание                    |
| files    | Список файлов, необязательно                        |

### Response:

```json
{
  "id": 2,
  "task": 1,
  "title": "Решение домашнего задания 1",
  "comment": "Текст ответа на домашнее задание 1",
  "files": [
    {
      "id": 3,
      "file": "http://127.0.0.1:8000/media/home_tasks/answers/eb8ba192-6426-4fa7-a673-0051b7c63c18.png"
    },
    {
      "id": 4,
      "file": "http://127.0.0.1:8000/media/home_tasks/answers/48d68809-5ac5-451d-8374-b6cb3cd049d4.png"
    }
  ]
}
```
