from api.models.task import TaskCollectionReceiver


def generator(lesson_event):
    receivers = []
    for student in lesson_event.module_student_group.get_students():
        for task_collection in lesson_event.lesson.taskcollection_set.all():
            receivers.append(TaskCollectionReceiver(event=lesson_event,
                                                    task_collection=task_collection,
                                                    student=student))

    TaskCollectionReceiver.objects.bulk_create(receivers)
