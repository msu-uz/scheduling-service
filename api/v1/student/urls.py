from django.urls import path, include
# teacher_room
urlpatterns = [
    path('task/', include("api.v1.student.task.urls")),
    path('feedback/', include("api.v1.student.feedback.urls")),
    path('library/', include("api.v1.student.library.urls")),
    path('notification/', include("api.v1.student.notification.urls")),
    path("schedule/", include("api.v1.student.schedule.urls")),
    path('results/', include("api.v1.student.results.urls")),
    path('dashboard/', include("api.v1.student.dashboard.urls"))
]
