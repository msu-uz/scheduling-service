from api.models.catalog import Category
from api.models.library import (LibraryBook, ModuleOfVideo, TypeOfBook,
                                VideoMaterial)
from django_filters.filters import (AllValuesMultipleFilter,
                                    ModelMultipleChoiceFilter)
from django_filters.rest_framework import FilterSet


class LibraryBookListFilterSet(FilterSet):
    publish_year = AllValuesMultipleFilter(field_name="publish_date__year")
    category = ModelMultipleChoiceFilter(queryset=Category.objects.all())
    book_type = ModelMultipleChoiceFilter(queryset=TypeOfBook.objects.all())

    class Meta:
        model = LibraryBook
        fields = ("category", "book_type", "publish_year")


class VideoMaterialListFilterSet(FilterSet):
    category = ModelMultipleChoiceFilter(queryset=Category.objects.all())
    module = ModelMultipleChoiceFilter(queryset=ModuleOfVideo.objects.all())

    class Meta:
        model = VideoMaterial
        fields = ("category", "module")
