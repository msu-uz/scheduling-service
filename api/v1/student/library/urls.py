from api.v1.student.library.views import (CategoryListView,
                                          CategoryNestedListView,
                                          LibraryBookListView,
                                          LibraryBookRetrieveView,
                                          LibraryBookShareListView,
                                          ModuleOfVideoListView,
                                          TypeOfBookListView,
                                          VideoMaterialListView,
                                          VideoMaterialRetrieveView,
                                          VideoMaterialShareListView,
                                          YearListView)
from django.urls.conf import path

urlpatterns = [
    path("typeofbook/list/", TypeOfBookListView.as_view()),
    path("category/list/", CategoryNestedListView.as_view()),
    path("shortcategory/list/", CategoryListView.as_view()),
    path("moduleofvideo/list/", ModuleOfVideoListView.as_view()),
    path("year/list/", YearListView.as_view()),
    path("librarybook/list/", LibraryBookListView.as_view()),
    path("librarybook/<int:id>/", LibraryBookRetrieveView.as_view()),
    path("librarybook/share/list/", LibraryBookShareListView.as_view()),
    path("videomaterial/list/", VideoMaterialListView.as_view()),
    path("videomaterial/<int:id>/", VideoMaterialRetrieveView.as_view()),
    path("videomaterial/share/list/", VideoMaterialShareListView.as_view()),
]
