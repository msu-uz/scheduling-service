from typing import List
from django.db.models.query_utils import Q
from api.models.catalog import Category
from api.models.library import (LibraryBook,
                                LibraryShare,
                                ModuleOfVideo,
                                TypeOfBook,
                                VideoMaterial)
from api.v1.permissions import StudentPermission
from api.v1.student.library.serializers import (CategorySerializer,
                                                CategoryShortSerializer,
                                                LibraryBookSerializer,
                                                LibraryBookShareShortSerializer,
                                                LibraryBookShortSerializer,
                                                LibraryBookYearSerializer,
                                                ModuleOfVideoSerializer,
                                                TypeOfBookSerializer,
                                                VideoMaterialSerializer,
                                                VideoMaterialShareShortSerializer,
                                                VideoMaterialShortSerializer)
from rest_framework.filters import SearchFilter
from rest_framework.generics import ListAPIView, RetrieveAPIView


def read_list_query_param(param: str) -> List[str]:
    """
    Функция для перевода любой query_param строки в список строк цифр.
    """

    if set(param).isdisjoint("0123456789"):
        return []

    clean_param = ''.join(symb for symb in param if symb in "0123456789,")
    return clean_param.split(",")


class CategoryNestedListView(ListAPIView):
    """
    Список категорий с вложенными категориями.
    """

    serializer_class = CategorySerializer
    permission_classes = (StudentPermission,)
    queryset = Category.objects.root_nodes()


class CategoryListView(ListAPIView):
    """
    Список категорий.
    """

    serializer_class = CategoryShortSerializer
    permission_classes = (StudentPermission,)
    queryset = Category.objects.all()


# LibraryBook


class TypeOfBookListView(ListAPIView):
    """
    Список типов книг.
    """

    serializer_class = TypeOfBookSerializer
    permission_classes = (StudentPermission,)
    queryset = TypeOfBook.objects.all()


class LibraryBookListView(ListAPIView):
    """
    Список книг.

    Query params: search,
                  publish_year,
                  category,
                  book_type
    """

    serializer_class = LibraryBookShortSerializer
    permission_classes = (StudentPermission,)
    queryset = LibraryBook.objects.all().select_related("category", "book_type", "publisher")

    filter_backends = (SearchFilter,)
    search_fields = ("title", "author", "description")

    def get_queryset(self):
        publish_year = self.request.query_params.get("publish_year")
        category = self.request.query_params.get("category")
        book_type = self.request.query_params.get("book_type")

        query = Q()

        if isinstance(category, str):
            category = read_list_query_param(category)
            query &= Q(category__in=category)

        if isinstance(book_type, str):
            book_type = read_list_query_param(book_type)
            query &= Q(book_type__in=book_type)

        if isinstance(publish_year, str):
            publish_year = read_list_query_param(publish_year)
            query &= Q(publish_date__year__in=publish_year)

        return LibraryBook.objects.filter(query).select_related("category", "book_type", "publisher")


class LibraryBookRetrieveView(RetrieveAPIView):
    """
    Детальный просмотр книги.
    """

    serializer_class = LibraryBookSerializer
    permission_classes = (StudentPermission,)
    queryset = LibraryBook.objects.all()
    lookup_url_kwarg = "id"


class YearListView(ListAPIView):
    """
    Список годов издания.
    """

    serializer_class = LibraryBookYearSerializer

    permission_classes = (StudentPermission,)

    def get_queryset(self):
        return sorted(set(LibraryBook.objects.all().values_list("publish_date__year", flat=True)))


# VideoMaterial


class ModuleOfVideoListView(ListAPIView):
    """
    Список модулей видео.
    """

    serializer_class = ModuleOfVideoSerializer
    permission_classes = (StudentPermission,)
    queryset = ModuleOfVideo.objects.all()


class VideoMaterialListView(ListAPIView):
    """
    Список видеоматериалов.

    Query params: search,
                  category,
                  module
    """

    serializer_class = VideoMaterialShortSerializer
    permission_classes = (StudentPermission,)
    filter_backends = (SearchFilter,)
    search_fields = ("title",)

    def get_queryset(self):
        category = self.request.query_params.get("category")
        module = self.request.query_params.get("module")

        query = Q()

        if isinstance(category, str):
            category = read_list_query_param(category)
            query &= Q(category__in=category)

        if isinstance(module, str):
            module = read_list_query_param(module)
            query &= Q(module__in=module)

        return VideoMaterial.objects.filter(query).select_related("module", "category", "publisher")


class VideoMaterialRetrieveView(RetrieveAPIView):
    """ Детальный просмотр видеоматериала. """

    serializer_class = VideoMaterialSerializer
    permission_classes = (StudentPermission,)
    queryset = VideoMaterial.objects.all()
    lookup_url_kwarg = "id"


# LibraryShare


class VideoMaterialShareListView(ListAPIView):
    """
    Список ссылок на видеоматериалы.

    Query params: module: int
    """

    serializer_class = VideoMaterialShareShortSerializer

    permission_classes = (StudentPermission,)

    def get_queryset(self):
        student_id = self.request.user.student.id
        query = (Q(students__id=student_id) | Q(student_groups__studentgroupforming__student_id=student_id)) & Q(video_material__isnull=False, library_book__isnull=True)

        module_of_video_id = self.request.query_params.get("module")
        if isinstance(module_of_video_id, int):
            query &= Q(video_material__module=module_of_video_id)

        return LibraryShare.objects.filter(query).distinct()


class LibraryBookShareListView(ListAPIView):
    serializer_class = LibraryBookShareShortSerializer

    permission_classes = (StudentPermission,)

    def get_queryset(self):
        student_id = self.request.user.student.id
        query = (Q(students__id=student_id) | Q(student_groups__studentgroupforming__student_id=student_id)) & Q(library_book__isnull=False, video_material__isnull=True)
        return LibraryShare.objects.filter(query).distinct()
