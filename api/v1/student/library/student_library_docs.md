# Библиотека в кабинете студента

## `GET student/library/typeofbook/list/`

Список типов книг.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Книга",
        "uz": "Book"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Журнал",
        "uz": "Magazine"
      }
    }
  ]
}
```

## `GET student/library/category/list/`

Список категорий, могут быть вложенные.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Закон",
        "uz": "Law"
      },
      "parent": null,
      "children": [],
      "checked": false
    },
    {
      "id": 2,
      "name": {
        "ru": "Психология",
        "uz": "Psychology"
      },
      "parent": null,
      "children": [
        {
          "id": 3,
          "name": {
            "ru": "Психология человека",
            "uz": "Human psychology"
          },
          "parent": 2,
          "children": [],
          "checked": false
        },
        {
          "id": 4,
          "name": {
            "ru": "Психология поведения",
            "uz": "Behavior psychology"
          },
          "parent": 2,
          "children": [],
          "checked": false
        }
      ],
      "checked": false,
      "open": false
    }
  ]
}
```

## `GET student/library/librarybook/list/`

Список книг.

### Query params:

| Название     | Описание                     |
| ------------ | ---------------------------- |
| publish_year | Список годов                 |
| category     | Список id категорий          |
| book_type    | Список id типов              |
| search       | Поиск по названию и описанию |

### Response:

```json
{
  "results": [
    {
      "id": 2,
      "title": {
        "ru": "Название",
        "uz": "Title"
      },
      "image": "http://127.0.0.1:8000/media/librarybooks/imgs/photo_2021-01-13_11-32-10_Ff09UNN.jpg",
      "description": {
        "ru": "Описание",
        "uz": "Description",
        "en": "Kniga"
      },
      "category": {
        "ru": "Закон",
        "uz": "Law"
      },
      "book_type": {
        "ru": "Книга",
        "uz": "Book"
      },
      "publish_date": "2020-01-01",
      "page_count": 15
    }
  ]
}
```

## `GET student/library/librarybook/<int:id>/`

Детальный просмотр книги.

### URL params:

| Название | Описание |
| -------- | -------- |
| id       | id книги |

### Response:

```json
{
  "id": 2,
  "title": {
    "ru": "Название",
    "uz": "Title"
  },
  "file": "http://127.0.0.1:8000/media/librarybooks/files/Insomnia_2021-01-04_jBScJuR",
  "image": "http://127.0.0.1:8000/media/librarybooks/imgs/photo_2021-01-13_11-32-10_Ff09UNN.jpg",
  "description": {
    "ru": "Описание",
    "uz": "Description",
    "en": "Kniga"
  },
  "category": {
    "ru": "Закон",
    "uz": "Law"
  },
  "author": "Stephen King",
  "book_type": {
    "ru": "Книга",
    "uz": "Book"
  },
  "publish_date": "2020-01-01",
  "page_count": 15,
  "publisher": {
    "id": 101,
    "first_name": "Амир",
    "last_name": "Муллагалиев"
  },
  "size": "1.82 Mb"
}
```

## `GET teacher/library/year/list/`

Список годов издания книг (упорядочено по возрастанию).

### Response:

```json
{
  "results": [
    {
      "checked": false,
      "name": "2020"
    }
  ]
}
```

## `GET student/library/librarybook/share/list/`

Список ссылок на книгу.

### Resonse:

```json
{
  "results": [
    {
      "id": 2,
      "media_id": 30, // id самой книги
      "teacher": {
        "image": "http://ekadr.sudya.uz/media/doctor_photos/test.jpg",
        "full_name": "Амир Муллагалиев"
      },
      "created_at": "2021-01-16T11:04:16.938440+05:00",
      "title": {
        "ru": "Название",
        "uz": "Title"
      },
      "book_type": {
        "ru": "Книга",
        "uz": "Book"
      }
    }
  ]
}
```

## `GET student/library/moduleofvideo/list/`

Список модулей видео.

```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Конституционное право",
        "uz": "Constitucionnoe pravo"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Психодиагностика",
        "uz": "Psychodiagnostics"
      }
    }
  ]
}
```

## `GET student/library/videomaterial/list/`

Список видеоматериалов.

### Query params:

| Название | Описание            | Пример               |
| -------- | ------------------- | -------------------- |
| category | Список id категорий | `?module=1&module=2` |
| search   | Поиск по названию   | `?search=`           |

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "title": {
        "ru": "Заголовок",
        "uz": "Zagolovok"
      },
      "image": "http://url.com",
      "video": "http://127.0.0.1:8000/media/videomaterials/Insomnia_2021-01-04_sB1Rb0w",
      "module": {
        "ru": "Конституционное право",
        "uz": "Constitucionnoe pravo"
      },
      "created_at": "2021-01-13T11:57:51.505912+05:00"
    }
  ]
}
```

## `GET student/library/videomaterial/<int:id>/`

Детальный просмотр видеоматериала.

### URL params:

| Название | Описание |
| -------- | -------- |
| id       | id книги |

### Response:

```json
{
  "id": 1,
  "title": {
    "ru": "Заголовок",
    "uz": "Zagolovok"
  },
  "description": {
    "ru": "Описание",
    "uz": "Opisaniye"
  },
  "image": "http://url.com",
  "video": "http://127.0.0.1:8000/media/videomaterials/Insomnia_2021-01-04_sB1Rb0w",
  "module": {
    "ru": "Конституционное право",
    "uz": "Constitucionnoe pravo"
  },
  "category": {
    "ru": "Закон",
    "uz": "Law"
  },
  "publisher": {
    "id": 101,
    "first_name": "Амир",
    "last_name": "Муллагалиев"
  },
  "size": "1.82 Mb"
}
```

## `GET student/library/videomaterial/share/list/?module=int`

Список ссылок на видеоматериалы.

### Query params:

| Название | Описание        |
| -------- | --------------- |
| module   | id модуля видео |

### Resonse:

```json
{
  "results": [
    {
      "id": 1,
      "media_id": 17, // id самого видео
      "teacher": {
        "image": "http://ekadr.sudya.uz/media/doctor_photos/test.jpg",
        "full_name": "Амир Муллагалиев"
      },
      "created_at": "2021-01-15T18:03:39.987569+05:00",
      "title": {
        "ru": "Заголовок",
        "uz": "Zagolovok"
      },
      "module": {
        "ru": "Конституционное право",
        "uz": "Constitucionnoe pravo"
      }
    }
  ]
}
```
