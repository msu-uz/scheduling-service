from api.models.role import Teacher
from api.models.catalog import Category
from api.models.library import (LibraryBook,
                                LibraryShare,
                                ModuleOfVideo,
                                TypeOfBook,
                                VideoMaterial)
from django.contrib.auth import get_user_model
from rest_framework.serializers import ModelSerializer, Serializer
from rest_framework_recursive.fields import RecursiveField

User = get_user_model()


def sizify(value):
    """
    Simple Kb/Mb/Gb size snippet.
    """

    if value < 512000:
        value = value / 1024.0
        ext = "Kb"
    elif value < 4194304000:
        value = value / 1048576.0
        ext = "Mb"
    else:
        value = value / 1073741824.0
        ext = "Gb"
    return "%s %s" % (str(round(value, 2)), ext)


class CategorySerializer(ModelSerializer):
    children = RecursiveField(many=True)

    class Meta:
        model = Category
        fields = ("id", "name", "parent", "children")

    def to_representation(self, instance):
        response = super().to_representation(instance)

        response["checked"] = False
        if instance.children.count() > 0:
            response["open"] = False

        return response


class CategoryShortSerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ("id", "name")


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "first_name", "last_name")


# LibraryBook


class TypeOfBookSerializer(ModelSerializer):
    class Meta:
        model = TypeOfBook
        fields = ("id", "name")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["checked"] = False
        return response


class LibraryBookShortSerializer(ModelSerializer):
    class Meta:
        model = LibraryBook
        fields = (
            "id",
            "title",
            "image",
            "description",
            "category",
            "book_type",
            "publish_date",
            "page_count",
        )

    def to_representation(self, instance: LibraryBook):
        response = super().to_representation(instance)

        response.update({
            "category": instance.category.name,
            "book_type": instance.book_type.name
        })

        return response


class LibraryBookSerializer(ModelSerializer):
    class Meta:
        model = LibraryBook
        fields = ("id", "title", "file", "image", "description", "category", "author", "book_type", "publish_date",
                  "page_count", "publisher")

    def to_representation(self, instance: LibraryBook):
        response = super().to_representation(instance)

        response.update({
            "publisher": UserSerializer(instance.publisher).data,
            "category": instance.category.name,
            "book_type": instance.book_type.name,
            "size": sizify(instance.file.size)
        })

        return response


class LibraryBookYearSerializer(Serializer):
    def to_representation(self, instance: int):
        response = {
            "id": instance,  # по просьбе фронта
            "checked": False,
            "name": str(instance)
        }
        return response


# VideoMaterial


class ModuleOfVideoSerializer(ModelSerializer):
    class Meta:
        model = ModuleOfVideo
        fields = ("id", "name")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["checked"] = False
        return response


class VideoMaterialShortSerializer(ModelSerializer):
    class Meta:
        model = VideoMaterial
        fields = ("id", "title", "image", "video", "module", "created_at")

    def to_representation(self, instance: VideoMaterial):
        response = super().to_representation(instance)
        response.update({"module": instance.module.name})
        return response


class VideoMaterialSerializer(ModelSerializer):
    class Meta:
        model = VideoMaterial
        fields = ("id", "title", "image", "description", "video", "module", "category", "publisher")
        read_only_fields = ("id", "publisher")

    def to_representation(self, instance: VideoMaterial):
        response = super().to_representation(instance)

        response.update({
            "publisher": UserSerializer(instance.publisher).data,
            "module": instance.module.name,
            "category": instance.category.name,
            "size": sizify(instance.video.size),
        })

        return response


# LibraryShare


class TeacherSerializer(ModelSerializer):
    class Meta:
        model = Teacher
        fields = ()

    def to_representation(self, instance: Teacher):
        return {"image": instance.employee.image, "full_name": str(instance)}


class VideoMaterialShareShortSerializer(ModelSerializer):
    teacher = TeacherSerializer()

    class Meta:
        model = LibraryShare
        fields = ("id", "teacher", "created_at")

    def to_representation(self, instance: LibraryShare):
        response = super().to_representation(instance)

        response.update({
            "media_id": instance.video_material.id,
            "title": instance.video_material.title,
            "module": instance.video_material.module.name,
        })

        return response


class LibraryBookShareShortSerializer(ModelSerializer):
    teacher = TeacherSerializer()

    class Meta:
        model = LibraryShare
        fields = ("id", "teacher", "created_at")

    def to_representation(self, instance: LibraryShare):
        response = super().to_representation(instance)

        response.update({
            "media_id": instance.library_book.id,
            "title": instance.library_book.title,
            "book_type": instance.library_book.book_type.name,
        })

        return response
