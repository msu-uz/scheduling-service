from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework.generics import ListAPIView
from django.utils.datetime_safe import datetime
from api.models import Course
from api.models.schedule import LessonEvent
from api.v1.permissions import StudentPermission
from api.v1.student.dashboard.serializers import LessonEventSerializer, CourseListSerializer

from api.v1.student.dashboard.serializers import *
from api.models import Student, LessonMaterial
from rest_framework.generics import RetrieveAPIView, ListAPIView
from api.v1.permissions import StudentPermission


class AccountView(RetrieveAPIView):
    queryset = Student.objects.all()
    serializer_class = AccountSerializer
    permission_classes = [StudentPermission]

    def get_object(self):
        return get_object_or_404(Student, id=self.request.user.student.id)


class LessonMaterialListView(ListAPIView):
    serializer_class = LessonMaterialSerializer
    permission_classes = [StudentPermission]

    def get_queryset(self):
        query = Q(student_group=self.request.user.student.student_groups.last()) | Q(student=self.request.user.student)
        return LessonMaterial.objects.filter(query).distinct().order_by('-created_at').select_related("lesson_event")


class LessonEventListView(ListAPIView):
    serializer_class = LessonEventSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        date_str = self.request.query_params.get("date", "")
        student = self.request.user.student
        date = datetime.today()

        try:
            date = datetime.fromisoformat(date_str)
        except ValueError:
            pass

        return LessonEvent.objects.filter(module_student_group__group__studentgroupforming__student=student,
                                          date=date).order_by("interval__number").distinct()


class CourseListView(ListAPIView):
    serializer_class = CourseListSerializer
    permission_classes = (StudentPermission,)

    def get_queryset(self):
        student = self.request.user.student
        qs = Course.objects.filter(studentgroup=student.student_groups.first())
        return qs
