# Главная страница студента

## `GET student/dashboard/schedule/?date=2021-01-27`

Расписание на определённую дату для студента (список мероприятий). По умолчанию возвращается расписание на сегодня.

### Query params:

| Название | Описание                    |
| -------- | --------------------------- |
| date     | Дата в формате `YYYY-MM-DD` |

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "date": "2021-01-27",
      "module": {
        "ru": "Квасник",
        "uz": "Pharmacist, community"
      },
      "time": "09:00:00"
    },
    {
      "id": 2,
      "date": "2021-01-27",
      "module": {
        "ru": "Мясник",
        "uz": "Therapeutic radiographer"
      },
      "time": "10:45:00"
    }
  ]
}
```

_С вопросами по API обращаться к Амиру._

## `GET /student/dashboard/course/list/`
Результаты студента по курсам 

### Response:    
```json
{
  "results": [
    {
      "id": 2,
      "name": {
        "ru": "Учитель",
        "uz": "Visual merchandiser"
      },
      "start_date": "2021-01-14",
      "end_date": "2021-01-28",
      "course_score": "2.14/100"
    }
  ]
}
```
_С вопросами по API обращаться к Эльдору._

---
##`GET` `account/`
>Response
```json
{
  "id": 101,
  "image": "http://ekadr.sudya.uz/media/doctor_photos/Screenshot_9_J6G1VQm.png",
  "age": 14,
  "name": "Осипов Иван Иванович"
}
```
---
##`GET` `material/list/`
>Response
```json
{
 "results": [
    {
      "id": 1,
      "title": "wqe",
      "file": "http://localhost:8000/media/lesson_material/files/script.sql",
      "lesson_event": 838,
      "teacher": [
        100
      ],
      "created_at": "2021-02-02"
    }
  ]
}
```
_с вопросами по API обращаться к Хумо._