from django.urls import path
from api.v1.student.dashboard.views import LessonEventListView, CourseListView, AccountView, LessonMaterialListView

urlpatterns = [
    path('course/list/', CourseListView.as_view()),
    path("schedule/", LessonEventListView.as_view()),
    path("account/", AccountView.as_view()),
    path("material/list/", LessonMaterialListView.as_view()),
]
