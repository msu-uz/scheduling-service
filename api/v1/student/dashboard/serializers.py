from rest_framework import serializers
from api.models import Student, LessonMaterial, Applicant, Employee
from api.models import Course
from api.v1.student.results.additional import modules_score_sum
from api.models.schedule import LessonEvent
from rest_framework.serializers import ModelSerializer


class AccountSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        if Applicant.objects.filter(student=obj.id).exists():
            return self.context['request'].build_absolute_uri(obj.applicant.image.url)
        elif Employee.objects.filter(student=obj.id).exists():
            return obj.employee.image

    class Meta:
        model = Student
        fields = [
            "id",
            "image",
        ]

    def to_representation(self, instance: Student):
        response = super(AccountSerializer, self).to_representation(instance)
        response["age"] = instance.age
        response["name"] = instance.__str__()
        return response


class LessonMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = LessonMaterial
        fields = ('id', 'title', 'file', 'lesson_event', 'teacher')
        read_only_fields = ("id",)

    def to_representation(self, instance):
        response = super(LessonMaterialSerializer, self).to_representation(instance)
        response.update(
            {
                "created_at": instance.created_at.date()
            }
        )
        return response


class CourseListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ("id", "name")

    def to_representation(self, instance: Course):
        response = super().to_representation(instance)
        events = LessonEvent.objects.filter(module_student_group__module_course__course=instance,
                                            module_student_group__group__studentgroupforming__student=self.context[
                                                "request"].user.student).distinct()
        if events:
            ordered_events = events.order_by("date")
            first_date = ordered_events.first().date
            last_date = ordered_events.last().date
        else:
            first_date = 0
            last_date = 0
        response['start_date'] = first_date
        response['end_date'] = last_date
        if instance.modulecourse_set.all():
            course_score = modules_score_sum(instance.modulecourse_set.all(), self.context['request'].user.student)
            score = course_score / instance.modulecourse_set.all().count()
        else:
            score = 0
        response['course_score'] = f'{score:.2f}'

        return response


class LessonEventSerializer(ModelSerializer):
    class Meta:
        model = LessonEvent
        fields = ("id", "date")

    def to_representation(self, instance: LessonEvent):
        response = super().to_representation(instance)

        response.update({
            "module": instance.module_student_group.module_course.discipline.name,
            "time": instance.interval.start_time,
        })

        return response
