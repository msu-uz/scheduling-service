from rest_framework.permissions import BasePermission, SAFE_METHODS

from api.models import Task
from api.models.user import User


class AccessLevelPermission(BasePermission):
    levels = [0, ]

    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False
        if request.method in SAFE_METHODS and request.user.access_level == User.APPLICATION_LEVEL:
            return True
        return request.user.access_level in self.levels


class AdmissionBoardPermission(AccessLevelPermission):
    levels = [User.ADMISSION_BOARD_LEVEL, ]


class StudentPermission(AccessLevelPermission):
    levels = [User.STUDENT_LEVEL, ]


class TeacherPermission(AccessLevelPermission):
    levels = [User.TEACHER_LEVEL, ]


class EducationOfficeUnitPermission(AccessLevelPermission):
    levels = [User.EDUCATION_OFFICE_UNITS_LEVEL, ]


class AdministratorPermission(AccessLevelPermission):
    levels = [User.ADMINISTRATOR_LEVEL, User.EDUCATION_OFFICE_UNITS_LEVEL]


class LibrarianPermission(AccessLevelPermission):
    levels = [User.LIBRARIAN_LEVEL, ]


class ApplicationPermission(AccessLevelPermission):
    levels = [User.APPLICATION_LEVEL, ]


class TeacherCreateTestPermission(TeacherPermission):
    can_create_test = True

    def has_permission(self, request, view):
        super().has_permission(request, view)
        return request.user.teacher.can_create_test == self.can_create_test

    def has_object_permission(self, request, view, obj):
        return request.user.teacher == obj.creator


class TeacherCheckTestPermission(TeacherPermission):
    can_check_test = True

    def has_permission(self, request, view):
        super().has_permission(request, view)
        task_review = Task.objects.get(id=view.kwargs['id']).get_review().last()
        if request.user.teacher.can_check_test == self.can_check_test:
            if task_review:
                return task_review.reviewer == request.user.teacher
            else:
                return True
        else:
            return False
