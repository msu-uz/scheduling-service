from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, Serializer
from rest_framework_recursive.fields import RecursiveField

from api.models.catalog import Category
from api.models.library import (LibraryBook,
                                ModuleOfVideo,
                                TypeOfBook,
                                VideoMaterial)

from api.v1.library.validators import validate_json

User = get_user_model()


def sizify(value):
    """
    Simple Kb/Mb/Gb size snippet.
    """

    if value < 512000:
        value = value / 1024.0
        ext = "Kb"
    elif value < 4194304000:
        value = value / 1048576.0
        ext = "Mb"
    else:
        value = value / 1073741824.0
        ext = "Gb"
    return "%s %s" % (str(round(value, 2)), ext)


class CategorySerializer(ModelSerializer):
    children = RecursiveField(many=True)

    class Meta:
        model = Category
        fields = ("id", "name", "parent", "children")

    def to_representation(self, instance):
        response = super().to_representation(instance)

        response["checked"] = False
        if instance.children.count() > 0:
            response["open"] = False

        return response


class CategoryShortSerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ("id", "name")


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "first_name", "last_name")


# LibraryBook


class TypeOfBookSerializer(ModelSerializer):
    class Meta:
        model = TypeOfBook
        fields = ("id", "name")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["checked"] = False
        return response


class LibraryBookShortSerializer(ModelSerializer):
    class Meta:
        model = LibraryBook
        fields = (
            "id",
            "title",
            "image",
            "description",
            "category",
            "book_type",
            "publish_date",
            "page_count",
        )

    def to_representation(self, instance: LibraryBook):
        response = super().to_representation(instance)

        response.update({
            "category": instance.category.name,
            "book_type": instance.book_type.name,
            "my": instance.publisher == self.context["request"].user
        })

        return response


class LibraryBookSerializer(ModelSerializer):
    title = serializers.JSONField(validators=(validate_json,))
    description = serializers.JSONField(validators=(validate_json,))

    class Meta:
        model = LibraryBook
        fields = (
            "id",
            "title",
            "file",
            "image",
            "description",
            "category",
            "author",
            "book_type",
            "publish_date",
            "page_count",
            "publisher"
        )
        read_only_fields = ("id", "publisher")

    def create(self, validated_data):
        validated_data["publisher"] = self.context["request"].user
        return super().create(validated_data)

    def update(self, instance, validated_data):
        title = validated_data.pop("title", [])
        description = validated_data.pop("description", [])

        instance.title.update(title)
        instance.description.update(description)

        return super().update(instance, validated_data)

    def to_representation(self, instance: LibraryBook):
        response = super().to_representation(instance)

        response.update({
            "publisher": UserSerializer(instance.publisher).data,
            "category": instance.category.name,
            "book_type": instance.book_type.name,
            "my": instance.publisher == self.context["request"].user,
            "size": sizify(instance.file.size)
        })

        return response


class LibraryBookYearSerializer(Serializer):
    def to_representation(self, instance: int):
        response = {
            "id": instance,  # по просьбе фронта
            "checked": False,
            "name": str(instance)
        }
        return response


# VideoMaterial


class ModuleOfVideoSerializer(ModelSerializer):
    class Meta:
        model = ModuleOfVideo
        fields = ("id", "name")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["checked"] = False
        return response


class VideoMaterialShortSerializer(ModelSerializer):
    class Meta:
        model = VideoMaterial
        fields = ("id", "title", "image", "video", "module", "created_at")

    def to_representation(self, instance: VideoMaterial):
        response = super().to_representation(instance)

        response.update({
            "module": instance.module.name,
            "my": instance.publisher == self.context["request"].user
        })

        return response


class VideoMaterialSerializer(ModelSerializer):
    title = serializers.JSONField(validators=(validate_json,))
    description = serializers.JSONField(validators=(validate_json,))

    class Meta:
        model = VideoMaterial
        fields = ("id", "title", "image", "description", "video", "module", "category", "publisher")
        read_only_fields = ("id", "publisher")

    def create(self, validated_data):
        validated_data["publisher"] = self.context["request"].user
        return super().create(validated_data)

    def update(self, instance, validated_data):
        title = validated_data.pop("title", [])
        description = validated_data.pop("description", [])

        instance.title.update(title)
        instance.description.update(description)

        return super().update(instance, validated_data)

    def to_representation(self, instance: VideoMaterial):
        response = super().to_representation(instance)

        response.update({
            "publisher": UserSerializer(instance.publisher).data,
            "module": instance.module.name,
            "category": instance.category.name,
            "my": instance.publisher == self.context["request"].user,
            "size": sizify(instance.video.size),
        })

        return response
