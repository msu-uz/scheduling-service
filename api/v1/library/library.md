# Library section documentation 

## Video record create
Создание ссылки для видеозаписи 

## `POST` `/library/video_record/create` 

>REQUEST:   
title - название видеозаписи   
description - описание к видеозаписи (необязательно)   
url - ссылка к видеозаписи   (необязательно)
module - (id) название модуля видеозаписи (необязательно)     
category - (id) категория видеозаписи (необязательно)    
duration - длительность видеозаписи (необязательно)     
thumbnail - превью изображение видеозаписи (необязательно)     
lesson_event - индетификатор урока 
```json
{
  "title": "Videorecording-1",
  "description": null,
  "url": null,
  "module": null,
  "category": null,
  "duration": null,
  "thumbnail": null,
  "lesson_event": 1
}
```

>RESPONSE:    

```json
{
   "id": 6,
  "title": "Videorecording",
  "description": null,
  "url": null,
  "module": null,
  "category": null,
  "duration": null,
  "thumbnail": null,
  "lesson_event": 1
}

```

## Video record list    
Список видеозаписей    
## `GET`  `/library/video_record/list` 

>RESPONSE:      
```json
{
   
      "id": 3,
      "title": "\"Videorecording-1\"",
      "description": null,
      "url": null,
      "module": null,
      "category": null,
      "duration": null,
      "thumbnail": null,
      "lesson_event": 1
    },
    {
      "id": 4,
      "title": "Videorecording",
      "description": null,
      "url": null,
      "module": null,
      "category": null,
      "duration": null,
      "thumbnail": null,
      "lesson_event": 1
    },
}
```

## Video record update/delete/detail

>URL:    
id - идентификатор  опеределенной видеозаписи    
## `PUT` `/video_record/<int:id>/`
## `DELETE` `/video_record/<int:id>/`
## `GET` `/video_record/<int:id>/`