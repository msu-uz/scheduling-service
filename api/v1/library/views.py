from typing import List
from django.db.models.query_utils import Q

from rest_framework.filters import SearchFilter
from rest_framework.generics import (CreateAPIView,
                                     ListAPIView,
                                     RetrieveUpdateDestroyAPIView)

from api.v1.permissions import LibrarianPermission
from api.models.catalog import Category
from api.models.library import (LibraryBook,
                                ModuleOfVideo,
                                TypeOfBook,
                                VideoMaterial)

from api.v1.library.serializers import (CategorySerializer,
                                        CategoryShortSerializer,
                                        LibraryBookSerializer,
                                        LibraryBookShortSerializer,
                                        LibraryBookYearSerializer,
                                        ModuleOfVideoSerializer,
                                        TypeOfBookSerializer,
                                        VideoMaterialSerializer,
                                        VideoMaterialShortSerializer)


def read_list_query_param(param: str) -> List[str]:
    """
    Функция для перевода любой query_param строки в список строк цифр.
    """

    clean_param = ''.join(symb for symb in param if symb in "0123456789,")
    return clean_param.split(",")


class CategoryNestedListView(ListAPIView):
    """
    Список категорий с вложенными категориями.
    """

    serializer_class = CategorySerializer
    permission_classes = (LibrarianPermission,)
    queryset = Category.objects.root_nodes()


class CategoryListView(ListAPIView):
    """
    Список категорий.
    """

    serializer_class = CategoryShortSerializer
    permission_classes = (LibrarianPermission,)
    queryset = Category.objects.all()


# LibraryBook


class TypeOfBookListView(ListAPIView):
    """
    Список типов книг.
    """

    serializer_class = TypeOfBookSerializer
    permission_classes = (LibrarianPermission,)
    queryset = TypeOfBook.objects.all()


class LibraryBookCreateView(CreateAPIView):
    """
    Создание книги.
    """

    serializer_class = LibraryBookSerializer
    permission_classes = (LibrarianPermission,)


class LibraryBookListView(ListAPIView):
    """
    Список книг.

    Query params: publish_year - id
                  category     - id
                  book_type    - bool
    """

    serializer_class = LibraryBookShortSerializer
    permission_classes = (LibrarianPermission,)
    filter_backends = (SearchFilter,)
    search_fields = ("title", "author", "description")

    def get_queryset(self):
        my = self.request.query_params.get("my")
        publish_year = self.request.query_params.get("publish_year")
        category = self.request.query_params.get("category")
        book_type = self.request.query_params.get("book_type")

        query = Q()

        if isinstance(category, str):
            category = read_list_query_param(category)
            query &= Q(category__in=category)

        if isinstance(book_type, str):
            book_type = read_list_query_param(book_type)
            query &= Q(book_type__in=book_type)

        if isinstance(publish_year, str):
            publish_year = read_list_query_param(publish_year)
            query &= Q(publish_date__year__in=publish_year)

        if isinstance(my, str) and my.lower() == "true":
            user = self.request.user
            query &= Q(publisher=user)

        return LibraryBook.objects.filter(query).select_related("category",
                                                                "book_type",
                                                                "publisher")


class LibraryBookRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    """
    Просмотр, обновление, удаление книги (специальное разрешение)
    """

    serializer_class = LibraryBookSerializer
    permission_classes = (LibrarianPermission,)
    queryset = LibraryBook.objects.all()
    lookup_url_kwarg = "id"


class YearListView(ListAPIView):
    """
    Список годов издания.
    """

    serializer_class = LibraryBookYearSerializer
    permission_classes = (LibrarianPermission,)

    def get_queryset(self):
        return sorted(set(LibraryBook.objects.all().values_list("publish_date__year", flat=True)))


# VideoMaterial


class ModuleOfVideoListView(ListAPIView):
    """
    Список модулей видео.
    """

    serializer_class = ModuleOfVideoSerializer
    permission_classes = (LibrarianPermission,)
    queryset = ModuleOfVideo.objects.all()


class VideoMaterialCreateView(CreateAPIView):
    """
    Создание видеоматериала.
    """

    serializer_class = VideoMaterialSerializer
    permission_classes = (LibrarianPermission,)


class VideoMaterialListView(ListAPIView):
    """
    Список видеоматериалов.

    Query params: category - id
                  module   - id
                  my       - bool
    """

    serializer_class = VideoMaterialShortSerializer
    permission_classes = (LibrarianPermission,)
    filter_backends = (SearchFilter,)
    search_fields = ("title",)

    def get_queryset(self):
        my = self.request.query_params.get("my")
        category = self.request.query_params.get("category")
        module = self.request.query_params.get("module")

        query = Q()

        if isinstance(category, str):
            category = read_list_query_param(category)
            query &= Q(category__in=category)

        if isinstance(module, str):
            module = read_list_query_param(module)
            query &= Q(module__in=module)

        if isinstance(my, str) and my.lower() == "true":
            user = self.request.user
            query &= Q(publisher=user)

        return VideoMaterial.objects.filter(query).select_related("module",
                                                                  "category",
                                                                  "publisher")


class VideoMaterialRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    """
    Просмотр, обновление, удаление видеоматериала
    """

    serializer_class = VideoMaterialSerializer
    permission_classes = (LibrarianPermission,)
    queryset = VideoMaterial.objects.all()
    lookup_url_kwarg = "id"
