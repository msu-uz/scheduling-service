from rest_framework.serializers import ValidationError


def validate_json(value):
    if not isinstance(value, dict):
        raise ValidationError("This field must be an object.")
    if len(value) == 0:
        raise ValidationError("This field must not be empty.")
