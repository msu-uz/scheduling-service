from django.urls import path

from api.v1.resources.views import ResourceListView, ResourceCreateView, ResourceUpdateDestroyView, \
    ResourcesTypeListView

urlpatterns = [
    path('list/', ResourceListView.as_view()),
    path('create/', ResourceCreateView.as_view()),
    path('<int:id>/update/', ResourceUpdateDestroyView.as_view()),
    path('<int:id>/destroy/', ResourceUpdateDestroyView.as_view()),
    path('resourcestype/list/', ResourcesTypeListView.as_view()),
]
