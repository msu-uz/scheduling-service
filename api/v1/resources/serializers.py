from rest_framework.serializers import ModelSerializer

from api.models.resources import ResourcesType, Resource


class ResourcesTypeSerializer(ModelSerializer):
    class Meta:
        model = ResourcesType
        fields = ('id', 'name')
        read_only_fields = ('id',)


class ResourceSerializer(ModelSerializer):
    class Meta:
        model = Resource
        fields = ('id', 'name', 'type')
        read_only_fields = ('id',)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['type'] = ResourcesTypeSerializer(instance.type).data
        return response
