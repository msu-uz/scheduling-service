from rest_framework.generics import (ListAPIView, CreateAPIView, UpdateAPIView, DestroyAPIView)

from api.models.resources import Resource, ResourcesType
from api.v1.permissions import EducationOfficeUnitPermission
from api.v1.resources.serializers import ResourceSerializer, ResourcesTypeSerializer


class ResourceListView(ListAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    permission_classes = (EducationOfficeUnitPermission,)


class ResourceCreateView(CreateAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    permission_classes = (EducationOfficeUnitPermission,)


class ResourceUpdateDestroyView(UpdateAPIView, DestroyAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    permission_classes = (EducationOfficeUnitPermission,)
    lookup_url_kwarg = 'id'


class ResourcesTypeListView(ListAPIView):
    queryset = ResourcesType.objects.all()
    serializer_class = ResourcesTypeSerializer
    permission_classes = (EducationOfficeUnitPermission,)
