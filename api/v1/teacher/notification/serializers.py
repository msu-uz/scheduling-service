from collections import OrderedDict

from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import serializers
from rest_framework.fields import SerializerMethodField, SkipField
from rest_framework.relations import PKOnlyObject

from rest_framework.serializers import ModelSerializer

from api.models import Notification, NotificationTemplate, User, Applicant, StudentGroupForming, StudentGroup
from api.models.notification import PivotNotification, NotificationComment
from api.models.role import Student, Teacher
from api.v1.groups.serializers import GroupListSerializer


class ApplicantRoleSerializer(ModelSerializer):
    class Meta:
        model = Applicant
        fields = '__all__'


class ExtraApplicantRoleSerializer(ModelSerializer):
    class Meta:
        model = Applicant
        fields = ['id', 'image', 'first_name', 'second_name', 'last_name', ]


class ExtraStudentRoleSerializer(ModelSerializer):
    applicant = ExtraApplicantRoleSerializer(read_only=True)

    class Meta:
        model = Student
        fields = '__all__'


class StudentRoleSerializer(ModelSerializer):
    applicant = ApplicantRoleSerializer(read_only=True)

    class Meta:
        model = Student
        fields = '__all__'


class TeacherRoleSerializer(ModelSerializer):
    class Meta:
        model = Teacher
        fields = '__all__'


class UserListSerializer(ModelSerializer):
    student = ExtraStudentRoleSerializer(read_only=True)
    teacher = TeacherRoleSerializer(read_only=True)
    profile_image = serializers.ImageField()

    def to_representation(self, instance):
        ret = OrderedDict()
        fields = self._readable_fields

        for field in fields:
            try:
                attribute = field.get_attribute(instance)
                if attribute in (None, ''):
                    continue
            except SkipField:
                continue

            check_for_none = attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
            if check_for_none is None:
                ret[field.field_name] = None
            else:
                ret[field.field_name] = field.to_representation(attribute)

        return ret

    class Meta:
        model = User
        fields = ['id', 'profile_image', 'first_name', 'last_name', 'username', 'student', 'teacher']


class UserSerializer(ModelSerializer):
    student = StudentRoleSerializer(read_only=True)
    teacher = TeacherRoleSerializer(read_only=True)
    access_level = serializers.CharField(source='get_access_level_display')
    profile_image = serializers.ImageField()

    def to_representation(self, instance):
        ret = OrderedDict()
        fields = self._readable_fields

        for field in fields:
            try:
                attribute = field.get_attribute(instance)
                if attribute in (None, ''):
                    continue
            except SkipField:
                continue

            check_for_none = attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
            if check_for_none is None:
                ret[field.field_name] = None
            else:
                ret[field.field_name] = field.to_representation(attribute)

        return ret

    class Meta:
        model = User
        fields = ['id', 'profile_image', 'first_name', 'last_name', 'username', 'email', 'access_level', 'student',
                  'teacher']


class NotificationSerializer(ModelSerializer):
    sender = UserSerializer(read_only=True)
    group = serializers.ListField(child=serializers.IntegerField(), write_only=True, required=False)
    receiver = serializers.ListField(child=serializers.IntegerField(), required=False)

    class Meta:
        model = Notification
        fields = ['id', 'sender', 'title', 'body', 'group', 'receiver', 'created_at']

    def create(self, validated_data):
        request = self.context.get('request')
        validated_data['sender'] = request.user
        # проверка на существование
        if group := validated_data.pop('group', []):
            for g in set(group):
                get_object_or_404(StudentGroup, id=g)
        if receivers := validated_data.pop('receiver', []):
            for r in set(receivers):
                get_object_or_404(User, id=r)
        #         конец проверки
        n = super(NotificationSerializer, self).create(validated_data)
        for g in group:
            for s in StudentGroup.objects.get(id=g).get_students():
                receivers.append(Student.objects.get(id=s.id).user_id)
        if receivers:
            for rec in set(receivers):
                PivotNotification.objects.create(receiver_id=rec, notification=n)
        return n


class PivotNotificationDetailSerializer(ModelSerializer):
    notification = NotificationSerializer(read_only=True)
    receiver = UserSerializer(read_only=True)

    class Meta:
        model = PivotNotification
        fields = '__all__'


class PivotNotificationReceiverSerializer(ModelSerializer):
    receiver = SerializerMethodField('get_receivers')
    sender = UserSerializer(read_only=True)

    def get_receivers(self, obj):
        return UserSerializer(User.objects.filter(receiver_real__notification=obj),
                              many=True).data

    class Meta:
        model = Notification
        fields = '__all__'


class PivotNotificationSenderSerializer(ModelSerializer):
    notification = NotificationSerializer(read_only=True)

    class Meta:
        model = PivotNotification
        fields = '__all__'


# ----------------- Notification Template Serializers ------------------


class NotificationTemplateSerializer(ModelSerializer):
    def to_representation(self, instance):
        resp = super(NotificationTemplateSerializer, self).to_representation(instance)
        resp['sender'] = UserSerializer(instance.sender).data
        resp['receiver'] = UserSerializer(instance.receiver.all(), many=True).data
        resp['group'] = GroupListSerializer(instance.group.all(), many=True).data
        return resp

    class Meta:
        model = NotificationTemplate
        fields = ['id', 'sender', 'title', 'body', 'receiver', 'group', 'created_at', 'updated_at']

    def create(self, validated_data):
        validated_data['sender'] = self.context['request'].user
        return super(NotificationTemplateSerializer, self).create(validated_data)


class CommentCreateSerializer(ModelSerializer):

    def create(self, validated_data):
        request = self.context.get('request')
        kwargs = self.context.get('view').kwargs.get('pivot_notification_id')
        notification = get_object_or_404(Notification, pivotnotification__id=kwargs).sender
        PivotNotification.objects.filter(id=kwargs).update(receiver=notification, is_read=False)
        return NotificationComment.objects.create(writer=request.user, notification_id=kwargs,
                                                  comment=validated_data.get('comment'))

    class Meta:
        model = NotificationComment
        fields = ('comment',)


class CommentListSerializer(ModelSerializer):
    writer = serializers.CharField()
    notification = serializers.CharField(source='notification.notification.title')

    class Meta:
        model = NotificationComment
        exclude = ('updated_at',)
