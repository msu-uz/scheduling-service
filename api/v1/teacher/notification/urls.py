from .views import *
from django.urls import path

urlpatterns = [
    path('list/sent/', PivotNotificationSenderListView.as_view()),
    path('list/received/', PivotNotificationReceiverListView.as_view()),
    path('create/', NotificationCreateView.as_view()),
    path('detail/<int:id>/', NotificationDetailView.as_view()),

    path('template/list/', NotificationTemplateListView.as_view()),
    path('template/create/', NotificationTemplateCreateView.as_view()),
    path('template/detail/<int:id>/', NotificationTemplateDetailView.as_view()),
    path('template/update/<int:id>/', NotificationTemplateUpdateView.as_view()),
    path('template/delete/<int:id>/', NotificationTemplateDeleteView.as_view()),

    path('user/list/', UserListView.as_view()),

    path('<int:pivot_notification_id>/comment/create/', NotificationCommentCreateView.as_view()),
    path('<int:pivot_notification_id>/comment/list/', NotificationCommentListView.as_view()),
    path('user/list/', UserListView.as_view()),
]
