# Расписание в кабинете преподавателя

## `GET teacher/schedule/?start_date=2020-01-01&end_date=2020-01-31`

### Query params:

| Название   | Описание       |
| ---------- | -------------- |
| start_date | Начальная дата |
| end_date   | Конечная дата  |

### Response:

```json
{
  "results": [
    {
      "id": 2,
      "date": "2021-01-05",
      "module": {
        "ru": "Мясник",
        "uz": "Therapeutic radiographer"
      },
      "group": "M1",
      "interval": {
        "id": 2,
        "number": 2,
        "start_time": "12:00:00",
        "end_time": "18:00:00"
      },
      "topic": {
        "ru": "Шлифовщик",
        "uz": "Marketing executive"
      },
      "room": {
        "id": 1,
        "name": "123",
        "type": "Аудитория"
      }
    },
    {
      "id": 1,
      "date": "2021-01-27",
      "module": {
        "ru": "Квасник",
        "uz": "Pharmacist, community"
      },
      "group": "M1",
      "interval": {
        "id": 1,
        "number": 1,
        "start_time": "00:00:00",
        "end_time": "06:00:00"
      },
      "topic": {
        "ru": "Финансист",
        "uz": "Passenger transport manager"
      },
      "room": {
        "id": 1,
        "name": "123",
        "type": "Аудитория"
      }
    }
  ]
}
```
