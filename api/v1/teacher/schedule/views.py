from api.v1.teacher.schedule.serializers import LessonEventSerializer
from datetime import datetime
from api.models.schedule import LessonEvent
from api.v1.permissions import TeacherPermission
from rest_framework.generics import ListAPIView


class LessonEventListView(ListAPIView):
    serializer_class = LessonEventSerializer
    permission_classes = (TeacherPermission,)

    def get_queryset(self):
        start_date = self.request.query_params.get("start_date", "")
        end_date = self.request.query_params.get("end_date", "")
        teacher = self.request.user.teacher

        queryset = LessonEvent.objects.none()

        try:
            start_date = datetime.fromisoformat(start_date).date()
            end_date = datetime.fromisoformat(end_date).date()
        except ValueError:
            pass
        else:
            queryset = LessonEvent.objects.filter(date__gte=start_date, date__lte=end_date,
                                                  teacher=teacher).distinct().order_by("date", "interval__start_time")

        return queryset
