from api.v1.teacher.schedule.views import LessonEventListView
from django.urls import path

urlpatterns = [
    path("", LessonEventListView.as_view()),
]
