from api.models.role import Teacher
from api.models.resources import Resource
from api.models.schedule import LessonEvent, LessonEventInterval
from rest_framework.serializers import ModelSerializer


class TeacherSerializer(ModelSerializer):
    class Meta:
        model = Teacher
        fields = ("id",)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response.update({"name": str(instance), "image": instance.employee.image})
        return response


class LessonEventIntervalSerializer(ModelSerializer):
    class Meta:
        model = LessonEventInterval
        fields = ("id", "number", "start_time", "end_time")


class ResourceSerializer(ModelSerializer):
    class Meta:
        model = Resource
        fields = ("id", "name")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["type"] = instance.type.name
        return response


class LessonEventSerializer(ModelSerializer):
    class Meta:
        model = LessonEvent
        fields = ("id", "date")

    def to_representation(self, instance: LessonEvent):
        response = super().to_representation(instance)

        response.update({
            "module": instance.module_student_group.module_course.discipline.name,
            "group": instance.module_student_group.group.name,
            "interval": LessonEventIntervalSerializer(instance.interval).data,
            "topic": instance.lesson.name,
            "room": ResourceSerializer(instance.room).data
        })

        return response
