from django_filters import AllValuesMultipleFilter
from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from rest_framework import generics, status, filters
from rest_framework.response import Response

from api.models.statement import Journal, Retake
from api.models.group import ModuleStudentGroup, StudentGroup
from .serializers import *
from api.models.role import Student
from api.v1.permissions import TeacherPermission
from api.models import Course


class CourseFilter(FilterSet):
    course = AllValuesMultipleFilter(field_name='course')

    class Meta:
        model = StudentGroup
        fields = ['course']


class GroupListAPIView(generics.ListAPIView):
    queryset = StudentGroup.objects.filter(is_completed=False)
    serializer_class = GroupListStatementSerializer
    permission_classes = [TeacherPermission, ]
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_class = CourseFilter
    search_fields = ['name']


class MSGCourseFilter(FilterSet):
    course = AllValuesMultipleFilter(field_name='module_course__course')

    class Meta:
        model = ModuleStudentGroup
        fields = ['module_course']


class ModuleStudentGroupListAPIView(generics.ListAPIView):
    serializer_class = ModuleStudentGroupSerializer
    # permission_classes = [TeacherPermission, ]
    filter_backends = [DjangoFilterBackend, ]
    filterset_class = MSGCourseFilter

    def get_queryset(self):
        queryset = ModuleStudentGroup.objects.filter(teacher_id=self.request.user.teacher.id)
        return queryset


class ModuleStudentRetrieveUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ModuleStudentGroupDetailSerializer
    permission_classes = [TeacherPermission, ]
    lookup_url_kwarg = 'msg_id'

    def get_queryset(self):
        queryset = ModuleStudentGroup.objects.filter(teacher_id=self.request.user.teacher.id)
        return queryset


class JournalCreateAPIView(generics.CreateAPIView):
    queryset = Journal.objects.all()
    serializer_class = JournalSerializer
    permission_classes = [TeacherPermission, ]


class JournalRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = JournalSerializer
    permission_classes = [TeacherPermission, ]
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        queryset = Journal.objects.filter(module_student_group__teacher_id=self.kwargs['id'])
        return queryset


class JournalControlListAPIView(generics.ListAPIView):
    serializer_class = JournalControlSerializer
    permission_classes = [TeacherPermission, ]
    lookup_url_kwarg = 'msg_id'

    def get_queryset(self):
        queryset = ModuleStudentGroup.objects.filter(id=self.kwargs['msg_id']).first().get_students()
        return queryset


class RetakeCreateAPIView(generics.CreateAPIView):
    queryset = Retake.objects.all()
    serializer_class = RetakeSerializer
    permission_classes = [TeacherPermission, ]

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instances = self.perform_create(serializer)
        instance_serializer = RetakeListSerializer(instances, many=True)
        return Response(instance_serializer.data, status=status.HTTP_201_CREATED)


class RetakeUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = Retake.objects.all()
    serializer_class = RetakeListSerializer
    permission_classes = [TeacherPermission, ]

    lookup_url_kwarg = 'id'


class StudentExpel(generics.RetrieveUpdateAPIView):
    queryset = Student.objects.all()
    serializer_class = StatementStudentSerializer
    permission_classes = [TeacherPermission, ]
    lookup_url_kwarg = 'id'


class CourseListAPIView(generics.ListAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseListSerializer
    permission_classes = [TeacherPermission, ]
