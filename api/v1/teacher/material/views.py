from django.db.models.query_utils import Q
from django_filters.rest_framework import FilterSet, DjangoFilterBackend
from rest_framework import filters
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView

from api.models import Student, Teacher, StudentGroup, LessonEvent, Course, ModuleCourse, TaskCollection, ModuleWeekType
from api.models.library import LessonMaterial
from .serializers import (LessonMaterialSerializer,
                          LessonMaterialCreateUpdateDestroySerializer, StudentSerializer, TeacherSerializer,
                          StudentGroupSerializer, LessonEventListSerializer, CourseListSerializer, ModuleListSerializer,
                          TaskCollectionListSerializer)
from ...permissions import TeacherPermission


class LessonMaterialListView(ListAPIView):
    serializer_class = LessonMaterialSerializer
    permission_classes = [TeacherPermission]

    def get_queryset(self):
        lesson_event = self.request.query_params.get("lesson_event")
        author_queryset = Q(lesson_event__teacher__user__id=self.request.user.id, lesson_event=lesson_event)
        teacher_queryset = Q(teacher__user__id=self.request.user.id, lesson_event=lesson_event)
        qs = author_queryset | teacher_queryset
        return LessonMaterial.objects.filter(qs).select_related("lesson_event")


class LessonMaterialCreateView(CreateAPIView):
    serializer_class = LessonMaterialCreateUpdateDestroySerializer
    permission_classes = [TeacherPermission]
    queryset = LessonMaterial.objects.all()


class LessonMaterialRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    serializer_class = LessonMaterialCreateUpdateDestroySerializer
    permission_classes = [TeacherPermission]
    queryset = LessonMaterial.objects.all()
    lookup_url_kwarg = "id"


class StudentListView(ListAPIView):
    serializer_class = StudentSerializer
    permission_classes = [TeacherPermission]

    def get_queryset(self):
        qs = Student.objects.filter(
            studentgroupforming__group__modulestudentgroup__teacher=self.request.user.teacher).distinct()
        return qs


class TeacherListView(ListAPIView):
    serializer_class = TeacherSerializer
    permission_classes = [TeacherPermission]
    queryset = Teacher.objects.all()


class StudentGroupListView(ListAPIView):
    serializer_class = StudentGroupSerializer
    permission_classes = [TeacherPermission]

    def get_queryset(self):
        qs = StudentGroup.objects.filter(modulestudentgroup__teacher=self.request.user.teacher,
                                         is_completed=False).distinct()
        return qs


class CourseListView(ListAPIView):
    serializer_class = CourseListSerializer
    permission_classes = [TeacherPermission]
    queryset = Course.objects.all()


class ModuleListView(ListAPIView):
    serializer_class = ModuleListSerializer
    permission_classes = [TeacherPermission]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ["course"]
    queryset = ModuleCourse.objects.all()


class LessonEventListView(ListAPIView):
    serializer_class = LessonEventListSerializer
    permission_classes = [TeacherPermission]

    def get_queryset(self):
        module_id = self.request.query_params.get("module_id", None)
        qs = Q(teacher=self.request.user.teacher)
        if module_id:
            qs &= Q(module_student_group__module_course=module_id)
        return LessonEvent.objects.filter(qs).select_related("module_student_group__module_course")


class TaskCollectionListView(ListAPIView):
    serializer_class = TaskCollectionListSerializer
    permission_classes = [TeacherPermission]

    def get_queryset(self):
        lesson_event = self.request.query_params.get("lesson_event")
        qs = TaskCollection.objects.filter(module_week_type__lessonevent=lesson_event, is_active=True).distinct()
        return qs


class TaskCollectionDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = TaskCollectionListSerializer
    # permission_classes = [TeacherPermission]
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        lesson_event = self.request.query_params.get("lesson_event")
        qs = TaskCollection.objects.filter(module_week_type__lessonevent=lesson_event, is_active=True).distinct()
        return qs
