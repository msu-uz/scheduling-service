from rest_framework import serializers
from rest_framework.exceptions import ValidationError, PermissionDenied

from api.models import LessonEvent, Student, Teacher, StudentGroup, Course, ModuleCourse, TaskCollection, Answer, Task, \
    IntermediateControlTopics
from api.models.library import LessonMaterial
from api.v1.discipline.generator import easy_generate
from api.v1.teacher.task.service import task_update


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('id', 'user', 'applicant', 'employee', 'course_type')

    def to_representation(self, instance: Student):
        response = super().to_representation(instance)
        if instance.applicant:
            response["name"] = instance.applicant.first_name + ' ' + instance.applicant.last_name

        elif instance.employee:
            response["name"] = instance.employee.first_name + ' ' + instance.employee.last_name

        return response


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = ('id', 'user', 'employee', 'disciplines')

    def to_representation(self, instance: Teacher):
        response = super().to_representation(instance)
        if instance.employee:
            response["name"] = instance.employee.first_name + ' ' + instance.employee.last_name
        else:
            response["name"] = str(instance)
        return response


class StudentGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentGroup
        fields = ('id', 'name', 'department', 'course', 'is_completed')


class LessonMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = LessonMaterial
        fields = ('id', 'title', 'file', 'lesson_event', 'teacher', 'student', 'student_group')
        read_only_fields = ("id",)

    def to_representation(self, instance):
        response = super(LessonMaterialSerializer, self).to_representation(instance)
        response.update(
            {
                "created_at": instance.created_at.date()
            }
        )
        return response


class LessonMaterialCreateUpdateDestroySerializer(serializers.ModelSerializer):
    student = serializers.PrimaryKeyRelatedField(many=True, queryset=Student.objects.all())
    teacher = serializers.PrimaryKeyRelatedField(many=True, queryset=Teacher.objects.all())
    student_group = serializers.PrimaryKeyRelatedField(many=True, queryset=StudentGroup.objects.all())

    class Meta:
        model = LessonMaterial
        fields = ('id', 'title', 'file', 'lesson_event', 'teacher', 'student', 'student_group')
        read_only_fields = ("id",)

    def validate_lesson_event(self, value):
        teacher = self.context["request"].user.teacher
        lesson_event = LessonEvent.objects.filter(id=value.id, teacher=teacher).first()
        if lesson_event:
            return value
        raise PermissionDenied("Нельзя загрузить материал, если преподаватель не вел этот урок")

    def update(self, instance, validated_data):
        validated_data.pop("lesson_event", None)
        return super(LessonMaterialCreateUpdateDestroySerializer, self).update(instance, validated_data)


class LessonEventListSerializer(serializers.ModelSerializer):
    class Meta:
        model = LessonEvent
        fields = ("id", "module_student_group", "lesson")

    def to_representation(self, instance: LessonEvent):
        response = super().to_representation(instance)
        if instance.lesson.lesson:
            response.update({
                "theme": instance.lesson.lesson.lesson.name
            })
        elif instance.lesson.fin_control:
            response.update({
                "theme": instance.lesson.fin_control.final_control.name
            })
        elif instance.lesson.int_control:
            response.update({
                "theme": instance.lesson.int_control.intermediate_control.name
            })
        # тут гарантируется что taskcollection с is_active = True будет единственным
        if instance.lesson.taskcollection_set.count():
            response.update({
                "question_count": instance.lesson.taskcollection_set.filter(is_active=True).first().tasks.count()
            })
        else:
            response.update({
                "question_count": 0
            })
        if instance.lessonmaterial_set.count():
            response["material_count"] = instance.lessonmaterial_set.count()
        else:
            response["material_count"] = 0
        return response


class CourseListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ("id", "name")


class ModuleListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModuleCourse
        fields = ("id", "course", "discipline")

    def to_representation(self, instance: ModuleCourse):
        response = super().to_representation(instance)
        response.update({
            "module_name": instance.discipline.name
        })
        return response


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ["id", "task", "answer", "is_correct"]


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ["id", "question"]

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["answers"] = AnswerSerializer(instance.answer_set, many=True).data
        return response


class TaskCollectionListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False, required=False)
    function = serializers.ChoiceField(choices=("REGENERATE", "ADD_TASK"),
                                       write_only=True, required=False)
    arguments = serializers.ListField(write_only=True, required=False)
    action = serializers.ChoiceField(choices=("DELETE", "REPLACE", "ADD"), write_only=True, required=False)
    chosen_tasks = serializers.ListField(write_only=True, required=False)
    topics = serializers.ListField(write_only=True, required=False)

    class Meta:
        model = TaskCollection
        fields = ["id",
                  "module_week_type",
                  "function",
                  "arguments",
                  "action",
                  "chosen_tasks",
                  "topics",
                  "tasks"]

    def to_representation(self, instance: TaskCollection):
        response = super().to_representation(instance)
        if instance.module_week_type.lesson:
            response["theme"] = instance.module_week_type.lesson.lesson.name
        elif instance.module_week_type.int_control:
            response["theme"] = instance.module_week_type.int_control.intermediate_control.name
        elif instance.module_week_type.fin_control:
            response["theme"] = instance.module_week_type.fin_control.final_control.name

        response["questions"] = TaskSerializer(instance.tasks.all(), many=True).data
        return response

    def update(self, instance, validated_data):
        print(validated_data)
        int_control_topic = IntermediateControlTopics.objects.filter(
            intermediate_control_id=instance.module_week_type.int_control.intermediate_control).values_list('topic_id',
                                                                                                            flat=True)
        task_collection_id = instance.id
        validated_data['task_collection_id'] = task_collection_id
        validated_data['module_week_type'] = instance.module_week_type
        function = validated_data.get('function')
        arguments = validated_data.get('arguments')
        action = validated_data.get('action')
        chosen_tasks = validated_data.get('chosen_tasks')
        topics = validated_data.get('topics')
        # print(function, action, arguments, chosen_tasks, topics)
        task_update(validated_data)
        if function:
            pass
        elif action:
            pass
        else:
            ValidationError("WTF BRO?!")
        # TODO: logic :)
        return easy_generate(instance.module_week_type, int_control_topic, task_collection_id=task_collection_id,
                             change_tasks=[4940], difficulty=None, question_count=None, deadline=2)
