# Материалы в кабинете преподавателя

##  Список тем (уроков) на первой странице материалов

### `GET` `/teacher/material/lesson/list/?module_id=1`   

>REQUEST:   
module_id - идентификатор модуля   
>RESPONSE   
```json
[
    {
      "id": 1,
      "module_student_group": 6,
      "lesson": 1,
      "theme": {
        "ru": "Сметчик",
        "uz": "Advertising art director"
      },
      "question_count": 6,
      "material_count": 25
    }
  ]
```

## Ссылки для фильтрации уроков по курсу и модулю. Сам модуль фильтруется по курсу

### `GET` `/teacher/material/course/list/`
>RESPONSE:    
````json
    {
      "id": 1,
      "name": {
        "ru": "Астроном",
        "uz": "Commercial art gallery manager"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Учитель",
        "uz": "Visual merchandiser"
      }
    },
    {
      "id": 3,
      "name": {
        "ru": "Сыровар",
        "uz": "Early years teacher"
      }
    },
    {
      "id": 4,
      "name": {
        "ru": "Автогонщик",
        "uz": "Accountant, chartered certified"
      }
    }
  ]
}
````

### `GET` `/teacher/material/module/list/?course=1`   
>REQUEST:    
course - индетификатор курса    
> RESPONSE:   
```json
{
      "id": 1,
      "course": 1,
      "discipline": 3,
      "module_name": {
        "ru": "Радиолог",
        "uz": "Clinical scientist, histocompatibility and immunogenetics"
      }
    },
    {
      "id": 2,
      "course": 1,
      "discipline": 2,
      "module_name": {
        "ru": "Бактериолог",
        "uz": "Barista"
      }
    },
    {
      "id": 3,
      "course": 1,
      "discipline": 4,
      "module_name": {
        "ru": "Мясник",
        "uz": "Soil scientist"
      }
    },
    {
      "id": 4,
      "course": 1,
      "discipline": 4,
      "module_name": {
        "ru": "Мясник",
        "uz": "Soil scientist"
      }
    },
    {
      "id": 5,
      "course": 1,
      "discipline": 2,
      "module_name": {
        "ru": "Бактериолог",
        "uz": "Barista"
      }
    }
  ]
}
```

## Создание рассылки преподавателем    
###  `POST` `/teacher/material/create/`    
>REQUEST:     
```json
 [
    {
      
      "title": {
        "ru": "Материал",
        "uz": "Material"
      },
      "file": "http://127.0.0.1:8000/media/lesson_material/files/%D0%92%D1%83%D0%BB%D1%84_%D0%A2%D0%BE%D0%BC_%D0%9C%D1%83%D0%B6%D1%87%D0%B8%D0%BD%D0%B0_%D0%B2_%D0%BF%D0%BE%D0%BB%D0%BD%D1%8B%D0%B9_%D1%80%D0%BE%D1%81%D1%82_A_Man_in_Full_royallib_com_fb2.zip",
      "lesson_event": 1,
      "teacher": [
        1
      ],
      "student": [
        1
      ],
      "student_group": [
        1
      ],
      "created_at": "2021-01-18"
    }
  ]
```

>RESPONSE:     
```json
  [
    {
      "id": 1,
      "title": {
        "ru": "Материал",
        "uz": "Material"
      },
      "file": "http://127.0.0.1:8000/media/lesson_material/files/%D0%92%D1%83%D0%BB%D1%84_%D0%A2%D0%BE%D0%BC_%D0%9C%D1%83%D0%B6%D1%87%D0%B8%D0%BD%D0%B0_%D0%B2_%D0%BF%D0%BE%D0%BB%D0%BD%D1%8B%D0%B9_%D1%80%D0%BE%D1%81%D1%82_A_Man_in_Full_royallib_com_fb2.zip",
      "lesson_event": 1,
      "teacher": [
        1
      ],
      "student": [
        1
      ],
      "student_group": [
        1
      ],
      "created_at": "2021-01-18"
    }
  ]

```

## Список материалов конкретного преподавателя для конкретного урока  
### `GET` `/teacher/material/list/?lesson_event=1`   
>REQUEST:     
lesson_event - идентификатор урока    
>RESPONSE:     
```json
[
    {
      "id": 2,
      "title": "\"C programming language\"",
      "file": "http://127.0.0.1:8000/media/lesson_material/files/yay-git-10.1.2.r0.g7d849a8-1-x86_64.pkg.tar.zst",
      "lesson_event": 1,
      "teacher": [
        1
      ],
      "student": [],
      "student_group": [
        1
      ],
      "created_at": "2021-01-13"
    },
    {
      "id": 3,
      "title": "\"C programming language\"",
      "file": "http://127.0.0.1:8000/media/lesson_material/files/yay-git-10.1.2.r0.g7d849a8-1-x86_64.pkg.tar_Bduzjre.zst",
      "lesson_event": 1,
      "teacher": [
        2
      ],
      "student": [],
      "student_group": [
        1
      ],
      "created_at": "2021-01-13"
    }
]
```
## Обновление и удаление материалов конкретного урока конкретным преподавателем 
### `PUT` `/teacher/material/update/<int:id>/`    
>REQUEST:    
id - идентификатор материала     

### `DELETE` `/teacher/material/delete/<int:id>/`
>REQUEST:    
id - идентификатор материала     

### `GET` `/teacher/material/detail/<int:id>/`    
>REQUEST:    
id - идентификатор материала     
```json
{
  "id": 1,
  "title": {
    "ru": "Материал",
    "uz": "Material"
  },
  "file": "http://127.0.0.1:8000/media/lesson_material/files/%D0%92%D1%83%D0%BB%D1%84_%D0%A2%D0%BE%D0%BC_%D0%9C%D1%83%D0%B6%D1%87%D0%B8%D0%BD%D0%B0_%D0%B2_%D0%BF%D0%BE%D0%BB%D0%BD%D1%8B%D0%B9_%D1%80%D0%BE%D1%81%D1%82_A_Man_in_Full_royallib_com_fb2.zip",
  "lesson_event": 1,
  "teacher": [
    1
  ],
  "student": [
    1
  ],
  "student_group": [
    1
  ]
}
```



## Список преподавателей, студентов(которым ведет этот препод), список групп для отправки материалов    
### `GET` `/teacher/material/teacher/list/`
```json
[
    {
      "id": 1,
      "user": 11,
      "employee": null,
      "disciplines": [],
      "name": "dor"
    },
    {
      "id": 2,
      "user": 8,
      "employee": null,
      "disciplines": [],
      "name": "Сысоева Варвара"
    }
  ]

```

### `GET`   `/teacher/material/student_group/list/` 
```json
[
    {
      "id": 1,
      "name": "M1-17",
      "department": null,
      "course": 1,
      "is_completed": false
    },
    {
      "id": 2,
      "name": "M2-17",
      "department": null,
      "course": 2,
      "is_completed": false
    }
  ]
```
### `GET` `/teacher/material/student/list`
```json
[
    {
      "id": 1,
      "user": 1,
      "applicant": 1,
      "employee": null,
      "course_type": 4,
      "name": "Белоусова Элеонора"
    },
    {
      "id": 2,
      "user": 2,
      "applicant": 2,
      "employee": null,
      "course_type": 2,
      "name": "Касьян Артурович"
    },
    {
      "id": 3,
      "user": 3,
      "applicant": 3,
      "employee": null,
      "course_type": 4,
      "name": "Прокл Игоревич"
    }
]
```

## Тема, список вопросов и ответов в разеделе материалы
### `GET` `/teacher/material/question/list/`?lesson_event=1

```json

 [
    {
      "id": 1,
      "module_week_type": 1,
      "tasks": [
        1,
        2,
        3,
        4,
        5,
        9
      ],
      "theme": {
        "ru": "Сметчик",
        "uz": "Advertising art director"
      },
      "questions": [
        {
          "id": 1,
          "question": {
            "ru": "Роскошный вариант медицина пятеро набор картинка поколение.",
            "uz": "Certain decide others view foreign seem major."
          },
          "answers": []
        },
        {
          "id": 2,
          "question": {
            "ru": "Эффект подробность господь разводить равнодушный сынок команда следовательно.",
            "uz": "Speech put training base challenge audience."
          },
          "answers": [
            {
              "id": 2,
              "task": 2,
              "answer": {
                "ru": [
                  "Угроза нервно совещание коричневый сынок лететь. Опасность совещание термин недостаток боец наслаждение поговорить. Правый степь каюта тута палата термин.",
                  "Важный запретить жестокий естественный. Господь салон светило лететь. Уточнить инвалид пасть житель неожиданно.",
                  "Освободить падать коммунизм печатать. Падаль табак забирать редактор господь факультет. Монета покинуть пламя зато приятель важный. Недостаток инструкция военный выкинуть порог куча.",
                  "Вздрогнуть кузнец издали сбросить трясти мягкий хозяйка. Тяжелый белье вперед. Кидать мусор интеллектуальный привлекать тусклый бабочка."
                ],
                "uz": [
                  "Defense writer song morning true. Improve case so project that born she degree.",
                  "Hospital democratic road member ground. Range yes inside out site. Teach away best crime first available.",
                  "Thank accept yourself meet work group. Painting interview Republican TV again direction skin go."
                ]
              },
              "is_correct": true
            }
          ]
]
```