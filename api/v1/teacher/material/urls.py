from django.urls import include, path
from .views import *

urlpatterns = [
    path("create/", LessonMaterialCreateView.as_view()),
    path("list/", LessonMaterialListView.as_view()),
    path("update/<int:id>/", LessonMaterialRetrieveUpdateDestroyView.as_view()),
    path("delete/<int:id>/", LessonMaterialRetrieveUpdateDestroyView.as_view()),
    path("detail/<int:id>/", LessonMaterialRetrieveUpdateDestroyView.as_view()),

    # Для рассылки пользователям
    path("student/list/", StudentListView.as_view()),
    path("teacher/list/", TeacherListView.as_view()),
    path("student_group/list/", StudentGroupListView.as_view()),

    # Для фильтрации
    path("course/list/", CourseListView.as_view()),
    path("module/list/", ModuleListView.as_view()),

    # Список тем
    path("lesson/list/", LessonEventListView.as_view()),

    # Список вопросов
    path("question/list/", TaskCollectionListView.as_view()),
    path("question/<int:id>/", TaskCollectionDetailView.as_view()),
    # path("question/regenerate/",),
    # path("question/add/",),
    # path("question/update/",),
    # path("question/change/",),
    # path("question/delete/",),


]
