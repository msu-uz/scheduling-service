from api.v1.teacher.feedback.views import (ModuleCourseFeedbackQuestionListStatisticsView,
                                           ModuleCourseListView)
from django.urls import path

urlpatterns = [
    path("module/list/", ModuleCourseListView.as_view()),
    path("module/<int:module_id>/question/list/statistics/",
         ModuleCourseFeedbackQuestionListStatisticsView.as_view()),
]
