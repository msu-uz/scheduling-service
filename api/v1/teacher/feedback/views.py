from api.models.discipline import Course, ModuleCourse
from api.models.feedback import (
    CourseFeedbackComment, ModuleCourseFeedback,
    ModuleCourseFeedbackQuestion,
)
from api.v1.permissions import TeacherPermission
from api.v1.teacher.feedback.serializers import (
    CourseFeedbackCommentSerializer,
    ModuleCourseFeedbackQuestionStatisticsSerializer,
    ModuleCourseSerializer,
)
from rest_framework.generics import ListAPIView
from rest_framework.exceptions import NotFound


class ModuleCourseListView(ListAPIView):
    """
    Список модулей для курса для данного преподавателя.
    """

    serializer_class = ModuleCourseSerializer
    permission_classes = (TeacherPermission,)

    def get_queryset(self):
        teacher = self.request.user.teacher

        return ModuleCourse.objects.filter(
            modulestudentgroup__teacher=teacher).distinct().select_related("discipline")


class ModuleCourseFeedbackQuestionListStatisticsView(ListAPIView):
    """
    Список вопросов по модулю со статистикой по ответам.
    """

    serializer_class = ModuleCourseFeedbackQuestionStatisticsSerializer
    permission_classes = (TeacherPermission,)

    def get_queryset(self):
        module_id = self.kwargs["module_id"]

        return ModuleCourseFeedbackQuestion.objects.filter(
            module_course_id=module_id
        ).prefetch_related(
            "modulecoursefeedbackanswer_set",
            "modulecoursefeedbackanswer_set__module_course_feedback_question",
        )

    def get(self, request, *args, **kwargs):
        module_id = self.kwargs["module_id"]
        module = ModuleCourse.objects.filter(id=module_id).first()

        if not module:
            raise NotFound("Module not found")

        response = self.list(request, *args, **kwargs)
        feedback_count = ModuleCourseFeedback.objects.filter(
            module_course_feedback_answer__module_course_feedback_question__module_course=module).count()

        response.data.update({"feedback_count": feedback_count})
        return response
