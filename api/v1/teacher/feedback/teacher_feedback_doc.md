# Обратная связь в кабинете преподавателя

## `GET - teacher/feedback/module/list/`

Список модулей для преподавателя.

### Response:

```json
[
  {
    "id": 4,
    "name": {
      "ru": "Маркшейдер",
      "uz": "Chartered public finance accountant"
    }
  }
]
```

---

## `GET - feedback/module/<int:module_id>/question/list/statistics/`

Подробная статистика по модулю (список вопросов со статистикиой по каждому варианту ответа).

### URL params:

| Название  | Описание             |
| --------- | -------------------- |
| module_id | Идентификатор модуля |

### Response:

```json
{
  "results": [
    {
      "id": 3,
      "text": "Вопрос 1 по модулю 1",
      "answers": [
        {
          "id": 17,
          "text": "Вариант 1.1.1",
          "percentage": 0
        },
        {
          "id": 18,
          "text": "Вариант 1.1.2",
          "percentage": 0
        },
        {
          "id": 19,
          "text": "Вариант 1.1.3",
          "percentage": 0
        }
      ]
    }
  ],
  "feedback_count": 5
}
```

---
