from api.models.role import Student
from api.models.discipline import ModuleCourse
from api.models.feedback import (
    CourseFeedbackComment,
    ModuleCourseFeedback,
    ModuleCourseFeedbackAnswer,
    ModuleCourseFeedbackQuestion,
)
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer


# ModuleCourse


class ModuleCourseSerializer(ModelSerializer):
    class Meta:
        model = ModuleCourse
        fields = ("id",)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["name"] = instance.discipline.name
        return response


class ModuleCourseFeedbackAnswerStatisticsSerializer(ModelSerializer):
    """
    Использутся для получения статистики по ModuleCourseFeedbackAnswer.

    Fields:
    percentage - содержит процент таких ответов в отношении к общему
                 количеству ответов на вопрос, соответствующий этому ответу.
    """

    percentage = serializers.SerializerMethodField()

    class Meta:
        model = ModuleCourseFeedbackAnswer
        fields = ("id", "text", "percentage")

    def get_percentage(self, instance):
        question = instance.module_course_feedback_question

        all_feedback_count = ModuleCourseFeedback.objects.filter(
            module_course_feedback_answer__module_course_feedback_question=question
        ).count()

        if all_feedback_count:
            this_feedback_count = instance.modulecoursefeedback_set.count()
            return this_feedback_count * 100 // all_feedback_count
        else:
            return 0


class ModuleCourseFeedbackQuestionStatisticsSerializer(ModelSerializer):
    """
    Использутся для получения ModuleCourseFeedbackQuestion и списка
    ModuleCourseFeedbackAnswer с соответствующей статистикой.
    """

    answers = ModuleCourseFeedbackAnswerStatisticsSerializer(
        many=True, source="modulecoursefeedbackanswer_set"
    )

    class Meta:
        model = ModuleCourseFeedbackQuestion
        fields = ("id", "text", "answers")


# CourseFeedbackComment


class StudentSerializer(ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = Student
        fields = ("id", "image")

    def get_image(self, instance):
        if instance.applicant:
            return self.context["request"].build_absolute_uri(
                instance.applicant.image.url
            )
        elif instance.employee:
            return instance.employee.image

    def to_representation(self, instance):
        response = super().to_representation(instance)

        if instance.applicant:
            response["first_name"] = instance.applicant.first_name
            response["last_name"] = instance.applicant.last_name
            response["second_name"] = instance.applicant.second_name
        elif instance.employee:
            response["first_name"] = instance.employee.first_name
            response["last_name"] = instance.employee.last_name
            response["second_name"] = instance.employee.second_name

        return response


class CourseFeedbackCommentSerializer(ModelSerializer):
    student = StudentSerializer()

    class Meta:
        model = CourseFeedbackComment
        fields = ("id", "text", "student")
