from rest_framework.exceptions import ValidationError

from api.models import TaskType, Difficulty, Type, TaskCollection, Task
from api.v1.discipline.generator import *


def function_switcher(function, data):
    module_week_type = data.get('module_week_type')
    topics = data.get('topics')
    arguments = data.get('arguments')
    task_collection_id = data.get('task_collection_id')
    if function == "REGENERATE":
        return regenerate(module_week_type, topics, arguments, task_collection_id)
    elif function == "ADD_TASK":
        return add_task(module_week_type, topics, arguments, task_collection_id, None)


def action_switcher(action, data):
    task_collection_id = data.get('task_collection_id')
    chosen_tasks = data.get("chosen_tasks")
    if action == "DELETE":
        return action_delete(task_collection_id, chosen_tasks)
    elif action == "REPLACE":
        return action_replace(task_collection_id, chosen_tasks)
    elif action == "ADD":
        return action_add(task_collection_id, chosen_tasks)


def task_update(validated_data):
    function = validated_data.get('function')
    action = validated_data.get('action')

    if function:
        validated_data.pop('function')
        return function_switcher(function, validated_data)
    elif action:
        validated_data.pop('action')
        return action_switcher(action, validated_data)
    else:
        raise ValidationError("WTF BRO?!")
