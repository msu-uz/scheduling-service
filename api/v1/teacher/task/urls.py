from api.v1.teacher.task.views import (
    HomeTaskAnswerReviewCreateUpdateView,
    HomeTaskCreateView,
    HomeTaskDetailView,
    HomeTaskListView,
    HomeTaskReceiverDetailView,
    HomeTaskSendView,
    ModuleCourseListView,
    ShortHomeTaskReceiverListView,
    StudentGroupListView,
    StudentListView, TaskCollectionListView, TaskCollectionDetailView,
    TestBaseListView, DifficultyListView, DisciplineListAPIView,
    TopicListAPIView, TaskTypeListView, TaskCollectionSendView, LessonEventListView
)
from django.urls import path

urlpatterns = [
    path("list/", HomeTaskListView.as_view()),
    path("<int:id>/", HomeTaskDetailView.as_view()),
    path("create/", HomeTaskCreateView.as_view()),
    path("<int:id>/send/", HomeTaskSendView.as_view()),
    path("modulecourse/list/", ModuleCourseListView.as_view()),
    path("group/list/", StudentGroupListView.as_view()),
    path("student/list/", StudentListView.as_view()),
    path("<int:module_id>/hometask/list/", ShortHomeTaskReceiverListView.as_view()),
    path("hometask/<int:id>/", HomeTaskReceiverDetailView.as_view()),
    path("hometaskreview/create/", HomeTaskAnswerReviewCreateUpdateView.as_view()),
    path("hometaskreview/<int:id>/update/", HomeTaskAnswerReviewCreateUpdateView.as_view()),
    path("taskcollection/list/", TaskCollectionListView.as_view()),
    path("taskcollection/<int:id>/", TaskCollectionDetailView.as_view()),
    path("taskcollection/<int:id>/send/", TaskCollectionSendView.as_view()),
    path("testbase/list/", TestBaseListView.as_view()),
    path('difficulty/list/', DifficultyListView.as_view()),
    path('task_type/list/', TaskTypeListView.as_view()),
    path('discipline/list/', DisciplineListAPIView.as_view()),
    path('topic/list/', TopicListAPIView.as_view()),
    path('event/list/', LessonEventListView.as_view()),
]
