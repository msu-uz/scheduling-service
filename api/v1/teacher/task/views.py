from datetime import datetime
from typing import List

from django_filters import FilterSet, NumberFilter, AllValuesFilter, DateFromToRangeFilter, DateFilter
from django_filters.rest_framework import DjangoFilterBackend

from django.db.models.query_utils import Q
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView, UpdateAPIView, \
    RetrieveUpdateDestroyAPIView

from api.models import (HomeTask, HomeTaskAnswerReview, HomeTaskReceiver, ModuleCourse, Student, StudentGroup,
                        TaskCollection, Task, Topic, Difficulty, TaskType, LessonEvent)
from api.v1.permissions import TeacherPermission
from api.v1.teacher.task.serializers import (HomeTaskAnswerReviewCreateSerializer, HomeTaskReceiverDetailSerializer,
                                             HomeTaskReceiverShortSerializer, HomeTaskSendSerializer,
                                             HomeTaskSerializer, HomeTaskShortSerializer, ModuleCourseSerializer,
                                             StudentGroupSerializer, StudentSerializer, TaskCollectionListSerializer,
                                             TaskCollectionDetailSerializer, TaskListSerializer,
                                             TaskTypeListSerializer, TopicShortSerializer, DifficultySerializer,
                                             DisciplineShortSerializer, TaskCollectionSendSerializer,
                                             LessonEventSerializer)


class HomeTaskListView(ListAPIView):
    """
    Список внеклассных заданий, созданных данным преподавателем.
    """

    serializer_class = HomeTaskShortSerializer
    permission_classes = (TeacherPermission,)

    def get_queryset(self):
        return HomeTask.objects.filter(creator__user=self.request.user).select_related("module_course")


class HomeTaskDetailView(RetrieveAPIView):
    """
    Детальный просмотр внеклассного задания.
    """

    serializer_class = HomeTaskSerializer
    permission_classes = (TeacherPermission,)
    lookup_url_kwarg = "id"

    def get_queryset(self):
        return HomeTask.objects.filter(creator__user=self.request.user).prefetch_related(
            "hometaskfiles_set", "hometaskreceiver_set").select_related("module_course")


class HomeTaskCreateView(CreateAPIView):
    """
    Создание внеклассного задания.
    """

    serializer_class = HomeTaskSerializer
    permission_classes = (TeacherPermission,)
    queryset = HomeTask.objects.all()


class HomeTaskSendView(CreateAPIView):
    """
    Отправка внеклассного задания.
    """

    serializer_class = HomeTaskSendSerializer
    permission_classes = (TeacherPermission,)
    queryset = HomeTask.objects.all()


class ModuleCourseListView(ListAPIView):
    """
    Список модулей (предметов).
    """

    serializer_class = ModuleCourseSerializer
    permission_classes = (TeacherPermission,)

    def get_queryset(self):
        teacher = self.request.user.teacher
        return ModuleCourse.objects.filter(modulestudentgroup__teacher=teacher).distinct()


class StudentListView(ListAPIView):
    """
    Список студентов для данного преподавателя.

    Query params: group_id
    """

    serializer_class = StudentSerializer
    permission_classes = (TeacherPermission,)

    def get_queryset(self):
        group_id = self.request.query_params.get("group_id")
        teacher = self.request.user.teacher

        query = Q(studentgroupforming__group__modulestudentgroup__teacher=teacher)
        if group_id:
            query &= Q(studentgroupforming__group__id=group_id)

        return Student.objects.filter(query).distinct()


class StudentGroupListView(ListAPIView):
    """
    Список групп.
    """

    serializer_class = StudentGroupSerializer
    permission_classes = (TeacherPermission,)

    def get_queryset(self):
        teacher = self.request.user.teacher
        return StudentGroup.objects.filter(modulestudentgroup__teacher=teacher).distinct()


class HomeTaskAnswerReviewCreateUpdateView(CreateAPIView, UpdateAPIView):
    """
    Создание или обновление проверки ответа на внеклассное задание.
    """

    serializer_class = HomeTaskAnswerReviewCreateSerializer
    permission_classes = (TeacherPermission,)
    lookup_url_kwarg = "id"
    queryset = HomeTaskAnswerReview.objects.all()


class ShortHomeTaskReceiverListView(ListAPIView):
    serializer_class = HomeTaskReceiverShortSerializer
    permission_classes = (TeacherPermission,)

    def get_queryset(self):
        module_id = self.kwargs["module_id"]
        teacher = self.request.user.teacher
        return HomeTaskReceiver.objects.filter(home_task__creator=teacher,
                                               home_task__module_course=module_id).select_related(
                                                   "hometaskanswer", "hometaskanswer__hometaskanswerreview")


class HomeTaskReceiverDetailView(RetrieveAPIView):
    serializer_class = HomeTaskReceiverDetailSerializer
    permission_classes = (TeacherPermission,)
    lookup_url_kwarg = "id"
    queryset = HomeTaskReceiver.objects.all()


# ///////////////////// TASK COLLECTION ///////////////////////////
def read_list_query_param(param: str) -> List[str]:
    """
    Функция для перевода любой query_param строки в список строк цифр.(Амир)
    """

    clean_param = ''.join(symb for symb in param if symb in "0123456789,")
    return clean_param.split(",")


class TaskCollectionSendView(CreateAPIView):
    queryset = TaskCollection.objects.all()
    serializer_class = TaskCollectionSendSerializer
    permission_classes = [TeacherPermission]


class TaskCollectionListView(ListAPIView):
    serializer_class = TaskCollectionListSerializer
    permission_classes = [TeacherPermission]

    def get_queryset(self):
        module = self.request.query_params.get("module")
        topic = self.request.query_params.get("topic")
        query = Q(parent__isnull=True) | Q(teacher_id=self.request.user.teacher.id)
        if module:
            query &= Q(module_week_type__week_day__module_course_id=module)
        if isinstance(topic, str):
            topic = read_list_query_param(topic)
            query &= Q(tasks__topic__in=topic)
        qs = TaskCollection.objects.filter(query).distinct()
        return qs


class TaskCollectionDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = TaskCollectionDetailSerializer
    permission_classes = [TeacherPermission]
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        print(self.request.query_params)
        qs = TaskCollection.objects.all().distinct()
        return qs


# ////////////////////////// TASK LIST ////////////////////////
ACCEPTED = 2


class TaskFilter(FilterSet):
    task_type = NumberFilter(field_name='type__task_type')
    difficulty = NumberFilter(field_name='type__difficulty')
    status = NumberFilter(field_name='status')
    discipline = NumberFilter(field_name='topic__discipline')
    topic = NumberFilter(field_name='topic')

    class Meta:
        model = Task
        fields = ['status', 'type', 'topic']


class TestBaseListView(ListAPIView):
    queryset = Task.objects.filter(status=ACCEPTED).prefetch_related('type', 'topic')
    serializer_class = TaskListSerializer
    permission_classes = [TeacherPermission, ]
    filter_backends = [DjangoFilterBackend]
    filterset_class = TaskFilter


class TopicListAPIView(ListAPIView):
    serializer_class = TopicShortSerializer
    permission_classes = [TeacherPermission, ]
    filter_backends = [DjangoFilterBackend, ]
    filterset_fields = ['discipline']

    def get_queryset(self):
        qs = Topic.objects.filter(discipline__in=self.request.user.teacher.disciplines.all())
        return qs


class DifficultyListView(ListAPIView):
    serializer_class = DifficultySerializer
    queryset = Difficulty.objects.all()
    permission_classes = [TeacherPermission, ]


class TaskTypeListView(ListAPIView):
    serializer_class = TaskTypeListSerializer
    queryset = TaskType.objects.all()
    permission_classes = [TeacherPermission, ]


class DisciplineListAPIView(ListAPIView):
    permission_classes = [TeacherPermission]
    serializer_class = DisciplineShortSerializer

    def get_queryset(self):
        qs = self.request.user.teacher.disciplines.all()
        return qs


# /////////////////////////// LESSON EVENT ///////////////////

class EventFilter(FilterSet):
    start_date = DateFilter(field_name='date', lookup_expr='gte')
    end_date = DateFilter(field_name='date', lookup_expr='lte')
    student_group = NumberFilter(field_name='module_student_group__group_id')
    module = NumberFilter(field_name='module_student_group__module_course')

    class Meta:
        model = LessonEvent
        fields = ['start_date', 'end_date', 'student_group', 'module']


class LessonEventListView(ListAPIView):
    serializer_class = LessonEventSerializer
    permission_classes = (TeacherPermission,)
    filter_backends = [DjangoFilterBackend, ]
    filterset_class = EventFilter

    def get_queryset(self):
        return LessonEvent.objects.filter(teacher=self.request.user.teacher).order_by("date", "interval__start_time")
