# Преподаватель: отправка внеклассных заданий

## `GET /teacher/task/list/`

Список внеклассных заданий, создаталем которых является преподаватель, осуществляющий запрос.

### Response:

```json
[
  {
    "id": 1,
    "title": "лол",
    "module_course": {
      "id": 1,
      "name": {
        "ru": "Сметчик",
        "uz": "Surveyor, building"
      }
    }
  },
  {
    "id": 2,
    "title": "кек",
    "module_course": {
      "id": 1,
      "name": {
        "ru": "Сметчик",
        "uz": "Surveyor, building"
      }
    }
  }
]
```

---

## `GET /teacher/task/<int:id>/`

Детальное представление внеклассного задания.

### URL params:

`id` - идентификатор внеклассного задания

### Response:

```json
{
  "id": 2,
  "module_course": {
    "id": 1,
    "name": {
      "ru": "Сметчик",
      "uz": "Surveyor, building"
    }
  },
  "title": "лол",
  "body": "кек",
  "creator": 103,
  "files": [],
  "students": [1, 2]
}
```

---

## `POST /teacher/task/create/`

Создание внеклассного задания.

### Request fields:

| Название | Описание                                |
| -------- | --------------------------------------- |
| files    | Необязательное поле и может быть пустым |

### Request:

```json
{
  "module_course": 2,
  "title": "ДЗ-1",
  "body": "Text",
  "files": [FILE_1, FILE_2]
}
```

### Response fields:

| Название | Описание                          |
| -------- | --------------------------------- |
| students | Всегда пустое для этого эндпоинта |

### Response:

```json
{
  "id": 1,
  "module_course": {
    "id": 1,
    "name": {
      "ru": "Ихтиолог",
      "uz": "Engineer, manufacturing"
    }
  },
  "title": "Заголовок внеклассного задания",
  "body": "Тело внеклассного задания",
  "creator": 1,
  "files": [
    {
      "id": 3,
      "file": "/media/home_tasks/tasks/photo_2021-01-13_11-32-10.jpg"
    },
    {
      "id": 4,
      "file": "/media/home_tasks/tasks/Insomnia_2021-01-04_BOdUJC8"
    }
  ],
  "students": []
}
```

---

## `POST /teacher/task/<int:id>/send/`

Рассылка внеклассного задания.

### URL params:

| Название | Описание                           |
| -------- | ---------------------------------- |
| id       | Идентификатор внеклассного задания |

### Request fields:

| Название       | Описание            |
| -------------- | ------------------- |
| students       | Список id студентов |
| student_groups | Список id групп     |

Хотя бы одно из students и student_groups необходимо.

### Request:

```json
{
  "start_date": "2020-11-25",
  "end_date": "2020-12-25",
  "students": [1, 2, 3],
  "student_groups": [2, 5]
}
```

### Response:

```json
{
  "id": 2,
  "module_course": {
    "id": 1,
    "name": {
      "ru": "Сметчик",
      "uz": "Surveyor, building"
    }
  },
  "title": "лол",
  "body": "кек",
  "creator": 103,
  "files": [],
  "students": [1, 2, 3, 44, 53]
}
```

---

## `GET /teacher/task/modulecourse/list/`

Список модулей для данного преподавателя.

### Response:

```json
[
  {
    "id": 15,
    "name": {
      "ru": "Сметчик",
      "uz": "Surveyor, building"
    }
  },
  {
    "id": 14,
    "name": {
      "ru": "Сметчик",
      "uz": "Surveyor, building"
    }
  }
]
```

---

## `GET /teacher/task/student/list/?group_id=int`

Список студентов для данного преподавателя.

### Query params:

| Название | Описание                             |
| -------- | ------------------------------------ |
| group_id | Идентификатор группы (необязательно) |

### Response:

```json
[
  {
    "id": 58,
    "first_name": "Прохорова",
    "middle_name": "Святославовна",
    "last_name": "Регина"
  },
  {
    "id": 58,
    "first_name": "Прохорова",
    "middle_name": "Святославовна",
    "last_name": "Регина"
  }
]
```

---

## `GET /teacher/task/group/list/`

Список групп для данного преподавателя.

### Response:

```json
[
  {
    "id": 4,
    "name": "Дизайнер рекламы"
  },
  {
    "id": 7,
    "name": "Оператор ЭВМ"
  }
]
```

---

## Новые эндпоинты

## `GET teacher/task/<int:module_id>/hometask/list/`

Список заданий.

### Reponse:

```json
{
  "results": [
    {
      "id": 1,
      "receiver": "1001",
      "title": "Заголовок внеклассного задания",
      "answer_date": "2021-01-08",
      "mark": "4",
      "checked_date": "2021-01-08"
    }
  ]
}
```

## `GET teacher/task/hometask/<hometaskreceiver_id>/`

Детальное представление задания.

### Response fields:

| Название  | Описание                                                       |
| --------- | -------------------------------------------------------------- |
| answer.id | Идентификатор hometaskanswer, пригодится для создания проверки |

### Response:

```json
{
  "id": 1,
  "home_task": {
    "id": 1,
    "module_course": {
      "id": 1,
      "name": {
        "ru": "Ихтиолог",
        "uz": "Engineer, manufacturing"
      }
    },
    "title": "Заголовок внеклассного задания",
    "body": "Тело внеклассного задания",
    "creator": 1,
    "files": [
      {
        "id": 1,
        "file": "http://127.0.0.1:8000/media/home_tasks/tasks/task.txt"
      }
    ],
    "students": [100]
  },
  "receiver": "1001",
  "answer": {
    "id": 1,
    "title": "Заголовок ответа на внеклассное задание",
    "comment": "Текст ответа на внеклассное задание",
    "files": [
      {
        "id": 1,
        "file": "http://127.0.0.1:8000/media/home_tasks/answers/photo_2021-01-13_11-32-10.jpg"
      },
      {
        "id": 2,
        "file": "http://127.0.0.1:8000/media/home_tasks/answers/image_2021-01-09_18-10-46.png"
      }
    ]
  },
  "review": {
    "id": 1,
    "comment": "Комментарий к ответу на внеклассное задание",
    "mark": 4
  }
}
```

## `POST teacher/task/hometaskreview/create/`

Создание проверки внеклассного задания.

### Request fields:

| Название         | Описание                     |
| ---------------- | ---------------------------- |
| home_task_answer | Идентификатор hometaskanswer |
| mark             | Оценка                       |

### Request:

```json
{
  "home_task_answer": 1,
  "comment": "Комментарий к решению",
  "mark": 5
}
```

### Response:

```json
{
  "id": 3,
  "home_task_answer": {
    "id": 1,
    "title": "Решение домашнего задания",
    "task": {
      "id": 18,
      "home_task": {
        "id": 2,
        "title": "Title",
        "module_course": {
          "id": 2,
          "name": {
            "ru": "Танатолог",
            "uz": "Tourism officer"
          }
        },
        "creator": 100
      },
      "receiver": 100,
      "start_date": "2020-11-11",
      "end_date": "2020-12-20"
    }
  },
  "comment": "Комментарий к решению",
  "mark": 5,
  "reviewer": 100
}
```

## `PATCH /teacher/task/hometaskreview/<int:id>/update/`

Обновление проверки внеклассного задания.

### URL params:

| Название | Описание                           |
| -------- | ---------------------------------- |
| id       | Идентификатор hometaskanswerreview |

### Request fields:

| Название | Описание |
| -------- | -------- |
| mark     | integer  |

### Request:

```json
{
  "comment": "Обновлённый комментарий к решению",
  "mark": 2
}
```

### Response:

```json
{
  "id": 3,
  "home_task_answer": {
    "id": 1,
    "title": "Решение домашнего задания",
    "task": {
      "id": 18,
      "home_task": {
        "id": 2,
        "title": "Title",
        "module_course": {
          "id": 2,
          "name": {
            "ru": "Танатолог",
            "uz": "Tourism officer"
          }
        },
        "creator": 100
      },
      "receiver": 100,
      "start_date": "2020-11-11",
      "end_date": "2020-12-20"
    }
  },
  "comment": "Обновлённый комментарий к решению",
  "mark": 2,
  "reviewer": 100
}
```
