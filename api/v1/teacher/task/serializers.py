from api.models.task import HomeTaskAnswerFiles, Type, Difficulty, TaskCollectionReceiver
from api.models import (
    HomeTask,
    HomeTaskAnswer,
    HomeTaskAnswerReview,
    HomeTaskFiles,
    HomeTaskReceiver,
    ModuleCourse,
    Student,
    StudentGroup, Answer, Task, TaskCollection, IntermediateControlTopics, Discipline, LessonEvent,
)
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.utils.datetime_safe import date
from rest_framework import serializers
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.serializers import ModelSerializer

from api.v1.discipline.generator import easy_generate, TaskType, Topic
from api.v1.teacher.task.service import task_update


class HomeTaskFileSerializer(ModelSerializer):
    class Meta:
        model = HomeTaskFiles
        fields = ("id", "file")
        read_only_fields = ("id",)


class ModuleCourseSerializer(ModelSerializer):
    name = serializers.JSONField(source="discipline.name")

    class Meta:
        model = ModuleCourse
        fields = ("id", "name")
        read_only_fields = ("id",)


class StudentSerializer(ModelSerializer):
    class Meta:
        model = Student
        fields = ("id",)

    def to_representation(self, instance):
        response = super().to_representation(instance)

        response.update({
            "first_name": instance.user.first_name,
            "middle_name": instance.user.middle_name,
            "last_name": instance.user.last_name,
        })

        return response


class StudentGroupSerializer(ModelSerializer):
    class Meta:
        model = StudentGroup
        fields = ("id", "name")


class HomeTaskShortSerializer(ModelSerializer):
    module_course = ModuleCourseSerializer()

    class Meta:
        model = HomeTask
        fields = ("id", "title", "module_course", "creator")
        read_only_fields = ("id",)


class HomeTaskSerializer(ModelSerializer):
    files = serializers.ListField(child=serializers.FileField(allow_empty_file=False), required=False, write_only=True)

    class Meta:
        model = HomeTask
        fields = ("id", "module_course", "title", "body", "files", "creator")
        read_only_fields = ("id", "creator")

    def create(self, validated_data):
        validated_data["creator"] = self.context["request"].user.teacher
        files = validated_data.pop("files", None)
        home_task = super().create(validated_data)

        if not files:
            return home_task

        home_task_files = []
        for file in files:
            home_task_files.append(HomeTaskFiles(file=file, home_task=home_task))

        HomeTaskFiles.objects.bulk_create(home_task_files)

        return home_task

    def to_representation(self, instance):
        response = super().to_representation(instance)

        response.update({
            "module_course": ModuleCourseSerializer(instance.module_course).data,
            "files": HomeTaskFileSerializer(instance.hometaskfiles_set.all(), many=True, context=self.context).data,
            "students": instance.hometaskreceiver_set.values_list("receiver", flat=True),
        })

        return response


class HomeTaskSendSerializer(ModelSerializer):
    start_date = serializers.DateField(write_only=True, required=True)
    end_date = serializers.DateField(write_only=True, required=True)
    students = serializers.ListField(child=serializers.IntegerField(), write_only=True, required=False)
    student_groups = serializers.ListField(child=serializers.IntegerField(), write_only=True, required=False)

    module_course = ModuleCourseSerializer(read_only=True)

    class Meta:
        model = HomeTask
        fields = (
            "start_date",
            "end_date",
            "students",
            "student_groups",
            "id",
            "module_course",
            "title",
            "body",
            "creator",
        )
        read_only_fields = ("id", "module_course", "title", "body", "creator")

    def validate(self, data):
        students = data.get("students")
        student_groups = data.get("student_groups")

        if students or student_groups:
            return data
        else:
            raise ValidationError("At least one field is required: students, student_groups")

    def create(self, validated_data):
        home_task_id = self.context["request"].parser_context["kwargs"]["id"]
        home_task = get_object_or_404(HomeTask, id=home_task_id)

        student_ids = validated_data.get("students", [])
        student_group_ids = validated_data.get("student_groups", [])

        query = Q(id__in=student_ids) | Q(studentgroupforming__group__in=student_group_ids)
        students = Student.objects.filter(query).distinct()

        if len(students) == 0:
            raise NotFound("Students or groups are not found.")

        start_date = validated_data["start_date"]
        end_date = validated_data["end_date"]

        receivers = []
        for student in students:
            receivers.append(
                HomeTaskReceiver(
                    receiver=student,
                    home_task=home_task,
                    start_date=start_date,
                    end_date=end_date,
                ))

        HomeTaskReceiver.objects.bulk_create(receivers)
        return home_task

    def to_representation(self, instance):
        response = super().to_representation(instance)

        response.update({
            "files": HomeTaskFileSerializer(instance.hometaskfiles_set.all(), many=True, context=self.context).data,
            "students": instance.hometaskreceiver_set.values_list("receiver", flat=True),
        })

        return response


class HomeTaskReceiverSerializer(ModelSerializer):
    home_task = HomeTaskShortSerializer()

    class Meta:
        model = HomeTaskReceiver
        fields = ("id", "home_task", "receiver", "start_date", "end_date")


class HomeTaskAnswerShortSerializer(ModelSerializer):
    task = HomeTaskReceiverSerializer()

    class Meta:
        model = HomeTaskAnswer
        fields = ("id", "title", "task")


class HomeTaskAnswerReviewCreateSerializer(ModelSerializer):
    class Meta:
        model = HomeTaskAnswerReview
        fields = ("id", "home_task_answer", "comment", "mark", "reviewer")
        read_only_fields = ("id", "reviewer")

    def create(self, validated_data):
        teacher = self.context["request"].user.teacher
        validated_data["reviewer"] = teacher
        return super().create(validated_data)

    def update(self, instance, validated_data):
        if validated_data.get("home_task_answer"):
            validated_data.pop("home_task_answer")

        teacher = self.context["request"].user.teacher
        validated_data["reviewer"] = teacher
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["home_task_answer"] = HomeTaskAnswerShortSerializer(instance.home_task_answer).data
        return response


class HomeTaskReceiverShortSerializer(ModelSerializer):
    class Meta:
        model = HomeTaskReceiver
        fields = ("id",)

    def to_representation(self, instance: HomeTaskReceiver):
        """
        Если задание не было выполнено вовремя, создаётся пустой
        HomeTaskAnswer и HomeTaskAnswerReview с минимальной оценкой.
        """

        response = super().to_representation(instance)

        uid = (str(instance.receiver.id) + str(instance.home_task.id) + str(instance.id)).zfill(8)
        response.update({
            "receiver": uid,
            "title": instance.home_task.title,
            "mark": "",
            "checked_date": "",
            "answer_date": ""
        })

        if hasattr(instance, "hometaskanswer"):
            created_at = instance.hometaskanswer.created_at.date()

            if created_at <= instance.end_date:
                response["answer_date"] = created_at

        elif date.today() > instance.end_date:
            answer = HomeTaskAnswer.objects.create(title="", task=instance, comment="")
            reviewer = instance.home_task.creator
            HomeTaskAnswerReview.objects.create(home_task_answer=answer, comment="", mark=0, reviewer=reviewer)

        if hasattr(instance, "hometaskanswer") and hasattr(instance.hometaskanswer, "hometaskanswerreview"):
            response.update({
                "mark": instance.hometaskanswer.hometaskanswerreview.mark,
                "checked_date": instance.hometaskanswer.hometaskanswerreview.created_at.date()
            })

        return response


class HomeTaskAnswerFilesSerializer(ModelSerializer):
    class Meta:
        model = HomeTaskAnswerFiles
        fields = ("id", "file")


class HomeTaskAnswerReviewShortSerializer(ModelSerializer):
    class Meta:
        model = HomeTaskAnswerReview
        fields = ("id", "comment", "mark")


class HomeTaskAnswerShortDetailSerializer(ModelSerializer):
    class Meta:
        model = HomeTaskAnswer
        fields = ("id", "title", "comment")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["files"] = HomeTaskAnswerFilesSerializer(instance.hometaskanswerfiles_set.all(),
                                                          many=True,
                                                          context=self.context).data
        return response


class HomeTaskReceiverDetailSerializer(ModelSerializer):
    home_task = HomeTaskSerializer()

    class Meta:
        model = HomeTaskReceiver
        fields = ("id", "home_task")

    def to_representation(self, instance: HomeTaskReceiver):
        response = super().to_representation(instance)

        uid = (str(instance.receiver.id) + str(instance.home_task.id) + str(instance.id)).zfill(8)
        response.update({"receiver": uid, "answer": dict(), "review": dict()})

        if hasattr(instance, "hometaskanswer"):
            response["answer"] = HomeTaskAnswerShortDetailSerializer(instance.hometaskanswer,
                                                                     context=self.context).data
            if hasattr(instance.hometaskanswer, "hometaskanswerreview"):
                response["review"] = HomeTaskAnswerReviewShortSerializer(
                    instance.hometaskanswer.hometaskanswerreview).data

        return response


# ///////////////////////////// TASK COLLECTION ///////////////////////////
class TopicShortSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Topic
        fields = (
            'id',
            'name',
        )


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ["id", "task", "answer", "is_correct"]


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ["id", "question"]

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["answers"] = AnswerSerializer(instance.answer_set, many=True).data
        return response


class TaskCollectionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskCollection
        fields = ["id"]

    def to_representation(self, instance: TaskCollection):
        response = super().to_representation(instance)
        if instance.module_week_type.lesson:
            response["theme"] = instance.module_week_type.lesson.lesson.name
        elif instance.module_week_type.int_control:
            response["theme"] = instance.module_week_type.int_control.intermediate_control.name
        elif instance.module_week_type.fin_control:
            response["theme"] = instance.module_week_type.fin_control.final_control.name
        response["task_type"] = instance.tasks.all().values_list('type__task_type', flat=True).distinct()
        response["parent"] = instance.is_parent
        response["parent_id"] = instance.parent_id
        return response


class TaskCollectionDetailSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False, required=False)
    function = serializers.ChoiceField(choices=("REGENERATE", "ADD_TASK"),
                                       write_only=True, required=False)
    arguments = serializers.ListField(write_only=True, required=False)
    action = serializers.ChoiceField(choices=("DELETE", "REPLACE", "ADD"), write_only=True, required=False)
    chosen_tasks = serializers.ListField(write_only=True, required=False)
    topics = serializers.ListField(write_only=True, required=False)

    class Meta:
        model = TaskCollection
        fields = ["id",
                  "module_week_type",
                  "function",
                  "arguments",
                  "action",
                  "chosen_tasks",
                  "topics",
                  "is_active",
                  "tasks",
                  ]

    def to_representation(self, instance: TaskCollection):
        response = super().to_representation(instance)
        if instance.module_week_type.lesson:
            response["theme"] = instance.module_week_type.lesson.lesson.name
        elif instance.module_week_type.int_control:
            response["theme"] = instance.module_week_type.int_control.intermediate_control.name
        elif instance.module_week_type.fin_control:
            response["theme"] = instance.module_week_type.fin_control.final_control.name

        response["questions"] = TaskSerializer(instance.tasks.all(), many=True).data
        response["discipline"] = instance.discipline
        response["topics"] = instance.get_topics.values_list("id", flat=True)
        event = LessonEvent.objects.filter(lesson=instance.module_week_type,
                                           teacher=self.context['request'].user.teacher).first()
        if event:
            response["event"] = event.id
        else:
            response["event"] = event
        return response

    def update(self, instance, validated_data):
        """
        TaskCollection создается в Программе Обучения и используется как шаблон. Обновление создаст
        новый instance для запросившего преподавателя.
        Если TaskCollection уже был разослан(is_active=False), его instance нельзя обновлять.
        Создается копия того instance и выполняется обновление на копии.
        """
        if not instance.is_active:
            new_instance = TaskCollection.objects.create(module_week_type=instance.module_week_type,
                                                         deadline_duration=instance.deadline_duration,
                                                         parent=instance.parent,
                                                         teacher=self.context['request'].user.teacher)
            new_instance.tasks.set(instance.tasks.all())
            validated_data['task_collection_id'] = new_instance.id
            validated_data['module_week_type'] = new_instance.module_week_type
            task_update(validated_data)
            return new_instance
        if instance.children.all():
            new_instance = instance.children.filter(teacher=self.context['request'].user.teacher, is_active=True).last()
            if new_instance:
                validated_data['task_collection_id'] = new_instance.id
                validated_data['module_week_type'] = new_instance.module_week_type

                return task_update(validated_data)
            else:
                new_instance = TaskCollection.objects.create(module_week_type=instance.module_week_type,
                                                             deadline_duration=instance.deadline_duration,
                                                             parent=instance,
                                                             teacher=self.context['request'].user.teacher)
                new_instance.tasks.set(instance.tasks.all())
                validated_data['task_collection_id'] = new_instance.id
                validated_data['module_week_type'] = new_instance.module_week_type
                task_update(validated_data)
                return new_instance
        elif instance.parent:
            validated_data['task_collection_id'] = instance.id
            validated_data['module_week_type'] = instance.module_week_type
            task_update(validated_data)

            return instance
        else:
            new_instance = TaskCollection.objects.create(module_week_type=instance.module_week_type,
                                                         deadline_duration=instance.deadline_duration,
                                                         parent=instance,
                                                         teacher=self.context['request'].user.teacher)
            new_instance.tasks.set(instance.tasks.all())
            validated_data['task_collection_id'] = new_instance.id
            validated_data['module_week_type'] = new_instance.module_week_type
            task_update(validated_data)

            return new_instance


class TaskTypeListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = TaskType
        fields = (
            'id',
            'name'
        )


class DisciplineShortSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Discipline
        fields = (
            'id',
            'name'
        )


class DifficultySerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Difficulty
        fields = ['id', 'name']

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class TypeDetailSerializer(serializers.ModelSerializer):
    difficulty = DifficultySerializer()
    task_type = TaskTypeListSerializer()

    class Meta:
        model = Type
        fields = ['id', 'task_type', 'difficulty', 'duration_seconds']


class TaskListSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField()

    class Meta:
        model = Task
        fields = ['id', 'question', 'created_at']

    def to_representation(self, instance):
        response = super(TaskListSerializer, self).to_representation(instance)
        response['type'] = TypeDetailSerializer(instance.type).data
        response['topic'] = TopicShortSerializer(instance.topic).data
        response['module'] = DisciplineShortSerializer(instance.topic.discipline).data
        response["answers"] = AnswerSerializer(instance.answer_set, many=True).data

        return response


class TaskCollectionSendSerializer(ModelSerializer):
    start_date = serializers.DateField(write_only=True, required=True)
    end_date = serializers.DateField(write_only=True, required=True)
    students = serializers.ListField(child=serializers.IntegerField(),
                                     write_only=True,
                                     required=False)
    student_groups = serializers.IntegerField(write_only=True,
                                              required=False)
    event = serializers.PrimaryKeyRelatedField(queryset=LessonEvent.objects.all(), required=True, write_only=True)
    module_course = ModuleCourseSerializer(read_only=True)

    class Meta:
        model = TaskCollection
        fields = (
            "event",
            "start_date",
            "end_date",
            "students",
            "student_groups",
            "id",
            "module_course",
            "teacher",
        )

    def validate(self, data):
        students = data.get("students")
        student_groups = data.get("student_groups")

        if students or student_groups:
            return data
        else:
            raise ValidationError("At least one field is required: students, student_groups")

    def create(self, validated_data):
        task_collection_id = self.context["request"].parser_context["kwargs"]["id"]
        task_collection = get_object_or_404(TaskCollection, id=task_collection_id)

        student_ids = validated_data.get("students", [])
        student_group_ids = validated_data.get("student_groups")
        students = Student.objects.filter(id__in=student_ids)
        if student_group_ids:
            students |= StudentGroup.objects.get(id=student_group_ids).get_students()
        students.distinct()
        task_collection.is_active = False
        task_collection.save()
        if len(students) == 0:
            raise NotFound("Students or groups are not found.")

        start_date = validated_data["start_date"]
        end_date = validated_data["end_date"]
        event = validated_data.get("event")
        receivers = []
        for student in students:
            receivers.append(
                TaskCollectionReceiver(
                    student=student,
                    task_collection=task_collection,
                    start_date=start_date,
                    end_date=end_date,
                    event_id=event.id
                )
            )

        TaskCollectionReceiver.objects.bulk_create(receivers)
        return task_collection

    def to_representation(self, instance: TaskCollection):
        response = super().to_representation(instance)
        if instance.module_week_type.lesson:
            response["theme"] = instance.module_week_type.lesson.lesson.name
        elif instance.module_week_type.int_control:
            response["theme"] = instance.module_week_type.int_control.intermediate_control.name
        elif instance.module_week_type.fin_control:
            response["theme"] = instance.module_week_type.fin_control.final_control.name

        response["questions"] = TaskSerializer(instance.tasks.all(), many=True).data
        response["discipline"] = instance.discipline
        response["topics"] = instance.get_topics.values_list("id", flat=True)
        return response


# //////////////////////// LESSON EVENT //////////////////////////

class LessonEventSerializer(ModelSerializer):
    class Meta:
        model = LessonEvent
        fields = ("id", "date")

    def to_representation(self, instance: LessonEvent):
        response = super().to_representation(instance)

        response.update({
            "module": {"id": instance.module_student_group.module_course.id,
                       "name": instance.module_student_group.module_course.discipline.name},
            "group": {"id": instance.module_student_group.group.id, "name": instance.module_student_group.group.name},
            "topic": {"id": instance.lesson.id, "name": instance.lesson.name},
        })

        return response
