from django.urls import path, include

# teacher_room
urlpatterns = [
    path("schedule/", include("api.v1.teacher.schedule.urls")),
    path("group/", include("api.v1.teacher.group.urls")),
    path("statement/", include("api.v1.teacher.statement.urls")),
    path("task/", include("api.v1.teacher.task.urls")),
    path("feedback/", include("api.v1.teacher.feedback.urls")),
    path("task_check/", include("api.v1.teacher.task_check.urls")),
    path("library/", include("api.v1.teacher.library.urls")),
    path("material/", include("api.v1.teacher.material.urls")),
    path("notification/", include("api.v1.teacher.notification.urls")),
]
