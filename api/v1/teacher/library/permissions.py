from api.models.library import LibraryBook, VideoMaterial
from typing import Union
from rest_framework.permissions import BasePermission


class DestroyUpdateOwnPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in ('PATCH', 'PUT', 'DELETE'):
            obj: Union[LibraryBook, VideoMaterial] = view.get_object()
            return obj.publisher == request.user
        else:
            return True
