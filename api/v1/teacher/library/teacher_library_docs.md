# Библиотека в кабинете преподавателя

## `GET teacher/library/typeofbook/list/`

Список типов книг.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Книга",
        "uz": "Book"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Журнал",
        "uz": "Magazine"
      }
    }
  ]
}
```

## `GET teacher/library/category/list/`

Список категорий, могут быть вложенные.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Закон",
        "uz": "Law"
      },
      "parent": null,
      "children": [],
      "checked": false
    },
    {
      "id": 2,
      "name": {
        "ru": "Психология",
        "uz": "Psychology"
      },
      "parent": null,
      "children": [
        {
          "id": 3,
          "name": {
            "ru": "Психология человека",
            "uz": "Human psychology"
          },
          "parent": 2,
          "children": [],
          "checked": false
        },
        {
          "id": 4,
          "name": {
            "ru": "Психология поведения",
            "uz": "Behavior psychology"
          },
          "parent": 2,
          "children": [],
          "checked": false
        }
      ],
      "checked": false,
      "open": false
    }
  ]
}
```

## `GET teacher/library/shortcategory/list/`

Список категорий без вложений.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Закон",
        "uz": "Law"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Психология",
        "uz": "Psychology"
      }
    },
    {
      "id": 3,
      "name": {
        "ru": "Психология человека",
        "uz": "Human psychology"
      }
    },
    {
      "id": 4,
      "name": {
        "ru": "Психология поведения",
        "uz": "Behavior psychology"
      }
    }
  ]
}
```

## `POST teacher/library/librarybook/create/`

Добавление книги преподавателем.

### Request:

```json
{
  "title": {
    "ru": "Заголовок",
    "uz": "Title"
  },
  "description": {
    "ru": "Описание",
    "uz": "Description"
  },
  "file": FILE,
  "image": IMAGE,
  "category": 1,
  "author": "Stephen King",
  "book_type": 1,
  "publish_date": "2020-01-01",
  "page_count": 15
}
```

### Response:

```json
{
  "id": 2,
  "title": {
    "ru": "Название",
    "uz": "Title"
  },
  "file": "http://127.0.0.1:8000/media/librarybooks/files/Insomnia_2021-01-04_jBScJuR",
  "image": "http://127.0.0.1:8000/media/librarybooks/imgs/photo_2021-01-13_11-32-10_Ff09UNN.jpg",
  "description": {
    "ru": "Описание",
    "uz": "Description",
    "en": "Kniga"
  },
  "category": {
    "ru": "Закон",
    "uz": "Law"
  },
  "author": "Stephen King",
  "book_type": {
    "ru": "Книга",
    "uz": "Book"
  },
  "publish_date": "2020-01-01",
  "page_count": 15,
  "publisher": {
    "id": 101,
    "first_name": "Амир",
    "last_name": "Муллагалиев"
  },
  "my": true
}
```

## `PATCH teacher/library/librarybook/<int:id>/`

Обновление информации о существующей книге.

### URL params:

| Название | Описание |
| -------- | -------- |
| id       | id книги |

### Request:

```json
{
  "title": {
    "ru": "Заголовок",
    "uz": "Title"
  },
  "description": {
    "ru": "Описание",
    "uz": "Description"
  },
  "file": FILE,
  "image": IMAGE,
  "category": 1,
  "author": "Stephen King",
  "book_type": 1,
  "publish_date": "2020-01-01",
  "page_count": 15
}
```

### Response:

```json
{
  "id": 2,
  "title": {
    "ru": "Название",
    "uz": "Title"
  },
  "file": "http://127.0.0.1:8000/media/librarybooks/files/Insomnia_2021-01-04_jBScJuR",
  "image": "http://127.0.0.1:8000/media/librarybooks/imgs/photo_2021-01-13_11-32-10_Ff09UNN.jpg",
  "description": {
    "ru": "Описание",
    "uz": "Description",
    "en": "Kniga"
  },
  "category": {
    "ru": "Закон",
    "uz": "Law"
  },
  "author": "Stephen King",
  "book_type": {
    "ru": "Книга",
    "uz": "Book"
  },
  "publish_date": "2020-01-01",
  "page_count": 15,
  "publisher": {
    "id": 101,
    "first_name": "Амир",
    "last_name": "Муллагалиев"
  },
  "my": true
}
```

## `DELETE teacher/library/librarybook/<int:id>/`

Удаление существующей книги.

### URL params:

| Название | Описание |
| -------- | -------- |
| id       | id книги |

## `GET teacher/library/librarybook/list/`

Список книг.

### Query params:

| Название     | Описание                     | Пример     |
| ------------ | ---------------------------- | ---------- |
| my           | Флаг "Мои книги"             | `?my=true` |
| publish_year | Список годов                 |            |
| category     | Список id категорий          |            |
| book_type    | Список id типов              |            |
| search       | Поиск по названию и описанию | `?search=` |

### Fields hints:

| Поле | Описание                                                                |
| ---- | ----------------------------------------------------------------------- |
| my   | boolean, означает, что пользователь может редактировать и удалять книгу |

### Response:

```json
{
  "results": [
    {
      "id": 2,
      "title": {
        "ru": "Название",
        "uz": "Title"
      },
      "image": "http://127.0.0.1:8000/media/librarybooks/imgs/photo_2021-01-13_11-32-10_Ff09UNN.jpg",
      "description": {
        "ru": "Описание",
        "uz": "Description",
        "en": "Kniga"
      },
      "category": {
        "ru": "Закон",
        "uz": "Law"
      },
      "book_type": {
        "ru": "Книга",
        "uz": "Book"
      },
      "publish_date": "2020-01-01",
      "page_count": 15,
      "my": true
    }
  ]
}
```

## `GET teacher/library/librarybook/<int:id>/`

Детальный просмотр книги.

### URL params:

| Название | Описание |
| -------- | -------- |
| id       | id книги |

### Fields hints:

| Поле | Описание                                                                |
| ---- | ----------------------------------------------------------------------- |
| my   | boolean, означает, что пользователь может редактировать и удалять книгу |

### Response:

```json
{
  "id": 2,
  "title": {
    "ru": "Название",
    "uz": "Title"
  },
  "file": "http://127.0.0.1:8000/media/librarybooks/files/Insomnia_2021-01-04_jBScJuR",
  "image": "http://127.0.0.1:8000/media/librarybooks/imgs/photo_2021-01-13_11-32-10_Ff09UNN.jpg",
  "description": {
    "ru": "Описание",
    "uz": "Description",
    "en": "Kniga"
  },
  "category": {
    "ru": "Закон",
    "uz": "Law"
  },
  "author": "Stephen King",
  "book_type": {
    "ru": "Книга",
    "uz": "Book"
  },
  "publish_date": "2020-01-01",
  "page_count": 15,
  "publisher": {
    "id": 101,
    "first_name": "Амир",
    "last_name": "Муллагалиев"
  },
  "my": true,
  "size": "1.82 Mb"
}
```

## `GET teacher/library/year/list/`

Список годов издания книг (упорядочено по возрастанию).

### Response:

```json
{
  "results": [
    {
      "checked": false,
      "name": "2020"
    }
  ]
}
```

## `GET teacher/library/moduleofvideo/list/`

Список модулей видео.

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Конституционное право",
        "uz": "Constitucionnoe pravo"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Психодиагностика",
        "uz": "Psychodiagnostics"
      }
    }
  ]
}
```

## `POST teacher/library/videomaterial/create/`

Создание видеоматериала.

### Request:

```json
{
  "title": {
    "ru": "Заголовок",
    "uz": "Zagolovok"
  },
  "description": {
    "ru": "Описание",
    "uz": "Opisaniye"
  },
  "video": FILE,
  "module": 1,
  "category": 1
}
```

### Response:

```json
{
  "id": 1,
  "title": {
    "ru": "Заголовок",
    "uz": "Zagolovok"
  },
  "description": {
    "ru": "Описание",
    "uz": "Opisaniye"
  },
  "video": "http://127.0.0.1:8000/media/videomaterials/Insomnia_2021-01-04_sB1Rb0w",
  "module": {
    "ru": "Конституционное право",
    "uz": "Constitucionnoe pravo"
  },
  "category": {
    "ru": "Закон",
    "uz": "Law"
  },
  "publisher": {
    "id": 101,
    "first_name": "Амир",
    "last_name": "Муллагалиев"
  },
  "my": true
}
```

## `GET teacher/library/videomaterial/list/`

Список видеоматериалов.

### Query params:

| Название | Описание            | Пример     |
| -------- | ------------------- | ---------- |
| module   | Список модулей      |            |
| category | Список id категорий |            |
| search   | Поиск по названию   | `?search=` |
| my       | Флаг "Мои видео"    | `?my=true` |

### Fields hints:

| Поле | Описание                                                                |
| ---- | ----------------------------------------------------------------------- |
| my   | boolean, означает, что пользователь может редактировать и удалять видео |

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "title": {
        "ru": "Заголовок",
        "uz": "Zagolovok"
      },
      "video": "http://127.0.0.1:8000/media/videomaterials/Insomnia_2021-01-04_sB1Rb0w",
      "module": {
        "ru": "Конституционное право",
        "uz": "Constitucionnoe pravo"
      },
      "created_at": "2021-01-13T11:57:51.505912+05:00",
      "my": true
    }
  ]
}
```

## `GET teacher/library/videomaterial/<int:id>/`

Детальный просмотр видеоматериала.

### URL params:

| Название | Описание |
| -------- | -------- |
| id       | id книги |

### Fields hints:

| Поле | Описание                                                                |
| ---- | ----------------------------------------------------------------------- |
| my   | boolean, означает, что пользователь может редактировать и удалять видео |

### Response:

```json
{
  "id": 1,
  "title": {
    "ru": "Заголовок",
    "uz": "Zagolovok"
  },
  "description": {
    "ru": "Описание",
    "uz": "Opisaniye"
  },
  "video": "http://127.0.0.1:8000/media/videomaterials/Insomnia_2021-01-04_sB1Rb0w",
  "module": {
    "ru": "Конституционное право",
    "uz": "Constitucionnoe pravo"
  },
  "category": {
    "ru": "Закон",
    "uz": "Law"
  },
  "publisher": {
    "id": 101,
    "first_name": "Амир",
    "last_name": "Муллагалиев"
  },
  "my": true,
  "size": "1.82 Mb"
}
```

## `PATCH teacher/library/videomaterial/<int:id>/`

Обновление существующего видеоматериала.

### URL params:

| Название | Описание          |
| -------- | ----------------- |
| id       | id видеоматериала |

### Request:

```json
{
  "title": {
    "ru": "Заголовок",
    "uz": "Zagolovok"
  },
  "description": {
    "ru": "Описание",
    "uz": "Opisaniye"
  },
  "video": FILE,
  "module": 1,
  "category": 1
}
```

### Response:

```json
{
  "id": 1,
  "title": {
    "ru": "Заголовок",
    "uz": "Zagolovok"
  },
  "description": {
    "ru": "Описание",
    "uz": "Opisaniye"
  },
  "video": "http://127.0.0.1:8000/media/videomaterials/Insomnia_2021-01-04_sB1Rb0w",
  "module": {
    "ru": "Конституционное право",
    "uz": "Constitucionnoe pravo"
  },
  "category": {
    "ru": "Закон",
    "uz": "Law"
  },
  "publisher": {
    "id": 101,
    "first_name": "Амир",
    "last_name": "Муллагалиев"
  },
  "my": true
}
```

## `DELETE teacher/library/videomaterial/<int:id>/`

Удаление существующего видеоматериала.

### URL params:

| Название | Описание          |
| -------- | ----------------- |
| id       | id видеоматериала |

# Рассылка книг и видеоматериалов из библиотеки

## `GET teacher/library/group/list/`

Список групп для данного преподавателя.

### Response:

```json
{
  "results": [
    {
      "id": 5,
      "name": "M1-17"
    }
  ]
}
```

## `GET teacher/library/student/list/`

Список студентов для данного преподавателя.

### Response:

```json
{
  "results": [
    {
      "id": 15,
      "name": "Иванов Иван Иванович"
    }
  ]
}
```

## `POST teacher/library/librarybook/share/`

Рассылка книги.

### Request:

```json
{
  "library_book": 2,
  "students": [1, 2, 3],
  "student_groups": [1, 2]
}
```

### Response:

```json
{
  "library_book": 2,
  "students": [1, 2, 3],
  "student_groups": [1, 2]
}
```

## `POST teacher/library/videomaterial/share/`

Рассылка видеоматериала.

### Request:

```json
{
  "video_material": 1,
  "students": [1, 2, 3],
  "student_groups": []
}
```

### Response:

```json
{
  "video_material": 1,
  "students": [1, 2, 3],
  "student_groups": []
}
```
