from api.v1.teacher.library.views import (
    CategoryListView, CategoryNestedListView, LibraryBookCreateView,
    LibraryBookListView, LibraryBookRetrieveUpdateDestroyView,
    LibraryBookShareCreateView, ModuleOfVideoListView, StudentGroupListView,
    StudentListView, TypeOfBookListView, VideoMaterialCreateView,
    VideoMaterialListView, VideoMaterialRetrieveUpdateDestroyView,
    VideoMaterialShareCreateView, YearListView)
from django.urls.conf import path

urlpatterns = [
    path("typeofbook/list/", TypeOfBookListView.as_view()),
    path("category/list/", CategoryNestedListView.as_view()),
    path("shortcategory/list/", CategoryListView.as_view()),
    path("moduleofvideo/list/", ModuleOfVideoListView.as_view()),
    path("year/list/", YearListView.as_view()),
    path("librarybook/create/", LibraryBookCreateView.as_view()),
    path("librarybook/list/", LibraryBookListView.as_view()),
    path("librarybook/<int:id>/", LibraryBookRetrieveUpdateDestroyView.as_view()),
    path("librarybook/share/", LibraryBookShareCreateView.as_view()),
    path("videomaterial/create/", VideoMaterialCreateView.as_view()),
    path("videomaterial/list/", VideoMaterialListView.as_view()),
    path("videomaterial/<int:id>/", VideoMaterialRetrieveUpdateDestroyView.as_view()),
    path("videomaterial/share/", VideoMaterialShareCreateView.as_view()),
    path("group/list/", StudentGroupListView.as_view()),
    path("student/list/", StudentListView.as_view()),
]
