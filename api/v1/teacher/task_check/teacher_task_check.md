# Distance learning API - teacher_task_check

## module list 
Список модулей для проверки преподавателем, который осуществляет запрос

## `GET` `/teacher/task_check/modules/ list`

>RESPONSE :   
id - идентификатор модуля  
name - имя модуля   
count - количество непроверенных заданий преподавателем

```json

[
    {
      "id": 6,
      "name": {
        "ru": "Иллюстратор",
        "uz": "Lecturer, higher education"
      },
      "count": 1
    },
    {
      "id": 17,
      "name": {
        "ru": "Диетолог",
        "uz": "Structural engineer"
      },
      "count": 2
    }
  ]

```

## Teacher task_check list
Список ответов на задания студентов по конкретному модулю для проверки преподавателем, который осуществляет запрос

## `GET` `/teacher/task_check/<int:module_course_id>/list`

>URL:  
`module_course_id` - идентификатор модуля курса 

>RESPONSE: 

> params:   
student_id - идентификатор студента  
created_at - дата сдачи   
type -   тип задания (один из трех)   
total_mark - общая оценка за задание   
check_date - дата проверки    
is_checked -  проверено ли, в задание это отображается как галочка либо стрелочка  

```json
[
    {
      "student_id": 8,
      "created_at": "2020-12-11",
      "type": "lesson",
      "total_mark": 100,
      "check_date": "2020-12-11",
      "is_checked": true
    },
    {
      "student_id": 1,
      "created_at": "2020-12-11",
      "type": "lesson",
      "is_checked": false
    },
    {
      "student_id": 8,
      "created_at": "2020-12-12",
      "type": "lesson",
      "is_checked": false
    }
  ]

```
# Ссылки для проверки задания отдельного студента (третья страница по дизайну проверки заданий)
    Ссылка для получения вопроса, ответа студента, правильного ответа (верхняя часть страницы по дизайну)
    
## `GET` `/teacher/task_check/<int:module_course_id>/<int:task_answer_collection_id>/answers/list/`

>URL:  
 module_course_id - идентификатор модуля курса  
 student_id - идентификатор студента  

>RESPONSE (отличается в зависимости от типа задания):  
Для казуса:    
id - идентификатор задания  
question - задание (вопрос)  
correct_answer - правильный ответ на вопрос  
plain_answer - ответ студента на казус   
Для тестов:   
id - идентификатор задания  
question - задание (вопрос)  
correct_answer - правильный ответ на вопрос  
test_answers - всевозможные варианта ответов на вопрос  
chosen_variant - выбранный  ответ(вариант) студентом 

```json
[
    {
      "id": 1,
      "question": {
        "ru": "Роскошный вариант медицина пятеро набор картинка поколение.",
        "uz": "Certain decide others view foreign seem major."
      },
      "correct_answer": null,
      "plain_answer": "This is answer"
    },
    {
      "id": 2,
      "question": {
        "ru": "Манера бабочка обида устройство банда левый полоска сынок изба более появление инструкция.",
        "uz": "Certain future knowledge box threat heart several consumer space past."
      },
      "correct_answer": {
        "ru": [
          "Редактор низкий мальчишка рота собеседник успокоиться прежний. Доставать коммунизм возбуждение ответить анализ выдержать скользить.",
          "Салон прошептать о. Прежде трубка выгнать пастух. Столетие упор степь холодно жестокий войти вперед.",
          "Выраженный тюрьма отражение мимо сутки жидкий. Ребятишки уточнить через вскинуть уничтожение.",
          "Ботинок около о мимо ведь наслаждение. Шлем один юный домашний налево развитый."
        ],
        "uz": [
          "Else service leg police. Find but must ball new.",
          "Seat win expert dark. Team right total body let east. Finish human here start.",
          "Follow direction environmental west hundred page somebody."
        ]
      },
      "test_answers": [
        {
          "id": 1,
          "answer": {
            "ru": [
              "Редактор низкий мальчишка рота собеседник успокоиться прежний. Доставать коммунизм возбуждение ответить анализ выдержать скользить.",
              "Салон прошептать о. Прежде трубка выгнать пастух. Столетие упор степь холодно жестокий войти вперед.",
              "Выраженный тюрьма отражение мимо сутки жидкий. Ребятишки уточнить через вскинуть уничтожение.",
              "Ботинок около о мимо ведь наслаждение. Шлем один юный домашний налево развитый."
            ],
            "uz": [
              "Else service leg police. Find but must ball new.",
              "Seat win expert dark. Team right total body let east. Finish human here start.",
              "Follow direction environmental west hundred page somebody."
            ]
          },
          "is_correct": true
        },
        {
          "id": 18,
          "answer": {
            "ru": "ночь",
            "uz": "financial"
          },
          "is_correct": false
        },
        {
          "id": 19,
          "answer": {
            "ru": "наткнуться",
            "uz": "if"
          },
          "is_correct": false
        },
        {
          "id": 20,
          "answer": {
            "ru": "налоговый",
            "uz": "fund"
          },
          "is_correct": false
        },
        {
          "id": 21,
          "answer": {
            "ru": "ныне",
            "uz": "however"
          },
          "is_correct": true
        }
      ],
      "chosen_variant": {
        "ru": [
          "Редактор низкий мальчишка рота собеседник успокоиться прежний. Доставать коммунизм возбуждение ответить анализ выдержать скользить.",
          "Салон прошептать о. Прежде трубка выгнать пастух. Столетие упор степь холодно жестокий войти вперед.",
          "Выраженный тюрьма отражение мимо сутки жидкий. Ребятишки уточнить через вскинуть уничтожение.",
          "Ботинок около о мимо ведь наслаждение. Шлем один юный домашний налево развитый."
        ],
        "uz": [
          "Else service leg police. Find but must ball new.",
          "Seat win expert dark. Team right total body let east. Finish human here start.",
          "Follow direction environmental west hundred page somebody."
        ]
      }
    },
    {
      "id": 3,
      "question": {
        "ru": "Манера бабочка обида устройство банда левый полоска сынок изба более появление инструкция.",
        "uz": "Certain future knowledge box threat heart several consumer space past."
      },
      "correct_answer": {
        "ru": [
          "Редактор низкий мальчишка рота собеседник успокоиться прежний. Доставать коммунизм возбуждение ответить анализ выдержать скользить.",
          "Салон прошептать о. Прежде трубка выгнать пастух. Столетие упор степь холодно жестокий войти вперед.",
          "Выраженный тюрьма отражение мимо сутки жидкий. Ребятишки уточнить через вскинуть уничтожение.",
          "Ботинок около о мимо ведь наслаждение. Шлем один юный домашний налево развитый."
        ],
        "uz": [
          "Else service leg police. Find but must ball new.",
          "Seat win expert dark. Team right total body let east. Finish human here start.",
          "Follow direction environmental west hundred page somebody."
        ]
      },
      "test_answers": [
        {
          "id": 1,
          "answer": {
            "ru": [
              "Редактор низкий мальчишка рота собеседник успокоиться прежний. Доставать коммунизм возбуждение ответить анализ выдержать скользить.",
              "Салон прошептать о. Прежде трубка выгнать пастух. Столетие упор степь холодно жестокий войти вперед.",
              "Выраженный тюрьма отражение мимо сутки жидкий. Ребятишки уточнить через вскинуть уничтожение.",
              "Ботинок около о мимо ведь наслаждение. Шлем один юный домашний налево развитый."
            ],
            "uz": [
              "Else service leg police. Find but must ball new.",
              "Seat win expert dark. Team right total body let east. Finish human here start.",
              "Follow direction environmental west hundred page somebody."
            ]
          },
          "is_correct": true
        },
        {
          "id": 18,
          "answer": {
            "ru": "ночь",
            "uz": "financial"
          },
          "is_correct": false
        },
        {
          "id": 19,
          "answer": {
            "ru": "наткнуться",
            "uz": "if"
          },
          "is_correct": false
        },
        {
          "id": 20,
          "answer": {
            "ru": "налоговый",
            "uz": "fund"
          },
          "is_correct": false
        },
        {
          "id": 21,
          "answer": {
            "ru": "ныне",
            "uz": "however"
          },
          "is_correct": true
        }
      ],
      "chosen_variant": {
        "ru": [
          "Сравнение основание хозяйка кузнец дружно заработать. Вообще мягкий оставить расстройство дурацкий заведение привлекать. Ученый сустав тысяча кольцо коллектив разнообразный дремать.",
          "Штаб мягкий полоска расстройство поздравлять единый салон. Фонарик зима находить. Миг поезд рассуждение интеллектуальный выраженный изучить серьезный наступать.",
          "Страсть радость вывести руководитель. Сынок левый поколение каюта приходить. Рассуждение около подземный витрина полевой поезд.",
          "Рота господь затянуться сохранять нож смеяться. Труп интернет материя ломать карман решение налево дальний."
        ],
        "uz": [
          "Environmental shake hard anything every. Happen foot great few. Eye common measure fill shake teach form.",
          "Physical describe body meeting hard. Concern speak morning for law. It than particularly foot blood. Father nation claim identify hit then most.",
          "Politics few expect parent. Responsibility first pass drug. Summer have wind stock maybe."
        ]
      }
    }
  ]
```

## Students ID List
Список id студентов (на страничке боковая панель справа)  
## `GET` `/teacher/task_check/<int:module_course_id>/student/list/`
>URL:  
 module_course_id - идентификатор модуля курса  
  

>RESPONSE:   
id - получателя задания 
student - идентификатор студента данного курса 


```json
 [
    {
      "id": 1,
      "student": 1,
      "task_answer_collection_id": 1
    }
  ]
```



## Mark Create
Создание оценки за набор заданий (и одновременно комментариев)
## `POST` `/teacher/task_check/mark/create/`  

>REQUEST:   
```json
{
  "task_answer_collection":5,
"total_mark":100,
"task_answer_review":[{"student_task_answer":3,
"is_correct":true,
"comment":"This is comment"}]
}
```

>RESPONSE:   
```json
{
  "id": 4,
  "task_answer_collection": 1,
  "total_mark": 100
}
```



# Ссылки для фронта содержащие списки 
Список ответов конкретного студента конкретного курса

## `GET` `/teacher/task_check/<int:module_course_id>/<int:student_id>/task_answer/list/'`  
  
>URL:  
 module_course_id - идентификатор модуля курса  
 student_id - идентификатор студента  

>RESPONSE:    
id - идентификатор задания   
task_answer_collection - идентификатор коллекции заданий
task - идентификатор задания    
is_test -  булевская переменная для определения тест или нет   
chosen_variant - ответ студента на тест
plain_answer - ответ студента на казус   
```json
[
    {
      "id": 1,
      "task_answer_collection": 1,
      "task": 1,
      "is_test": false,
      "chosen_variant": null,
      "plain_answer": "This is answer"
    },
    {
      "id": 2,
      "task_answer_collection": 1,
      "task": 9,
      "is_test": true,
      "chosen_variant": 1,
      "plain_answer": ""
    },
    {
      "id": 3,
      "task_answer_collection": 1,
      "task": 9,
      "is_test": true,
      "chosen_variant": 5,
      "plain_answer": ""
    }
]
```

## Students mark list 
Список оценок за набор заданий для определенного студента 
## `GET` `/teacher/task_check/<int:module_course_id>/<int:student_id>/task_answer_review_collection/list/`  

>URL:  
 module_course_id - идентификатор модуля курса  
 student_id - идентификатор студента

>RESPONSE:     
id - идентификатор оценок за коллекцию заданий   
task_answer_collection - идентификатор коллекции заданий   
total_mark - общая оценка    
```json
{
  "id": 4,
  "task_answer_collection": 1,
  "total_mark": 100
}
```


## Tasks comments list 
Список комментариев для заданий определенного студента
## `GET` `/teacher/task_check/<int:module_course_id>/<int:student_id>/task_answer_review/list/`   

>URL:  
 module_course_id - идентификатор модуля курса  
 student_id - идентификатор студента   

>RESPONSE:   
id - идентификатор комментариев для задания     
task_answer_collection - идентификатор коллекции заданий     
student_task_answer - идентификатор ответов студента   
is_correct -  булевская переменная, правильный ли ответ на задание    
comment - комментарий

```json
{
            "id": 3,
            "task_answer_review_collection": 3,
            "student_task_answer": 1,
            "is_correct": true,
            "comment": "this is comment"
        }
```

# Ссылки для обновления, удаления и детального просмотра комментариев и оценок 



```
## Ссылка для удаления комментария к определенному заданию

## `DELETE` `/teacher/task_check/task_answer_review/<int:id>/`   

## Ссылка для детального просмтора комментария к определенному заданию   

## `GET` `/teacher/task_check/task_answer_review/<int:id>/`   


## Ссылка для обновления оценок к колеекции заданий (одновременно комментариев)

## `PATCH` `/teacher/task_check/task_answer_review_collection/<int:id>/`    
   
>URL:    
id - идентификатор оценок  

>REQUEST:      
```json
{{
	"task_answer_collection":5,
	"total_mark":100,
	"task_answer_review": [
		{
			"id":5,
			"is_correct":true,
			"comment":"This is comment version 2"
		},
		{	
			"student_task_answer":2,
			"is_correct":true,
			"comment":"This is comment"
		}
	]
}
}
```
>RESPONSE:     
id - идентификатор оценок за коллекцию заданий   
task_answer_collection - идентификатор коллекции заданий   
total_mark - общая оценка    
```json
{
  "id": 4,
  "task_answer_collection": 1,
  "total_mark": 100
}
```   
##Ссылка для удаления оценки    
 ## `DELETE` `/teacher/task_check/task_answer_review_collection/<int:id>/`
 

## Ссылка для детального просмотра оценки    
## `GET` `/teacher/task_check/task_answer_review_collection/<int:id>/`