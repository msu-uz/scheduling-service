from django.db.models.signals import post_save
from django.dispatch import receiver
from api.models import TaskAnswerReviewCollection, Journal


@receiver(post_save, sender=TaskAnswerReviewCollection)
def journal_score_from_tasks(sender, created, update_fields, instance: TaskAnswerReviewCollection, **kwargs):
    event = instance.task_answer_collection.task_collection_receiver.event
    student = instance.task_answer_collection.task_collection_receiver.student
    msg = instance.task_answer_collection.task_collection_receiver.event.module_student_group
    if created:
        """ POST - Create"""
        if instance.total_mark:
            Journal.objects.create(event=event, student=student, module_student_group=msg, score=instance.total_mark)
    else:
        """ PUT/PATCH - Update"""
        if update_fields:
            if 'total_mark' in update_fields:
                journal = Journal.objects.filter(event=event, student=student, module_student_group=msg).last()
                journal.score = instance.total_mark
                journal.save()
