from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from api.models import (ModuleCourse, TaskAnswerCollection, TaskAnswerReviewCollection, TaskAnswer, Answer,
                        TaskAnswerReview, TaskCollectionReceiver)


class TaskCollectionReceiverList(serializers.ModelSerializer):
    """
    Special serializer for student id list
    """

    class Meta:
        model = TaskCollectionReceiver
        fields = ['id', 'student']

    def to_representation(self, instance: TaskCollectionReceiver):
        response = super().to_representation(instance)
        response['task_answer_collection_id'] = instance.taskanswercollection.id
        return response


class TaskAnswerListSerializer(serializers.ModelSerializer):
    """
    Special serializer for task_type = test
    """

    class Meta:
        model = TaskAnswer
        fields = ['id', 'task_answer_collection', 'task', 'is_test', 'chosen_variant', 'plain_answer']


class AnswerSerializer(serializers.ModelSerializer):
    """
    special serializer for list of answers
    """

    class Meta:
        model = Answer
        fields = ['id', 'answer', 'is_correct']


class TaskAnswerReviewCollectionSerializer(serializers.ModelSerializer):
    """
    Standard serializer for CRUD
    """

    class Meta:
        model = TaskAnswerReviewCollection
        fields = ['id', 'task_answer_collection', 'total_mark', 'reviewer']


class TaskAnswerReviewSerializer(serializers.ModelSerializer):
    """
    Standard serializer for CRUD
    """

    class Meta:
        model = TaskAnswerReview
        fields = ['id', 'task_answer_review_collection', 'student_task_answer', 'is_correct', 'comment']


class TaskAnswerReviewShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskAnswerReview
        fields = ['id', 'student_task_answer', 'is_correct', 'comment']
        extra_kwargs = {
            "id": {"read_only": False, "required": False}
        }

    def to_internal_value(self, data):
        return super(TaskAnswerReviewShortSerializer, self).to_internal_value(data)

    def validate(self, attrs):
        return super(TaskAnswerReviewShortSerializer, self).validate(attrs)


class ModuleCourseAnswerSerializer(serializers.ModelSerializer):
    """
    Special serializer for the first page of task_check.  Consist of module_course name and count of unchecked tasks
    """
    name = serializers.JSONField(source='discipline.name')

    class Meta:
        model = ModuleCourse
        fields = ('id', 'name')
        read_only_fields = ('id',)

    def to_representation(self, instance):
        response = super(ModuleCourseAnswerSerializer, self).to_representation(instance)
        count_all = TaskAnswerCollection.objects.filter(
            task_collection_receiver__event__module_student_group__module_course=instance).count()
        count_checked = TaskAnswerReviewCollection.objects.filter(
            task_answer_collection__task_collection_receiver__event__module_student_group__module_course=instance).count()
        response['count'] = count_all - count_checked
        return response


class TaskAnswerCollectionSerializer(serializers.ModelSerializer):
    """
    Special serializer for the second page of task_check.
    """
    student_id = serializers.IntegerField(source='task_collection_receiver.student.id')

    class Meta:
        model = TaskAnswerCollection
        fields = ('id', 'student_id', 'created_at')
        read_only_fields = ('id',)

    def to_representation(self, instance: TaskAnswerCollection):
        response = super().to_representation(instance)
        is_checked = False
        if instance.task_collection_receiver.event.lesson.lesson:
            response['type'] = 'lesson'
        elif instance.task_collection_receiver.event.lesson.int_control:
            response['type'] = 'intermediate_control'
        elif instance.task_collection_receiver.event.lesson.fin_control:
            response['type'] = 'final_control'

        response['created_at'] = instance.created_at.date()
        if hasattr(instance, "taskanswerreviewcollection"):
            total_mark_object = instance.taskanswerreviewcollection
            if total_mark_object:
                total_mark = total_mark_object.total_mark
                check_date = total_mark_object.updated_at
                if total_mark:
                    is_checked = True
                    response['mark'] = total_mark
                    response['checked_date'] = check_date.date()
                    response['is_checked'] = is_checked

        else:
            response['is_checked'] = is_checked

        return response


class TaskAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskAnswer
        fields = ['id', 'is_test']

    def to_representation(self, instance: TaskAnswer):
        response = super().to_representation(instance)
        # # if hasattr(instance, "task_answer_collection") and hasattr(instance.task_answer_collection,
        # #                                                            "taskanswerreviewcollection"):
        # #     response['total_mark_object'] = instance.task_answer_collection.taskanswerreviewcollection.total_mark
        #
        if hasattr(instance, "taskanswerreview"):
            response['task_answer_review'] = TaskAnswerReviewSerializer(instance.taskanswerreview).data
        response['question'] = instance.task.question
        answers = instance.task.answer_set
        correct_answer = answers.filter(is_correct=True).first()
        correct_answer_serialized = AnswerSerializer(correct_answer).data
        response['correct_answer'] = correct_answer_serialized['answer']
        if instance.is_test:
            response['test_answers'] = AnswerSerializer(answers, many=True).data
            response['chosen_variant'] = instance.chosen_variant.answer

        else:
            response['plain_answer'] = instance.plain_answer
        return response


class TaskAnswerReviewCollectionCreateSerializer(serializers.ModelSerializer):
    """
    Serializer for create
    """

    task_answer_review = TaskAnswerReviewShortSerializer(many=True, required=False)

    class Meta:
        model = TaskAnswerReviewCollection
        fields = ['id', 'task_answer_collection', 'total_mark', 'task_answer_review']

    def create(self, validated_data):
        reviewer = self.context['request'].user.teacher

        validated_data["reviewer"] = reviewer
        tar = validated_data.pop('task_answer_review', None)

        tarc = super().create(validated_data)
        if tar:
            for item in tar:
                is_correct = item.pop("is_correct", None)
                task_answer = item['student_task_answer']
                if task_answer.is_test:
                    correct_answers = task_answer.task.answer_set.filter(is_correct=True)
                    if task_answer.chosen_variant in correct_answers:
                        is_correct = True
                    else:
                        is_correct = False

                TaskAnswerReview.objects.create(is_correct=is_correct, task_answer_review_collection=tarc, **item)

        return tarc

    def update(self, instance, validated_data):
        tar = validated_data.pop('task_answer_review', None)
        validated_data.pop('task_answer_collection', None)

        tarc = super().update(instance, validated_data)
        tarc.save(update_fields=validated_data.keys())
        if tar:
            for item in tar:
                if review_id := item.get("id"):
                    item.pop("student_task_answer", None)
                    TaskAnswerReview.objects.filter(id=review_id).update(**item)
                else:
                    is_correct = item.pop("is_correct", None)
                    task_answer = item['student_task_answer']

                    if task_answer.is_test:
                        correct_answers = task_answer.task.answer_set.filter(is_correct=True)
                        if task_answer.chosen_variant in correct_answers:
                            is_correct = True
                        else:
                            is_correct = False
                    TaskAnswerReview.objects.create(is_correct=is_correct, task_answer_review_collection=tarc, **item)

        return tarc


class TaskAnswerCollectionMarkSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskAnswerCollection
        fields = []

    def to_representation(self, instance: TaskAnswerCollection):
        response = super(TaskAnswerCollectionMarkSerializer, self).to_representation(instance)
        if hasattr(instance, 'taskanswerreviewcollection'):
            response['total_mark'] = instance.taskanswerreviewcollection.total_mark
            response['task_answer_review_collection_id'] = instance.taskanswerreviewcollection.id
        return response
