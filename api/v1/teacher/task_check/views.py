from django.shortcuts import get_object_or_404
from django.utils.datetime_safe import datetime
from rest_framework.exceptions import NotFound
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView

from api.models import ModuleCourse, TaskAnswerCollection, TaskAnswer, TaskAnswerReview, TaskAnswerReviewCollection, TaskCollectionReceiver
from api.v1.permissions import TeacherPermission
from api.v1.teacher.task_check.serializers import (ModuleCourseAnswerSerializer, TaskAnswerCollectionSerializer,
    TaskCollectionReceiverList, TaskAnswerReviewCollectionCreateSerializer, TaskAnswerCollectionMarkSerializer)
from api.v1.teacher.task_check.serializers import (TaskAnswerSerializer,
                                                   TaskAnswerReviewCollectionSerializer, TaskAnswerListSerializer,
                                                   TaskAnswerReviewSerializer)


class ModuleListAPIView(ListAPIView):
    serializer_class = ModuleCourseAnswerSerializer
    permission_classes = [TeacherPermission]

    def get_queryset(self):
        qs = ModuleCourse.objects.filter(modulestudentgroup__teacher=self.request.user.teacher.id).distinct()  # 1
        return qs


class TasksReviewList(ListAPIView):
    serializer_class = TaskAnswerCollectionSerializer

    permission_classes = [TeacherPermission]

    def get_queryset(self):
        module_course_id = self.kwargs.get('module_course_id')
        qs = TaskAnswerCollection.objects.filter(
            deadline__lte=datetime.now(),
            task_collection_receiver__event__module_student_group__teacher=self.request.user.teacher.id,
            task_collection_receiver__event__module_student_group__module_course_id=module_course_id
        ).select_related(
            "task_collection_receiver",
            "task_collection_receiver__event",
            "task_collection_receiver__event__module_student_group",
            "task_collection_receiver__student",
            "task_collection_receiver__event__lesson",
            "taskanswerreviewcollection").order_by('-created_at')
        return qs


class TaskAnswerList(ListAPIView):
    serializer_class = TaskAnswerSerializer
    permission_classes = [TeacherPermission]

    def get_queryset(self):
        tac_id = self.kwargs.get('task_answer_collection_id')
        qs = get_object_or_404(TaskAnswerCollection, pk=tac_id)
        return qs.taskanswer_set.all()

    def get(self, request, *args, **kwargs):
        tac = TaskAnswerCollection.objects.filter(id=self.kwargs.get('task_answer_collection_id')).first()
        if not tac:
            raise NotFound("TaskAnswerCollection")
        response = self.list(request, *args, **kwargs)
        response.data = {
            "results": response.data,
            "mark": TaskAnswerCollectionMarkSerializer(tac).data,
        }
        return response


class TaskAnswerReviewCollectionCreate(CreateAPIView):
    """
    Создание оценки за набор заданий
    """
    serializer_class = TaskAnswerReviewCollectionCreateSerializer
    permission_classes = [TeacherPermission]


class TaskAnswerReviewCollectionList(ListAPIView):
    """
    Список оценок за набор заданий для определенного курса и студента (для фронта)
    Нужно ли в фильтр добавлять препода?
    """
    serializer_class = TaskAnswerReviewCollectionSerializer
    permission_classes = [TeacherPermission]

    def get_queryset(self):
        module_course_id = self.kwargs.get('module_course_id')
        student_id = self.kwargs.get('student_id')
        qs = TaskAnswerReviewCollection.objects.filter(
            task_answer_collection__task_collection_receiver__student_id=student_id,
            task_answer_collection__task_collection_receiver__event__module_student_group__module_course_id=module_course_id,
            task_answer_collection__task_collection_receiver__event__module_student_group__teacher=self.request.user.teacher.id)  # 1
        return qs


class TaskAnswerReviewList(ListAPIView):
    """
    Список ответов студента с комментариями препода (для фронта)
    Нужно ли для препода в фильтр добавлять?
    """
    serializer_class = TaskAnswerReviewSerializer
    permission_classes = [TeacherPermission]

    def get_queryset(self):
        module_course_id = self.kwargs.get('module_course_id')
        student_id = self.kwargs.get('student_id')
        qs = TaskAnswerReview.objects.filter(
            task_answer_review_collection__task_answer_collection__task_collection_receiver__student_id=student_id,
            task_answer_review_collection__task_answer_collection__task_collection_receiver__event__module_student_group__module_course_id=module_course_id,
            task_answer_review_collection__task_answer_collection__task_collection_receiver__event__module_student_group__teacher=self.request.user.teacher.id)  # 1
        return qs


class StudentTaskAnswerList(ListAPIView):
    """
    Список ответов конкретного  студента конкретного курса
    """
    serializer_class = TaskAnswerListSerializer
    permission_classes = [TeacherPermission]

    def get_queryset(self):
        module_course_id = self.kwargs.get('module_course_id')
        student_id = self.kwargs.get('student_id')
        qs = TaskAnswer.objects.filter(
            task_answer_collection__task_collection_receiver__student_id=student_id,
            task_answer_collection__task_collection_receiver__event__module_student_group__module_course_id=module_course_id,
            task_answer_collection__task_collection_receiver__event__module_student_group__teacher=self.request.user.teacher.id)  # 1
        return qs


class TaskAnswerReviewRUDView(RetrieveUpdateDestroyAPIView):
    """
    Read Update Delete View for TaskAnswerReview model
    """
    serializer_class = TaskAnswerReviewSerializer
    permission_classes = [TeacherPermission]
    lookup_url_kwarg = 'id'
    queryset = TaskAnswerReview.objects.all()


class TaskAnswerReviewCollectionRUDView(RetrieveUpdateDestroyAPIView):
    """
    Read Update Delete View for TaskAnswerReviewCollection model
    """
    # serializer_class = TaskAnswerReviewCollectionSerializer
    serializer_class = TaskAnswerReviewCollectionCreateSerializer
    permission_classes = [TeacherPermission]
    lookup_url_kwarg = 'id'
    queryset = TaskAnswerReviewCollection.objects.all()


class StudentIdList(ListAPIView):
    """
    Список id студентов для данного модуля и преподавателя
    """
    serializer_class = TaskCollectionReceiverList
    permission_classes = [TeacherPermission]
    lookup_url_kwarg = 'module_course_id'

    def get_queryset(self):
        module_course_id = self.kwargs.get('module_course_id')
        qs = TaskCollectionReceiver.objects.filter(
            event__module_student_group__module_course_id=module_course_id,
            event__module_student_group__teacher=self.request.user.teacher.id,
            event__teacher__taskanswerreviewcollection__isnull=True,
            taskanswercollection__isnull=False
        )

        return qs
