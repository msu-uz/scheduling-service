from django.urls import path
from .views import (ModuleListAPIView, TasksReviewList, TaskAnswerList,
                    TaskAnswerReviewCollectionCreate, StudentTaskAnswerList, TaskAnswerReviewCollectionList,
                    TaskAnswerReviewList, StudentIdList, TaskAnswerReviewRUDView, TaskAnswerReviewCollectionRUDView)


urlpatterns = [
    path('modules/list/', ModuleListAPIView.as_view()),  # for the first page

    path('<int:module_course_id>/list/', TasksReviewList.as_view()),  # for the second page


    # for the third page
    path('<int:module_course_id>/<int:task_answer_collection_id>/answers/list/', TaskAnswerList.as_view()),
    path('<int:module_course_id>/student/list/', StudentIdList.as_view()),
    path('mark/create/', TaskAnswerReviewCollectionCreate.as_view()),




    # for frontend
    path('<int:module_course_id>/<int:student_id>/task_answer/list/',
         StudentTaskAnswerList.as_view()),
    path('<int:module_course_id>/<int:student_id>/task_answer_review_collection/list/',
         TaskAnswerReviewCollectionList.as_view()),
    path('<int:module_course_id>/<int:student_id>/task_answer_review/list/',
         TaskAnswerReviewList.as_view()),

    # update delete detail TaskAnswerReview, TaskAnswerReviewCollection
    path('task_answer_review/<int:id>/', TaskAnswerReviewRUDView.as_view()),
    path('task_answer_review_collection/<int:id>/', TaskAnswerReviewCollectionRUDView.as_view()),

]
