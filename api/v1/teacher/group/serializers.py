from rest_framework import serializers
from api.models.group import Student, StudentGroup


class StudentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Student
        fields = ('id',)

    def to_representation(self, instance):
        response = super(StudentSerializer, self).to_representation(instance)
        request = self.context['request']

        if instance.applicant:
            response['image'] = request.build_absolute_uri(
                instance.applicant.image.url)
        elif instance.employee:
            response['image'] = instance.employee.image

        response['first_name'] = instance.user.first_name
        response['middle_name'] = instance.user.middle_name
        response['last_name'] = instance.user.last_name

        return response


class GroupListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = StudentGroup
        fields = [
            'id',
            'name',
            'department',
            'course',

        ]

    def to_representation(self, instance):
        response = super(GroupListSerializer, self).to_representation(instance)
        response['course'] = ''
        if instance.course:
            response['course'] = instance.course.name
        response['count'] = StudentGroup.get_students(instance).count()
        return response
