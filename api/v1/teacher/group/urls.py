from django.urls import path, include
from .views import TeacherGroupListView, StudentListView

# teacher_room

urlpatterns = [
    path("list/", TeacherGroupListView.as_view()),
    path("<int:group_id>/", StudentListView.as_view()),
]
