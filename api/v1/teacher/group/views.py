from rest_framework import generics

from api.v1.permissions import TeacherPermission
from api.models.group import StudentGroup
from api.models.role import Student, Teacher
from api.v1.permissions import TeacherPermission
from .serializers import (
    StudentSerializer,
    GroupListSerializer,
)
from rest_framework import filters


class TeacherGroupListView(generics.ListAPIView):
    serializer_class = GroupListSerializer
    permission_classes = [
        TeacherPermission,
    ]
    filter_backends = (filters.SearchFilter,)
    search_fields = [
        "name",
    ]

    def get_queryset(self):
        teacher = Teacher.objects.get(user_id=self.request.user.id)
        return StudentGroup.objects.filter(
            modulestudentgroup__teacher=teacher
        ).distinct()


class StudentListView(generics.ListAPIView):
    serializer_class = StudentSerializer
    permission_classes = (TeacherPermission,)

    def get_queryset(self):
        group_id = self.kwargs["group_id"]

        return Student.objects.filter(studentgroupforming__group_id=group_id)
