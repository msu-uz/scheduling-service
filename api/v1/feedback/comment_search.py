from django.db.models import Q


def filter_comment(search, comment, obj, cs):
    return comment.filter(
        Q(comment__icontains=search) &
        Q(student=obj) &
        Q(course_id=cs)
    )


def search_comment(search, queryset, obj, cs):
    words = len(search.split())
    split = search.split()
    for word in range(words):
        search = split[word]
        queryset = filter_comment(search, queryset, obj, cs)
    return queryset
