from typing import Dict
from api.models.discipline import Course
from api.models.role import Student


def get_course_statistics(course: Course) -> Dict:
    students_count = Student.objects.filter(
        studentgroupforming__group__modulestudentgroup__module_course__course=course
    ).distinct().count()

    if students_count:
        course_students_responded = Student.objects.filter(
            studentgroupforming__group__modulestudentgroup__module_course__course=course,
            coursefeedback__course_feedback_answer__course_feedback_question__course=course,
        ).distinct().count()

        module_course_students_responded = Student.objects.filter(
            studentgroupforming__group__modulestudentgroup__module_course__course=course,
            modulecoursefeedback__module_course_feedback_answer__module_course_feedback_question__module_course__course=course,
        ).distinct().count()

        response = {
            "course_responded": course_students_responded,
            "course_not_responded": students_count - course_students_responded,
            "module_course_responded": module_course_students_responded * 100 // students_count,
            "module_course_not_responded": 100 - module_course_students_responded * 100 // students_count
        }

    else:
        response = {
            "course_responded": 0,
            "course_not_responded": 0,
            "module_course_responded": 0,
            "module_course_not_responded": 0
        }

    return response
