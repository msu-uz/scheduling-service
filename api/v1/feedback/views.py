from django.db.models import Q
from rest_framework.exceptions import NotFound
from rest_framework.filters import SearchFilter
from rest_framework.generics import (CreateAPIView, DestroyAPIView, ListAPIView, UpdateAPIView)

from api.models.discipline import Course, ModuleCourse
from api.models.feedback import (CourseFeedback, CourseFeedbackComment, CourseFeedbackQuestion, ModuleCourseFeedback, ModuleCourseFeedbackQuestion)
from api.models.group import StudentGroup
from api.models.role import Student

from api.v1.feedback.serializers import (CourseFeedbackCommentSerializer, CourseFeedbackCommentShortSerializer, CourseFeedbackQuestionSerializer, CourseFeedbackQuestionStatisticsSerializer, CourseFeedbackQuestionUpdateSerializer, CourseFeedbackSerializer, CourseListSerializer, ModuleCourseFeedbackQuestionSerializer, ModuleCourseFeedbackQuestionStatisticsSerializer, ModuleCourseFeedbackQuestionUpdateSerializer, ModuleCourseFeedbackSerializer, ModuleCourseSerializer, StudentGroupSerializer, StudentSerializer)
from api.v1.feedback.statistics import get_course_statistics
from api.v1.permissions import EducationOfficeUnitPermission


class CourseListView(ListAPIView):
    """
    Список курсов.
    """

    queryset = Course.objects.all()
    serializer_class = CourseListSerializer
    permission_classes = (EducationOfficeUnitPermission,)


class StudentGroupListView(ListAPIView):
    """
    Список групп для курса.

    URL params: course_id
    """

    serializer_class = StudentGroupSerializer
    permission_classes = (EducationOfficeUnitPermission,)

    def get_queryset(self):
        course_id = self.kwargs["course_id"]

        return StudentGroup.objects.filter(
            modulestudentgroup__module_course__course_id=course_id).distinct()


class ModuleCourseListView(ListAPIView):
    """
    Список модулей для курса.

    URL params: course_id
    """

    serializer_class = ModuleCourseSerializer
    permission_classes = (EducationOfficeUnitPermission,)

    def get_queryset(self):
        course_id = self.kwargs["course_id"]
        return ModuleCourse.objects.filter(
            course_id=course_id).select_related("discipline")


class StudentListView(ListAPIView):
    """
    Список студентов, которые оставляли фидбек, а также статистика по курсу.

    URL params: course_id
    Query params: group_id (необязательно)
    """

    serializer_class = StudentSerializer
    permission_classes = (EducationOfficeUnitPermission,)

    def get_queryset(self):
        course_id = self.kwargs["course_id"]
        group_id = self.request.query_params.get("group_id")

        query = Q(coursefeedback__isnull=False, studentgroupforming__group__course_id=course_id)

        if group_id:
            query &= Q(studentgroupforming__group_id=group_id)

        return Student.objects.filter(query).distinct().select_related("applicant", "employee")

    def get(self, request, *args, **kwargs):
        course_id = self.kwargs["course_id"]
        course = Course.objects.filter(id=course_id).first()

        if not course:
            raise NotFound("Course not found")

        response = self.list(request, *args, **kwargs)
        response.data.update(
            {"statistics": get_course_statistics(course=course)}
        )

        return response


# CourseFeedbackQuestion


class CourseFeedbackQuestionCreateView(CreateAPIView):
    """
    Создание вопроса обратной связи по курсу со списком ответов.
    """

    queryset = CourseFeedbackQuestion.objects.all(
    ).prefetch_related("coursefeedbackanswer_set")
    serializer_class = CourseFeedbackQuestionSerializer
    permission_classes = (EducationOfficeUnitPermission,)


class CourseFeedbackQuestionUpdateDestroyView(UpdateAPIView, DestroyAPIView):
    """
    Обновление вопроса обратной связи по курсу со списком ответов.

    URL params: id - идентификатор вопроса
    """

    queryset = CourseFeedbackQuestion.objects.all(
    ).prefetch_related("coursefeedbackanswer_set")
    serializer_class = CourseFeedbackQuestionUpdateSerializer
    permission_classes = (EducationOfficeUnitPermission,)
    lookup_url_kwarg = "id"

    def perform_destroy(self, instance):
        instance.coursefeedbackanswer_set.all().delete()
        return super().perform_destroy(instance)


class CourseFeedbackQuestionListView(ListAPIView):
    """
    Список вопросов обратной связи по курсу.

    URL params: course_id
    """

    serializer_class = CourseFeedbackQuestionSerializer
    permission_classes = (EducationOfficeUnitPermission,)

    def get_queryset(self):
        course_id = self.kwargs["course_id"]
        return CourseFeedbackQuestion.objects.filter(
            course_id=course_id).prefetch_related("coursefeedbackanswer_set")


class CourseFeedbackQuestionListStatisticsView(ListAPIView):
    """
    Список вопросов по курсу со статистикой по ответам.
    """

    serializer_class = CourseFeedbackQuestionStatisticsSerializer
    permission_classes = (EducationOfficeUnitPermission,)

    def get_queryset(self):
        course_id = self.kwargs["course_id"]
        return CourseFeedbackQuestion.objects.filter(
            course_id=course_id).prefetch_related(
            "coursefeedbackanswer_set",
            "coursefeedbackanswer_set__course_feedback_question")


# ModuleCourseFeedbackQuestion


class ModuleCourseFeedbackQuestionCreateView(CreateAPIView):
    """
    Создание вопроса обратной связи по модулю со списком ответов.
    """

    queryset = ModuleCourseFeedbackQuestion.objects.all().prefetch_related(
        "modulecoursefeedbackanswer_set"
    )
    serializer_class = ModuleCourseFeedbackQuestionSerializer
    permission_classes = (EducationOfficeUnitPermission,)


class ModuleCourseFeedbackQuestionUpdateDestroyView(
        UpdateAPIView, DestroyAPIView):
    """
    Обновление вопроса обратной связи по модулю со списком ответов.

    URL params: id - идентификатор вопроса
    """

    queryset = ModuleCourseFeedbackQuestion.objects.all().prefetch_related(
        "modulecoursefeedbackanswer_set"
    )
    serializer_class = ModuleCourseFeedbackQuestionUpdateSerializer
    permission_classes = (EducationOfficeUnitPermission,)
    lookup_url_kwarg = "id"

    def perform_destroy(self, instance):
        instance.modulecoursefeedbackanswer_set.all().delete()
        return super().perform_destroy(instance)


class ModuleCourseFeedbackQuestionListView(ListAPIView):
    """
    Список вопросов обратной связи по модулю.

    URL params: module_id
    """

    serializer_class = ModuleCourseFeedbackQuestionSerializer
    permission_classes = (EducationOfficeUnitPermission,)

    def get_queryset(self):
        module_id = self.kwargs["module_id"]
        return ModuleCourseFeedbackQuestion.objects.filter(
            module_course_id=module_id
        ).prefetch_related("modulecoursefeedbackanswer_set")


class ModuleCourseFeedbackQuestionListStatisticsView(ListAPIView):
    """
    Список вопросов по модулю со статистикой по ответам.
    """

    serializer_class = ModuleCourseFeedbackQuestionStatisticsSerializer
    permission_classes = (EducationOfficeUnitPermission,)

    def get_queryset(self):
        module_id = self.kwargs["module_id"]
        return ModuleCourseFeedbackQuestion.objects.filter(
            module_course_id=module_id
        ).prefetch_related(
            "modulecoursefeedbackanswer_set",
            "modulecoursefeedbackanswer_set__module_course_feedback_question",
        )


# CourseFeedback


class CourseFeedbackListView(ListAPIView):
    """
    Список фидбеков по курсу, оставленных студентом.

    URL params: course_id
                student_id
    """

    serializer_class = CourseFeedbackSerializer
    permission_classes = (EducationOfficeUnitPermission,)

    def get_queryset(self):
        course_id = self.kwargs["course_id"]
        student_id = self.kwargs["student_id"]

        return (
            CourseFeedback.objects.filter(
                student=student_id,
                course_feedback_answer__course_feedback_question__course_id=course_id,
            )
            .select_related(
                "student",
                "course_feedback_answer",
                "course_feedback_answer__course_feedback_question",
            )
            .prefetch_related(
                "course_feedback_answer__course_feedback_question__coursefeedbackanswer_set"
            )
        )

    def get(self, request, *args, **kwargs):
        student_id = self.kwargs["student_id"]
        student = Student.objects.filter(id=student_id).first()

        if not student:
            raise NotFound("Student not found")

        response = self.list(request, *args, **kwargs)
        context = super().get_serializer_context()
        response.data.update({"student": StudentSerializer(student, context=context).data})

        return response


# ModuleCourseFeedback


class ModuleCourseFeedbackListView(ListAPIView):
    """
    Список фидбеков по модулю, оставленных студентом.

    URL params: module_id
                student_id
    """

    serializer_class = ModuleCourseFeedbackSerializer
    permission_classes = (EducationOfficeUnitPermission,)

    def get_queryset(self):
        module_id = self.kwargs["module_id"]
        student_id = self.kwargs["student_id"]

        return (
            ModuleCourseFeedback.objects.filter(
                student=student_id,
                module_course_feedback_answer__module_course_feedback_question__module_course_id=module_id,
            )
            .select_related(
                "student",
                "module_course_feedback_answer",
                "module_course_feedback_answer__module_course_feedback_question",
            )
            .prefetch_related(
                "module_course_feedback_answer__module_course_feedback_question__modulecoursefeedbackanswer_set"
            )
        )

    def get(self, request, *args, **kwargs):
        student_id = self.kwargs["student_id"]
        student = Student.objects.filter(id=student_id).first()

        if not student:
            raise NotFound("Student not found")

        response = self.list(request, *args, **kwargs)
        context = super().get_serializer_context()
        response.data.update({"student": StudentSerializer(student, context=context).data})

        return response


# CourseFeedbackComment


class CourseFeedbackCommentListView(ListAPIView):
    """
    Список всех комментариев обратной связи по курсу.

    URL params: course_id
    """

    serializer_class = CourseFeedbackCommentSerializer
    permission_classes = (EducationOfficeUnitPermission,)
    filter_backends = (SearchFilter,)
    search_fields = ("text",)

    def get_queryset(self):
        course_id = self.kwargs["course_id"]
        return CourseFeedbackComment.objects.filter(
            course_id=course_id).select_related("student")


class StudentCourseFeedbackCommentListView(ListAPIView):
    """
    Список комментариев обратной связи по курсу, оставленных студентом.

    URL params: course_id
                student_id
    """

    serializer_class = CourseFeedbackCommentShortSerializer
    permission_classes = (EducationOfficeUnitPermission,)
    filter_backends = (SearchFilter,)
    search_fields = ("text",)

    def get_queryset(self):
        course_id = self.kwargs["course_id"]
        student_id = self.kwargs["student_id"]

        return CourseFeedbackComment.objects.filter(
            student=student_id, course_id=course_id)

    def get(self, request, *args, **kwargs):
        student_id = self.kwargs["student_id"]
        student = Student.objects.filter(id=student_id).first()

        if not student:
            raise NotFound("Student not found")

        response = self.list(request, *args, **kwargs)
        context = super().get_serializer_context()
        response.data.update({"student": StudentSerializer(student, context=context).data})

        return response
