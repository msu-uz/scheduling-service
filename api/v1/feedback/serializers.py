from rest_framework.exceptions import ValidationError
from api.models.group import StudentGroup
from api.models.discipline import Course, ModuleCourse
from api.models.feedback import (
    CourseFeedback,
    CourseFeedbackAnswer,
    CourseFeedbackComment,
    CourseFeedbackQuestion,
    ModuleCourseFeedback,
    ModuleCourseFeedbackAnswer,
    ModuleCourseFeedbackQuestion,
)
from api.models.role import Student
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer


class CourseListSerializer(ModelSerializer):
    class Meta:
        model = Course
        fields = ("id", "name")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["course_question_count"] = instance.coursefeedbackquestion_set.count()
        response["module_question_count"] = ModuleCourseFeedbackQuestion.objects.filter(module_course__course=instance).count()
        return response


class StudentGroupSerializer(ModelSerializer):
    class Meta:
        model = StudentGroup
        fields = ("id", "name")


class ModuleCourseSerializer(ModelSerializer):
    class Meta:
        model = ModuleCourse
        fields = ("id",)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["name"] = instance.discipline.name
        return response


class StudentSerializer(ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = Student
        fields = ("id", "image")

    def get_image(self, instance):
        if instance.applicant:
            return self.context["request"].build_absolute_uri(instance.applicant.image.url)
        elif instance.employee:
            return instance.employee.image

    def to_representation(self, instance):
        response = super().to_representation(instance)

        if course_id := self.context["request"].parser_context["kwargs"].get("course_id"):
            course = Course.objects.get(id=course_id)
            response["course"] = course.name

        if module_id := self.context["request"].parser_context["kwargs"].get("module_id"):
            module_course = ModuleCourse.objects.get(id=module_id)
            response["module"] = module_course.discipline.name

        if instance.applicant:
            response.update({
                "first_name": instance.applicant.first_name,
                "last_name": instance.applicant.last_name,
                "second_name": instance.applicant.second_name,
            })
        elif instance.employee:
            response.update({
                "first_name": instance.employee.first_name,
                "last_name": instance.employee.last_name,
                "second_name": instance.employee.second_name,
            })

        return response


# CourseFeedbackQuestion


class CourseFeedbackAnswerSerializer(ModelSerializer):
    class Meta:
        model = CourseFeedbackAnswer
        fields = ("id", "text")
        read_only_fields = ("id",)

    def validate_text(self, value):
        if isinstance(value, dict):
            return value
        raise ValidationError("Text must be an object")


class CourseFeedbackQuestionSerializer(ModelSerializer):
    """
    Используется для создания CourseFeedbackQuestion.
    При создании принимает также список ответов типа CourseFeedbackAnswer.
    """

    text = serializers.JSONField()
    answers = CourseFeedbackAnswerSerializer(many=True,
                                             allow_empty=False,
                                             source="coursefeedbackanswer_set")

    class Meta:
        model = CourseFeedbackQuestion
        fields = ("id", "text", "course", "answers")
        read_only_fields = ("id",)

    def validate_text(self, value):
        if isinstance(value, dict):
            return value
        raise ValidationError("Text must be an object")

    def create(self, validated_data):
        answers_data = validated_data.pop("coursefeedbackanswer_set")
        instance = super().create(validated_data)

        answers = []
        for answer_data in answers_data:
            answers.append(CourseFeedbackAnswer(course_feedback_question=instance,
                                                **answer_data))

        CourseFeedbackAnswer.objects.bulk_create(answers)
        return instance


class CourseFeedbackAnswerUpdateSerializer(ModelSerializer):
    id = serializers.IntegerField(read_only=False, required=False)
    text = serializers.JSONField(required=False)

    class Meta:
        model = CourseFeedbackAnswer
        fields = ("id", "text")

    def validate_text(self, value):
        if isinstance(value, dict):
            return value
        raise ValidationError("Text must be an object")


class CourseFeedbackQuestionUpdateSerializer(ModelSerializer):
    """
    Используется для обновления CourseFeedbackQuestion.
    При обновлении принимает также список ответов типа CourseFeedbackAnswer.
    Если у ответа указывается id, то обновляется существующий ответ с таким id.
    Если у ответа не указывается id, создаётся новый ответ.
    Все ранее созданные ответы, кроме переданных, удяляются.
    """

    answers = CourseFeedbackAnswerUpdateSerializer(many=True,
                                                   allow_empty=True,
                                                   source="coursefeedbackanswer_set")

    class Meta:
        model = CourseFeedbackQuestion
        fields = ("id", "text", "answers")
        read_only_fields = ("id",)

    def validate_text(self, value):
        if isinstance(value, dict):
            return value
        raise ValidationError("Text must be an object")

    def update(self, instance, validated_data):
        answers_data = validated_data.pop("coursefeedbackanswer_set", [])

        text = validated_data.pop("text", [])
        instance.text.update(text)
        instance.save()

        instance = super().update(instance, validated_data)
        answer_ids = []

        for answer_data in answers_data:
            if answer_id := answer_data.pop("id", None):
                if answer := CourseFeedbackAnswer.objects.filter(
                        id=answer_id, course_feedback_question=instance).first():
                    text = answer_data.pop("text", [])
                    answer.text.update(text)
                    answer.save()
                answer_ids.append(answer_id)
            else:
                answer = CourseFeedbackAnswer.objects.create(course_feedback_question=instance,
                                                             **answer_data)
                answer_ids.append(answer.id)

        if answer_ids:
            instance.coursefeedbackanswer_set.exclude(id__in=answer_ids).delete()

        return instance


class CourseFeedbackAnswerStatisticsSerializer(ModelSerializer):
    """
    Использутся для получения статистики по CourseFeedbackAnswer.

    Fields:
    percentage - содержит процент таких ответов в отношении к общему
                 количеству ответов на вопрос, соответствующий этому ответу.
    """

    percentage = serializers.SerializerMethodField()

    class Meta:
        model = CourseFeedbackAnswer
        fields = ("id", "text", "percentage")

    def get_percentage(self, instance):
        question = instance.course_feedback_question

        all_feedback_count = CourseFeedback.objects.filter(
            course_feedback_answer__course_feedback_question=question
        ).count()

        if all_feedback_count:
            this_feedback_count = instance.coursefeedback_set.count()
            return this_feedback_count * 100 // all_feedback_count
        else:
            return 0


class CourseFeedbackQuestionStatisticsSerializer(ModelSerializer):
    """
    Использутся для получения CourseFeedbackQuestion и списка
    CourseFeedbackAnswer с соответствующей статистикой.
    """

    answers = CourseFeedbackAnswerStatisticsSerializer(
        many=True, source="coursefeedbackanswer_set"
    )

    class Meta:
        model = CourseFeedbackQuestion
        fields = ("id", "text", "answers")


# ModuleCourseFeedbackQuestion


class ModuleCourseFeedbackAnswerSerializer(ModelSerializer):
    class Meta:
        model = ModuleCourseFeedbackAnswer
        fields = ("id", "text")
        read_only_fields = ("id",)

    def validate_text(self, value):
        if isinstance(value, dict):
            return value
        raise ValidationError("Text must be an object")


class ModuleCourseFeedbackQuestionSerializer(ModelSerializer):
    """
    Используется для создания ModuleCourseFeedbackQuestion.
    При создании принимает также список ответов типа ModuleCourseFeedbackAnswer.
    """

    answers = ModuleCourseFeedbackAnswerSerializer(
        many=True, allow_empty=False, source="modulecoursefeedbackanswer_set"
    )

    class Meta:
        model = ModuleCourseFeedbackQuestion
        fields = ("id", "text", "module_course", "answers")
        read_only_fields = ("id",)

    def validate_text(self, value):
        if isinstance(value, dict):
            return value
        raise ValidationError("Text must be an object")

    def create(self, validated_data):
        answers_data = validated_data.pop("modulecoursefeedbackanswer_set")
        instance = super().create(validated_data)

        answers = []
        for answer_data in answers_data:
            answers.append(
                ModuleCourseFeedbackAnswer(module_course_feedback_question=instance,
                                           ** answer_data))

        ModuleCourseFeedbackAnswer.objects.bulk_create(answers)
        return instance


class ModuleCourseFeedbackAnswerUpdateSerializer(ModelSerializer):
    id = serializers.IntegerField(read_only=False, required=False)
    text = serializers.JSONField(required=False)

    class Meta:
        model = ModuleCourseFeedbackAnswer
        fields = ("id", "text")

    def validate_text(self, value):
        if isinstance(value, dict):
            return value
        raise ValidationError("Text must be an object")


class ModuleCourseFeedbackQuestionUpdateSerializer(ModelSerializer):
    """
    Используется для обновления ModuleCourseFeedbackQuestion.
    При обновлении принимает также список ответов типа ModuleCourseFeedbackAnswer.
    Если у ответа указывается id, то обновляется существующий ответ с таким id.
    Если у ответа не указывается id, создаётся новый ответ.
    Все ранее созданные ответы, кроме переданных, удяляются.
    """

    answers = ModuleCourseFeedbackAnswerUpdateSerializer(many=True,
                                                         allow_empty=True,
                                                         source="modulecoursefeedbackanswer_set")

    class Meta:
        model = ModuleCourseFeedbackQuestion
        fields = ("id", "text", "answers")
        read_only_fields = ("id",)

    def validate_text(self, value):
        if isinstance(value, dict):
            return value
        raise ValidationError("Text must be an object")

    def update(self, instance, validated_data):
        answers_data = validated_data.pop("modulecoursefeedbackanswer_set", [])

        text = validated_data.pop("text", [])
        instance.text.update(text)
        instance.save()

        instance = super().update(instance, validated_data)
        answer_ids = []

        for answer_data in answers_data:
            if answer_id := answer_data.pop("id", None):
                if answer := ModuleCourseFeedbackAnswer.objects.filter(
                        id=answer_id, module_course_feedback_question=instance).first():
                    text = answer_data.pop("text", [])
                    answer.text.update(text)
                    answer.save()
                answer_ids.append(answer_id)
            else:
                answer = ModuleCourseFeedbackAnswer.objects.create(
                    module_course_feedback_question=instance, **answer_data)
                answer_ids.append(answer.id)

        if answer_ids:
            instance.modulecoursefeedbackanswer_set.exclude(
                id__in=answer_ids).delete()

        return instance


class ModuleCourseFeedbackAnswerStatisticsSerializer(ModelSerializer):
    """
    Использутся для получения статистики по ModuleCourseFeedbackAnswer.

    Fields:
    percentage - содержит процент таких ответов в отношении к общему
                 количеству ответов на вопрос, соответствующий этому ответу.
    """

    percentage = serializers.SerializerMethodField()

    class Meta:
        model = ModuleCourseFeedbackAnswer
        fields = ("id", "text", "percentage")

    def get_percentage(self, instance):
        question = instance.module_course_feedback_question

        all_feedback_count = ModuleCourseFeedback.objects.filter(
            module_course_feedback_answer__module_course_feedback_question=question
        ).count()

        if all_feedback_count:
            this_feedback_count = instance.modulecoursefeedback_set.count()
            return this_feedback_count * 100 // all_feedback_count
        else:
            return 0


class ModuleCourseFeedbackQuestionStatisticsSerializer(ModelSerializer):
    """
    Использутся для получения ModuleCourseFeedbackQuestion и списка
    ModuleCourseFeedbackAnswer с соответствующей статистикой.
    """

    answers = ModuleCourseFeedbackAnswerStatisticsSerializer(
        many=True, source="modulecoursefeedbackanswer_set"
    )

    class Meta:
        model = ModuleCourseFeedbackQuestion
        fields = ("id", "text", "answers")


# CourseFeedback


class CourseFeedbackSerializer(ModelSerializer):
    answer = CourseFeedbackAnswerSerializer(source="course_feedback_answer")
    question = CourseFeedbackQuestionSerializer(
        source="course_feedback_answer.course_feedback_question"
    )

    class Meta:
        model = CourseFeedback
        fields = ("id", "answer", "question")


# ModuleCourseFeedback


class ModuleCourseFeedbackSerializer(ModelSerializer):
    answer = ModuleCourseFeedbackAnswerSerializer(
        source="module_course_feedback_answer")
    question = ModuleCourseFeedbackQuestionSerializer(
        source="module_course_feedback_answer.module_course_feedback_question"
    )

    class Meta:
        model = CourseFeedback
        fields = ("id", "answer", "question")


# CourseFeedbackComment


class CourseFeedbackCommentSerializer(ModelSerializer):
    student = StudentSerializer()

    class Meta:
        model = CourseFeedbackComment
        fields = ("id", "text", "student", "created_at")


class CourseFeedbackCommentShortSerializer(ModelSerializer):
    class Meta:
        model = CourseFeedbackComment
        fields = ("id", "text", "created_at")
