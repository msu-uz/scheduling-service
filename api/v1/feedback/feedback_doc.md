# Обратная связь в кабинете учебного отдела

## 1. `GET - feedback/course/list/`

Список всех курсов. Для каждого курса указано количество вопросов по курсу и по модулям, входящих в курс.

### Response:

```json
[
  {
    "id": 1,
    "name": {
      "ru": "Акушер",
      "uz": "Corporate treasurer"
    },
    "course_question_count": 3,
    "module_question_count": 3
  }
]
```

---

## 2. `GET - feedback/course/<int:course_id>/student/list/?group_id=<int>`

Список студентов, оставлявших фидбек по курсу или по модулям внутри курса.

### URL params:

| Название  | Описание            |
| --------- | ------------------- |
| course_id | Идентификатор курса |

### Query params:

| Название | Описание                             |
| -------- | ------------------------------------ |
| group_id | Идентификатор группы (необязательно) |

### Response fields:

| Название                               | Описание                          |
| -------------------------------------- | --------------------------------- |
| results                                | Список студентов                  |
| statistics.course_responded            | Количество ответивших по курсу    |
| statistics.course_not_responded        | Количество не ответивших по курсу |
| statistics.module_course_responded     | Процент ответивших по модулям     |
| statistics.module_course_not_responded | Процент не ответивших по модулям  |

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "image": "http://127.0.0.1:8000/media/face_1suZyGc.jpg",
      "first_name": "Захаров",
      "last_name": "Касьян",
      "second_name": "Владиленович"
    }
  ],
  "statistics": {
    "course_responded": 0,
    "course_not_responded": 8,
    "module_course_responded": 0,
    "module_course_not_responded": 100
  }
}
```

---

## 3. `GET - feedback/course/<int:course_id>/module/list/`

Список модулей внутри курса.

### URL params:

| Название  | Описание            |
| --------- | ------------------- |
| course_id | Идентификатор курса |

### Response:

```json
[
  {
    "id": 4,
    "name": {
      "ru": "Маркшейдер",
      "uz": "Chartered public finance accountant"
    }
  }
]
```

---

## 4. `GET - feedback/course/<int:course_id>/question/list/`

Список вопросов по курсу.

### URL params:

| Название  | Описание            |
| --------- | ------------------- |
| course_id | Идентификатор курса |

### Response:

```json
[
  {
    "id": 4,
    "text": { "ru": "Вопрос по курсу 2", "uz": "Question for course2" },
    "course": 2,
    "answers": [
      {
        "id": 13,
        "text": { "ru": "Вариант", "uz": "Variant" }
      },
      {
        "id": 14,
        "text": { "ru": "Вариант", "uz": "Variant" }
      },
      {
        "id": 15,
        "text": { "ru": "Вариант", "uz": "Variant" }
      },
      {
        "id": 16,
        "text": { "ru": "Вариант", "uz": "Variant" }
      }
    ]
  }
]
```

---

## 5. `GET - feedback/course/<int:course_id>/comment/list/?search=<string>`

Список комментариев по курсу с поиском по тексту комментария.

### URL params:

| Название  | Описание            |
| --------- | ------------------- |
| course_id | Идентификатор курса |

### Query params:

| Название | Описание                                   |
| -------- | ------------------------------------------ |
| search   | Поиск по тексту комментария (скрытая фича) |

### Response:

```json
[
  {
    "id": 2,
    "text": "Мой комментарий",
    "student": {
      "id": 40,
      "image": "http://127.0.0.1:8000/media/face_VwUlp4v.jpg",
      "first_name": "Ольга",
      "last_name": "Юльевна",
      "second_name": "Мамонтова"
    }
  }
]
```

---

## 7. `GET - feedback/course/<int:course_id>/student/<int:student_id>/feedback/list/`

Фидбек студента по курсу (список вопросов с вариантами ответа на вопросы и с ответом студнета на каждый вопрос).

### URL params:

| Название   | Описание               |
| ---------- | ---------------------- |
| course_id  | Идентификатор курса    |
| student_id | Идентификатор студента |

### Response fields:

| Название         | Описание                          |
| ---------------- | --------------------------------- |
| answer           | Ответ студента                    |
| question         | Вопрос-объект                     |
| question.answers | Список вариантов ответа на вопрос |

### Response:

```json
{
  "results": [
    {
      "id": 22,
      "answer": {
        "id": 8,
        "text": { "ru": "Вариант", "uz": "Variant" }
      },
      "question": {
        "id": 2,
        "text": { "ru": "Вопрос для курса", "uz": "Question for course" },
        "course": 4,
        "answers": [
          {
            "id": 7,
            "text": { "ru": "Вариант", "uz": "Variant" }
          },
          {
            "id": 8,
            "text": { "ru": "Вариант", "uz": "Variant" }
          },
          {
            "id": 9,
            "text": { "ru": "Вариант", "uz": "Variant" }
          }
        ]
      }
    }
  ],
  "student": {
    "id": 40,
    "image": "http://127.0.0.1:8000/media/face_XLDQM8w.jpg",
    "first_name": "Руслан",
    "last_name": "Августович",
    "second_name": "Гущин",
    "module": {
      "ru": "Ихтиолог",
      "uz": "Engineer, manufacturing"
    }
  }
}
```

---

## 8. `GET - feedback/module/<int:module_id>/student/<int:student_id>/feedback/list/`

Фидбек студента по модулю (список вопросов с вариантами ответа на вопросы и с ответом студнета на каждый вопрос).

### URL params:

| Название   | Описание               |
| ---------- | ---------------------- |
| module_id  | Идентификатор модуля   |
| student_id | Идентификатор студента |

### Response fields:

| Название         | Описание                          |
| ---------------- | --------------------------------- |
| answer           | Ответ студента                    |
| question         | Вопрос-объект                     |
| question.answers | Список вариантов ответа на вопрос |

### Response:

```json
{
  "results": [
    {
      "id": 12,
      "answer": {
        "id": 26,
        "text": { "ru": "Вариант", "uz": "Variant" }
      },
      "question": {
        "id": 6,
        "text": { "ru": "Вопрос", "uz": "Question" },
        "module_course": 2,
        "answers": [
          {
            "id": 26,
            "text": { "ru": "Вариант", "uz": "Variant" }
          },
          {
            "id": 27,
            "text": { "ru": "Вариант", "uz": "Variant" }
          },
          {
            "id": 28,
            "text": { "ru": "Вариант", "uz": "Variant" }
          }
        ]
      }
    }
  ],
  "student": {
    "id": 40,
    "image": "http://127.0.0.1:8000/media/face_XLDQM8w.jpg",
    "first_name": "Руслан",
    "last_name": "Августович",
    "second_name": "Гущин",
    "course": {
      "ru": "Монтажник связи",
      "uz": "Automotive engineer"
    }
  }
}
```

---

## 9. `GET - feedback/course/<int:course_id>/student/<int:student_id>/comment/list/?search=<string>`

Список комментариев студента по курсу с поиском по тексту комментария.

### URL params:

| Название   | Описание               |
| ---------- | ---------------------- |
| module_id  | Идентификатор модуля   |
| student_id | Идентификатор студента |

### Query params:

| Название | Описание                    |
| -------- | --------------------------- |
| search   | Поиск по тексту комментария |

### Response:

```json
{
  "results": [
    {
      "id": 2,
      "text": "Мой комментарий 1"
    },
    {
      "id": 3,
      "text": "Мой комментарий 2"
    }
  ],
  "student": {
    "id": 40,
    "image": "http://127.0.0.1:8000/media/face_XLDQM8w.jpg",
    "first_name": "Руслан",
    "last_name": "Августович",
    "second_name": "Гущин",
    "course": {
      "ru": "Монтажник связи",
      "uz": "Automotive engineer"
    }
  }
}
```

---

## 10. `GET - feedback/course/<int:course_id>/question/list/statistics/`

Подробная статистика по курсу (список вопросов со статистикиой по каждому варианту ответа).

### URL params:

| Название  | Описание            |
| --------- | ------------------- |
| course_id | Идентификатор курса |

### Response:

```json
[
  {
    "id": 4,
    "text": { "ru": "Вопрос по курсу 2", "uz": "Question for course2" },
    "answers": [
      {
        "id": 13,
        "text": { "ru": "Вариант", "uz": "Variant" },
        "percentage": 0
      },
      {
        "id": 14,
        "text": { "ru": "Вариант", "uz": "Variant" },
        "percentage": 0
      },
      {
        "id": 15,
        "text": { "ru": "Вариант", "uz": "Variant" },
        "percentage": 0
      },
      {
        "id": 16,
        "text": { "ru": "Вариант", "uz": "Variant" },
        "percentage": 0
      }
    ]
  }
]
```

---

## 11. `POST - feedback/course/question/create/`

Создание вопроса по курсу.

### Requset:

```json
{
  "text": { "ru": "Вопрос для курса", "uz": "Question for course" },
  "course": 1,
  "answers": [
    {
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "text": { "ru": "Вариант", "uz": "Variant" }
    }
  ]
}
```

### Response:

```json
{
  "id": 5,
  "text": { "ru": "Вопрос для курса", "uz": "Question for course" },
  "course": 1,
  "answers": [
    {
      "id": 17,
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "id": 18,
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "id": 19,
      "text": { "ru": "Вариант", "uz": "Variant" }
    }
  ]
}
```

---

## 12. `POST - feedback/course/question/<int:question_id>/update/`

Обновление вопроса по курсу (в том числе вариантов ответа).

Для обновления варианта ответа указывается его `id` и `text`. Если `id` какого-либо варианта не указывается или его тело не подаётся вовсе, то этот вариант удаляется.

Если подаются варианты с посторонними `id`, они игнорируются.

### URL params:

| Название    | Описание              |
| ----------- | --------------------- |
| question_id | Идентификатор вопроса |

### Requset:

```json
{
  "text": { "ru": "Вопрос для курса", "uz": "Question for course" },
  "course": 1,
  "answers": [
    {
      "id": 10,
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "id": 11,
      "text": { "ru": "Изменение", "uz": "Variant" }
    },
    {
      "id": 12,
      "text": { "ru": "Вариант", "uz": "Variant" }
    }
  ]
}
```

### Response:

```json
{
  "id": 3,
  "text": { "ru": "Вопрос для курса", "uz": "Question for course" },
  "answers": [
    {
      "id": 10,
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "id": 11,
      "text": { "ru": "Изменение", "uz": "Variant" }
    },
    {
      "id": 12,
      "text": { "ru": "Вариант", "uz": "Variant" }
    }
  ]
}
```

---

## 13. `DELETE - feedback/course/question/<int:question_id>/delete/`

Удаление вопроса по курсу.

### URL params:

| Название    | Описание              |
| ----------- | --------------------- |
| question_id | Идентификатор вопроса |

### Response:

```json
No body
```

---

## 14. `GET - feedback/module/<int:module_id>/question/list/`

Список вопросов по модулю.

### URL params:

| Название  | Описание             |
| --------- | -------------------- |
| module_id | Идентификатор модуля |

### Response:

```json
[
  {
    "id": 6,
    "text": { "ru": "Вопрос", "uz": "Question" },
    "module_course": 2,
    "answers": [
      {
        "id": 26,
        "text": { "ru": "Вариант", "uz": "Variant" }
      },
      {
        "id": 27,
        "text": { "ru": "Вариант", "uz": "Variant" }
      },
      {
        "id": 28,
        "text": { "ru": "Вариант", "uz": "Variant" }
      }
    ]
  }
]
```

---

## 15. `GET - feedback/module/<int:module_id>/question/list/statistics/`

Подробная статистика по модулю (список вопросов со статистикиой по каждому варианту ответа).

### URL params:

| Название  | Описание             |
| --------- | -------------------- |
| module_id | Идентификатор модуля |

### Response:

```json
[
  {
    "id": 3,
    "text": { "ru": "Вопрос", "uz": "Question" },
    "answers": [
      {
        "id": 17,
        "text": { "ru": "Вариант", "uz": "Variant" },
        "percentage": 0
      },
      {
        "id": 18,
        "text": { "ru": "Вариант", "uz": "Variant" },
        "percentage": 0
      },
      {
        "id": 19,
        "text": { "ru": "Вариант", "uz": "Variant" },
        "percentage": 0
      }
    ]
  }
]
```

---

## 16. `POST - feedback/module/question/create/`

Создание вопроса по модулю.

### Requset:

```json
{
  "text": { "ru": "Вопрос", "uz": "Question" },
  "module_course": 2,
  "answers": [
    {
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "text": { "ru": "Вариант", "uz": "Variant" }
    }
  ]
}
```

### Response:

```json
{
  "id": 9,
  "text": { "ru": "Вопрос", "uz": "Question" },
  "module_course": 2,
  "answers": [
    {
      "id": 34,
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "id": 35,
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "id": 36,
      "text": { "ru": "Вариант", "uz": "Variant" }
    }
  ]
}
```

---

## 17. `POST - feedback/module/question/<int:question_id>/update/`

Обновление вопроса по модулю (в том числе вариантов ответа).

Для обновления варианта ответа указывается его `id` и `text`. Если `id` какого-либо варианта не указывается или его тело не подаётся вовсе, то этот вариант удаляется.

Если подаются варианты с посторонними `id`, они игнорируются.

### URL params:

| Название    | Описание              |
| ----------- | --------------------- |
| question_id | Идентификатор вопроса |

### Requset:

```json
{
  "text": { "ru": "Вопрос", "uz": "Question" },
  "module_course": 2,
  "answers": [
    {
      "id": 34,
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "id": 35,
      "text": { "ru": "Изменение", "uz": "Variant" }
    },
    {
      "id": 36,
      "text": { "ru": "Вариант", "uz": "Variant" }
    }
  ]
}
```

### Response:

```json
{
  "id": 9,
  "text": { "ru": "Вопрос", "uz": "Question" },
  "answers": [
    {
      "id": 34,
      "text": { "ru": "Вариант", "uz": "Variant" }
    },
    {
      "id": 35,
      "text": { "ru": "Изменение", "uz": "Variant" }
    },
    {
      "id": 36,
      "text": { "ru": "Вариант", "uz": "Variant" }
    }
  ]
}
```

---

## 18. `DELETE - feedback/module/question/<int:question_id>/delete/`

Удаление вопроса по модулю.

### URL params:

| Название    | Описание              |
| ----------- | --------------------- |
| question_id | Идентификатор вопроса |

### Response:

```json
No body
```

---

## 19. `GET - feedback/course/<int:course_id>/group/list/`

Список всех групп для курса.

### URL params:

| Название  | Описание            |
| --------- | ------------------- |
| course_id | Идентификатор курса |

### Response:

```json
[
  {
    "id": 1,
    "name": "M1"
  }
]
```

---
