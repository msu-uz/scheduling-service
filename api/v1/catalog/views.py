from django.http import JsonResponse
from rest_framework.generics import (ListAPIView, CreateAPIView, UpdateAPIView, DestroyAPIView, RetrieveUpdateAPIView,
                                     ListCreateAPIView, RetrieveAPIView, RetrieveUpdateDestroyAPIView)
from rest_framework.views import APIView

from api.models import (Language, Nationality, CatalogLanguage, Organization, Position, TaskType, CourseType,
                        LibraryElementType, City, District, LibraryElementCategory, Category, TypeOfBook)
from api.models.resources import ResourcesType
from api.v1.admin.ekadr_request import *
from api.v1.catalog.serializers import (LanguageListSerializer, NationalityListSerializer,
                                        CatalogLanguageListSerializer, OrganizationListSerializer,
                                        PositionListSerializer, TaskTypeListSerializer, CourseTypeListSerializer,
                                        CityListSerializer, DistrictListSerializer,
                                        LibraryElementCategoryListSerializer, LibraryElementTypeListSerializer,
                                        CategorySerializer, CityListByLanguageSerializer, CategoryCreateSerializer,
                                        CategoryListSerializer, ResourcesTypeSerializer, TypeOfBookSerializer)
from api.v1.permissions import AdministratorPermission, LibrarianPermission


# ListViews


class LanguageListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = LanguageListSerializer

    def get_queryset(self):
        qs = Language.objects.all()
        id = self.request.GET.get('id')
        if id:
            qs = qs.filter(id=id)
        return qs


class NationalityListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = NationalityListSerializer

    def get_queryset(self):
        qs = Nationality.objects.all()
        # lgid = self.request.GET.get('language_group_id')
        # if lgid:
        #     qs = Nationality.objects.filter(language_group_id=lgid)
        # lgcode = self.request.GET.get('lang_code')
        # stext = self.request.GET.get('search')
        # if id:
        #     qs = qs.filter(language_group_id=lgid)
        # if lgcode and stext:
        #     qs = qs.filter(language__code=lgcode, name__icontains=stext)
        return qs


class CatalogLanguageListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = CatalogLanguageListSerializer

    def get_queryset(self):
        qs = CatalogLanguage.objects.all()
        return qs


class OrganizationListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = OrganizationListSerializer

    def get_queryset(self):
        qs = Organization.objects.all()
        return qs


class PositionListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = PositionListSerializer

    def get_queryset(self):
        qs = Position.objects.all()
        return qs


class TaskTypeListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = TaskTypeListSerializer

    def get_queryset(self):
        qs = TaskType.objects.all()
        return qs


class CourseTypeListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = CourseTypeListSerializer

    def get_queryset(self):
        qs = CourseType.objects.all()
        return qs


class LibraryElementTypeListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = LibraryElementTypeListSerializer

    def get_queryset(self):
        qs = LibraryElementType.objects.all()
        return qs


class CityListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = CityListSerializer

    def get_queryset(self):
        qs = City.objects.all()
        return qs


class CityListByLanguageAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = CityListByLanguageSerializer
    queryset = Language.objects.all()


class DistrictListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = DistrictListSerializer

    def get_queryset(self):
        qs = District.objects.all()
        return qs


class LibraryElementCategoryListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = LibraryElementCategoryListSerializer

    def get_queryset(self):
        qs = LibraryElementCategory.objects.all()
        return qs


class CategoryListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = CategoryListSerializer

    def get_queryset(self):
        qs = Category.objects.filter(parent=None)
        return qs


class CategoriesListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = CategoryListSerializer

    def get_queryset(self):
        qs = Category.objects.all()
        return qs


class CategoryDetailAPIView(RetrieveAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = CategoryCreateSerializer
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        qs = Category.objects.filter(id=self.kwargs['id'])
        return qs


class CategoryCreateDetailAPIView(ListCreateAPIView):
    permission_classes = [AdministratorPermission, ]
    serializer_class = CategoryCreateSerializer
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        qs = Category.objects.filter(id=self.kwargs['id'])
        return qs


class ResourcesTypeListAPIView(ListAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = ResourcesType.objects.all()
    serializer_class = ResourcesTypeSerializer


# CreateViews


class LanguageCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Language.objects.all()
    serializer_class = LanguageListSerializer


class NationalityCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Nationality.objects.all()
    serializer_class = NationalityListSerializer


class CatalogLanguageCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = CatalogLanguage.objects.all()
    serializer_class = CatalogLanguageListSerializer


class OrganizationCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Organization.objects.all()
    serializer_class = OrganizationListSerializer


class PositionCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Position.objects.all()
    serializer_class = PositionListSerializer


class TaskTypeCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = TaskType.objects.all()
    serializer_class = TaskTypeListSerializer


class CourseTypeCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = CourseType.objects.all()
    serializer_class = CourseTypeListSerializer


class LibraryElementTypeCreateListAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = LibraryElementType.objects.all()
    serializer_class = LibraryElementTypeListSerializer


class CityCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = City.objects.all()
    serializer_class = CityListSerializer


class DistrictCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = District.objects.all()
    serializer_class = DistrictListSerializer


class LibraryElementCategoryCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = LibraryElementCategory.objects.all()
    serializer_class = LibraryElementCategoryListSerializer


class CategoryCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Category.objects.all()
    serializer_class = CategoryCreateSerializer


class ResourcesTypeCreateAPIView(CreateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = ResourcesType.objects.all()
    serializer_class = ResourcesTypeSerializer


# UpdateViews


class LanguageUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Language.objects.all()
    serializer_class = LanguageListSerializer
    lookup_url_kwarg = 'id'


class NationalityUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Nationality.objects.all()
    serializer_class = NationalityListSerializer
    lookup_url_kwarg = 'id'


class CatalogLanguageUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = CatalogLanguage.objects.all()
    serializer_class = CatalogLanguageListSerializer
    lookup_url_kwarg = 'id'


class OrganizationUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Organization.objects.all()
    serializer_class = OrganizationListSerializer
    lookup_url_kwarg = 'id'


class PositionUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Position.objects.all()
    serializer_class = PositionListSerializer
    lookup_url_kwarg = 'id'


class TaskTypeUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = TaskType.objects.all()
    serializer_class = TaskTypeListSerializer
    lookup_url_kwarg = 'id'


class CourseTypeUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = CourseType.objects.all()
    serializer_class = CourseTypeListSerializer
    lookup_url_kwarg = 'id'


class LibraryElementTypetUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = LibraryElementType.objects.all()
    serializer_class = LibraryElementTypeListSerializer
    lookup_url_kwarg = 'id'


class CityUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = City.objects.all()
    serializer_class = CityListSerializer
    lookup_url_kwarg = 'id'


class DistrictUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = District.objects.all()
    serializer_class = DistrictListSerializer
    lookup_url_kwarg = 'id'


class LibraryElementCategoryUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = LibraryElementCategory.objects.all()
    serializer_class = LibraryElementCategoryListSerializer
    lookup_url_kwarg = 'id'


class CategoryUpdateAPIView(RetrieveUpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Category.objects.all()
    serializer_class = CategoryListSerializer
    lookup_url_kwarg = 'id'


class ResourcesTypeUpdateAPIView(UpdateAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = ResourcesType.objects.all()
    serializer_class = ResourcesTypeSerializer
    lookup_url_kwarg = 'id'


# DestroyViews


class LanguageDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Language.objects.all()
    serializer_class = LanguageListSerializer
    lookup_url_kwarg = 'id'


class NationalityDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Nationality.objects.all()
    serializer_class = NationalityListSerializer
    lookup_url_kwarg = 'id'


class CatalogLanguageDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = CatalogLanguage.objects.all()
    serializer_class = CatalogLanguageListSerializer
    lookup_url_kwarg = 'id'


class OrganizationDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Organization.objects.all()
    serializer_class = OrganizationListSerializer
    lookup_url_kwarg = 'id'


class PositionDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Position.objects.all()
    serializer_class = PositionListSerializer
    lookup_url_kwarg = 'id'


class TaskTypeDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = TaskType.objects.all()
    serializer_class = TaskTypeListSerializer
    lookup_url_kwarg = 'id'


class CourseTypeDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = CourseType.objects.all()
    serializer_class = CourseTypeListSerializer
    lookup_url_kwarg = 'id'


class LibraryElementTypeDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = LibraryElementType.objects.all()
    serializer_class = LibraryElementTypeListSerializer
    lookup_url_kwarg = 'id'


class CityDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = City.objects.all()
    serializer_class = CityListSerializer
    lookup_url_kwarg = 'id'


class DistrictDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = District.objects.all()
    serializer_class = DistrictListSerializer
    lookup_url_kwarg = 'id'


class LibraryElementCategoryDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = LibraryElementCategory.objects.all()
    serializer_class = LibraryElementCategoryListSerializer
    lookup_url_kwarg = 'id'


class CategoryDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    lookup_url_kwarg = 'id'


class ResourcesTypeDestroyAPIView(DestroyAPIView):
    permission_classes = [AdministratorPermission, ]
    queryset = ResourcesType.objects.all()
    serializer_class = ResourcesTypeSerializer
    lookup_url_kwarg = 'id'


class TypeOfBookListView(ListAPIView):
    """
    Список типов книг.
    """

    serializer_class = TypeOfBookSerializer
    permission_classes = (AdministratorPermission,)
    queryset = TypeOfBook.objects.all()


class TypeOfBookCreateView(CreateAPIView):
    serializer_class = TypeOfBookSerializer
    permission_classes = (AdministratorPermission,)
    queryset = TypeOfBook.objects.all()


class TypeOfBookEditView(RetrieveUpdateDestroyAPIView):
    serializer_class = TypeOfBookSerializer
    permission_classes = (AdministratorPermission,)
    queryset = TypeOfBook.objects.all()
    lookup_url_kwarg = 'id'
