from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField

from api.models import (Language, Nationality, CatalogLanguage, Organization, Position, TaskType, CourseType,
                        LibraryElementType, City, District, LibraryElementCategory, Category, TypeOfBook)
from api.models.resources import ResourcesType


class LanguageListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Language
        fields = (
            'id',
            'name',
            'code',
            'created_at',
            'updated_at',
        )


#
# class ExtraContextMetaclass(SerializerMetaclass):
#     def __new__(cls, name, bases, dct):
#         """
#
#         :param name:  name of class to be created
#         :param bases: sub classes
#         :param dct: other attributes
#         :return:
#         """
#
#         new_class = super().__new__(cls, name, bases, dct)
#         new_class.Meta = cls.dynamic_meta(new_class.Meta)
#         return new_class
#
#     @classmethod
#     def dynamic_meta(cls, old_meta):
#         # create a class called "Meta"
#         new_meta = type('Meta', (), dict(fields=None))
#         # CONTEXT_CLASSES = dict()
#         # contexts = ['EmailContext', 'SmsMessageContext', ...]
#
#         # copy over required attributes from original Meta
#         # new_meta.model = old_meta.model
#         new_meta.fields = list(old_meta.fields[:])
#
#         # set new attributes dynamically
#         # for attr_name, instance in CONTEXT_CLASSES.items():
#         #     setattr(new_meta, attr_name, instance)
#         #
#         return new_meta


class CityListByLanguageSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    cities = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Language
        fields = (
            'id',
            'name',
            'code',
            'cities',
            'created_at',
            'updated_at',
        )

    def get_cities(self, obj):
        qs = City.objects.filter(language__code=obj.code)
        return CityListSerializer(qs, many=True).data


#
# class MultilingualContentSerializer(serializers.Serializer, metaclass=ExtraContextMetaclass):
#     class Meta:
#         lc = []
#         ls = Language.objects.all()
#         for language in ls:
#             lc.append(language.code)
#         fields = lc
#
#
# class MultilingualJsonField(serializers.JSONField):
#     default_error_messages = {
#         'invalid': 'Value must be valid JSON.',
#         'required': 'Some subfields not entered.'
#     }
#
#     def to_internal_value(self, data):
#         try:
#             if self.binary or getattr(data, 'is_json_string', False):
#                 if isinstance(data, bytes):
#                     data = data.decode()
#                 return json.loads(data)
#             else:
#                 json.dumps(data, cls=self.encoder)
#         except (TypeError, ValueError):
#             self.fail('invalid')
#
#         keylist = []
#         ls = Language.objects.all()
#         for language in ls:
#             keylist.append(language.code)
#         value = list(set(keylist) - set(data))
#         if len(value) > 0:
#             self.fail('required')
#         return data


class NationalityListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = Nationality
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        # print(set(name))
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class CatalogLanguageListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = CatalogLanguage
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )
        # read_only_fields = (
        #     'language',
        # )

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        # print(set(name))
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class OrganizationListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = Organization
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        # print(set(name))
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class PositionListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = Position
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        # print(set(name))
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class TaskTypeListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = TaskType
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        # print(set(name))
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class CourseTypeListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = CourseType
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        # print(set(name))
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class LibraryElementTypeListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = LibraryElementType
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        # print(set(name))
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class CategoryListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'parent',
            'children',
        )
        extra_kwargs = {
            'children': {'read_only': True},
        }

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class CategoryCreateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'parent',
        )
        extra_kwargs = {
            'parent': {'read_only': True},
        }

    def to_representation(self, instance):
        response = super(CategoryCreateSerializer, self).to_representation(instance)
        response['children'] = CategoryListSerializer(instance.children.all(), many=True).data
        print(response['children'])
        return response

    def create(self, validated_data):
        if self.context.get('view').kwargs:
            n = Category(**validated_data, parent_id=self.context.get('view').kwargs['id'])
        else:
            n = Category(**validated_data)
        n.save()
        return n


class CategorySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    children = RecursiveField(many=True)
    name = serializers.JSONField()

    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'parent',
            'children'
        )

    def to_representation(self, instance):
        response = super(CategorySerializer, self).to_representation(instance)
        response['checked'] = False
        if instance.children.count() > 0:
            response['open'] = False
        return response


class CityListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = City
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        # print(set(name))
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class DistrictListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    city_id = serializers.IntegerField(write_only=True)
    city = CityListSerializer(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = District
        fields = (
            'id',
            'name',
            'city',
            'city_id',
            'created_at',
            'updated_at'
        )

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        city_id = validated_data.get('city_id')
        # print(set(name))
        instance.city_id = city_id
        instance.save()
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class LibraryElementCategoryListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = LibraryElementCategory
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        # print(set(name))
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class ResourcesTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourcesType
        fields = ('id', 'name')
        read_only_fields = ('id',)


class TypeOfBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypeOfBook
        fields = ("id", "name")

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance
