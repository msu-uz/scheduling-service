from api.v1.catalog import views
from django.urls import path, include

urlpatterns = [
    #List
    path('language/list/', views.LanguageListAPIView.as_view()),
    path('nationality/list/', views.NationalityListAPIView.as_view()),
    path('cataloglanguage/list/', views.CatalogLanguageListAPIView.as_view()),
    path('organization/list/', views.OrganizationListAPIView.as_view()),
    path('position/list/', views.PositionListAPIView.as_view()),
    path('tasktype/list/', views.TaskTypeListAPIView.as_view()),
    path('coursetype/list/', views.CourseTypeListAPIView.as_view()),
    path('libraryelementtype/list/', views.LibraryElementTypeListAPIView.as_view()),
    path('city/list/', views.CityListAPIView.as_view()),
    path('city/language/list/', views.CityListByLanguageAPIView.as_view()),
    path('district/list/', views.DistrictListAPIView.as_view()),
    path('libraryelementcategory/list/', views.LibraryElementCategoryListAPIView.as_view()),
    path('category/list/', views.CategoryListAPIView.as_view()),
    path('categories/', views.CategoriesListAPIView.as_view()),
    path('category/create/', views.CategoryCreateAPIView.as_view()),
    path('category/<int:id>/', views.CategoryDetailAPIView.as_view()),
    path('category/<int:id>/create/', views.CategoryCreateDetailAPIView.as_view()),
    path('category/<int:id>/update/', views.CategoryUpdateAPIView.as_view()),
    path('category/<int:id>/destroy/', views.CategoryDestroyAPIView.as_view()),
    path('resourcestype/list/', views.ResourcesTypeListAPIView.as_view()),
    path('typeofbook/list/', views.TypeOfBookListView.as_view()),

    #Create
    path('language/create/', views.LanguageCreateAPIView.as_view()),
    path('nationality/create/', views.NationalityCreateAPIView.as_view()),
    path('cataloglanguage/create/', views.CatalogLanguageCreateAPIView.as_view()),
    path('organization/create/', views.OrganizationCreateAPIView.as_view()),
    path('position/create/', views.PositionCreateAPIView.as_view()),
    path('tasktype/create/', views.TaskTypeCreateAPIView.as_view()),
    path('coursetype/create/', views.CourseTypeCreateAPIView.as_view()),
    path('libraryelementtype/create/', views.LibraryElementTypeCreateListAPIView.as_view()),
    path('city/create/', views.CityCreateAPIView.as_view()),
    path('district/create/', views.DistrictCreateAPIView.as_view()),
    path('libraryelementcategory/create/', views.LibraryElementCategoryCreateAPIView.as_view()),
    path('resourcestype/create/', views.ResourcesTypeCreateAPIView.as_view()),
    path('typeofbook/create/', views.TypeOfBookCreateView.as_view()),

    #Update
    path('language/update/<int:id>/', views.LanguageUpdateAPIView.as_view()),
    path('nationality/update/<int:id>/', views.NationalityUpdateAPIView.as_view()),
    path('cataloglanguage/update/<int:id>/', views.CatalogLanguageUpdateAPIView.as_view()),
    path('organization/update/<int:id>/', views.OrganizationUpdateAPIView.as_view()),
    path('position/update/<int:id>/', views.PositionUpdateAPIView.as_view()),
    path('tasktype/update/<int:id>/', views.TaskTypeUpdateAPIView.as_view()),
    path('coursetype/update/<int:id>/', views.CourseTypeUpdateAPIView.as_view()),
    path('libraryelementtype/update/<int:id>/', views.LibraryElementTypetUpdateAPIView.as_view()),
    path('city/update/<int:id>/', views.CityUpdateAPIView.as_view()),
    path('district/update/<int:id>/', views.DistrictUpdateAPIView.as_view()),
    path('libraryelementcategory/update/<int:id>/', views.LibraryElementCategoryUpdateAPIView.as_view()),
    path('resourcestype/update/<int:id>/', views.ResourcesTypeUpdateAPIView.as_view()),
    path('typeofbook/<int:id>/', views.TypeOfBookEditView.as_view()),

    #Destroy
    path('language/destroy/<int:id>/', views.LanguageDestroyAPIView.as_view()),
    path('nationality/destroy/<int:id>/', views.NationalityDestroyAPIView.as_view()),
    path('cataloglanguage/destroy/<int:id>/', views.CatalogLanguageDestroyAPIView.as_view()),
    path('organization/destroy/<int:id>/', views.OrganizationDestroyAPIView.as_view()),
    path('position/destroy/<int:id>/', views.PositionDestroyAPIView.as_view()),
    path('tasktype/destroy/<int:id>/', views.TaskTypeDestroyAPIView.as_view()),
    path('coursetype/destroy/<int:id>/', views.CourseTypeDestroyAPIView.as_view()),
    path('libraryelementtype/destroy/<int:id>/', views.LibraryElementTypeDestroyAPIView.as_view()),
    path('city/destroy/<int:id>/', views.CityDestroyAPIView.as_view()),
    path('district/destroy/<int:id>/', views.DistrictDestroyAPIView.as_view()),
    path('libraryelementcategory/destroy/<int:id>/', views.LibraryElementCategoryDestroyAPIView.as_view()),
    path('resourcestype/destroy/<int:id>/', views.ResourcesTypeDestroyAPIView.as_view()),

    #   /////////////////////////////////// E-kadr //////////////////////////////////////

    path('e_kadr/', include("api.v1.admin.urls")),

]
