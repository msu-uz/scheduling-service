from django.db.models.signals import pre_save, post_save
from django.dispatch.dispatcher import receiver
from api.models.task import Task, TaskHistory, TaskReview, NEW, UPDATED, ACCEPTED, DECLINED


@receiver(post_save, sender=Task)
def task_update(sender, instance, **kwargs):
    if instance.status == NEW | instance.status == UPDATED:
        TaskHistory.objects.create(task=instance, status=instance.status)
    else:
        TaskHistory.objects.create(task=instance, task_review=instance.taskreview_set.last(), status=instance.status)
