# База тестов

---
## `api/v1/task/`
### LIST of URLs
```djangourlpath
    Main:                   Filter:
    
    list/                   [type__task_type; status; task__difficulty; topic__discipline; created_at; ]
    create/
    <int:id>/
    <int:id>/check/
    <int:id>/history/
    
    ===================
    Additional:
    
    statistics/list/
    topic/list/
    type/list/
    task_type/list/
    difficulty/list/
    
```

## `GET` `difficulty/list/`
Список сложностей заданий 
> Response:
```json
{
  "count": 3,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Легкая сложность",
        "uz": "Easy difficulty"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Средняя сложность",
        "uz": "Medium difficulty"
      }
    },
    {
      "id": 3,
      "name": {
        "ru": "Тяжелая сложность",
        "uz": "Hard difficulty"
      }
    }
  ]
}
```

---

## `GET` `task_type/list/`
Список типов Заданий

> Response:
```json
{
  "count": 2,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Казус",
        "uz": "Case"
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Тест",
        "uz": "Test"
      }
    }
  ]
}
```

---
## `GET` `type/list/`
Тип задания
duration - длительность задания

> Response:
```json
{
  "count": 6,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 1,
      "task_type": {
        "id": 1,
        "name": {
          "ru": "Казус",
          "uz": "Case"
        }
      },
      "difficulty": {
        "id": 1,
        "name": {
          "ru": "Легкая сложность",
          "uz": "Easy difficulty"
        }
      },
      "duration_seconds": 150
    },
    {
      "id": 2,
      "task_type": {
        "id": 2,
        "name": {
          "ru": "Тест",
          "uz": "Test"
        }
      },
      "difficulty": {
        "id": 1,
        "name": {
          "ru": "Легкая сложность",
          "uz": "Easy difficulty"
        }
      },
      "duration_seconds": 300
    }
  ]
}
```

---
## `GET` `/list/`
Список заданий

> Response:
```json
{
  "count": 132,
  "next": "http://localhost:8000/api/v1/task/list/?limit=20&offset=20",
  "previous": null,
  "results": [
    {
      "id": 33,
      "type": {
        "id": 1,
        "task_type": {
          "id": 1,
          "name": {
            "ru": "Казус",
            "uz": "Case"
          }
        },
        "difficulty": {
          "id": 1,
          "name": {
            "ru": "Легкая сложность",
            "uz": "Easy difficulty"
          }
        },
        "duration_seconds": 150
      },
      "topic": {
        "id": 2,
        "name": {
          "ru": "Сметчик",
          "uz": "Administrator, local government"
        }
      },
      "module": {
        "id": 1,
        "name": {
          "ru": "Тыловик",
          "uz": "Race relations officer"
        }
      },
      "status": "одобрен",
      "creator": {
        "id": 99,
        "image": "http://ekadr.sudya.uz/media/doctor_photos/1231313_hdYNxcg.jpg",
        "first_name": "Горшков",
        "middle_name": "Бенедиктович",
        "last_name": "Боян"
      },
      "checked_by": {
        "id": 100,
        "image": "http://ekadr.sudya.uz/media/doctor_photos/Screenshot_9_J6G1VQm.png",
        "first_name": "",
        "middle_name": "ioeehfow",
        "last_name": ""
      }
    }
  ]
}
```

---
## `GET` `/<int:id>/`

>Response:
```json
{
  "id": 33,
  "type": {
    "id": 1,
    "task_type": {
      "id": 1,
      "name": {
        "ru": "Казус",
        "uz": "Case"
      }
    },
    "difficulty": {
      "id": 1,
      "name": {
        "ru": "Легкая сложность",
        "uz": "Easy difficulty"
      }
    },
    "duration_seconds": 150
  },
  "topic": {
    "id": 2,
    "name": {
      "ru": "Сметчик",
      "uz": "Administrator, local government"
    }
  },
  "question": {
    "uz": "Xernya",
    "ru": "Xernya"
  },
  "answer": [
    {
      "id": 69,
      "answer": {
        "ru": "академик",
        "uz": "member",
        "en": "KUU"
      },
      "is_correct": false
    },
    {
      "id": 70,
      "answer": {
        "ru": "запеть",
        "uz": "mission",
        "en": "fKUUuhgg"
      },
      "is_correct": true
    },
    {
      "id": 72,
      "answer": {
        "ru": "мгновение",
        "uz": "important",
        "en": "qKUU"
      },
      "is_correct": false
    },
    {
      "id": 15013,
      "answer": {
        "en": "qeeeKUU"
      },
      "is_correct": false
    }
  ],
  "module": {
    "id": 1,
    "name": {
      "ru": "Тыловик",
      "uz": "Race relations officer"
    }
  },
  "status": "одобрен",
  "creator": {
    "id": 99,
    "image": "http://ekadr.sudya.uz/media/doctor_photos/1231313_hdYNxcg.jpg",
    "first_name": "Горшков",
    "middle_name": "Бенедиктович",
    "last_name": "Боян"
  },
  "checked_by": {
    "id": 100,
    "image": "http://ekadr.sudya.uz/media/doctor_photos/Screenshot_9_J6G1VQm.png",
    "first_name": "",
    "middle_name": "ioeehfow",
    "last_name": ""
  }
}
```
---
`POST` `/create/`
>Request
```json
{
	"type":1,
	"question":"qweqweq?",
	"topic":1,
	"answer":[
		{
			"answer":"wqeqweq2323we",
			"is_correct":true
		},
		{
			"answer":"wqeqwe1231qwe",
			"is_correct":false
		}
	]
}
```
>Request
```json
{
  "id": 5995,
  "type": {
    "id": 1,
    "task_type": {
      "id": 1,
      "name": {
        "ru": "Казус",
        "uz": "Case"
      }
    },
    "difficulty": {
      "id": 1,
      "name": {
        "ru": "Легкая сложность",
        "uz": "Easy difficulty"
      }
    },
    "duration_seconds": 150
  },
  "topic": {
    "id": 1,
    "name": {
      "ru": "Татуировщик",
      "uz": "Emergency planning/management officer"
    }
  },
  "question": "qweqweq?",
  "answer": [
    {
      "id": 15017,
      "answer": "wqeqweq2323we",
      "is_correct": true
    },
    {
      "id": 15018,
      "answer": "wqeqwe1231qwe",
      "is_correct": false
    }
  ],
  "module": {
    "id": 1,
    "name": {
      "ru": "Тыловик",
      "uz": "Race relations officer"
    }
  },
  "status": "новый",
  "creator": {
    "id": 100,
    "image": "http://ekadr.sudya.uz/media/doctor_photos/Screenshot_9_J6G1VQm.png",
    "first_name": "",
    "middle_name": "ioeehfow",
    "last_name": ""
  },
  "checked_by": null
}
```

---

##`POST` `<int:id>/check/`
>Request
```json
{
	"accepted":false,
	"comment":"GJ"
}
```
>Response

---
`GET` `<int:id>/history/`
>Response
```json
{
    "results": [
        {
          "id": 13,
          "task": 33,
          "task_review": 5,
          "status": "новый",
          "comment": null
        },
        {
          "id": 14,
          "task": 33,
          "task_review": null,
          "status": "обновлён",
          "comment": null
        },
        {
          "id": 26,
          "task": 33,
          "task_review": null,
          "status": "одобрен",
          "comment": null
        }
    ]
}
```
---
`GET` `statistics/list/`
>Response
```json
{
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Тыловик",
        "uz": "Race relations officer"
      },
      "question_count": 29,
      "accepted_question_count": 1
    },
    {
      "id": 2,
      "name": {
        "ru": "Старшина",
        "uz": "Marine scientist"
      },
      "question_count": 28,
      "accepted_question_count": 0
    },
    {
      "id": 3,
      "name": {
        "ru": "Телохранитель",
        "uz": "Librarian, academic"
      },
      "question_count": 25,
      "accepted_question_count": 0
    },
    {
      "id": 4,
      "name": {
        "ru": "Кромкозакатчик",
        "uz": "Podiatrist"
      },
      "question_count": 26,
      "accepted_question_count": 0
    },
    {
      "id": 5,
      "name": {
        "ru": "Лаборант",
        "uz": "Chemical engineer"
      },
      "question_count": 25,
      "accepted_question_count": 0
    },
    {
      "id": 6,
      "name": "Discipline",
      "question_count": 0,
      "accepted_question_count": 0
    },
    {
      "id": 8,
      "name": {
        "en": "Discipline"
      },
      "question_count": 0,
      "accepted_question_count": 0
    },
    {
      "id": 9,
      "name": "Discipline",
      "question_count": 0,
      "accepted_question_count": 0
    }
  ]
}
```