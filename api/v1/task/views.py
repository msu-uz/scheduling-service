from django.shortcuts import get_object_or_404
from django_filters import FilterSet, AllValuesFilter, DateFilter, ChoiceFilter, ModelChoiceFilter, NumberFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, filters
from rest_framework.views import APIView

from .serializers import *
from api.v1.permissions import TeacherPermission, TeacherCheckTestPermission, TeacherCreateTestPermission
from ...models import TaskReview


class TaskFilter(FilterSet):
    date = DateFilter(field_name='created_at__date')
    task_type = NumberFilter(field_name='type__task_type')
    difficulty = NumberFilter(field_name='type__difficulty')
    status = NumberFilter(field_name='status')
    discipline = NumberFilter(field_name='topic__discipline')

    class Meta:
        model = Task
        fields = ['created_at', 'status', 'type', 'topic']


class DifficultyCreateView(generics.CreateAPIView):
    serializer_class = DifficultySerializer


class DifficultyListView(generics.ListAPIView):
    serializer_class = DifficultySerializer
    queryset = Difficulty.objects.all()
    permission_classes = [TeacherPermission, ]


class DifficultyGetUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DifficultySerializer
    lookup_url_kwarg = 'id'
    queryset = Difficulty.objects.all()
    permission_classes = [TeacherPermission, ]


# -


class TypeListView(generics.ListAPIView):
    serializer_class = TypeDetailSerializer
    queryset = Type.objects.all()
    permission_classes = [TeacherPermission, ]


class TypeCreateSerializer(generics.CreateAPIView):
    serializer_class = TypeSerializer
    permission_classes = [TeacherPermission, ]


class TypeGetUpdateDeleteSerializer(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TypeSerializer
    lookup_url_kwarg = 'id'
    queryset = Type.objects.all()
    permission_classes = [TeacherPermission, ]


# -

class TaskTypeCreateView(generics.CreateAPIView):
    serializer_class = TaskTypeListSerializer
    permission_classes = [TeacherCreateTestPermission, ]


class TaskTypeListView(generics.ListAPIView):
    serializer_class = TaskTypeListSerializer
    queryset = TaskType.objects.all()
    permission_classes = [TeacherPermission, ]


class TaskTypeGetUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TaskTypeListSerializer
    lookup_url_kwarg = 'id'
    queryset = TaskType.objects.all()
    permission_classes = [TeacherCreateTestPermission, ]


class TaskCreateAPIView(generics.CreateAPIView):
    serializer_class = TaskCreateSerializer
    permission_classes = [TeacherCreateTestPermission, ]


class TaskListAPIView(generics.ListAPIView):
    queryset = Task.objects.all().prefetch_related('type', 'taskreview_set', 'taskreview_set__taskhistory_set',
                                                   'creator').order_by('-created_at')
    serializer_class = TaskListSerializer
    permission_classes = [TeacherPermission, ]
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_class = TaskFilter
    search_fields = ['creator__employee__first_name', 'creator__employee__second_name', 'creator__employee__last_name']


class TaskDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskCreateSerializer
    lookup_url_kwarg = 'id'
    permission_classes = [TeacherCreateTestPermission | TeacherCheckTestPermission]


class TopicListAPIView(generics.ListAPIView):
    serializer_class = TopicShortSerializer
    permission_classes = [TeacherPermission, ]
    filter_backends = [DjangoFilterBackend, ]
    filterset_fields = ['discipline']

    def get_queryset(self):
        qs = Topic.objects.filter(discipline__in=self.request.user.teacher.disciplines.all())
        return qs


class TaskReviewCreateAPIView(generics.CreateAPIView):
    queryset = TaskReview.objects.all()
    serializer_class = TaskReviewSerializer
    permission_classes = [TeacherCheckTestPermission, ]
    lookup_url_kwarg = 'id'


# -

class DisciplineListAPIView(generics.ListAPIView):
    permission_classes = [TeacherPermission]
    serializer_class = DisciplineListSerializer

    def get_queryset(self):
        qs = self.request.user.teacher.disciplines.all()
        return qs


class TaskHistoryListAPIView(generics.ListAPIView):
    permission_classes = [TeacherPermission, ]
    serializer_class = TaskHistorySerializer
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        queryset = TaskHistory.objects.filter(task_id=self.kwargs['id'])
        return queryset


class Permissions(generics.RetrieveAPIView):
    serializer_class = PermissionSerializer
    permission_classes = [TeacherPermission, ]

    def get_object(self):
        return self.request.user.teacher


class StatisticsListAPIView(generics.ListAPIView):
    serializer_class = StatisticSerializer
    queryset = Discipline.objects.all()
