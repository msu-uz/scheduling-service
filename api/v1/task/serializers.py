from django.shortcuts import get_object_or_404
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from api.models import TaskType, Topic, Discipline, Teacher
from api.models.task import Difficulty, Type, Task, Answer, TaskReview, ACCEPTED, DECLINED, NEW, UPDATED, TaskHistory, \
    TASK_STATUS_CHOICES


class TeacherDetailSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Teacher
        fields = (
            'id',
        )

    def to_representation(self, instance):
        response = super(TeacherDetailSerializer, self).to_representation(instance)
        response['image'] = instance.employee.image
        response['first_name'] = instance.user.first_name
        response['middle_name'] = instance.user.middle_name
        response['last_name'] = instance.user.last_name
        return response


class DisciplineShortSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Discipline
        fields = (
            'id',
            'name'
        )


class TopicShortSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Topic
        fields = (
            'id',
            'name',
        )


class DisciplineListSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Discipline
        fields = (
            'id',
            'name',
        )


class TaskTypeListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = TaskType
        fields = (
            'id',
            'name'
        )

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        # print(set(name))
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class DifficultySerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Difficulty
        fields = ['id', 'name']

    def update(self, instance, validated_data):
        name = validated_data.get('name')
        for lang in set(name):
            instance.name[lang] = name[lang]
            instance.save()
        return instance


class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = ['id', 'task_type', 'difficulty', 'duration_seconds']


class TypeDetailSerializer(serializers.ModelSerializer):
    difficulty = DifficultySerializer()
    task_type = TaskTypeListSerializer()

    class Meta:
        model = Type
        fields = ['id', 'task_type', 'difficulty', 'duration_seconds']


class AnswerCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ['id', 'answer', 'is_correct']


class AnswerSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False, required=False)

    class Meta:
        model = Answer
        fields = ['id', 'answer', 'is_correct']

    def update(self, instance, validated_data):
        answer = validated_data.get('answer')
        if answer:
            for lang in set(answer):
                instance.answer[lang] = answer[lang]
                instance.save()
            validated_data.pop('answer')
        return super().update(instance, validated_data)


class TaskListSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField()

    class Meta:
        model = Task
        fields = ['id', 'created_at']

    def to_representation(self, instance):
        response = super(TaskListSerializer, self).to_representation(instance)
        response['type'] = TypeDetailSerializer(instance.type).data
        response['topic'] = TopicShortSerializer(instance.topic).data
        response['module'] = DisciplineShortSerializer(instance.topic.discipline).data
        response['status'] = {'id': instance.status, 'name': instance.get_status_display()}
        response['creator'] = TeacherDetailSerializer(instance.creator).data
        if TaskReview.objects.filter(task=instance):
            response['checked_by'] = TeacherDetailSerializer(
                TaskReview.objects.filter(task=instance).last().reviewer).data
        else:
            response['checked_by'] = None
        response['has_edit_permission'] = self.context['request'].user.teacher.can_create_test and instance.creator == \
                                          self.context[
                                              'request'].user.teacher
        response['has_check_permission'] = self.context['request'].user.teacher.can_check_test
        return response


class TaskCreateSerializer(serializers.ModelSerializer):
    answer = AnswerSerializer(required=True, many=True, source='answer_set')
    task_type = serializers.PrimaryKeyRelatedField(source='type.task_type', queryset=TaskType.objects.all(), write_only=True)
    difficulty = serializers.PrimaryKeyRelatedField(source='type.difficulty', queryset=Difficulty.objects.all(), write_only=True)

    class Meta:
        model = Task
        fields = ['id', 'task_type', 'difficulty', 'topic', 'question', 'answer', 'created_at']

    def to_representation(self, instance):
        response = super(TaskCreateSerializer, self).to_representation(instance)
        response['type'] = TypeDetailSerializer(instance.type).data
        response['topic'] = TopicShortSerializer(instance.topic).data
        response['module'] = DisciplineShortSerializer(instance.topic.discipline).data
        response['status'] = {'id': instance.status, 'name': instance.get_status_display()}
        response['creator'] = TeacherDetailSerializer(instance.creator).data
        response['has_edit_permission'] = self.context['request'].user.teacher.can_create_test and instance.creator == \
                                          self.context['request'].user.teacher
        response['has_check_permission'] = self.context['request'].user.teacher.can_check_test
        if TaskReview.objects.filter(task=instance):
            response['checked_by'] = TeacherDetailSerializer(
                TaskReview.objects.filter(task=instance).last().reviewer).data
        else:
            response['checked_by'] = None
        return response

    def create(self, validated_data):
        type_options = validated_data.pop('type')
        validated_data['type'] = get_object_or_404(Type, task_type=type_options.get('task_type'),
                                                   difficulty=type_options.get('difficulty'))
        answers = validated_data.pop('answer_set')
        validated_data['creator'] = self.context['request'].user.teacher
        instance = super(TaskCreateSerializer, self).create(validated_data)

        for ans in answers:
            Answer.objects.create(task=instance, answer=ans['answer'], is_correct=ans['is_correct'])

        return instance

    def update(self, instance, validated_data):
        if not instance.status == ACCEPTED:
            validated_data['status'] = UPDATED
            if validated_data.get('type'):
                type_options = validated_data.pop('type')
                validated_data['type'] = get_object_or_404(Type, task_type=type_options.get('task_type'),
                                                           difficulty=type_options.get('difficulty'))
            question = validated_data.get('question')
            answer = validated_data.get('answer_set')
            if question:
                for lang in set(question):
                    instance.question[lang] = question[lang]
                    instance.save()

                validated_data.pop('question')
            if answer:
                for ans in answer:
                    if ans.get('id'):
                        ans_id = ans.pop('id')
                        if ans:
                            AnswerSerializer().update(instance.answer_set.all().get(id=ans_id), ans)
                        else:
                            Answer.objects.filter(id=ans_id).delete()
                    else:
                        ans['task'] = instance
                        AnswerSerializer().create(ans)
                validated_data.pop('answer_set')
            return super().update(instance, validated_data)
        else:
            raise ValidationError("Уже одобрено!")


class TaskReviewSerializer(serializers.ModelSerializer):
    accepted = serializers.BooleanField()
    task = serializers.PrimaryKeyRelatedField(required=False, queryset=Task.objects.all())
    reviewer = TeacherDetailSerializer(required=False)

    class Meta:
        model = TaskReview
        fields = ['id', 'task', 'accepted', 'comment', 'reviewer']

    def create(self, validated_data):
        task = get_object_or_404(Task, id=self.context['view'].kwargs.get('id'))
        if not task.status == ACCEPTED:
            validated_data['task'] = task
            validated_data['reviewer'] = self.context['request'].user.teacher
            instance = TaskReview.objects.create(**validated_data)
            if validated_data['accepted']:
                task.status = ACCEPTED
            else:
                task.status = DECLINED
            task.save()
            return instance
        else:
            raise ValidationError("Уже одобрено!")


class TaskHistorySerializer(serializers.ModelSerializer):
    task_review = TaskReviewSerializer()
    date = serializers.DateTimeField(source='created_at')

    class Meta:
        model = TaskHistory
        fields = ['id', 'task', 'task_review', 'status', 'date']

    def to_representation(self, instance):
        response = super(TaskHistorySerializer, self).to_representation(instance)
        response['status'] = {'id': instance.status, 'name': instance.get_status_display()}
        return response


class PermissionSerializer(serializers.ModelSerializer):
    has_add_permission = serializers.BooleanField(source='can_create_test')
    has_check_permission = serializers.BooleanField(source='can_check_test')

    class Meta:
        model = Teacher
        fields = [
            'has_add_permission',
            'has_check_permission'
        ]


class StatisticSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Discipline
        fields = ['id', 'name']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['question_count'] = Task.objects.filter(topic__discipline_id=instance.id).count()
        response['accepted_question_count'] = Task.objects.filter(topic__discipline_id=instance.id,
                                                                  status=ACCEPTED).count()
        return response
