from django.urls import path, include
from . import views
urlpatterns = [
    path('permissions/', views.Permissions.as_view()),
    path('difficulty/create/', views.DifficultyCreateView.as_view()),
    path('difficulty/list/', views.DifficultyListView.as_view()),
    path('difficulty/<int:id>', views.DifficultyGetUpdateDeleteView.as_view()),

    path('type/create/', views.TypeCreateSerializer.as_view()),
    path('type/list/', views.TypeListView.as_view()),
    path('type/<int:id>', views.TypeGetUpdateDeleteSerializer.as_view()),

    path('task_type/list/', views.TaskTypeListView.as_view()),
    path('task_type/create/', views.TaskTypeCreateView.as_view()),
    path('task_type/<int:id>', views.TaskTypeGetUpdateDeleteView.as_view()),

    path('create/', views.TaskCreateAPIView.as_view()),
    path('list/', views.TaskListAPIView.as_view()),
    path('<int:id>/', views.TaskDetailView.as_view()),
    path('<int:id>/check/', views.TaskReviewCreateAPIView.as_view()),
    path('<int:id>/history/', views.TaskHistoryListAPIView.as_view()),
    path('discipline/list/', views.DisciplineListAPIView.as_view()),
    path('topic/list/', views.TopicListAPIView.as_view()),

    # ------------------ TEST BASE STATISTICS -------------------
    path('statistics/list/', views.StatisticsListAPIView.as_view())
]