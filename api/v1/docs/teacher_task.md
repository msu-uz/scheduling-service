# Teacher Task


## `GET` `/teacher/task/list`
Список
заданий, автором
который
является
преподаватель, который
делает
запрос(request
user)
> Response:
```json5
[
    {

        "id": 1,
        "type": "Внеклассный",
        "module_course": {"id": 1, "name": {}},
        "object_id": {
            "id": 1,
            "title": "ДЗ-1"
        },
        "start_date": "date",
        "end_date": "date",
    }
]

```

## `GET` `/teacher/task/<int:id>`
> Response:
```json
{
    "id": 1,
    "type": "Внеклассный",
    "module_course": {"id": 1, "name": {}},
    "object_id": {
        "id": 1,
        "title": "ДЗ-1",
        "body": "",
        "files": [],
        "creator": "teacher_id"
    },
    "student_group": [
        {
            "id": 1,
            "name": {},
            "students": [{
                "id": 1,
                "profile_image": "url",
                "first_name": "",
                "last_name": "",
                "middle_name": "",
                "start_date": "date",
                "end_date": "date"

            }]
        }
    ]
}

```

## `POST` `/teacher/hometask/create` `multipart/form-data`
Добавления внеклассного задания
> Request:
```json
{
    "module_course": "module_course_id",
    "title": "ДЗ-1",
    "body": "",
    "files": []

}
```

> Response:
```json
{
    "id": 1,
    "title": "ДЗ-1",
    "body": "",
    "files": [],
    "module_course": {"id": 1, "name": {}},
    "creator": "teacher_id"

}
```

## `POST` `/teacher/hometask/<int:id>/send` 

> Request:
```json
{

    "start_date": "",
    "end_date": "",
    "students": [],
    "student_group": []

}
```

> Response:
```json
{
    "id": 1,
    "type": "Внеклассный",
    "module_course": {"id": 1, "name": {}},
    "object_id": {
        "id": 1,
        "title": "ДЗ-1",
        "body": "",
        "files": [],
        "creator": "teacher_id"
    },
    "student_group": [
        {
            "id": 1,
            "name": {},
            "students": [{
                "id": 1,
                "profile_image": "url",
                "first_name": "",
                "last_name": "",
                "middle_name": "",
                "start_date": "date",
                "end_date": "date"

            }]
        }
    ]
}
```