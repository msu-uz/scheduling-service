#DISTANCE LEARNING API

##TEACHER GROUP 

### /teacher/group/list   -GET
    Список групп которые относятся к  преподавателю.
    Преподаватель должен зайти на сайт
    count - количество учащихся в этой группе
 ```json
 
{
  "count": 1,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 2,
      "name": "Энтомолог",
      "department": null,
      "course": {
        "ru": "Мастер маникюра",
        "uz": "Solicitor"
      },
      "count": 10
    }
  ]
}
```

## TEACHER GROUP поиск

### /teacher/group/list/?search='string'

## СПИСОК УЧАЩИХСЯ ГРУППУ по group_id

### /teacher/group/<<int:group_id>> -GET

```json

{
  "students": [
    {
      "id": 9,
      "image": "/media/profile_images/face_LLM3hQx.jpg",
      "first_name": "Меркушев",
      "middle_name": "Гавриилович",
      "last_name": "Измаил"
    },
    {
      "id": 4,
      "image": "/media/profile_images/face_Kg61gA5.jpg",
      "first_name": "Русаков",
      "middle_name": "Феодосьевич",
      "last_name": "Ферапонт"
    },
    {
      "id": 1,
      "image": "/media/profile_images/face_R3LRt7g.jpg",
      "first_name": "Федор",
      "middle_name": "Павлов",
      "last_name": "Исидорович"
    },
    {
      "id": 4,
      "image": "/media/profile_images/face_Kg61gA5.jpg",
      "first_name": "Русаков",
      "middle_name": "Феодосьевич",
      "last_name": "Ферапонт"
    },
    {
      "id": 1,
      "image": "/media/profile_images/face_R3LRt7g.jpg",
      "first_name": "Федор",
      "middle_name": "Павлов",
      "last_name": "Исидорович"
    },
    {
      "id": 5,
      "image": "/media/profile_images/face_c5JRqD2.jpg",
      "first_name": "Елена",
      "middle_name": "Лебедева",
      "last_name": "Константиновна"
    },
    {
      "id": 9,
      "image": "/media/profile_images/face_LLM3hQx.jpg",
      "first_name": "Меркушев",
      "middle_name": "Гавриилович",
      "last_name": "Измаил"
    },
    {
      "id": 3,
      "image": "/media/profile_images/face_RrU4oUb.jpg",
      "first_name": "Белоусов",
      "middle_name": "Тимурович",
      "last_name": "Софон"
    },
    {
      "id": 8,
      "image": "/media/profile_images/face_0qNeBiJ.jpg",
      "first_name": "Филарет",
      "middle_name": "Власов",
      "last_name": "Харитонович"
    },
    {
      "id": 10,
      "image": "/media/profile_images/face_AcU9TgS.jpg",
      "first_name": "Натан",
      "middle_name": "Харитонов",
      "last_name": "Аверьянович"
    }
  ]
}
```