# Distance learning API

## Statement
```diff
- text in red
+ text in green
! text in orange
# text in gray
@@ text in purple (and bold)@@

```
## Get group list
>### `GET` `/statement/group/list`
>Response:
```json
{
  "count": 1,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 1,
      "name": "Аллерголог",
      "department": null,
      "course": {
        "id": 3,
        "name": {
          "ru": "Невролог",
          "uz": "Chiropodist"
        }
      },
      "created_at": "2020-11-30T15:20:01.533230+05:00",
      "updated_at": "2020-11-30T15:30:43.133236+05:00",
      "count": 25
    }
  ]
}
   
```

## Get group detail (module list of group)
>### `GET` `/statement/group/<int:id>`
>Response:
```json
{
  "count": 2,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 1,
      "created_at": "2020-11-30T15:20:01.544978+05:00",
      "updated_at": "2020-12-01T12:08:09.999418+05:00",
      "group": 1,
      "module_course": 11,
      "teacher": [
        {
          "id": 1,
          "image": "/media/profile_images/face_Xl6Sk9m.jpg",
          "access_level": "студент",
          "created_at": "2020-11-30T15:19:22.118729+05:00",
          "updated_at": "2020-11-30T15:19:22.118754+05:00",
          "first_name": "Милен",
          "middle_name": "Анисимов",
          "last_name": "Юлианович"
        },
        {
          "id": 2,
          "image": "/media/profile_images/face_8Tp8kWH.jpg",
          "access_level": "студент",
          "created_at": "2020-11-30T15:19:22.304808+05:00",
          "updated_at": "2020-11-30T15:19:22.304901+05:00",
          "first_name": "Симонова",
          "middle_name": "Егоровна",
          "last_name": "Анастасия"
        },
        {
          "id": 3,
          "image": "/media/profile_images/face_Wwo0JNP.jpg",
          "access_level": "студент",
          "created_at": "2020-11-30T15:19:22.647201+05:00",
          "updated_at": "2020-11-30T15:19:22.647289+05:00",
          "first_name": "Лукия",
          "middle_name": "Морозова",
          "last_name": "Яковлевна"
        }
      ]
    },
    {
      "id": 2,
      "created_at": "2020-11-30T15:20:01.553309+05:00",
      "updated_at": "2020-11-30T15:20:01.553349+05:00",
      "group": 1,
      "module_course": 12,
      "teacher": []
    }
  ]
}
```

## Get Module Detail
>### `GET` `/statement/module/<int:id>`
>Response:
```json
{
    "id": 2,
  "students": [
    {
      "id": 100,
      "image": "http://localhost:8000/media/profile_images/face_NaE2Yjs.jpg",
      "is_student": true,
      "created_at": "2020-11-30T15:19:21.923187+05:00",
      "updated_at": "2020-11-30T15:19:21.923278+05:00",
      "first_name": "Крюкова",
      "middle_name": "Валериевна",
      "last_name": "Евдокия",
      "education_job": "Веб-программист",
      "journal": [
        {
          "id": 1,
          "created_at": "2020-11-30T15:34:16.659187+05:00",
          "updated_at": "2020-11-30T15:34:16.659264+05:00",
          "score": 57,
          "module_student_group": 2,
          "student": 100,
          "event": 25,
          "qa": {
            "id": 2,
            "name": {
              "ru": "Cool"
            }
          }
        },
        {
          "id": 2,
          "created_at": "2020-11-30T15:42:09.374638+05:00",
          "updated_at": "2020-11-30T15:42:09.374711+05:00",
          "score": 55,
          "module_student_group": 2,
          "student": 100,
          "event": 26,
          "qa": {
            "id": 1,
            "name": {
              "ru": "Bad"
            }
          }
        },
        {
          "id": 3,
          "created_at": "2020-11-30T15:49:35.077444+05:00",
          "updated_at": "2020-11-30T15:49:35.077486+05:00",
          "score": 55,
          "module_student_group": 2,
          "student": 100,
          "event": 28,
          "qa": {
            "id": 1,
            "name": {
              "ru": "Bad"
            }
          }
        }
      ],
      "failed_control": [
        {
          "id": 3,
          "created_at": "2020-11-30T15:49:35.077444+05:00",
          "updated_at": "2020-11-30T15:49:35.077486+05:00",
          "score": 55,
          "module_student_group": 2,
          "student": 100,
          "event": 28,
          "qa": {
            "id": 1,
            "name": {
              "ru": "Bad"
            }
          }
        }
      ],
      "retake_event": [],
      "checked": false
    },
    {
      "id": 73,
      "image": "http://localhost:8000/media/profile_images/face_Cz4wIoW.jpg",
      "is_student": true,
      "created_at": "2020-11-30T15:19:16.243716+05:00",
      "updated_at": "2020-11-30T15:19:16.243804+05:00",
      "first_name": "Фирс",
      "middle_name": "Владимиров",
      "last_name": "Теймуразович",
      "education_job": "Инженер",
      "journal": [],
      "failed_control": [],
      "retake_event": [],
      "checked": false
    }
  ],
  "events": [
        {
            "id": 25,
            "created_at": "2020-11-30T15:30:43.111693+05:00",
            "updated_at": "2020-11-30T15:30:43.111731+05:00",
            "date": "2020-12-07",
            "module_student_group": 2,
            "interval": 1,
            "teacher": null,
            "lesson": 89,
            "room": null
        },
        {
            "id": 26,
            "created_at": "2020-11-30T15:30:43.111789+05:00",
            "updated_at": "2020-11-30T15:30:43.111812+05:00",
            "date": "2020-12-07",
            "module_student_group": 2,
            "interval": 2,
            "teacher": null,
            "lesson": 89,
            "room": null
        },
        {
            "id": 27,
            "created_at": "2020-11-30T15:30:43.111851+05:00",
            "updated_at": "2020-11-30T15:30:43.111868+05:00",
            "date": "2020-12-07",
            "module_student_group": 2,
            "interval": 3,
            "teacher": null,
            "lesson": 89,
            "room": null
        },
        {
            "id": 28,
            "created_at": "2020-11-30T15:30:43.111906+05:00",
            "updated_at": "2020-11-30T15:30:43.111925+05:00",
            "date": "2020-12-09",
            "module_student_group": 2,
            "interval": 1,
            "teacher": null,
            "lesson": 90,
            "room": null
        }
  ]
}
```
Visualize data 
>| №          | `event - 1`|`event - 2`|`event - 3`|
>| ---------  | -----------| --------- |-----------|
>|`Students-1`| `journal`  |           |           |
>|`Students-1`|            | `journal` | `journal` |
>|`Students-1`| `journal`  |           |           |


## Get control of module
>### `GET` `/statement/module/<int:id>/control`
>Response:
```json
{
    "count": 25,
    "next": "http://127.0.0.1:8000/api/v1/statement/module/2/control/?limit=20&offset=20",
    "previous": null,
    "results": [
        {
            "id": 100,
            "image": "http://127.0.0.1:8000/media/profile_images/face_NaE2Yjs.jpg",
            "created_at": "2020-11-30T15:19:21.923187+05:00",
            "updated_at": "2020-11-30T15:19:21.923278+05:00",
            "first_name": "Крюкова",
            "middle_name": "Валериевна",
            "last_name": "Евдокия",
            "current_score": 100,
            "current_score_qa": {
                "id": 2,
                "name": {
                    "ru": "Cool"
                }
            },
            "intermediate_control": 55.0,
            "intermediate_control_qa": {
                "id": 1,
                "name": {
                    "ru": "Bad"
                }
            },
            "final_control": null,
            "checked": false
        },
        {
            "id": 73,
            "image": "http://127.0.0.1:8000/media/profile_images/face_Cz4wIoW.jpg",
            "created_at": "2020-11-30T15:19:16.243716+05:00",
            "updated_at": "2020-11-30T15:19:16.243804+05:00",
            "first_name": "Фирс",
            "middle_name": "Владимиров",
            "last_name": "Теймуразович",
            "current_score": null,
            "intermediate_control": null,
            "final_control": null,
            "checked": false
        }
    ]
}
```

## Create Retake
>### `POST` `/statement/retake/create`
>Request:
>```json
>{
>"retake_event": [1,2],
>"description": "GG WP",
>"date": "2020-12-1",
>"start_time": "09:00",
>"end_time": "18:00"
>}
>```
>Response:
```json
[
    {
        "id": null,
        "retake_event": {
            "id": 1,
            "created_at": "2020-11-30T15:34:16.659187+05:00",
            "updated_at": "2020-11-30T15:34:16.659264+05:00",
            "score": 57,
            "module_student_group": 2,
            "student": 100,
            "event": 25,
            "qa": {
                "id": 2,
                "name": {
                    "ru": "Cool"
                }
            }
        },
        "score": null,
        "passed": false,
        "date": "2020-12-01",
        "description": null,
        "start_time": "13:46:00",
        "end_time": "12:12:00"
    },
    {
        "id": null,
        "retake_event": {
            "id": 2,
            "created_at": "2020-11-30T15:42:09.374638+05:00",
            "updated_at": "2020-11-30T15:42:09.374711+05:00",
            "score": 55,
            "module_student_group": 2,
            "student": 100,
            "event": 26,
            "qa": {
                "id": 1,
                "name": {
                    "ru": "Bad"
                }
            }
        },
        "score": null,
        "passed": false,
        "date": "2020-12-01",
        "description": null,
        "start_time": "13:46:00",
        "end_time": "12:12:00"
    }
]
```
retake_event
    : object id from Journal list
    
## Retake Edit
### `PATCH` `/statement/retake/<int:id>/edit`
>Request:
```json
{
  "date": "2020-12-12",
  "start_time": "12:00",
  "end_time": "15:00"
}
```
>Response:
```json
{
    "id": 1,
    "retake_event": {
        "score": null,
        "module_student_group": null,
        "student": null,
        "event": null
    },
    "score": null,
    "passed": false,
    "date": "2020-12-12",
    "description": null,
    "start_time": "12:00:00",
    "end_time": "15:00:00"
}
```
    
## Student Expel
### `PUT` `/statement/student/<int:id>`
>Request:
>```json
>{
>  "is_student": false
>}
>```
>Response:
```json
{
    "id": 1,
    "image": "http://127.0.0.1:8000/media/profile_images/face_m0RXlDy.jpg",
    "is_student": false,
    "created_at": "2020-11-30T15:19:01.283532+05:00",
    "updated_at": "2020-12-01T13:41:35.240781+05:00",
    "first_name": "Пономарев",
    "middle_name": "Гавриилович",
    "last_name": "Лазарь",
    "education_job": "Менеджер по работе с клиентами",
    "journal": [],
    "failed_control": [],
    "retake_event": [],
    "checked": false
}
```
