# Student Task

## `GET` `/student/task/list`
Список заданий, который прендназначен студенту

> Response:
```json5
[
    {

        "id": 1,
        "type": "Внеклассный",
        "module_course": {"id": 1, "name": {}},
        "object_id": {
            "id": 1,
            "title": "ДЗ-1"
        },
        "start_date": "date",
        "end_date": "date",
    }
]
```

## `GET` `/student/task/<int:id>`
> Response:
```json
{
    "id": 1,
    "type": "Внеклассный",
    "module_course": {"id": 1, "name": {}},
    "object_id": {
        "id": 1,
        "title": "ДЗ-1",
        "body": "",
        "files": [],
        "creator": "teacher_id"
    }
}

```

## `GET` `/student/task/<int:id>/send`
Выполнить внекласное задание
> Request:
```json
{

    "title": "ДЗ-1",
    "comment": "",
    "files": []
    
}

```

> Response:
```json
{
    "id": "",
    "title": "ДЗ-1",
    "comment": "",
    "files": [],
    "student": "student_id"
}
```