# Distance learning API - Schedule

## interval

### /schedule/event/interval/list/' -GET

Список интервалов для уроков.

```json
{
  "count": 1,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 1,
      "number": 1,
      "start_time": "11:58:13",
      "end_time": "11:58:13"
    }
  ]
}
```

number - номер пары <br>
start_time - начало пары <br>
end_time - конец пары <br>

### /schedule/event/interval/create/' - POST

request:

```json
{
  "number": 2,
  "start_time": "11:45:00",
  "end_time": "12:15:00"
}
```

response:

```json
{
  "id": 2,
  "number": 2,
  "start_time": "11:45:00",
  "end_time": "12:15:00"
}
```

### /schedule/event/interval/<<int:id>>' - GET/UPDATE/DELETE

response:

```json
{
  "id": 2,
  "number": 2,
  "start_time": "11:45:00",
  "end_time": "12:15:00"
}
```

## Student-group

### /schedule/studentgroup/list?course='int:id'&search='string' -GET

response:

```json
{
  "count": 4,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 1,
      "name": "Телемастер",
      "course": {
        "id": 3,
        "name": {
          "ru": "Оперативный работник",
          "uz": "Conservation officer, nature"
        },
        "type": "{'ru': 'Очное', 'uz': 'Traditional'}"
      }
    },
    {
      "id": 2,
      "name": "Астрофизик",
      "course": {
        "id": 4,
        "name": {
          "ru": "Радиолог",
          "uz": "Higher education careers adviser"
        },
        "type": "{'ru': 'Очное', 'uz': 'Traditional'}"
      }
    }
  ]
}
```

### /schedule/studentgroup/<<int:id>>/update - PUT/PATCH

request:

```json
{
  "course": "course_id",
  "start_date": "date"
}
```

response:

```json
{
  "course": {
    "id": 4,
    "name": {
      "ru": "Радиолог",
      "uz": "Higher education careers adviser"
    }
  },
  "name": "Астрофизик",
  "course_type": {
    "id": 1,
    "name": {
      "ru": "Очное",
      "uz": "Traditional"
    }
  }
}
```

### /schedule/studentgroup/<<int:id>>/event/list?start='datetime'&end='datetime' - GET

расписание для группы studentgroup(множ-во LessonEvent) в промежутке start - end <br>

response:

```json
{
  "results": [
    {
      "id": 2,
      "title": "{'ru': 'Иконописец', 'uz': 'Theatre manager'}",
      "teacher": null,
      "room": "",
      "start": "2020-11-26T11:58:13",
      "end": "2020-11-26T11:58:13"
    },
    {
      "id": 3,
      "title": "{'ru': 'Иконописец', 'uz': 'Theatre manager'}",
      "teacher": null,
      "room": "",
      "start": "2020-11-26T11:45:00",
      "end": "2020-11-26T12:15:00"
    }
  ]
}
```

### /schedule/studentgroup/<<int:id>>/teacher/list/?module_course=int - GET

Список преподавателей для определённой группы и модуля

response:

```json
{
  "results": [
    {
      "id": 1,
      "image": "http://127.0.0.1:8000/media/profile_images/face_hHYMCDH.jpg",
      "access_level": "студент",
      "created_at": "2020-12-01T15:08:42.389656+05:00",
      "updated_at": "2020-12-01T15:08:42.389656+05:00",
      "first_name": "Дмитрий",
      "middle_name": "Семенов",
      "last_name": "Чеславович"
    },
    {
      "id": 2,
      "image": "http://127.0.0.1:8000/media/profile_images/face_RhZoSjR.jpg",
      "access_level": "студент",
      "created_at": "2020-12-01T15:08:42.527330+05:00",
      "updated_at": "2020-12-01T15:08:42.527330+05:00",
      "first_name": "Аксенов",
      "middle_name": "Игоревич",
      "last_name": "Боян"
    }
  ]
}
```

### /schedule/studentgroup/<<int:id>>/modulecourse/list/?search='discipline_name' - GET

Список модулей для определённой группы

response:

```json
{
  "results": [
    {
      "id": 11,
      "lecture_duration": 90,
      "practice_duration": 41,
      "distantly_duration": 82,
      "course": {
        "id": 3,
        "name": {
          "ru": "Конвоир",
          "uz": "Careers adviser"
        }
      },
      "discipline": {
        "id": 26,
        "name": {
          "ru": "Лимфолог",
          "uz": "Equities trader"
        },
        "department": {
          "name": "Name"
        }
      }
    },
    {
      "id": 12,
      "lecture_duration": 25,
      "practice_duration": 40,
      "distantly_duration": 70,
      "course": {
        "id": 3,
        "name": {
          "ru": "Конвоир",
          "uz": "Careers adviser"
        }
      },
      "discipline": {
        "id": 25,
        "name": {
          "ru": "Радиомеханик",
          "uz": "Administrator, sports"
        },
        "department": {
          "name": "Name"
        }
      }
    }
  ]
}
```

### /schedule/studentgroup/<<int:id>>/event/create/ - POST

Создание одного занятия (мероприятия) для определённой группы

request:

```json
{
  "interval": 1,
  "lesson": 35,
  "room": 17,
  "date": "2020-12-01",
  "teacher": 2,
  "module_course": 4
}
```

response:

```json
{
  "interval": 1,
  "lesson": 35,
  "room": 17,
  "date": "2020-12-01",
  "teacher": 2
}
```

### /schedule/studentgroup/<<int:id>>/event/<<int:event_id>>/update/ - PUT, PATCH

Обновление определённого занятия (мероприятия) для определённой группы

request:

```json
{
  "interval": 1,
  "lesson": 35,
  "room": 17,
  "date": "2020-12-01",
  "teacher": 2,
  "module_course": 4
}
```

response:

```json
{
  "interval": 1,
  "lesson": 35,
  "room": 17,
  "date": "2020-12-01",
  "teacher": 2
}
```

### /schedule/studentgroup/<<int:id>>/event/create/repeat/ - POST

Создание повторяющихся занятий (мероприятий) для определённой группы в date-интервале

request:

```json
{
  "interval": 1,
  "lesson": 1,
  "room": 1,
  "days": [0, 1, 4], // понедельник, вторник, пятница
  "teacher": 2,
  "module_course": 4,
  "start_date": "2020-12-01",
  "end_date": "2020-12-20"
}
```

response:

```json
[
  {
    "id": 2,
    "title": "{'ru': 'Радиомеханик', 'uz': 'Administrator, sports'}",
    "lesson": 1,
    "teacher": 2,
    "room": "123",
    "start": "2020-12-01T15:37:54",
    "end": "2020-12-01T15:37:54"
  },
  {
    "id": 3,
    "title": "{'ru': 'Радиомеханик', 'uz': 'Administrator, sports'}",
    "lesson": 1,
    "teacher": 2,
    "room": "123",
    "start": "2020-12-04T15:37:54",
    "end": "2020-12-04T15:37:54"
  }
]
```

### /schedule/resource/list/ - GET

Список ресурсов (аудиторий).

response:

```json
[
  {
    "id": 1,
    "name": "123",
    "type": "Аудитория"
  },
  {
    "id": 2,
    "name": "213",
    "type": "Аудитория"
  }
]
```

### /schedule/moduleweektype/list/?module_course_id=int - GET

Список тем для модуля.

query params:

`module_course_id` - идентификатор модуля

response:

```json
[
  {
    "id": 1,
    "name": {
      "ru": "Травматолог",
      "uz": "Administrator, Civil Service"
    }
  },
  {
    "id": 2,
    "name": {
      "ru": "Врач скорой помощи",
      "uz": "Engineer, petroleum"
    }
  }
]
```

## Course

### /schedule/course/list - GET

```json
{
  "count": 4,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 1,
      "name": {
        "ru": "Метранпаж",
        "uz": "Estate manager/land agent"
      },
      "course_type": {
        "id": 1,
        "name": {
          "ru": "Очное",
          "uz": "Traditional"
        }
      }
    },
    {
      "id": 2,
      "name": {
        "ru": "Товаровед",
        "uz": "Manufacturing systems engineer"
      },
      "course_type": {
        "id": 2,
        "name": {
          "ru": "Дистанционное",
          "uz": "Remote"
        }
      }
    }
  ]
}
```

## LessonEvent change

Если возможно меняет дату или номер(интервал) определенного урока в расписании. Если нет, то возвращает ошибку.

### schedule/event/<int:id>/change - PUT

Request:

```json
{
  "date": "2020-12-21",
  "interval": 2
}
```

Response:

```json
{
  "id": 4,
  "date": "2020-12-21",
  "interval": 2
}
```

date - на какой день меняют
interval - на какой интервал меняют

## LessonEvent swap

Если в этот день нету другой пары, то меняет, иначе меняет местами уже существующий.

### schedule/event/<int:id>/swap -PUT

Request:

```json
{
  "date": "2020-12-30",
  "interval": 2
}
```

Response:

```json
{
  "id": 7,
  "date": "2020-12-30",
  "interval": 2
}
```

date - на какой день меняют
interval - на какой интервал меняют

# Прикрепление преподавателей к группам

## `GET schedule/teacher/list/`

Список всех преподавателей.

### Response:

```json
[
  {
    "id": 1,
    "image": "http://url.jpg",
    "created_at": "2021-01-05T15:05:16.249311+05:00",
    "updated_at": "2021-01-05T15:05:16.249378+05:00",
    "first_name": "Павлова",
    "second_name": "Геннадьевна",
    "last_name": "Анастасия"
  }
]
```

---

## `GET schedule/studentgroup/<int:group_id>/modulestudentgroup/list/`

Список сущностей ModuleStudentGroup, связывающих группу `group_id`, преподавателя и модуль.

### URL params:

| Название | Описание             |
| -------- | -------------------- |
| group_id | Идентификатор группы |

### Response fields:

| Название      | Описание                              |
| ------------- | ------------------------------------- |
| id            | Идентификатор ModuleStudentGroup      |
| module_course | Объект-модуль                         |
| teacher       | Объект-преподаватель, может быть null |

### Response:

```json
[
  {
    "id": 1,
    "module_course": {
      "id": 1,
      "lecture_duration": 49,
      "practice_duration": 62,
      "distantly_duration": 76,
      "course": {
        "id": 1,
        "name": {
          "ru": "Монтажник связи",
          "uz": "Automotive engineer"
        }
      },
      "discipline": {
        "id": 2,
        "name": {
          "ru": "Ихтиолог",
          "uz": "Engineer, manufacturing"
        },
        "department": {
          "name": null
        }
      }
    },
    "teacher": {
      "id": 1,
      "image": "http://url.jpg",
      "access_level": "студент",
      "created_at": "2021-01-05T15:05:16.249311+05:00",
      "updated_at": "2021-01-05T15:05:16.249378+05:00",
      "first_name": "Павлова",
      "middle_name": "Геннадьевна",
      "last_name": "Анастасия"
    },
    "group": "M1"
  }
]
```

---

## `PATCH, PUT schedule/modulestudentgroup/<int:id>/update/`

Обновление сущности ModuleStudentGroup (указывается преподаватель).

### URL params:

| Название | Описание                         |
| -------- | -------------------------------- |
| id       | Идентификатор ModuleStudentGroup |

### Request fields:

| Название | Описание                    |
| -------- | --------------------------- |
| teacher  | Идентификатор преподавателя |

### Request:

```json
{
  "teacher": 2
}
```

### Response:

```json
{
  "id": 1,
  "module_course": {
    "id": 1,
    "lecture_duration": 49,
    "practice_duration": 62,
    "distantly_duration": 76,
    "course": {
      "id": 1,
      "name": {
        "ru": "Монтажник связи",
        "uz": "Automotive engineer"
      }
    },
    "discipline": {
      "id": 2,
      "name": {
        "ru": "Ихтиолог",
        "uz": "Engineer, manufacturing"
      },
      "department": {
        "name": null
      }
    }
  },
  "teacher": {
    "id": 2,
    "image": "http://url.jpg",
    "access_level": "студент",
    "created_at": "2021-01-05T15:17:19.894397+05:00",
    "updated_at": "2021-01-05T15:17:19.894449+05:00",
    "first_name": "Ульяна",
    "middle_name": "Елисеева",
    "last_name": "Афанасьевна"
  },
  "group": "M1"
}
```
