# Distance learning API


## teacher/schedule

### teacher/schedule/event/list?start='datetime'&end='datetime' - GET
Список мероприятий для авторизованного преподавателя

response:
```json
[
    {
        "id": 1,
        "title": "{'ru': 'Машинист локомотива', 'uz': 'Publishing copy'}",
        "teacher": 5,
        "room": "256",
        "start": "2020-11-17T09:00:00",
        "end": "2020-11-17T10:30:00"
    },
    {
        "id": 2,
        "title": "{'ru': 'Машинист локомотива', 'uz': 'Publishing copy'}",
        "teacher": 5,
        "room": "123",
        "start": "2020-11-18T09:00:00",
        "end": "2020-11-18T10:30:00"
    },
    {
        "id": 3,
        "title": "{'ru': 'Машинист локомотива', 'uz': 'Publishing copy'}",
        "teacher": 5,
        "room": "351",
        "start": "2020-11-24T09:00:00",
        "end": "2020-11-24T10:30:00"
    }
]
```
