from django.db.models import F, Avg
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import status, filters
from rest_framework.generics import (
    ListAPIView, RetrieveAPIView, CreateAPIView, DestroyAPIView
)
from rest_framework.response import Response
from rest_framework.validators import ValidationError
from rest_framework.views import APIView
from drf_renderer_xlsx.mixins import XLSXFileMixin
from api.models.applicant import Applicant, ApplicantExamResults, ApplicantsResultsList
from api.models.role import AdmissionBoardUnit
from api.models.user import User
from api.v1.permissions import AdmissionBoardPermission, AdministratorPermission
from drf_renderer_xlsx.renderers import XLSXRenderer
from . import serializers


# list applicants
class ApplicantsList(ListAPIView):
    permission_classes = [AdmissionBoardPermission]
    serializer_class = serializers.ApplicantsListSerializer
    filter_backends = [filters.SearchFilter, ]
    search_fields = ['last_name', 'first_name', 'second_name']

    def get_queryset(self):
        queryset = Applicant.objects.all()
        review = self.request.query_params.get('review', None)
        if review == "accept":
            queryset = queryset.filter(applicantreview__accepted=True)
        elif review == "reject":
            queryset = queryset.filter(applicantreview__accepted=False)
        elif review == "none":
            queryset = queryset.filter(applicantreview__isnull=True)
        return queryset


# detail applicant
class ApplicantDetail(RetrieveAPIView):
    permission_classes = [AdmissionBoardPermission]
    serializer_class = serializers.ApplicantSerializer
    queryset = Applicant.objects.all()


# accept applicant
class AcceptApplicant(APIView):
    permission_classes = [AdmissionBoardPermission]

    def post(self, request, pk):
        applicant = get_object_or_404(Applicant, id=pk)
        applicant.accept(request.user)

        return JsonResponse({"ok": True})


# decline applicant
class DeclineApplicant(APIView):
    permission_classes = [AdmissionBoardPermission]

    def post(self, request, pk):
        applicant = get_object_or_404(Applicant, id=pk)
        applicant.reject(request.user)

        return JsonResponse({"ok": True})


class ListApplicantsExamResults(ListAPIView):
    serializer_class = serializers.ApplicantExamResultListSerializer
    queryset = ApplicantExamResults.objects.all()
    filter_backends = (filters.SearchFilter,)
    search_fields = ['applicant__first_name', 'applicant__second_name', 'applicant__last_name']


class CreateApplicantsResultsList(APIView):
    permission_classes = [AdmissionBoardPermission]

    def post(self, request):
        data = serializers.GenerateApplicantsResultsListSerializer(request.data)
        passed_students = Applicant.objects.filter(applicantexamresults__first_exam_points__isnull=False,
                                                   applicantexamresults__second_exam_points__isnull=False).annotate(
            exam=F("applicantexamresults__second_exam_points") + F("applicantexamresults__first_exam_points")).order_by(
            "-exam")[:data["max_students"].value]

        results_list = ApplicantsResultsList()
        results_list.creator = request.user
        results_list.save()

        for s in passed_students:
            results_list.add_applicant(s)

        return JsonResponse({"ok": True})


class ListPassedApplicants(ListAPIView):
    permission_classes = [AdmissionBoardPermission]
    serializer_class = serializers.ListPassedApplicantSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ["first_name", "second_name", "last_name"]

    def get_queryset(self):
        return Applicant.objects.filter(results_list=self.kwargs["pk"]).annotate(
            exam=F("applicantexamresults__second_exam_points") + F("applicantexamresults__first_exam_points")).order_by(
            "-exam")


class RemoveApplicantFromPassedView(APIView):
    permission_classes = [AdmissionBoardPermission]

    def delete(self, request, pk, applicant_id):
        passed_list = get_object_or_404(ApplicantsResultsList, pk=pk)
        applicant = get_object_or_404(Applicant, pk=applicant_id)

        if applicant.results_list != passed_list:
            raise ValidationError("applicant is not in results list")

        passed_list.remove_applicant(applicant.id)

        return JsonResponse({"ok": True})


class AddApplicantToPassedView(APIView):
    permission_classes = [AdmissionBoardPermission]

    def post(self, request, pk, applicant_id):
        passed_list = get_object_or_404(ApplicantsResultsList, pk=pk)
        applicant = get_object_or_404(Applicant, pk=applicant_id)

        passed_list.add_applicant(applicant)

        return JsonResponse({"ok": True})


class ApproveApplicantsResultsList(APIView):
    permission_classes = [AdmissionBoardPermission]

    def post(self, request, pk):
        results_list = get_object_or_404(ApplicantsResultsList, pk=pk)

        if request.user == results_list.creator:
            return JsonResponse({"msg": "can't vote for your own lists"}, status=400)

        results_list.add_vote(True, request.user)

        return JsonResponse({"ok": True})


class DeclineApplicantsResultsList(APIView):
    permission_classes = [AdmissionBoardPermission]

    def post(self, request, pk):
        results_list = get_object_or_404(ApplicantsResultsList, pk=pk)

        if request.user == results_list.creator:
            return JsonResponse({"msg": "can't vote for your own lists"}, status=400)

        results_list.add_vote(False, request.user)

        return JsonResponse({"ok": True})


class DetailResultsApplicantsList(RetrieveAPIView):
    permission_classes = [AdmissionBoardPermission]
    serializer_class = serializers.DetailResultsListSerializer

    def get_object(self):
        return ApplicantsResultsList.objects.last()


class ExcelExportView(XLSXFileMixin, ListAPIView):
    serializer_class = serializers.ListPassedApplicantExcelSerializer
    permission_classes = [AdmissionBoardPermission]
    renderer_classes = (XLSXRenderer,)
    filename = 'list_of_passed_applicants.xlsx'

    def get_queryset(self):
        result_list = ApplicantsResultsList.objects.last()
        qs = Applicant.objects.filter(results_list=result_list).annotate(
            exam=F("applicantexamresults__second_exam_points") + F("applicantexamresults__first_exam_points")).order_by(
            "-exam")
        return qs

    column_header = {
        'titles': [
            "ФИО",
            "Результат за 1-й экзамен",
            "Результат за 2-oй экзамен",
        ],
        'column_width': [70, 35, 35],
        'height': 25,
        'style': {
            'fill': {
                'fill_type': 'solid',
                'start_color': 'FFFFFFFF',
            },
            'alignment': {
                'horizontal': 'center',
                'vertical': 'center',
                'wrapText': True,
                'shrink_to_fit': True,
            },
            'border_side': {
                'border_style': 'thin',
                'color': 'FF000000',
            },
            'font': {
                'name': 'Arial',
                'size': 14,
                'bold': True,
                'color': 'FF000000',
            },
        },
    }
    body = {
        'style': {
            'fill': {
                'fill_type': 'solid',
                'start_color': 'FFFFFFFF',
            },
            'alignment': {
                'horizontal': 'center',
                'vertical': 'center',
                'wrapText': True,
                'shrink_to_fit': True,
            },
            'border_side': {
                'border_style': 'thin',
                'color': 'FF000000',
            },
            'font': {
                'name': 'Times New Roman',
                'size': 14,
                'bold': False,
                'color': 'FF000000',
            }
        },
        'height': 40,
    }


# create results list
# class CreateApplicantsResultsList(CreateAPIView):
#     permission_classes = [AdmissionBoardPermission]
#     serializer_class = serializers.CreateApplicantsResultsListSerializer
#
#
# class RUDApplicantsResultsList(RetrieveUpdateDestroyAPIView):
#     permission_classes = [AdmissionBoardPermission]
#     serializer_class = serializers.DetailApplicationResultsListSerializer
#     queryset = ApplicantsResultsList.objects.all()
#
#
# class ListApplicantsOfResultsList(ListAPIView):
#     permission_classes = [AdmissionBoardPermission]
#     serializer_class = serializers.ApplicantSerializer
#
#     def get_queryset(self):
#         return Applicant.objects.filter(get_object_or_404(Applicant, self.kwargs["pk"]))
#
#
# # vote to approve the results list
# class PositiveVoteResultsList(APIView):
#     permission_classes = [AdmissionBoardPermission]
#
#     def post(self, request):
#         get_object_or_404(ApplicantsResultsList, self.kwargs["pk"]).add_vote(True, request.user)
#         return JsonResponse({"ok": True})
#
#
# # vote to decline the results list
# class NegativeVoteResultsList(APIView):
#     permission_classes = [AdmissionBoardPermission]
#
#     def post(self, request):
#         get_object_or_404(ApplicantsResultsList, self.kwargs["pk"]).add_vote(False, request.user)
#         return JsonResponse({"ok": True})


# Admission Board Units

class AdmissionBoardUnitsListApiView(ListAPIView):
    queryset = AdmissionBoardUnit.objects.all()
    serializer_class = serializers.AdmissionBoardUnitsSerializer
    permission_classes = [AdmissionBoardPermission]


class AdmissionBoardUnitsCreateApiView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = serializers.AdmissionBoardUnitCreateSerializer
    permission_classes = [AdministratorPermission]


class AdmissionBoardUnitsDetailApiView(RetrieveAPIView):
    queryset = AdmissionBoardUnit.objects.all()
    serializer_class = serializers.AdmissionBoardUnitsSerializer
    permission_classes = [AdmissionBoardPermission]
    lookup_url_kwarg = ['unit_id']

    def get_queryset(self):
        return AdmissionBoardUnit.objects.filter(id=self.kwargs['unit_id'])


class AdmissionBoardUnitsDeleteApiView(DestroyAPIView):
    serializer_class = serializers.AdmissionBoardUnitsSerializer
    permission_classes = [AdministratorPermission]
    lookup_url_kwarg = ['unit_id']

    def destroy(self, request, *args, **kwargs):
        try:
            obj = AdmissionBoardUnit.objects.get(id=self.kwargs['unit_id'])
            self.perform_destroy(obj)
            return Response(status=status.HTTP_204_NO_CONTENT)

        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
