from django.forms import ModelForm
from api.models.role import AdmissionBoardUnit
from django.core.validators import ValidationError


class AdmissionBoardUnitForm(ModelForm):
    class Meta:
        model = AdmissionBoardUnit
        fields = '__all__'

    def clean(self):
        data = self.cleaned_data
        count = AdmissionBoardUnit.objects.count()
        if AdmissionBoardUnit.objects.filter(user=data.get('user')):
            raise ValidationError('Этот пользователь уже является членом комиссии')
        if count == 5:
            raise ValidationError('Достигнут предел: 5 членов Приемной Комиссии')
        return data
