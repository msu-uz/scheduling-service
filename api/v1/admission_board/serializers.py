from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer, SerializerMethodField, Serializer, IntegerField, ImageField
from api.models.user import User
from api.models.applicant import (
    Applicant, ApplicantsResultsList, ApplicantReview, ApplicantExamResults, ApplicantParentInfo,
    ApplicantResultsListVote
)
from api.models.role import AdmissionBoardUnit
from rest_framework.validators import ValidationError
import datetime


class AdmissionBoardModeratorSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "first_name", "last_name", "middle_name", "profile_image")


class ApplicantReviewListSerializer(ModelSerializer):
    reviewer = SerializerMethodField()

    def get_reviewer(self, obj: ApplicantReview):
        return AdmissionBoardModeratorSerializer(obj.reviewed_by, context={'request': self.context.get('request')}).data

    class Meta:
        model = ApplicantReview
        fields = ("accepted", "reviewer",)


class ApplicantsListSerializer(ModelSerializer):
    review = SerializerMethodField()

    def get_review(self, obj: Applicant):
        review = obj.applicantreview_set.last()
        if review:
            return ApplicantReviewListSerializer(review, context={'request': self.context.get('request')}).data
        else:
            return {}

    class Meta:
        model = Applicant
        fields = (
            "id",
            "image",
            "first_name",
            "second_name",
            "last_name",
            "review"
        )


class ParentSerializer(ModelSerializer):
    class Meta:
        model = ApplicantParentInfo
        fields = "__all__"


class ApplicantSerializer(ModelSerializer):
    mother_info = ParentSerializer()
    father_info = ParentSerializer()

    class Meta:
        model = Applicant
        fields = "__all__"


class DetailApplicationResultsListSerializer(ModelSerializer):
    class Meta:
        model = ApplicantsResultsList
        fields = [
            "id",
            "name",
        ]


class ApplicantExamResultListSerializer(ModelSerializer):
    applicant = SerializerMethodField()

    def get_applicant(self, obj: ApplicantExamResults):
        return ApplicantsListSerializer(obj.applicant, context={'request': self.context.get('request')}).data

    class Meta:
        model = ApplicantExamResults
        fields = (
            "id",
            "applicant",
            "first_exam_points",
            "second_exam_points",
        )


class ExamResultsSerializer(ApplicantExamResultListSerializer):
    class Meta:
        model = ApplicantExamResults
        fields = (
            "id",
            "applicant",
            "first_exam_points",
            "second_exam_points",
        )


class ListPassedApplicantSerializer(ModelSerializer):
    exam_results = SerializerMethodField()

    def get_exam_results(self, obj: Applicant):
        return ExamResultsSerializer(ApplicantExamResults.objects.filter(applicant=obj).last()).data

    class Meta:
        model = Applicant
        fields = (
            "id",
            "first_name",
            "second_name",
            "last_name",
            "exam_results",
            "image"
        )


class ListPassedApplicantExcelSerializer(ModelSerializer):
    class Meta:
        model = Applicant
        fields = ()

    def to_representation(self, instance):
        response = super(ListPassedApplicantExcelSerializer, self).to_representation(instance)
        response['fullname'] = f"{instance.first_name} {instance.second_name} {instance.last_name}"
        response['first_exam_points'] = ApplicantExamResults.objects.get(applicant=instance.id).first_exam_points
        response['second_exam_points'] = ApplicantExamResults.objects.get(applicant=instance.id).second_exam_points
        return response


class ResultsListVoteSerializer(ModelSerializer):
    user = SerializerMethodField()

    def get_user(self, obj: ApplicantResultsListVote):
        return AdmissionBoardModeratorSerializer(obj.user, context={'request': self.context.get('request')}).data

    class Meta:
        model = ApplicantResultsListVote
        exclude = ("results_list",)


class DetailResultsListSerializer(ModelSerializer):
    reviews = SerializerMethodField()
    creator = AdmissionBoardModeratorSerializer()

    def get_reviews(self, obj: ApplicantsResultsList):
        return ResultsListVoteSerializer(obj.applicantresultslistvote_set.all(), many=True,
                                         context={'request': self.context.get('request')}).data

    class Meta:
        model = ApplicantsResultsList
        fields = "__all__"


class GenerateApplicantsResultsListSerializer(Serializer):
    max_students = IntegerField(required=True)


class AdmissionBoardUnitCreateSerializer(ModelSerializer):
    id = IntegerField(read_only=True)
    image = ImageField(source='profile_image')

    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'last_name',
            'access_level',
            'image',
            'username',
            'password',
            'email',
            'phone',
            'is_active')
        extra_kwargs = {
            'password': {'write_only': True},
            'access_level': {'read_only': True}
        }

    def create(self, validated_data):
        count = AdmissionBoardUnit.objects.all().count()
        if count == 5:
            raise ValidationError("Максимальное количество участников приемной комиссии 5")
        else:
            password = validated_data.pop('password')
            user = User.objects.create(**validated_data)
            user.set_password(password)
            user.access_level = User.ADMISSION_BOARD_LEVEL
            user.save()
            AdmissionBoardUnit.objects.create(user=user)
            return user


class AdmissionBoardUnitsSerializer(ModelSerializer):
    id = IntegerField(read_only=True)
    image = ImageField(source='user.profile_image')
    access_level = CharField(source='user.get_access_level_display')

    class Meta:
        model = AdmissionBoardUnit
        fields = (
            'id',
            'image',
            'access_level',
        )

    def to_representation(self, instance):
        response = super(AdmissionBoardUnitsSerializer, self).to_representation(instance)
        response['first_name'] = instance.user.first_name
        response['last_name'] = instance.user.last_name
        response['middle_name'] = instance.user.middle_name
        return response
