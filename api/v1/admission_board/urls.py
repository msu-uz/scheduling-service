from django.urls import path

from . import views


urlpatterns = [
    path("excel_export/", views.ExcelExportView.as_view()),
    path("applicants/list/", views.ApplicantsList.as_view(), name="applicants_list"),
    path("applicants/<int:pk>/", views.ApplicantDetail.as_view(), name="applicant_detail"),
    path("applicants/approve/<int:pk>/", views.AcceptApplicant.as_view(), name="accept_applicant"),
    path("applicants/decline/<int:pk>/", views.DeclineApplicant.as_view(), name="reject_applicant"),
    path("applicants/results/list/", views.ListApplicantsExamResults.as_view(), name="applicants_results_list"),
    path("applicants/generate-passed-list/", views.CreateApplicantsResultsList.as_view(), name="generate-passed-list"),
    path("results-list/current/", views.DetailResultsApplicantsList.as_view(), name="detail-current-results-list"),
    path("results-list/<int:pk>/add/<int:applicant_id>/", views.AddApplicantToPassedView.as_view(), name="add-applicant-to-results-list"),
    path("results-list/<int:pk>/remove/<int:applicant_id>/", views.RemoveApplicantFromPassedView.as_view(), name="remove-applicant-from-results-list"),
    path("results-list/<int:pk>/applicants/list/", views.ListPassedApplicants.as_view(), name="list-passed-applicants"),
    path("results-list/<int:pk>/approve/", views.ApproveApplicantsResultsList.as_view(), name="approve-applicants-list"),
    path("results-list/<int:pk>/decline/", views.DeclineApplicantsResultsList.as_view(), name="decline-applicants-list"),
    path("admission-board-units/list/", views.AdmissionBoardUnitsListApiView.as_view()),
    path("admission-board-units/create/", views.AdmissionBoardUnitsCreateApiView.as_view()),
    path("admission-board-units/<int:unit_id>/", views.AdmissionBoardUnitsDetailApiView.as_view()),
    path("admission-board-units/<int:unit_id>/delete/", views.AdmissionBoardUnitsDeleteApiView.as_view()),
]
