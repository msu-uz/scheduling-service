import os
import urllib.request
from faker import Faker
from django.conf import settings
from django.test import TestCase, Client
from api.models import applicant, user, role

fake = Faker(["ru_RU"])
c = Client()

a = applicant.Applicant()

url = "https://i.pinimg.com/736x/ef/be/5a/efbe5acc1fd183c5d65493ef5483d9b2.jpg"
testing_photo_path = os.path.join(settings.MEDIA_ROOT, "korean-girl.jpg")
urllib.request.urlretrieve(url, testing_photo_path)

a.image = os.path.basename(testing_photo_path)
full_name = fake.name().split(" ")
a.first_name = full_name[0]
a.last_name = full_name[1]
a.second_name = full_name[2]
a.birth_date = fake.date()
a.sex = applicant.Applicant.MALE
a.passport_number = 123123123
a.passport_address = fake.address()
a.birth_place = fake.address()
a.education_job = fake.job()
a.living_address = fake.address()
a.university_name = "harvard"
a.specialization = fake.job()
a.university_study_start_year = fake.date()
a.university_study_end_year = fake.date()
a.university_foreign_language = "english"
a.university_study_language = "english"
a.job_place = "Online Service Group"
a.job_address = fake.address()
a.war_ticket_info = "Проходил службу в воздушных войсках"


class AdmissionBoardTestCase(TestCase):
    def setUp(self):
        # save applicant
        a.save()

        # create an admission board user
        u = user.User()
        u.first_name = "dils"
        u.email = "d.matchanov@osg.uz"
        u.phone = "123123"
        u.save()

        ad_user = role.AdmissionBoardUnit(user=u)
        ad_user.save()

        # client.force_login(ad_user)


    def test_list_applicants(self):
        pass


    def test_detail_applicant(self):
        pass

    def test_approve_applicant(self):
        pass

    def test_decline_applicant(self):
        pass


