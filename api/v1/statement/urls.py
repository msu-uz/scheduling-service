from django.urls import path
from api.v1.statement import views

urlpatterns = [
    path('group/list/', views.GroupListAPIView.as_view()),
    path('group/<int:id>/', views.ModuleStudentGroupListAPIView.as_view()),
    path('module/<int:msg_id>/', views.ModuleStudentRetrieveUpdateAPIView.as_view()),
    path('module/<int:msg_id>/control/', views.JournalControlListAPIView.as_view()),
    path('journal_score/create/', views.JournalCreateAPIView.as_view()),
    path('journal_score/<int:id>/edit/', views.JournalRetrieveUpdateDestroyAPIView.as_view()),
    path('retake/create/', views.RetakeCreateAPIView.as_view()),
    path('retake/<int:id>/edit/', views.RetakeUpdateAPIView.as_view()),
    path('student/<int:id>/', views.StudentExpel.as_view()),
    path('course/list/', views.CourseListAPIView.as_view()),

]
