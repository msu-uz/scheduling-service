from django_filters import AllValuesMultipleFilter
from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from rest_framework import generics, status, filters
from rest_framework.response import Response

from api.models.statement import Journal, Retake
from api.models.group import ModuleStudentGroup, StudentGroup
from .seriailizers import ModuleStudentGroupSerializer, ModuleStudentGroupDetailSerializer, JournalSerializer, \
    JournalControlSerializer, GroupListStatementSerializer, RetakeSerializer, RetakeListSerializer, \
    StatementStudentSerializer, CourseListSerializer
# from api.v1.groups.serializers import GroupListSerializer, GroupCreateSerializer
from api.models.role import Student
from ..permissions import EducationOfficeUnitPermission
from ...models import Course


class CourseFilter(FilterSet):
    course = AllValuesMultipleFilter(field_name='course')

    class Meta:
        model = StudentGroup
        fields = ['course']


class GroupListAPIView(generics.ListAPIView):
    queryset = StudentGroup.objects.filter(is_completed=False)
    serializer_class = GroupListStatementSerializer
    permission_classes = [EducationOfficeUnitPermission, ]
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_class = CourseFilter
    search_fields = ['name']


class ModuleStudentGroupListAPIView(generics.ListAPIView):
    serializer_class = ModuleStudentGroupSerializer
    permission_classes = [EducationOfficeUnitPermission, ]
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        queryset = ModuleStudentGroup.objects.filter(group_id=self.kwargs['id'])
        return queryset


class ModuleStudentRetrieveUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ModuleStudentGroupDetailSerializer
    permission_classes = [EducationOfficeUnitPermission, ]
    lookup_url_kwarg = 'msg_id'

    def get_queryset(self):
        queryset = ModuleStudentGroup.objects.all()
        return queryset


class JournalCreateAPIView(generics.CreateAPIView):
    queryset = Journal.objects.all()
    serializer_class = JournalSerializer
    permission_classes = [EducationOfficeUnitPermission, ]


class JournalRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Journal.objects.all()
    serializer_class = JournalSerializer
    permission_classes = [EducationOfficeUnitPermission, ]
    lookup_url_kwarg = 'id'


class JournalControlListAPIView(generics.ListAPIView):
    serializer_class = JournalControlSerializer
    permission_classes = [EducationOfficeUnitPermission, ]
    lookup_url_kwarg = 'msg_id'

    def get_queryset(self):
        queryset = ModuleStudentGroup.objects.filter(id=self.kwargs['msg_id']).first().get_students()
        return queryset


class RetakeCreateAPIView(generics.CreateAPIView):
    queryset = Retake.objects.all()
    serializer_class = RetakeSerializer
    permission_classes = [EducationOfficeUnitPermission, ]

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instances = self.perform_create(serializer)
        instance_serializer = RetakeListSerializer(instances, many=True)
        return Response(instance_serializer.data, status=status.HTTP_201_CREATED)


class RetakeUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = Retake.objects.all()
    serializer_class = RetakeListSerializer
    permission_classes = [EducationOfficeUnitPermission, ]

    lookup_url_kwarg = 'id'


class StudentExpel(generics.RetrieveUpdateAPIView):
    queryset = Student.objects.all()
    serializer_class = StatementStudentSerializer
    permission_classes = [EducationOfficeUnitPermission, ]
    lookup_url_kwarg = 'id'


class CourseListAPIView(generics.ListAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseListSerializer
    permission_classes = [EducationOfficeUnitPermission, ]
