from datetime import datetime

from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from api.models import ModuleStudentGroup, StudentGroup, LessonEvent, ModuleCourse, Discipline, Course, Applicant, \
    Employee
from api.models.role import Student
from api.models.statement import Journal, Retake, QualitativeAssessment
from api.v1.attendance.serializers import CourseShortSerializer
from api.v1.groups.serializers import TeacherSerializer


class CourseListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = Course
        fields = [
            'id',
            'name'
        ]


class EventSerializer(serializers.ModelSerializer):
    interval_number = serializers.IntegerField(source='interval.number')

    class Meta:
        model = LessonEvent
        fields = [
            'id',
            'module_student_group',
            'date',
            'interval_number',
        ]

    def to_representation(self, instance):
        response = super().to_representation(instance)
        if instance.lesson.lesson:
            response["lesson_obj"] = instance.lesson.lesson.lesson.name
            response["type"] = "lesson"
        elif instance.lesson.int_control:
            response["lesson_obj"] = instance.lesson.int_control.intermediate_control.name
            response["type"] = "intermediate_control"
        elif instance.lesson.fin_control:
            response["lesson_obj"] = instance.lesson.fin_control.final_control.name
            response["type"] = "final_control"

        return response


class ModuleCourseListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = ModuleCourse
        fields = [
            'id',
        ]

    def to_representation(self, instance):
        response = super(ModuleCourseListSerializer, self).to_representation(instance)
        response['name'] = Discipline.objects.get(id=instance.discipline_id).name
        return response


class GroupListStatementSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = StudentGroup
        fields = [
            'id',
            'name',
            'department',
            'course',
            'created_at',
            'updated_at',
        ]

    def to_representation(self, instance):
        response = super(GroupListStatementSerializer, self).to_representation(instance)
        student = StudentGroup.objects.get(id=instance.id)
        response['course'] = CourseShortSerializer(instance.course).data
        response['count'] = student.studentgroupforming_set.count()
        return response


class StatementStudentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    is_student = serializers.BooleanField()
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        if Applicant.objects.filter(student=obj.id).exists():
            return self.context['request'].build_absolute_uri(obj.applicant.image.url)
        elif Employee.objects.filter(student=obj.id).exists():
            return obj.employee.image

    class Meta:
        model = Student
        fields = (
            'id',
            'image',
            'is_student',
            'created_at',
            'updated_at'
        )

    def to_representation(self, instance):
        response = super(StatementStudentSerializer, self).to_representation(instance)
        response['first_name'] = instance.user.first_name
        response['middle_name'] = instance.user.middle_name
        response['last_name'] = instance.user.last_name
        events = instance.journal_set.all()
        msg = self.context['view'].kwargs.get('msg_id')
        if msg is not None and events:
            events = instance.journal_set.all().filter(module_student_group_id=msg)

        if self.context['request'].GET.get('date'):
            date = self.context['request'].GET.get('date')
            events = instance.journal_set.all().filter(module_student_group_id=msg, event__date=date)
        response['journal'] = JournalSerializer(events, many=True).data
        journal = instance.journal_set.all().filter(event__lesson__lesson__isnull=True, score__lte=55,
                                                    student_id=instance.id)
        if msg is not None and journal:
            journal = instance.journal_set.all().filter(event__lesson__lesson__isnull=True, score__lte=55,
                                                        student_id=instance.id, module_student_group=msg)
        response['failed_control'] = JournalSerializer(journal, many=True).data
        retake = Retake.objects.filter(retake_event__student_id=instance.id)
        if msg is not None:
            retake = Retake.objects.filter(retake_event__student_id=instance.id, retake_event__module_student_group=msg)
        response['retake_event'] = RetakeListSerializer(retake, many=True).data
        response['checked'] = False
        return response


class ModuleStudentGroupSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = ModuleStudentGroup
        fields = [
            'id',
            'group',
            'module_course',
        ]

    def to_representation(self, instance):
        response = super(ModuleStudentGroupSerializer, self).to_representation(instance)
        response['group'] = GroupListStatementSerializer(instance.group).data
        response['module_course'] = ModuleCourseListSerializer(instance.module_course).data
        response['teacher'] = TeacherSerializer(instance.teacher).data
        return response


class ModuleStudentGroupDetailSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    students = serializers.SerializerMethodField()
    events = serializers.SerializerMethodField()

    def get_students(self, obj):
        queryset = obj.get_students()
        return StatementStudentSerializer(queryset, many=True, context=self.context).data

    def get_events(self, obj):
        queryset = obj.lessonevent_set.filter(module_student_group__group__is_completed=False).order_by('date',
                                                                                                        'interval__number')
        return EventSerializer(queryset, many=True).data

    class Meta:
        model = ModuleStudentGroup
        fields = [
            'id',
            'group',
            'module_course',
            'students',
            'events',
        ]

    def to_representation(self, instance):
        response = super(ModuleStudentGroupDetailSerializer, self).to_representation(instance)
        response['group'] = GroupListStatementSerializer(instance.group).data
        response['module_course'] = ModuleCourseListSerializer(instance.module_course).data
        response['teacher'] = TeacherSerializer(instance.teacher).data
        return response


class JournalListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Journal
        fields = '__all__'


class QASerializer(serializers.ModelSerializer):
    class Meta:
        model = QualitativeAssessment
        fields = [
            'id',
            'name'
        ]


class JournalSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Journal
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(
                queryset=Journal.objects.all(),
                fields=['student', 'event']
            )
        ]

    def to_representation(self, instance):
        response = super(JournalSerializer, self).to_representation(instance)
        scores = QualitativeAssessment.objects.get(min_score__lte=instance.score,
                                                   max_score__gte=instance.score)
        response['qa'] = QASerializer(scores).data
        return response


class JournalControlSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    image = serializers.ImageField(source='user.profile_image')

    class Meta:
        model = Student
        fields = (
            'id',
            'image',
            'created_at',
            'updated_at'
        )

    def to_representation(self, instance):
        date = datetime.today().date()
        msg = self.context['view'].kwargs.get('msg_id')
        response = super(JournalControlSerializer, self).to_representation(instance)
        response['first_name'] = instance.user.first_name
        response['middle_name'] = instance.user.middle_name
        response['last_name'] = instance.user.last_name
        response['current_score'] = Journal.get_lesson_score(instance, date, instance.id, msg)
        if response['current_score'] is not None:
            scores = QualitativeAssessment.objects.get(min_score__lte=response['current_score'],
                                                       max_score__gte=response['current_score'])
            response['current_score_qa'] = QASerializer(scores).data
        response['intermediate_control'] = Journal.get_int_score(self=instance,
                                                                 date=date,
                                                                 student=instance.id,
                                                                 msg=msg)
        if response['intermediate_control'] is not None:
            scores = QualitativeAssessment.objects.get(min_score__lte=response['intermediate_control'],
                                                       max_score__gte=response['intermediate_control'])
            response['intermediate_control_qa'] = QASerializer(scores).data
        response['final_control'] = Journal.get_final_score(self=instance,
                                                            date=date,
                                                            student=instance.id,
                                                            msg=msg)
        if response['final_control'] is not None:
            scores = QualitativeAssessment.objects.get(min_score__lte=response['final_control'],
                                                       max_score__gte=response['final_control'])
            response['final_control_qa'] = QASerializer(scores).data
        response['checked'] = False
        return response


class RetakeListSerializer(serializers.ModelSerializer):
    passed = serializers.BooleanField(read_only=True)

    class Meta:
        model = Retake
        fields = [
            'id',
            'retake_event',
            'score',
            'passed',
            'date',
            'description',
            'start_time',
            'end_time'
        ]

    def to_representation(self, instance):
        response = super(RetakeListSerializer, self).to_representation(instance)
        response['retake_event'] = JournalSerializer(instance.retake_event).data
        return response

    def update(self, instance, validated_data):
        if validated_data.get('score'):
            score = validated_data['score']
            if score >= 55:
                validated_data['passed'] = True
            else:
                validated_data['passed'] = False
        return super(RetakeListSerializer, self).update(instance, validated_data)


class RetakeSerializer(serializers.ModelSerializer):
    retake_event = serializers.PrimaryKeyRelatedField(queryset=Journal.objects.all(), many=True)
    date = serializers.DateField()
    start_time = serializers.TimeField()
    end_time = serializers.TimeField()

    class Meta:
        model = Retake
        fields = [
            'id',
            'retake_event',
            'score',
            'date',
            'description',
            'start_time',
            'end_time'
        ]
        extra_kwargs = {
            'score': {'required': False}
        }

    def create(self, validated_data):
        retake_event = validated_data.get('retake_event')
        date = validated_data.get('date')
        description = validated_data.get('description')
        start_time = validated_data.get('start_time')
        end_time = validated_data.get('end_time')
        data = []
        for event in retake_event:
            data.append(Retake(retake_event=event, date=date, start_time=start_time, end_time=end_time,
                               description=description))
        retakes = Retake.objects.bulk_create(data)
        return retakes
