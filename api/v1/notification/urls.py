from api.v1.notification import views
from django.urls import path

urlpatterns = [
    path('list/sent/', views.PivotNotificationSenderListView.as_view()),
    path('list/received/', views.PivotNotificationReceiverListView.as_view()),
    path('create/', views.NotificationCreateView.as_view()),
    path('detail/<int:id>/', views.NotificationDetailView.as_view()),

    path('template/list/', views.NotificationTemplateListView.as_view()),
    path('template/create/', views.NotificationTemplateCreateView.as_view()),
    path('template/detail/<int:id>/', views.NotificationTemplateDetailView.as_view()),
    path('template/update/<int:id>/', views.NotificationTemplateUpdateView.as_view()),
    path('template/delete/<int:id>/', views.NotificationTemplateDeleteView.as_view()),

    path('user/list/', views.UserListView.as_view()),

    path('<int:pivot_notification_id>/comment/create/', views.NotificationCommentCreateView.as_view()),
    path('<int:pivot_notification_id>/comment/list/', views.NotificationCommentListView.as_view()),
    path('user/list/', views.UserListView.as_view()),
    path('count/', views.PivotNotificationCountView.as_view()),
]
