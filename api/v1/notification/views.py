from django.http import JsonResponse
from rest_framework import generics
from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from api.models import Notification, NotificationTemplate, User, PivotNotification
from api.models.notification import PivotNotification, NotificationComment
from api.v1.notification.serializers import NotificationSerializer, NotificationTemplateSerializer, \
    PivotNotificationDetailSerializer, PivotNotificationSenderSerializer, \
    PivotNotificationReceiverSerializer, UserListSerializer, \
    CommentCreateSerializer, CommentListSerializer

# ----------------- Notification ------------------
from api.v1.permissions import StudentPermission


class PivotNotificationSenderListView(ListAPIView):
    serializer_class = PivotNotificationReceiverSerializer
    queryset = Notification.objects.all()

    def get_queryset(self):
        return Notification.objects.filter(sender=self.request.user).order_by('-id')


class PivotNotificationReceiverListView(ListAPIView):
    serializer_class = PivotNotificationSenderSerializer
    queryset = PivotNotification.objects.all()
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        pn = PivotNotification.objects.filter(receiver=self.request.user).order_by('-id')
        return pn


class NotificationCreateView(generics.CreateAPIView):
    serializer_class = NotificationSerializer
    permission_classes = [IsAuthenticated, ]


class NotificationDetailView(generics.RetrieveAPIView):
    serializer_class = PivotNotificationDetailSerializer
    lookup_field = 'id'
    queryset = PivotNotification.objects.all()
    permission_classes = [IsAuthenticated, ]

    def get_object(self, queryset=None):
        qs = get_object_or_404(Notification, id=self.kwargs['id'])
        rec = get_object_or_404(PivotNotification, receiver=self.request.user, notification=qs)

        if rec.receiver == self.request.user:
            rec.is_read = True
            rec.save()
            return rec
        raise PermissionDenied({"message": "You don't have permission to access", "object_id": qs.id})


# ----------------- Notification Template ------------------

class NotificationTemplateCreateView(generics.CreateAPIView):
    serializer_class = NotificationTemplateSerializer
    permission_classes = [IsAuthenticated, ]


class NotificationTemplateListView(generics.ListAPIView):
    serializer_class = NotificationTemplateSerializer
    queryset = NotificationTemplate.objects.all()
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        return NotificationTemplate.objects.filter(sender=self.request.user).order_by('-id')


class NotificationTemplateDetailView(generics.RetrieveAPIView):
    serializer_class = NotificationTemplateSerializer
    lookup_field = 'id'
    queryset = NotificationTemplate.objects.all()
    permission_classes = [IsAuthenticated, ]


class NotificationTemplateUpdateView(generics.UpdateAPIView):
    serializer_class = NotificationTemplateSerializer
    lookup_field = 'id'
    queryset = NotificationTemplate.objects.all()
    permission_classes = [IsAuthenticated, ]


class NotificationTemplateDeleteView(generics.DestroyAPIView):
    serializer_class = NotificationTemplateSerializer
    lookup_field = 'id'
    queryset = NotificationTemplate.objects.all()
    permission_classes = [IsAuthenticated, ]


# ----------------- User List ------------------


class UserListView(ListAPIView):
    serializer_class = UserListSerializer
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated, ]


# ----------------- Notification Comments ------------------


class NotificationCommentCreateView(generics.CreateAPIView):
    serializer_class = CommentCreateSerializer
    queryset = NotificationComment.objects.all()
    permission_classes = [IsAuthenticated, StudentPermission]


class NotificationCommentListView(generics.ListAPIView):
    serializer_class = CommentListSerializer
    queryset = NotificationComment.objects.all()
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        a = get_object_or_404(Notification, pivotnotification__id=self.kwargs['pivot_notification_id'])
        c = self.request.user
        b = get_object_or_404(PivotNotification, id=self.kwargs['pivot_notification_id'])
        if c.is_superuser:
            return NotificationComment.objects.filter(notification__notification=a,
                                                      notification__notification__pivotnotification=b).order_by(
                '-created_at')
        return NotificationComment.objects.filter(notification__notification=a,
                                                  notification__notification__pivotnotification=b, writer=c).order_by(
            '-created_at')


# --------------------------------------------

class PivotNotificationCountView(APIView):
    permission_classes = [IsAuthenticated, ]

    def get(self, request):
        notification_count = PivotNotification.objects.filter(receiver=request.user, is_read=False).count()
        return JsonResponse({"notification_count": notification_count})
