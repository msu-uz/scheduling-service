# Прикрепление перед конструктором

## `GET schedule/constructor/group/<int:group_id>/module/list/`

Список модулей для группы с преподавателем для каждого модуля (используется до конструктора при назначении преподавателей для модуля).

### URL params:

| Название | Описание             |
| -------- | -------------------- |
| group_id | Идентификатор группы |

### Response fields:

| Название | Описание                         |
| -------- | -------------------------------- |
| teacher  | Объект. Может быть пустым - `{}` |

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "teacher": {
        "id": 2,
        "name": "Ульяна Афанасьевна",
        "image": "http://url.jpg"
      },
      "teachers": [
        {
          "id": 2,
          "name": "Ульяна Афанасьевна",
          "image": "http://url.jpg"
        }
      ],
      "name": {
        "ru": "Ихтиолог",
        "uz": "Engineer, manufacturing"
      }
    }
  ]
}
```

---

## `PATCH schedule/constructor/module/<int:module_id>/update/`

Обновление модуля (указывается преподаватель).

### URL params:

| Название | Описание |
| -------- | -------- |
| id       | id       |

### Request fields:

| Название | Описание                    |
| -------- | --------------------------- |
| teacher  | Идентификатор преподавателя |

### Request:

```json
{
  "teacher": 1
}
```

### Response:

```json
{
  "id": 3,
  "teacher": {
    "id": 1,
    "name": "Амир Муллагалиев",
    "image": "http://url.jpg"
  },
  "name": {
    "ru": "Океанолог",
    "uz": "Commissioning editor"
  }
}
```

# Конструктор расписания

## `GET schedule/constructor/module/<module_id>/info/`

Возвращается список преподавателей, список тем, рекомендуемая тема и рекомендуемый преподаватель для модуля.

### Response fields:

| Название    | Описание                                                          |
| ----------- | ----------------------------------------------------------------- |
| topics      | Список всех тем для модуля                                        |
| teachers    | Список всех учителей для модуля                                   |
| rec_topic   | Рекомендуемая тема. Возвращается `{}`, если все темы уже пройдены |
| rec_teacher | Рекомендуемый учитель                                             |

### Response:

```json
{
  "topics": [
    {
      "id": 1,
      "name": {
        "ru": "Оперативный работник",
        "uz": "Immigration officer"
      }
    }
  ],
  "teachers": [
    {
      "id": 14,
      "name": "John Doe"
    }
  ],
  "rec_topic": {
    "id": 7,
    "name": {
      "ru": "Оперативный работник",
      "uz": "Immigration officer"
    }
  },
  "rec_teacher": {
    "id": 14,
    "name": "John Doe"
  }
}
```

## `GET schedule/constructor/group/<group_id>/shortmodule/list/`

Список модулей для группы для использования в конструкторе при выборе предмета для интервала.

### Response:

```json
{
  "results": [
    {
      "id": 4,
      "name": {
        "uz": "Module name",
        "ru": "Название модуля"
      }
    }
  ]
}
```

## `POST schedule/constructor/event/validate/`

Валидация: проверка занятости _преподавателя_ или _комнаты_ или _группы_.

### Request:

```json
{
  "group": 5,
  "date": "2020-01-01",
  "interval": 3,
  "teacher": 7,
  "room": 72
}
```

### Response - `200`:

```
No body.
```

### Response - `400`:

```json
{
  "group": ["Group is busy."],
  "teacher": ["Teacher is busy by group GroupName."],
  "room": ["Room is busy by group GroupName."]
}
```

## `POST schedule/constructor/event/create/`

Создание мероприятия.

### Request:

```json
{
  "group": 11,
  "date": "2020-01-01",
  "interval": 3,
  "module": 4, // BACKEND: modulestudentgroup
  "topic": 11,
  "teacher": 7,
  "room": 72
}
```

### Response - `201`:

```json
{
  "id": 8,
  "date": "2021-01-16",
  "interval": 1,
  "module": {
    "id": 1,
    "name": {
      "ru": "Ихтиолог",
      "uz": "Engineer, manufacturing"
    }
  },
  "topic": {
    "id": 1,
    "name": {
      "ru": "Оперативный работник",
      "uz": "Immigration officer"
    }
  },
  "teacher": {
    "id": 1,
    "name": "Амир Муллагалиев",
    "image": "http://url.jpg"
  },
  "room": 1
}
```

### Response - `400`:

```json
{
  "group": ["Group is busy."],
  "teacher": ["Teacher is busy by group GroupName."],
  "room": ["Room is busy by group GroupName."]
}
```

## `PATCH schedule/constructor/event/<event_id>/update/`

Обновление существующего мероприятия.

### Request:

```json
{
  "room": 2,
  "teacher": 2,
  "module": 2,
  "topic": 1
}
```

### Response - `200`:

```json
{
  "id": 1,
  "module": {
    "id": 2,
    "name": {
      "ru": "Ихтиолог",
      "uz": "Engineer, manufacturing"
    }
  },
  "topic": {
    "id": 1,
    "name": {
      "ru": "Оперативный работник",
      "uz": "Immigration officer"
    }
  },
  "teacher": {
    "id": 2,
    "name": "Ульяна Афанасьевна",
    "image": "http://url.jpg"
  },
  "room": 2
}
```

### Response - `400`:

```json
{
  "teacher": ["Teacher is busy by group GroupName."],
  "room": ["Room is busy by group GroupName."]
}
```

## `GET schedule/constructor/event/<event_id>/`

Детальный просмотр внеклассного задания при обновлении.

### Response:

```json
{
  "id": 1,
  "teacher": {
    "id": 2,
    "name": "Ульяна Афанасьевна",
    "image": "http://url.jpg"
  },
  "room": {
    "id": 2,
    "name": "213"
  },
  "module": {
    "id": 1,
    "name": {
      "ru": "Ихтиолог",
      "uz": "Engineer, manufacturing"
    }
  },
  "topic": {
    "id": 1,
    "name": {
      "ru": "Оперативный работник",
      "uz": "Immigration officer"
    }
  }
}
```

## `GET schedule/constructor/group/<int:group_id>/event/list/?date=

Список мероприятий на день (по дате).

### Response:

```json
{
  "results": [
    {
      "id": 1,
      "teacher": {
        "id": 1,
        "name": "amir",
        "image": "http://url.jpg"
      },
      "room": {
        "id": 1,
        "name": "123"
      },
      "interval": {
        "id": 1,
        "number": 1,
        "start_time": "00:00:00",
        "end_time": "06:00:00"
      },
      "module": {
        "id": 1,
        "name": {
          "ru": "Квасник",
          "uz": "Pharmacist, community"
        }
      },
      "topic": {
        "id": 1,
        "name": {
          "ru": "Финансист",
          "uz": "Passenger transport manager"
        }
      }
    }
  ]
}
```

👆 готово всё, что выше
