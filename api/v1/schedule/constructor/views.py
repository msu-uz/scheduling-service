from api.v1.permissions import EducationOfficeUnitPermission
from api.models.schedule import LessonEvent
from api.v1.schedule.constructor.serializers import (LessonEventCreateSerializer,
                                                     LessonEventSerializer,
                                                     LessonEventUpdateSerializer,
                                                     LessonEventValidateSerializer,
                                                     ModuleStudentGroupInfoSerializer,
                                                     ModuleStudentGroupSerializer,
                                                     ModuleStudentGroupShortSerializer)
from api.models.group import ModuleStudentGroup, StudentGroup
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.exceptions import NotFound
from datetime import datetime


class ModuleStudentGroupListView(ListAPIView):
    serializer_class = ModuleStudentGroupSerializer
    permission_classes = (EducationOfficeUnitPermission,)

    def get_queryset(self):
        group_id = self.kwargs["group_id"]
        return ModuleStudentGroup.objects.filter(group_id=group_id)


class ShortModuleStudentGroupListView(ListAPIView):
    serializer_class = ModuleStudentGroupShortSerializer
    permission_classes = (EducationOfficeUnitPermission,)

    def get_queryset(self):
        group_id = self.kwargs["group_id"]
        return ModuleStudentGroup.objects.filter(group_id=group_id)


class ModuleStudentGroupUpdateView(UpdateAPIView):
    serializer_class = ModuleStudentGroupSerializer
    permission_classes = (EducationOfficeUnitPermission,)
    lookup_url_kwarg = "module_id"
    queryset = ModuleStudentGroup.objects.all()


class ModuleStudentGroupInfoView(RetrieveAPIView):
    serializer_class = ModuleStudentGroupInfoSerializer
    permission_classes = (EducationOfficeUnitPermission,)
    lookup_url_kwarg = "module_id"
    queryset = ModuleStudentGroup.objects.all()


class LessonEventValidateView(CreateAPIView):
    serializer_class = LessonEventValidateSerializer
    permission_classes = (EducationOfficeUnitPermission,)


class LessonEventCreateView(CreateAPIView):
    serializer_class = LessonEventCreateSerializer
    permission_classes = (EducationOfficeUnitPermission,)


class LessonEventUpdateView(UpdateAPIView):
    serializer_class = LessonEventUpdateSerializer
    permission_classes = (EducationOfficeUnitPermission,)
    lookup_url_kwarg = "event_id"
    queryset = LessonEvent.objects.all()


class LessonEventDetailView(RetrieveAPIView):
    serializer_class = LessonEventSerializer
    permission_classes = (EducationOfficeUnitPermission,)
    lookup_url_kwarg = "event_id"
    queryset = LessonEvent.objects.all()


class LessonEventByDateDetailView(ListAPIView):
    serializer_class = LessonEventSerializer
    permission_classes = (EducationOfficeUnitPermission,)
    lookup_url_kwarg = "group_id"

    def get_queryset(self):
        group_id = self.kwargs["group_id"]
        date = self.request.query_params.get("date", "")

        if not StudentGroup.objects.filter(id=group_id).exists():
            raise NotFound("Group not found.")

        queryset = LessonEvent.objects.none()

        try:
            date = datetime.fromisoformat(date).date()
        except ValueError:
            pass
        else:
            queryset = LessonEvent.objects.filter(module_student_group__group=group_id,
                                                  date=date).distinct().order_by("interval__start_time")

        return queryset
