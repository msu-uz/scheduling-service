from api.v1.schedule.constructor.views import (LessonEventByDateDetailView,
                                               LessonEventCreateView,
                                               LessonEventDetailView,
                                               LessonEventUpdateView,
                                               LessonEventValidateView,
                                               ModuleStudentGroupInfoView,
                                               ModuleStudentGroupListView,
                                               ModuleStudentGroupUpdateView,
                                               ShortModuleStudentGroupListView)
from django.urls.conf import path

urlpatterns = [
    path("group/<int:group_id>/module/list/", ModuleStudentGroupListView.as_view()),
    path("group/<int:group_id>/shortmodule/list/", ShortModuleStudentGroupListView.as_view()),
    path("module/<int:module_id>/update/", ModuleStudentGroupUpdateView.as_view()),
    path("module/<int:module_id>/info/", ModuleStudentGroupInfoView.as_view()),
    path("event/validate/", LessonEventValidateView.as_view()),
    path("event/create/", LessonEventCreateView.as_view()),
    path("event/<int:event_id>/update/", LessonEventUpdateView.as_view()),
    path("event/<int:event_id>/", LessonEventDetailView.as_view()),
    path("group/<int:group_id>/event/list/", LessonEventByDateDetailView.as_view()),
]
