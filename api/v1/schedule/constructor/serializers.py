from api.models.resources import Resource
from rest_framework.exceptions import ValidationError
from api.models.schedule import LessonEvent, LessonEventInterval
from api.models.discipline import ModuleWeekType
from api.models.role import Teacher
from api.models.group import ModuleStudentGroup
from rest_framework.serializers import ModelSerializer
from rest_framework import serializers


class TeacherShortSerializer(ModelSerializer):
    class Meta:
        model = Teacher
        fields = ("id",)

    def to_representation(self, instance: Teacher):
        response = super().to_representation(instance)
        response.update({
            "name": str(instance),
            "image": instance.employee.image
        })
        return response


class ModuleStudentGroupSerializer(ModelSerializer):
    class Meta:
        model = ModuleStudentGroup
        fields = ("id", "teacher")
        read_only_fields = ("id",)

    def to_representation(self, instance: ModuleStudentGroup):
        response = super().to_representation(instance)
        teachers = instance.module_course.discipline.disciplines.all()
        response.update({
            "teacher": TeacherShortSerializer(instance.teacher).data,
            "teachers": TeacherShortSerializer(teachers, many=True).data,
            "name": instance.module_course.discipline.name
        })
        return response


class ModuleStudentGroupShortSerializer(ModelSerializer):
    class Meta:
        model = ModuleStudentGroup
        fields = ("id",)
        read_only_fields = ("id",)

    def to_representation(self, instance: ModuleStudentGroup):
        response = super().to_representation(instance)
        response["name"] = instance.module_course.discipline.name
        return response


class ModuleWeekTypeSerializer(ModelSerializer):
    class Meta:
        model = ModuleWeekType
        fields = ("id", "name")


class ModuleStudentGroupInfoSerializer(ModelSerializer):
    class Meta:
        model = ModuleStudentGroup
        fields = ("id",)

    def to_representation(self, instance: ModuleStudentGroup):
        topics = ModuleWeekType.objects.filter(week_day__module_course=instance.module_course).distinct()
        rec_topic = topics.exclude(id__in=instance.lessonevent_set.values_list("lesson", flat=True)).first()
        teachers = instance.module_course.discipline.disciplines.all()

        response = {
            "topics": ModuleWeekTypeSerializer(topics, many=True).data,
            "teachers": TeacherShortSerializer(teachers, many=True).data,
            "rec_topic": ModuleWeekTypeSerializer(rec_topic).data,
            "rec_teacher": TeacherShortSerializer(instance.teacher).data
        }

        return response


class LessonEventValidateSerializer(ModelSerializer):
    group = serializers.IntegerField(required=True)

    class Meta:
        model = LessonEvent
        fields = ("group", "date", "interval", "teacher", "room")

    def validate(self, attrs):
        teacher = attrs.get("teacher")
        room = attrs.get("room")
        date = attrs["date"]
        group = attrs["group"]
        interval = attrs["interval"]

        if not (teacher or room):
            raise ValidationError("At least one field is required: teacher, room")

        errors = dict()

        if LessonEvent.objects.filter(date=date, interval=interval, module_student_group__group=group).exists():
            errors["group"] = "Group is busy."

        if teacher and (lesson := LessonEvent.objects.filter(date=date, interval=interval, teacher=teacher).first()):
            errors["teacher"] = {
                "detail": "Teacher is busy by group",
                "value": lesson.module_student_group.group.name
            }

        if room and (lesson := LessonEvent.objects.filter(date=date, interval=interval, room=room).first()):
            errors["room"] = {
                "detail": "Room is busy by group",
                "value": lesson.module_student_group.group.name
            }

        if errors:
            raise ValidationError(detail=errors)
        return super().validate(attrs)

    def create(self, validated_data):
        return {}

    def to_representation(self, instance):
        return instance


# class ResourceTypeSerializer(ModelSerializer):
#     class Meta:
#         model = ResourcesType
#         fields = ("id", "name")


class ResourceSerializer(ModelSerializer):
    class Meta:
        model = Resource
        fields = ("id", "name")


class LessonEventCreateSerializer(ModelSerializer):
    module = serializers.PrimaryKeyRelatedField(queryset=ModuleStudentGroup.objects.all(),
                                                source="module_student_group")
    topic = serializers.PrimaryKeyRelatedField(queryset=ModuleWeekType.objects.all(), source="lesson")

    class Meta:
        model = LessonEvent
        fields = ("id", "date", "interval", "module", "topic", "teacher", "room")
        read_only_fields = ("id",)
        extra_kwargs = {
            "interval": {"required": True},
            "module": {"required": True},
            "topic": {"required": True},
            "room": {"required": True},
            "teacher": {"required": True},
        }

    def validate(self, attrs):
        teacher = attrs["teacher"]
        room = attrs["room"]
        date = attrs["date"]
        module_student_group = attrs["module_student_group"]
        interval = attrs["interval"]

        errors = dict()

        if LessonEvent.objects.filter(date=date,
                                      interval=interval,
                                      module_student_group=module_student_group).exists():
            errors["group"] = "Group is busy."

        if teacher and (lesson := LessonEvent.objects.filter(date=date, interval=interval, teacher=teacher).first()):
            errors["teacher"] = {
                "detail": "Teacher is busy by group",
                "value": lesson.module_student_group.group.name
            }

        if room and (lesson := LessonEvent.objects.filter(date=date, interval=interval, room=room).first()):
            errors["room"] = {
                "detail": "Room is busy by group",
                "value": lesson.module_student_group.group.name
            }

        if errors:
            raise ValidationError(detail=errors)
        return super().validate(attrs)

    def to_representation(self, instance: LessonEvent):
        response = super().to_representation(instance)
        response.update({
            "module": ModuleStudentGroupShortSerializer(instance.module_student_group).data,
            "topic": ModuleWeekTypeSerializer(instance.lesson).data,
            "teacher": TeacherShortSerializer(instance.teacher).data
        })
        return response


class LessonEventUpdateSerializer(ModelSerializer):
    module = serializers.PrimaryKeyRelatedField(queryset=ModuleStudentGroup.objects.all(),
                                                source="module_student_group")
    topic = serializers.PrimaryKeyRelatedField(queryset=ModuleWeekType.objects.all(), source="lesson")

    class Meta:
        model = LessonEvent
        fields = ("id", "module", "topic", "teacher", "room")
        read_only_fields = ("id",)

    def validate(self, attrs):
        date = self.instance.date
        interval = self.instance.interval
        teacher = attrs["teacher"]
        room = attrs["room"]

        errors = dict()

        if teacher != self.instance.teacher and (lesson := LessonEvent.objects.filter(
                date=date, interval=interval, teacher=teacher).first()):
            errors["teacher"] = {
                "detail": "Teacher is busy by group",
                "value": lesson.module_student_group.group.name
            }

        if room != self.instance.room and (lesson := LessonEvent.objects.filter(
                date=date, interval=interval, room=room).first()):
            errors["room"] = {
                "detail": "Room is busy by group",
                "value": lesson.module_student_group.group.name
            }

        if errors:
            raise ValidationError(detail=errors)
        return super().validate(attrs)

    def to_representation(self, instance: LessonEvent):
        response = super().to_representation(instance)
        response.update({
            "module": ModuleStudentGroupShortSerializer(instance.module_student_group).data,
            "topic": ModuleWeekTypeSerializer(instance.lesson).data,
            "teacher": TeacherShortSerializer(instance.teacher).data
        })
        return response


class LessonEventIntervalSerializer(ModelSerializer):
    class Meta:
        model = LessonEventInterval
        fields = (
            "id",
            "number",
            "start_time",
            "end_time",
        )


class LessonEventSerializer(ModelSerializer):
    class Meta:
        model = LessonEvent
        fields = ("id", "teacher", "room")
        read_only_fields = ("id",)

    def to_representation(self, instance: LessonEvent):
        response = super().to_representation(instance)
        response.update({
            "interval": LessonEventIntervalSerializer(instance.interval).data,
            "module": ModuleStudentGroupShortSerializer(instance.module_student_group).data,
            "topic": ModuleWeekTypeSerializer(instance.lesson).data,
            "teacher": TeacherShortSerializer(instance.teacher).data,
            "room": ResourceSerializer(instance.room).data,
        })
        return response
