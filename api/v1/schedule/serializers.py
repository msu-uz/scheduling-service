from datetime import datetime, timedelta

from dateutil.rrule import rrule, DAILY
from rest_framework import serializers
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.serializers import ModelSerializer

from api.models import (
    LessonEvent,
    LessonEventInterval,
    StudentGroup,
    Course,
    ModuleCourse,
    Discipline,
    Department,
    Teacher,
    ModuleWeekType,
)
from api.models.group import ModuleStudentGroup, Student
from api.models.resources import Resource, ResourcesType
from api.v1.discipline.serializers import CourseTypeSerializer
from api.v1.schedule import generator


class ResourceSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source="type.name")

    class Meta:
        model = Resource
        fields = ("id", "name", "type")


class TeacherSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    access_level = serializers.CharField(
        source="user.get_access_level_display")

    class Meta:
        model = Teacher
        fields = (
            "id",
            "access_level",
            "created_at",
            "updated_at",
        )

    def to_representation(self, instance):
        response = super(TeacherSerializer, self).to_representation(instance)
        response["image"] = instance.employee.image
        response["first_name"] = instance.user.first_name
        response["middle_name"] = instance.user.middle_name
        response["last_name"] = instance.user.last_name
        return response


class EventSerializer(ModelSerializer):
    class Meta:
        model = LessonEvent
        fields = "__all__"


class EventIntervalSerializer(ModelSerializer):
    class Meta:
        model = LessonEventInterval
        fields = ["id", "number", "start_time", "end_time"]


class DepartmentSerializer(serializers.ModelSerializer):
    name = serializers.JSONField()

    class Meta:
        model = Department
        fields = ("id", "name")


class CourseShortSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = Course
        fields = ("id", "name")


class DisciplineShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discipline
        fields = ("id", "name", "department")

    def to_representation(self, instance):
        response = super(DisciplineShortSerializer,
                         self).to_representation(instance)
        response["department"] = DepartmentSerializer(instance.department).data
        return response


class ScheduleCourseSerializer(ModelSerializer):
    type = serializers.JSONField(source="course_type.name")

    class Meta:
        model = Course
        fields = ("id", "name", "type")
        read_only_fields = ("id", "name", "type")


class ScheduleStudentGroupSerializer(ModelSerializer):
    course = ScheduleCourseSerializer()

    class Meta:
        model = StudentGroup
        fields = ("id", "name", "course")
        read_only_fields = ("id", "name", "course")


class LessonEventSerializer(ModelSerializer):
    title = serializers.JSONField(
        source="module_student_group.module_course.discipline.name")
    room = serializers.SerializerMethodField()
    resource = ResourceSerializer(many=True)

    class Meta:
        model = LessonEvent
        fields = ("id", "title", "lesson", "room", "resource")

    def get_room(self, instance):
        name = ""
        if instance.room:
            name = instance.room.name
        return name

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["start"] = datetime.combine(
            instance.date, instance.interval.start_time
        ).isoformat()
        response["end"] = datetime.combine(
            instance.date, instance.interval.end_time).isoformat()

        if instance.lesson.lesson:
            response["lesson_obj"] = instance.lesson.lesson.lesson.name
            response["type"] = "lesson"
        elif instance.lesson.int_control:
            response["lesson_obj"] = instance.lesson.int_control.intermediate_control.name
            response["type"] = "intermediate_control"
        elif instance.lesson.fin_control:
            response["lesson_obj"] = instance.lesson.fin_control.final_control.name
            response["type"] = "final_control"

        return response


def create_module_student_groups(student_group: StudentGroup) -> None:
    course = student_group.course
    if course:
        module_student_groups = []
        for module_course in course.modulecourse_set.all():
            module_student_groups.append(ModuleStudentGroup(group=student_group,
                                                            module_course=module_course))
        ModuleStudentGroup.objects.bulk_create(module_student_groups)


class ScheduleStudentGroupUpdateSerializer(ModelSerializer):
    start_date = serializers.DateField(write_only=True)

    class Meta:
        model = StudentGroup
        fields = ("course", "start_date")

    def update(self, instance: StudentGroup, validated_data):
        course = validated_data.get("course")
        if course:
            if not instance.course:
                instance.course = course
                instance.save()
                create_module_student_groups(instance)
                return instance
            else:
                instance.is_completed = True
                instance.save()
                new_instance = StudentGroup.objects.create(name=instance.name,
                                                           uuid=instance.uuid,
                                                           course=course,
                                                           department=instance.department)
                create_module_student_groups(new_instance)
                return new_instance

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["name"] = instance.name
        response["course"] = CourseShortSerializer(instance.course).data
        response["course_type"] = CourseTypeSerializer(instance.course.course_type).data
        return response


class LessonEventCreateUpdateSerializer(ModelSerializer):
    module_course = serializers.IntegerField(write_only=True, required=True)

    class Meta:
        model = LessonEvent
        fields = ("interval", "lesson", "room",
                  "date", "module_course", "resource")

    def validate(self, attrs):
        group_id = self.context["request"].parser_context["kwargs"]["id"]
        interval = attrs.get("interval")
        room = attrs.get("room")
        date = attrs.get("date")

        if date and interval:
            if (
                room
                and LessonEvent.objects.filter(date=date, interval=interval, room=room).exists()
            ):
                raise ValidationError("This room is busy at this time.")

            if LessonEvent.objects.filter(
                    date=date, interval=interval,
                    module_student_group__group_id=group_id).exists():
                raise ValidationError(
                    "This group already has an event at this time.")

        return attrs

    def create(self, validated_data):
        group_id = self.context["request"].parser_context["kwargs"]["id"]
        module_course_id = validated_data.pop("module_course")
        module_student_group = ModuleStudentGroup.objects.filter(
            group_id=group_id, module_course_id=module_course_id
        ).first()
        validated_data["module_student_group"] = module_student_group
        return super().create(validated_data)

    def update(self, instance, validated_data):
        group_id = self.context["request"].parser_context["kwargs"]["id"]
        module_course_id = validated_data.pop("module_course", None)

        if module_course_id is not None:
            module_student_group = ModuleStudentGroup.objects.filter(
                group_id=group_id, module_course_id=module_course_id
            ).first()
            validated_data["module_student_group"] = module_student_group

        return super().update(instance, validated_data)


class RepeatLessonEventsCreateSerializer(ModelSerializer):
    module_course = serializers.IntegerField(write_only=True)
    days = serializers.ListField(
        child=serializers.IntegerField(),
        write_only=True, min_length=1, max_length=7)
    start_date = serializers.DateField(write_only=True)
    end_date = serializers.DateField(write_only=True)

    class Meta:
        model = LessonEvent
        fields = (
            "interval",
            "lesson",
            "room",
            "resource",
            "days",
            "module_course",
            "start_date",
            "end_date",
        )

    def validate(self, attrs):
        group_id = self.context["request"].parser_context["kwargs"]["id"]
        interval = attrs["interval"]
        room = attrs["room"]
        start_date = attrs["start_date"]
        end_date = attrs["end_date"]
        days = attrs["days"]

        dates = list(
            rrule(
                freq=DAILY, dtstart=start_date, until=end_date +
                timedelta(days=1),
                byweekday=days))

        if LessonEvent.objects.filter(
                date__in=dates, interval=interval, room=room).exists():
            raise ValidationError("This room is busy at this time.")

        if event := LessonEvent.objects.filter(
                date__in=dates, interval=interval,
                module_student_group__group_id=group_id).first():
            raise ValidationError(
                f"This group already has an event at {event.date}.")

        return attrs

    def create(self, validated_data):
        group_id = self.context["request"].parser_context["kwargs"]["id"]
        module_course_id = validated_data.pop("module_course")
        module_student_group = ModuleStudentGroup.objects.filter(
            group_id=group_id, module_course_id=module_course_id
        ).first()
        if not module_student_group:
            raise NotFound("ModuleStudentGroup not found.")

        validated_data["module_student_group"] = module_student_group
        validated_data["room"] = validated_data.pop("room")
        return generator.repeat(**validated_data)


class CourseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.JSONField()

    class Meta:
        model = Course
        fields = ("id", "name")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["course_type"] = CourseTypeSerializer(
            instance.course_type).data
        return response


class ModuleCourseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = ModuleCourse
        fields = "__all__"

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["course"] = CourseShortSerializer(instance.course).data
        response["discipline"] = DisciplineShortSerializer(
            instance.discipline).data
        return response


class ModuleWeekTypeSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = ModuleWeekType
        fields = ("id", "name")

    def get_name(self, instance):
        name = ""

        if instance.lesson:
            name = instance.lesson.lesson.name
        elif instance.int_control:
            name = instance.int_control.intermediate_control.name
        elif instance.fin_control:
            name = instance.fin_control.final_control.name

        return name


class LessonEventChangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LessonEvent
        fields = ("id", "date", "interval")
        extra_kwargs = {
            "interval": {"required": False},
            "date": {"required": False}}

    def update(self, instance, validated_data):
        if validated_data.get("date") and validated_data.get("interval"):
            date = validated_data.pop("date")
            interval = validated_data.pop("interval")

            if LessonEvent.objects.filter(
                    module_student_group=instance.module_student_group, date=date,
                    interval=interval).count():
                raise ValidationError("Дата и время занято другим уроком ")
            else:
                validated_data["date"] = date
                validated_data["interval"] = interval
        elif validated_data.get("interval"):
            interval = validated_data.pop("interval")
            if LessonEvent.objects.filter(
                module_student_group=instance.module_student_group,
                date=instance.date,
                interval=interval,
            ).count():
                raise ValidationError("Данное время занято другим уроком")
            else:
                validated_data["interval"] = interval
                validated_data["date"] = instance.date
        else:
            date = validated_data.pop("date")
            if LessonEvent.objects.filter(
                module_student_group=instance.module_student_group,
                date=date,
                interval=instance.interval,
            ).count():
                raise ValidationError("Данная дата уже занята другим уроком")
            else:
                validated_data["date"] = date
                validated_data["interval"] = instance.interval

        return super().update(instance, validated_data)


class LessonEventSwapSerializer(serializers.ModelSerializer):
    class Meta:
        model = LessonEvent
        fields = ("id", "date", "interval")
        extra_kwargs = {
            "interval": {"required": False},
            "date": {"required": False}}

    def update(self, instance, validated_data):
        new_date = validated_data.get("date")
        new_interval = validated_data.get("interval")

        if not new_interval:
            new_interval = instance.interval

        if not new_date:
            new_date = instance.date

        existing_lesson_event = LessonEvent.objects.filter(
            module_student_group=instance.module_student_group,
            date=new_date,
            interval=new_interval,
        ).first()

        if existing_lesson_event:
            existing_lesson_event.date = instance.date
            existing_lesson_event.interval = instance.interval
            existing_lesson_event.save()

        validated_data["date"] = new_date
        validated_data["interval"] = new_interval

        return super().update(instance, validated_data)


class ModuleStudentGroupSerializer(ModelSerializer):
    module_course = ModuleCourseSerializer()
    group = serializers.CharField(source="group.name")

    class Meta:
        model = ModuleStudentGroup
        fields = ("id", "module_course", "teacher", "group")
        read_only_fields = ("id", "module_course", "group")

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["teacher"] = TeacherSerializer(instance.teacher).data
        return response


class ResourceTypeSerializer(ModelSerializer):
    class Meta:
        model = ResourcesType
        fields = ("id", "name")


class ResourceSerializer(ModelSerializer):
    type = ResourceTypeSerializer()

    class Meta:
        model = Resource
        fields = ("id", "name", "type")
