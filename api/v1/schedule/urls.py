from django.urls import path, include
from api.v1.schedule import views

urlpatterns = [
    # path('list/', views.ScheduleListView.as_view()),
    # path('<int:id>', views.ScheduleUpdateDeleteView.as_view()),
    # path('create/', views.ScheduleCreateView.as_view()),
    path("event/list/", views.EventListView.as_view()),
    path("room/list/", views.RoomListView.as_view()),
    path("teacher/list/", views.TeacherListView.as_view()),
    # path('event/<int:id>', views.EventGetUpdateDeleteView.as_view() ),
    path("event/<int:id>", views.EventDetailUpdateDeleteView.as_view()),
    path("event/create/", views.EventCreateView.as_view()),
    path("event/interval/list/", views.EventIntervalListView.as_view()),
    path("event/interval/<int:id>/", views.EventIntervalGetUpdateDeleteView.as_view()),
    path("event/interval/create/", views.EventIntervalCreateView.as_view()),
    path("studentgroup/list/", views.StudentGroupListView.as_view()),
    path("studentgroup/<int:id>/event/list/", views.LessonEventListView.as_view()),
    path("studentgroup/<int:id>/update/", views.StudentGroupUpdateView.as_view()),
    path("course/list/", views.CourseListView.as_view()),
    # path('studentgroup/<int:id>/teacher/list/', views.TeacherListView.as_view()),  # TODO: remove
    path("studentgroup/<int:id>/modulecourse/list/", views.ModuleCourseListView.as_view()),
    path("studentgroup/<int:id>/event/create/", views.LessonEventCreateUpdateView.as_view()),
    path(
        "studentgroup/<int:id>/event/<int:event_id>/update/",
        views.LessonEventCreateUpdateView.as_view(),
    ),
    path(
        "studentgroup/<int:id>/event/create/repeat/", views.RepeatLessonEventsCreateView.as_view()
    ),
    path(
        "studentgroup/<int:id>/modulestudentgroup/list/",
        views.ModuleStudentGroupListView.as_view(),
    ),
    path(
        "modulestudentgroup/<int:id>/update/", views.ModuleStudentGroupUpdateView.as_view()
    ),
    path("resource/list/", views.ResourceListView.as_view()),
    path("moduleweektype/list/", views.ModuleWeekTypeListView.as_view()),
    path("event/<int:id>/change/", views.LessonEventChangeView.as_view()),
    path("event/<int:id>/swap/", views.LessonEventSwapView.as_view()),
    path("constructor/", include("api.v1.schedule.constructor.urls")),
]
