from datetime import datetime

from api.models import (Course, LessonEventInterval, ModuleCourse,
                        ModuleWeekType, StudentGroup, Teacher)
from api.models.group import ModuleStudentGroup
from api.models.resources import Resource
from api.v1.groups.serializers import TeacherSerializer
from api.v1.permissions import (AdministratorPermission,
                                EducationOfficeUnitPermission)
from api.v1.schedule.serializers import (CourseSerializer,
                                         EventIntervalSerializer,
                                         EventSerializer, LessonEvent,
                                         LessonEventChangeSerializer,
                                         LessonEventCreateUpdateSerializer,
                                         LessonEventSerializer,
                                         LessonEventSwapSerializer,
                                         ModuleCourseSerializer,
                                         ModuleStudentGroupSerializer,
                                         ModuleWeekTypeSerializer,
                                         RepeatLessonEventsCreateSerializer,
                                         ResourceSerializer,
                                         ScheduleStudentGroupSerializer,
                                         ScheduleStudentGroupUpdateSerializer)
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework.response import Response
from rest_framework.exceptions import NotFound
from rest_framework.filters import SearchFilter
from rest_framework.generics import (CreateAPIView, ListAPIView,
                                     RetrieveUpdateDestroyAPIView,
                                     UpdateAPIView)

# EventInterval


class EventIntervalCreateView(CreateAPIView):
    serializer_class = EventIntervalSerializer
    permission_classes = [AdministratorPermission]


class EventIntervalListView(ListAPIView):
    serializer_class = EventIntervalSerializer
    queryset = LessonEventInterval.objects.all()
    permission_classes = [EducationOfficeUnitPermission |
                          AdministratorPermission]


class EventIntervalGetUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    def get_permissions(self):
        if self.request.method == "GET":
            self.permission_classes = [
                EducationOfficeUnitPermission | AdministratorPermission]
        else:
            self.permission_classes = [AdministratorPermission]
        return super(EventIntervalGetUpdateDeleteView, self).get_permissions()

    serializer_class = EventIntervalSerializer
    lookup_url_kwarg = "id"
    queryset = LessonEventInterval.objects.all()


# Event


class EventCreateView(CreateAPIView):
    serializer_class = EventSerializer
    permission_classes = [EducationOfficeUnitPermission]


class EventListView(ListAPIView):
    serializer_class = EventSerializer
    permission_classes = [EducationOfficeUnitPermission]

    def get_queryset(self):
        return LessonEvent.objects.order_by("date", "interval__start_time")


class EventDetailUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    serializer_class = EventSerializer
    lookup_url_kwarg = "id"
    queryset = LessonEvent.objects.all()
    permission_classes = [EducationOfficeUnitPermission]


# Schedule


class StudentGroupListView(ListAPIView):
    queryset = StudentGroup.objects.filter(is_completed=False)
    serializer_class = ScheduleStudentGroupSerializer
    permission_classes = [EducationOfficeUnitPermission]
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["course"]
    search_fields = ["name"]


class LessonEventListView(ListAPIView):
    serializer_class = LessonEventSerializer
    permission_classes = [EducationOfficeUnitPermission]
    pagination_class = None

    def get_queryset(self):
        group_id = self.kwargs["id"]

        start = self.request.query_params.get("start")
        end = self.request.query_params.get("end")

        if start and end:
            start_date = datetime.fromisoformat(start).date()
            end_date = datetime.fromisoformat(end).date()
            qs = LessonEvent.objects.filter(
                module_student_group__group=group_id, date__gte=start_date,
                date__lte=end_date).order_by(
                "date", "interval__start_time")
        else:
            qs = LessonEvent.objects.none()
        return qs

    def get(self, request, *args, **kwargs):
        group = StudentGroup.objects.filter(id=self.kwargs.get("id")).first()
        if not group:
            raise NotFound("StudentGroup not found.")
        response = self.list(request, *args, **kwargs)
        response.data = {
            "results": response.data,
            "student_group": ScheduleStudentGroupSerializer(group).data,
        }
        return response


class StudentGroupUpdateView(UpdateAPIView):
    serializer_class = ScheduleStudentGroupUpdateSerializer
    queryset = StudentGroup.objects.all()
    lookup_url_kwarg = "id"


class CourseListView(ListAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    permission_classes = [EducationOfficeUnitPermission]
    filter_backends = [filters.SearchFilter]
    search_fields = ["name"]


class TeacherListView(ListAPIView):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer
    permission_classes = [EducationOfficeUnitPermission]


class ModuleCourseListView(ListAPIView):
    serializer_class = ModuleCourseSerializer
    permission_classes = [EducationOfficeUnitPermission]
    filter_backends = [filters.SearchFilter]
    search_fields = ["discipline__name"]

    def get_queryset(self):
        group_id = self.kwargs.get("id")
        student_group = get_object_or_404(StudentGroup, id=group_id)
        course_id = student_group.course.id
        return ModuleCourse.objects.filter(course_id=course_id)


class LessonEventCreateUpdateView(CreateAPIView, UpdateAPIView):
    queryset = LessonEvent.objects.all()
    serializer_class = LessonEventCreateUpdateSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_field = "id"
    lookup_url_kwarg = "event_id"


class RepeatLessonEventsCreateView(CreateAPIView):
    queryset = LessonEvent.objects.all()
    serializer_class = RepeatLessonEventsCreateSerializer
    permission_classes = [EducationOfficeUnitPermission]

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instances = self.perform_create(serializer)
        instance_serializer = LessonEventSerializer(instances, many=True)
        return Response(instance_serializer.data,
                        status=status.HTTP_201_CREATED)


class ResourceListView(ListAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    permission_classes = [EducationOfficeUnitPermission]


class ModuleWeekTypeListView(ListAPIView):
    """
    Список тем.

    Query params: module_course_id
    """

    serializer_class = ModuleWeekTypeSerializer
    permission_classes = [EducationOfficeUnitPermission]

    def get_queryset(self):
        module_course_id = self.request.query_params.get("module_course_id")
        if module_course_id:
            qs = ModuleWeekType.objects.filter(
                week_day__module_course_id=module_course_id)
        else:
            qs = ModuleWeekType.objects.none()
        return qs


class LessonEventChangeView(UpdateAPIView):
    serializer_class = LessonEventChangeSerializer
    permission_classes = [
        EducationOfficeUnitPermission,
    ]
    queryset = LessonEvent.objects.all()
    lookup_url_kwarg = "id"


class LessonEventSwapView(UpdateAPIView):
    serializer_class = LessonEventSwapSerializer
    queryset = LessonEvent.objects.all()
    lookup_url_kwarg = "id"
    permission_classes = [EducationOfficeUnitPermission]


class ModuleStudentGroupListView(ListAPIView):
    serializer_class = ModuleStudentGroupSerializer
    permission_classes = [EducationOfficeUnitPermission]

    def get_queryset(self):
        group_id = self.kwargs["id"]
        return ModuleStudentGroup.objects.filter(group_id=group_id)


class ModuleStudentGroupUpdateView(UpdateAPIView):
    queryset = ModuleStudentGroup.objects.all()
    serializer_class = ModuleStudentGroupSerializer
    permission_classes = [EducationOfficeUnitPermission]
    lookup_url_kwarg = "id"


class RoomListView(ListAPIView):
    serializer_class = ResourceSerializer
    permission_classes = [EducationOfficeUnitPermission]

    def get_queryset(self):
        return Resource.objects.filter(type__id=1)
