import datetime
from typing import List

from api.models import LessonEvent, LessonEventInterval
from api.models.discipline import ModuleCourse, ModuleWeekType
from api.models.group import ModuleStudentGroup, StudentGroup
from api.models.schedule import Holiday
from dateutil.rrule import DAILY, rrule
from rest_framework.exceptions import ValidationError


def is_holiday(day):  # TODO: исправить
    year = day.year
    holidays = {
        "new_year": datetime.date(year, 1, 1),
        "women_day": datetime.date(year, 3, 8),
        "navruz": datetime.date(year, 3, 21),
        "memory_day": datetime.date(year, 5, 9),
        "independance_day": datetime.date(year, 9, 1),
        "teachers_day": datetime.date(year, 10, 1),
        "constitution_day": datetime.date(year, 12, 8),
    }
    return day in holidays.values() or day.weekday() == 6


def module_week_type_iterator(module_course_list: List[List]):
    i = 0
    while module_course_list:
        if module_course_list[i]:
            yield module_course_list[i].pop()
            i += 1
        else:
            del module_course_list[i]

        if i == len(module_course_list):
            i = 0


def generate_lesson_events(student_group: StudentGroup, start_date: datetime.datetime):
    module_course_list = []

    for module_student_group in ModuleStudentGroup.objects.filter(group=student_group):
        module_course_list.append([])
        for module_week_type in ModuleWeekType.objects.filter(
            week_day__module_course__modulestudentgroup=module_student_group
        ):
            module_course_list[-1].append((module_week_type, module_student_group))
        module_course_list[-1].reverse()

    holidays = Holiday.objects.values_list("date", flat=True)

    intervals = LessonEventInterval.objects.all()
    current_date = start_date
    events = []

    iterator = module_week_type_iterator(module_course_list)

    while True:
        try:
            if not is_holiday(current_date) and current_date not in holidays:
                for interval in intervals:
                    module_week_type, module_student_group = next(iterator)
                    events.append(
                        LessonEvent(
                            module_student_group=module_student_group,
                            date=current_date,
                            interval=interval,
                            lesson=module_week_type,
                        )
                    )

                current_date += datetime.timedelta(days=1)
            else:
                current_date += datetime.timedelta(days=1)

        except StopIteration:
            break

    LessonEvent.objects.bulk_create(events)


def generate(students_group, start_date):
    holidays = Holiday.objects.values_list("date", flat=True)

    current_date = start_date
    event_intervals = LessonEventInterval.objects.all()
    events = []
    for module_student_group in students_group.modulestudentgroup_set.all():  # MSG
        for week in module_student_group.module_course.week_set.all():
            for module_week_type in week.moduleweektype_set.all():
                if not is_holiday(current_date) and current_date not in holidays:
                    for event_interval in event_intervals:
                        events.append(
                            LessonEvent(
                                module_student_group=module_student_group,
                                date=current_date,
                                interval=event_interval,
                                lesson=module_week_type,
                            )
                        )
                    current_date += datetime.timedelta(days=1)

                while is_holiday(current_date) or current_date in holidays:
                    current_date += datetime.timedelta(days=1)

    LessonEvent.objects.bulk_create(events)


def repeat(
    module_student_group,
    interval,
    lesson,
    room,
    start_date,
    end_date,
    days: List[int],
    *args,
    **kwargs
):
    rule = rrule(
        freq=DAILY,
        dtstart=start_date,
        until=end_date + datetime.timedelta(days=1),
        byweekday=days,
    )
    holidays = Holiday.objects.values_list("date", flat=True)

    events = []

    for dt in list(rule):
        date = dt.date()
        if date not in holidays or is_holiday(date):
            events.append(
                LessonEvent(
                    module_student_group=module_student_group,
                    date=date,
                    interval=interval,
                    lesson=lesson,
                    room=room,
                )
            )

    return LessonEvent.objects.bulk_create(events)
