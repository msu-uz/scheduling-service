from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'api'

    def ready(self):
        # import api.v1.discipline.signals
        import api.v1.groups.signals
        import api.v1.task.signals
        import api.v1.teacher.task_check.signals
