from api.models import Course, ModuleCourse, Department
from api.models.base import Base
from api.models.role import Student, Teacher
from django.db import models
import uuid


class StudentGroup(Base):
    name = models.CharField(max_length=256, verbose_name='Group name')
    department = models.ForeignKey(Department, on_delete=models.CASCADE, verbose_name='Кафедра', null=True, blank=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, null=True)
    uuid = models.UUIDField(default=uuid.uuid4)
    is_completed = models.BooleanField(default=False)

    class Meta:
        db_table = 'students_group'

    def get_students(self):
        return Student.objects.filter(studentgroupforming__group__uuid=self.uuid)

    def __str__(self):
        return self.name


class StudentGroupForming(models.Model):
    group = models.ForeignKey(StudentGroup, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.group.name}"


class ModuleStudentGroup(Base):
    group = models.ForeignKey(StudentGroup, on_delete=models.CASCADE, null=True)
    module_course = models.ForeignKey(ModuleCourse, on_delete=models.CASCADE, null=True)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, null=True)

    def get_students(self):
        return StudentGroup.get_students(self.group)
