from django.db import models
from api.models.base import Base


class ResourcesType(Base):
    name = models.TextField()


class Resource(Base):
    name = models.TextField()
    type = models.ForeignKey(ResourcesType, on_delete=models.CASCADE)