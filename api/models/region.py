from api.models.base import Base
from django.db import models
from api.models import Language


class City(Base):
    name = models.JSONField()

    class Meta:
        db_table = 'cities'


class District(Base):
    name = models.JSONField()
    city = models.ForeignKey(City, on_delete=models.CASCADE)

    class Meta:
        db_table = 'districts'
