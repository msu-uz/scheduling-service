from api.models import Base, Organization, Position, District, City, Language, Nationality
from django.db import models


class EmployeeRelative(Base):
    full_name = models.JSONField(max_length=255)
    birth_date = models.DateField()
    relative_level = models.JSONField(null=True)
    job_place = models.TextField()
    living_place = models.ForeignKey(District, on_delete=models.CASCADE, related_name='e_r_living_place')
    birth_place = models.ForeignKey(District, on_delete=models.CASCADE, related_name='e_r_birth_place')

    class Meta:
        db_table = 'employee_relatives'

    def __str__(self):
        return self.full_name['ru'] or self.full_name


class Employee(Base):
    """
    Названия полей отличается от полей в Е-кадре,но совпадают с моделем Applicant
    Applicant - E-Кадр
    image - photo
    second_name - middle_name
    passport_address - birth_place (FK -> District)
    living_address - living_address (FK -> District)

    """
    MALE = 1
    FEMALE = 2

    SEX_CHOICES = (
        (MALE, "мужской"),
        (FEMALE, "женский"),
    )
    image = models.URLField(null=True, blank=True)
    first_name = models.CharField(max_length=250, null=False, blank=False)
    second_name = models.CharField(max_length=250, null=False, blank=False)
    last_name = models.CharField(max_length=250, null=False, blank=False)
    birth_date = models.DateField(null=True)
    passport_number = models.CharField(max_length=255, unique=True, null=True)
    nationality = models.CharField(max_length=255, null=True)
    passport_address = models.ForeignKey(District, on_delete=models.CASCADE, related_name='b_place', null=True)
    living_place = models.ForeignKey(District, on_delete=models.CASCADE, related_name='l_place', null=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True)
    position = models.ForeignKey(Position, on_delete=models.CASCADE, null=True)
    training_date_from = models.DateField(null=True)
    training_date_to = models.DateField(null=True)
    sex = models.IntegerField(choices=SEX_CHOICES)
    phone = models.CharField(null=True, max_length=255)
    email = models.CharField(max_length=255, blank=True, null=True)
    university_name = models.CharField(null=False, max_length=300)
    specialization = models.CharField(null=True, max_length=300)
    university_study_start_date = models.DateField(null=True)
    university_study_end_date = models.DateField(null=True)
    level = models.JSONField(null=True)
    language = models.ForeignKey(Language, on_delete=models.CASCADE, null=True)
    qualification = models.CharField(null=True, max_length=300)
    mother_info = models.ForeignKey(EmployeeRelative, on_delete=models.CASCADE, related_name="mother_info", null=True)
    father_info = models.ForeignKey(EmployeeRelative, on_delete=models.CASCADE, related_name="father_info", null=True)
    ekadr_id = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return f"{self.first_name} {self.second_name} {self.last_name}"
