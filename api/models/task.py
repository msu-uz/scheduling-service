from django.db import models
from django.db.models import Sum

from api.models import Topic, ModuleCourse, ModuleWeekType, LessonEvent, TaskType, ModuleStudentGroup, Discipline
from api.models.base import Base
from api.models.role import Teacher, Student

# //////////////////////////TASK STATUS CHOICES //////////////////////

NEW = 0
UPDATED = 1
ACCEPTED = 2
DECLINED = 3

TASK_STATUS_CHOICES = (
    (NEW, "новый"),
    (UPDATED, "обновлён"),
    (ACCEPTED, "одобрен"),
    (DECLINED, "отклонён")
)


# /////////////////////////// TASK BASE ///////////////////////////////


class Difficulty(Base):
    name = models.JSONField()


class Type(Base):
    task_type = models.ForeignKey(TaskType, on_delete=models.CASCADE)
    difficulty = models.ForeignKey(Difficulty, on_delete=models.CASCADE)
    duration_seconds = models.IntegerField()


class Task(Base):
    status = models.IntegerField(choices=TASK_STATUS_CHOICES, default=0)
    type = models.ForeignKey(Type, on_delete=models.CASCADE)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    creator = models.ForeignKey(Teacher, on_delete=models.DO_NOTHING)
    question = models.JSONField()

    def get_review(self):
        return TaskReview.objects.filter(task_id=self.id)

    # TODO: signal on Update and Create to Create TaskHistory


class TaskReview(Base):
    accepted = models.BooleanField(null=False)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    reviewer = models.ForeignKey(Teacher, on_delete=models.DO_NOTHING)
    comment = models.TextField(null=True, blank=True)

    # TODO: signal on Update to Create TaskHistory


class TaskHistory(Base):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, null=True, blank=True)
    task_review = models.ForeignKey(TaskReview, on_delete=models.CASCADE, null=True, blank=True)
    status = models.IntegerField(choices=TASK_STATUS_CHOICES)


class Answer(Base):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    answer = models.JSONField()
    is_correct = models.BooleanField(default=False)


class TaskCollection(Base):
    module_week_type = models.ForeignKey(ModuleWeekType, on_delete=models.CASCADE)
    tasks = models.ManyToManyField(Task)
    deadline_duration = models.PositiveSmallIntegerField()
    is_active = models.BooleanField(default=True)
    teacher = models.ForeignKey(Teacher, on_delete=models.SET_NULL, null=True, blank=True)
    msg = models.ManyToManyField(ModuleStudentGroup, blank=True)
    parent = models.ForeignKey('TaskCollection', null=True, blank=True,
                               on_delete=models.SET_NULL, related_name='children')

    @property
    def is_parent(self):
        return self.parent is None

    @property
    def get_topics(self):
        return Topic.objects.filter(task__in=self.tasks.all()).distinct()

    @property
    def discipline(self):
        return self.module_week_type.week_day.module_course.discipline.id

    def get_types(self):
        return TaskType.objects.filter(type__task__in=self.tasks.all())


class TaskCollectionReceiver(Base):
    event = models.ForeignKey(LessonEvent, on_delete=models.CASCADE)
    task_collection = models.ForeignKey(TaskCollection, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    is_active = models.BooleanField(default=False)

    def get_total_duration(self):
        return self.task_collection.tasks.all().aggregate(Sum('type__duration_seconds'))['type__duration_seconds__sum']


class TaskAnswerCollection(Base):
    task_collection_receiver = models.OneToOneField(TaskCollectionReceiver, on_delete=models.CASCADE)
    deadline = models.DateTimeField(null=True)

    def get_tasks(self):
        return Task.objects.filter(taskcollection__taskcollectionreceiver__taskanswercollection=self.id)


class TaskAnswer(Base):
    task_answer_collection = models.ForeignKey(TaskAnswerCollection, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    is_test = models.BooleanField()
    chosen_variant = models.ForeignKey(Answer, on_delete=models.CASCADE, blank=True, null=True)
    plain_answer = models.TextField(blank=True, null=True)


class TaskAnswerReviewCollection(Base):
    task_answer_collection = models.OneToOneField(TaskAnswerCollection, on_delete=models.CASCADE)
    total_mark = models.PositiveSmallIntegerField(null=True, blank=True)
    reviewer = models.ForeignKey(Teacher, on_delete=models.CASCADE)


class TaskAnswerReview(Base):
    task_answer_review_collection = models.ForeignKey(TaskAnswerReviewCollection, on_delete=models.CASCADE)
    student_task_answer = models.OneToOneField(TaskAnswer, on_delete=models.CASCADE)
    is_correct = models.BooleanField(default=False)
    comment = models.TextField(null=True, blank=True)


# ////////////////////// HOME TASKS //////////////////////////


class HomeTask(Base):
    title = models.CharField(max_length=256)
    body = models.TextField()
    creator = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    module_course = models.ForeignKey(ModuleCourse, on_delete=models.CASCADE)


class HomeTaskReceiver(Base):
    home_task = models.ForeignKey(HomeTask, on_delete=models.CASCADE)
    receiver = models.ForeignKey(Student, on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()


class HomeTaskAnswer(Base):
    title = models.CharField(max_length=512, null=True, blank=True)
    task = models.OneToOneField(HomeTaskReceiver, on_delete=models.CASCADE)
    comment = models.TextField(null=True, blank=True)


class HomeTaskFiles(Base):
    file = models.FileField(upload_to='home_tasks/tasks/')
    home_task = models.ForeignKey(HomeTask, on_delete=models.CASCADE)


class HomeTaskAnswerFiles(Base):
    file = models.FileField(upload_to='home_tasks/answers/')
    home_task = models.ForeignKey(HomeTaskAnswer, on_delete=models.CASCADE)


class HomeTaskAnswerReview(Base):
    home_task_answer = models.OneToOneField(HomeTaskAnswer, on_delete=models.CASCADE)
    comment = models.TextField()
    mark = models.PositiveSmallIntegerField()
    reviewer = models.ForeignKey(Teacher, on_delete=models.CASCADE)
