from django.db import models
from django.contrib.auth import get_user_model

from api.models.schedule import LessonEvent
from api.models.base import Base
from api.models.catalog import Category

User = get_user_model()


class ModuleOfVideo(models.Model):
    name = models.JSONField()

    class Meta:
        db_table = 'moduleofvideo'


class TypeOfBook(models.Model):
    name = models.JSONField()


class LibraryBook(Base):
    title = models.JSONField()
    file = models.FileField(null=False, upload_to='librarybooks/files/')
    image = models.ImageField(null=True, blank=True, upload_to='librarybooks/imgs/')
    description = models.JSONField(null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    author = models.TextField(null=True, blank=True)
    book_type = models.ForeignKey(TypeOfBook, on_delete=models.CASCADE, null=True, blank=True)
    publish_date = models.DateField(null=True, blank=True)
    page_count = models.PositiveIntegerField(null=True, blank=True)
    publisher = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    class Meta:
        db_table = 'librarybook'


class VideoMaterial(Base):
    title = models.JSONField()
    description = models.JSONField()
    image = models.ImageField(null=False, upload_to='videomaterials/imgs/')
    video = models.FileField(null=False, upload_to='videomaterials/')
    module = models.ForeignKey(ModuleOfVideo, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    publisher = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    class Meta:
        db_table = 'videomaterials'


class VideoRecording(Base):
    title = models.JSONField()
    description = models.TextField(null=True, blank=True)
    url = models.URLField(verbose_name='ссылка на видеозапись', null=True, blank=True)
    module = models.ForeignKey(ModuleOfVideo, on_delete=models.CASCADE, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True)
    duration = models.CharField(max_length=50, null=True, blank=True)
    thumbnail = models.ImageField(null=True, blank=True)
    lesson_event = models.ForeignKey(LessonEvent, on_delete=models.CASCADE)

    class Meta:
        db_table = 'video_recording'


class LessonMaterial(Base):
    title = models.JSONField()
    file = models.FileField(upload_to='lesson_material/files/')
    lesson_event = models.ForeignKey("LessonEvent", on_delete=models.CASCADE)
    teacher = models.ManyToManyField("Teacher", blank=True)
    student = models.ManyToManyField("Student", blank=True)
    student_group = models.ManyToManyField("StudentGroup", blank=True)

    class Meta:
        db_table = 'lesson_material'


class LibraryShare(Base):
    library_book = models.ForeignKey(LibraryBook, on_delete=models.CASCADE, null=True)
    video_material = models.ForeignKey(VideoMaterial, on_delete=models.CASCADE, null=True)
    teacher = models.ForeignKey("Teacher", on_delete=models.CASCADE, verbose_name="Sender")
    students = models.ManyToManyField("Student", blank=True)
    student_groups = models.ManyToManyField("StudentGroup", blank=True)

    class Meta:
        db_table = 'library_share'
