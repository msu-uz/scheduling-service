from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    STUDENT_LEVEL = 1
    TEACHER_LEVEL = 2
    ADMISSION_BOARD_LEVEL = 3
    EDUCATION_OFFICE_UNITS_LEVEL = 4
    ADMINISTRATOR_LEVEL = 5
    LIBRARIAN_LEVEL = 6
    APPLICATION_LEVEL = 100

    ACCESS_LEVEL_CHOICES = (
        (STUDENT_LEVEL, "студент"),
        (TEACHER_LEVEL, "преподаватель"),
        (ADMISSION_BOARD_LEVEL, "приемная комиссия"),
        (EDUCATION_OFFICE_UNITS_LEVEL, "учебный отдел"),
        (ADMINISTRATOR_LEVEL, "администратор"),
        (LIBRARIAN_LEVEL, "библиотекарь"),
        (APPLICATION_LEVEL, "приложения"),
    )
    profile_image = models.ImageField(null=True, blank=True, upload_to='profile_images')
    middle_name = models.CharField(max_length=100, null=True, blank=True)
    phone = models.CharField(max_length=25)
    access_level = models.IntegerField(choices=ACCESS_LEVEL_CHOICES, default=1)

    def get_full_name(self) -> str:
        full_name = super().get_full_name()
        if self.middle_name:
            full_name = f"{full_name} {self.middle_name}"
        return full_name
