from django.db import models
from django.shortcuts import get_object_or_404

from api.models import base, User


class ApplicantsResultsList(base.Base):
    creator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def add_vote(self, positive, user):
        vote = ApplicantResultsListVote.objects.filter(user=user).last()
        if vote:
            vote.positive = positive
            vote.save()
        else:
            ApplicantResultsListVote(positive=positive, user=user, results_list=self).save()

    def add_applicant(self, applicant):
        applicant.results_list_id = self.id
        applicant.save()

    def remove_applicant(self, applicant_id):
        applicant = get_object_or_404(Applicant, pk=applicant_id)
        applicant.results_list_id = None
        applicant.save()


class ApplicantResultsListVote(base.Base):
    results_list = models.ForeignKey(ApplicantsResultsList, on_delete=models.CASCADE)
    positive = models.BooleanField(default=False, null=False)
    user = models.ForeignKey(User, null=False, on_delete=models.CASCADE)


class ApplicantParentInfo(base.Base):
    full_name = models.CharField(max_length=250)
    birth_date = models.DateField()
    birth_place = models.CharField(max_length=100)  # TODO: take from catalog
    job_place = models.CharField(max_length=100)
    job_address = models.CharField(max_length=300)
    phone_number = models.CharField(max_length=20)


class Applicant(base.Base):
    MALE = 1
    FEMALE = 2

    SEX_CHOICES = (
        (MALE, "мужской"),
        (FEMALE, "женский"),
    )

    image = models.ImageField(null=True, blank=True)
    first_name = models.CharField(max_length=250, null=False, blank=False)
    second_name = models.CharField(max_length=250, null=True, blank=True)
    last_name = models.CharField(max_length=250, null=False, blank=False)
    birth_date = models.DateField(null=True, blank=True)
    sex = models.IntegerField(choices=SEX_CHOICES, null=True, blank=True)
    passport_number = models.CharField(max_length=500, null=True, blank=True)
    birth_place = models.CharField(null=True, blank=True, max_length=300)
    passport_address = models.CharField(null=True, blank=True, max_length=300)
    living_address = models.CharField(null=True, blank=True, max_length=300)
    education_job = models.CharField(null=True, blank=True, max_length=100)
    university_name = models.CharField(null=True, blank=True, max_length=250)
    specialization = models.CharField(null=True, blank=True, max_length=250)
    university_study_start_year = models.DateField(null=True, blank=True)
    university_study_end_year = models.DateField(null=True, blank=True)
    university_foreign_language = models.CharField(max_length=300, null=True, blank=True)
    university_study_language = models.CharField(null=True, blank=True, max_length=100)
    job_place = models.CharField(null=True, blank=True, max_length=300)
    job_address = models.CharField(null=True, blank=True, max_length=300)
    war_ticket_info = models.TextField(null=True, blank=True)
    qualification = models.CharField(null=True, blank=True, max_length=200)
    mother_info = models.ForeignKey(ApplicantParentInfo, on_delete=models.CASCADE, related_name="mother_info", null=True, blank=True)
    father_info = models.ForeignKey(ApplicantParentInfo, on_delete=models.CASCADE, related_name="father_info", null=True, blank=True)
    results_list = models.ForeignKey(ApplicantsResultsList, on_delete=models.SET_NULL, null=True, blank=True)

    def get_review(self):
        return ApplicantReview.objects.filter(applicant_id=self.id)

    def accept(self, reviewer: User):
        ApplicantReview(accepted=True, applicant_id=self.id, reviewed_by=reviewer).save()

    def reject(self, reviewer: User):
        ApplicantReview(accepted=False, applicant_id=self.id, reviewed_by=reviewer).save()

    def __str__(self):
        full_name = f"{self.first_name} {self.last_name}"
        if self.second_name:
            full_name = f"{full_name} {self.second_name}"
        return full_name


class ApplicantReview(base.Base):
    accepted = models.BooleanField(null=False)
    applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE)
    reviewed_by = models.ForeignKey(User, on_delete=models.CASCADE)


class ApplicantExamResults(base.Base):
    applicant = models.OneToOneField(Applicant, on_delete=models.CASCADE)
    first_exam_points = models.FloatField(null=False)
    second_exam_points = models.FloatField(null=False)
