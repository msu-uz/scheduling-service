from api.models.base import Base
from django.db import models
from api.models import Language, CourseType, Department


class Discipline(Base):
    name = models.JSONField()
    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = 'disciplines'

    def __str__(self) -> str:
        if isinstance(self.name, dict):
            return next(iter(self.name.values()))
        return super().__str__()


class Section(Base):
    name = models.JSONField()
    discipline = models.ForeignKey(Discipline, on_delete=models.CASCADE)

    class Meta:
        db_table = 'sections'


class Topic(Base):
    discipline = models.ForeignKey(Discipline, on_delete=models.CASCADE)
    name = models.JSONField()
    content = models.JSONField()

    class Meta:
        db_table = 'topics'

    def __str__(self) -> str:
        if isinstance(self.name, dict):
            return next(iter(self.name.values()))
        return super().__str__()


class IntermediateControl(Base):
    discipline = models.ForeignKey(Discipline, on_delete=models.CASCADE)
    name = models.JSONField()
    content = models.JSONField()

    def get_topics(self):
        return Topic.objects.filter(intermediatecontroltopics__intermediate_control=self.id)


class IntermediateControlTopics(models.Model):
    intermediate_control = models.ForeignKey(IntermediateControl, on_delete=models.CASCADE)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)


class FinalControl(Base):
    discipline = models.ForeignKey(Discipline, on_delete=models.CASCADE)
    name = models.JSONField()
    content = models.JSONField()

    def get_topics(self):
        return Topic.objects.filter(finalcontroltopics__final_control=self.id)


class FinalControlTopics(models.Model):
    final_control = models.ForeignKey(FinalControl, on_delete=models.CASCADE)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)


class Course(Base):
    name = models.JSONField()
    course_type = models.ForeignKey(CourseType, on_delete=models.CASCADE)
    regulation = models.JSONField()
    anons = models.JSONField()
    duration = models.PositiveIntegerField(verbose_name='duration in weeks')

    class Meta:
        db_table = 'courses'


class ModuleCourse(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    discipline = models.ForeignKey(Discipline, on_delete=models.CASCADE)
    lecture_duration = models.PositiveIntegerField(verbose_name='Lecture duration in hour')
    practice_duration = models.PositiveIntegerField(verbose_name='Practice duration in hour')
    distantly_duration = models.PositiveIntegerField(verbose_name='Distantly duration in hour')


class Week(Base):
    name = models.JSONField()
    module_course = models.ForeignKey(ModuleCourse, on_delete=models.CASCADE)


class ModuleWeekControlDay(Base):
    intermediate_control = models.ForeignKey(IntermediateControl, on_delete=models.CASCADE)
    easy_question_count = models.PositiveIntegerField(default=0)
    medium_question_count = models.PositiveIntegerField(default=0)
    hard_question_count = models.PositiveIntegerField(default=0)
    deadline_duration = models.PositiveSmallIntegerField(default=0)


class ModuleWeekFinalControlDay(Base):
    final_control = models.ForeignKey(FinalControl, on_delete=models.CASCADE)
    easy_question_count = models.PositiveIntegerField(default=0)
    medium_question_count = models.PositiveIntegerField(default=0)
    hard_question_count = models.PositiveIntegerField(default=0)
    deadline_duration = models.PositiveSmallIntegerField(default=0)


class ModuleWeekLessonDay(Base):
    lesson = models.ForeignKey(Topic, on_delete=models.CASCADE)
    question_count = models.PositiveIntegerField(default=0)
    deadline_duration = models.PositiveSmallIntegerField(default=0)


class ModuleWeekType(Base):
    week_day = models.ForeignKey(Week, on_delete=models.CASCADE)
    lesson = models.OneToOneField(ModuleWeekLessonDay, on_delete=models.CASCADE, null=True, blank=True)
    int_control = models.OneToOneField(ModuleWeekControlDay, on_delete=models.CASCADE, null=True, blank=True)
    fin_control = models.OneToOneField(ModuleWeekFinalControlDay, on_delete=models.CASCADE, null=True, blank=True)

    @property
    def name(self):
        name = ""

        if self.lesson:
            name = self.lesson.lesson.name
        elif self.int_control:
            name = self.int_control.intermediate_control.name
        elif self.fin_control:
            name = self.fin_control.final_control.name

        return name
