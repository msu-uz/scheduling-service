from api.models.base import Base
from django.db import models
from api.models import Language, District
# from django_postgres_extensions.models.fields import JSONField
from mptt.models import TreeForeignKey, MPTTModel
from django.db.models import JSONField
from django.contrib.postgres.fields import jsonb


# City+ Области
# Disctrict+ Районы
# Nationality+ Национальности
# CatalogLanguages+ Владения языками
# postition+ Должности
# organization+ Организации
# task_type+ Типы задач
# library_element_type+ Типы элемента библиотеки
# library_element_category+ Категории элемента библиотеки
# course_type+ Типы курсов
# languages языки


class Nationality(Base):
    name = JSONField()

    class Meta:
        db_table = 'nationalities'


class CatalogLanguage(Base):
    name = JSONField()

    class Meta:
        db_table = 'category_languages'


class Position(Base):
    name = JSONField()

    class Meta:
        db_table = 'positions'


class Organization(Base):
    name = JSONField()
    district = models.ForeignKey(District, on_delete=models.CASCADE)

    class Meta:
        db_table = 'organizations'


class TaskType(Base):
    name = JSONField()

    class Meta:
        db_table = 'task_types'

    def __str__(self):
        if self.name['ru']:
            return self.name['ru'] or self.name['uz']
        else:
            return self.name


class LibraryElementType(Base):
    name = JSONField()

    class Meta:
        db_table = 'library_element_types'


class LibraryElementCategory(Base):
    name = JSONField()

    class Meta:
        db_table = 'library_element_categories'


class CourseType(Base):
    name = JSONField()

    class Meta:
        db_table = 'course_types'


class Category(MPTTModel):
    name = JSONField()
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    class Meta:
        db_table = 'category'
