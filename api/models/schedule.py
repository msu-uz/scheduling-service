from django.db import models

from django.db.models.signals import post_save

from django.dispatch import receiver

from api.models import Course, ModuleCourse, Topic, IntermediateControl, FinalControl
from api.models.base import Base


# /////////////////////////// LESSON EVENT /////////////////////////

class LessonEventInterval(Base):
    number = models.IntegerField()
    start_time = models.TimeField()
    end_time = models.TimeField()


class LessonEvent(Base):
    module_student_group = models.ForeignKey('ModuleStudentGroup', on_delete=models.CASCADE, null=True, blank=True)
    date = models.DateField()
    interval = models.ForeignKey(LessonEventInterval, on_delete=models.CASCADE, null=True, blank=True)
    lesson = models.ForeignKey('ModuleWeekType', on_delete=models.CASCADE, null=True, blank=True)
    room = models.ForeignKey('Resource', on_delete=models.CASCADE, null=True, blank=True)
    resource = models.ManyToManyField('Resource', blank=True, related_name='resource')
    teacher = models.ForeignKey('Teacher', on_delete=models.CASCADE, null=True, blank=True)

# ////////////////////////////////////////////////////////////////////////////////////////

# /////////////////////////// SPECIAL EVENT /////////////////////////


class SpecialEventType(Base):
    name = models.TextField()


class SpecialEvent(Base):
    date = models.DateField()
    type = models.ForeignKey(SpecialEventType, on_delete=models.CASCADE, null=True)
    start_time = models.TimeField()
    end_time = models.TimeField()
    description = models.TextField()


class Holiday(Base):
    """ Праздничный день. """
    name = models.JSONField(verbose_name="Название")
    date = models.DateField(verbose_name="Дата")
    description = models.JSONField(verbose_name="Описание", blank=True, null=True)

    class Meta:
        verbose_name = "Выходной"
        verbose_name_plural = "Выходные"


# //////////////////////////////////////////////////////////////////////////////////////////////

class EventType(Base):
    topic = models.OneToOneField(Topic, on_delete=models.CASCADE, null=True)
    intermediate_control = models.OneToOneField(IntermediateControl, on_delete=models.CASCADE, null=True)
    final_control = models.OneToOneField(FinalControl, on_delete=models.CASCADE, null=True)


class Event(Base):
    lesson = models.OneToOneField(LessonEvent, on_delete=models.CASCADE, null=True, blank=True)
    special = models.OneToOneField(SpecialEvent, on_delete=models.CASCADE, null=True, blank=True)


@receiver(post_save, sender=LessonEvent)
def my_handler(sender, **kwargs):
    Event.objects.create(lesson=kwargs['instance'])


@receiver(post_save, sender=SpecialEvent)
def my_handler(sender, **kwargs):
    Event.objects.create(special=kwargs['instance'])

