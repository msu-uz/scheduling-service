from api.models import Course
from api.models.base import Base
from api.models.discipline import ModuleCourse, Topic
from api.models.role import Student
from django.db import models


class CourseFeedbackQuestion(Base):
    text = models.JSONField()
    course = models.ForeignKey(Course, on_delete=models.CASCADE)


class CourseFeedbackAnswer(Base):
    text = models.JSONField()
    course_feedback_question = models.ForeignKey(CourseFeedbackQuestion,
                                                 on_delete=models.CASCADE)


class CourseFeedback(Base):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course_feedback_answer = models.ForeignKey(CourseFeedbackAnswer,
                                               on_delete=models.CASCADE)

    class Meta:
        unique_together = ("student", "course_feedback_answer")


class ModuleCourseFeedbackQuestion(Base):
    text = models.JSONField()
    module_course = models.ForeignKey(ModuleCourse, on_delete=models.CASCADE)


class ModuleCourseFeedbackAnswer(Base):
    text = models.JSONField()
    module_course_feedback_question = models.ForeignKey(
        ModuleCourseFeedbackQuestion, on_delete=models.CASCADE)


class ModuleCourseFeedback(Base):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    module_course_feedback_answer = models.ForeignKey(
        ModuleCourseFeedbackAnswer, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("student", "module_course_feedback_answer")


class CourseFeedbackComment(Base):
    text = models.TextField()
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
