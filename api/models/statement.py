from django.db import models
from django.db.models import Sum, Avg
from .schedule import Event
from .base import Base


class Journal(Base):
    module_student_group = models.ForeignKey("ModuleStudentGroup", on_delete=models.CASCADE)
    student = models.ForeignKey("Student", on_delete=models.CASCADE)
    score = models.PositiveSmallIntegerField(null=True, blank=True)
    event = models.ForeignKey("LessonEvent", on_delete=models.CASCADE)
    is_absent = models.BooleanField(default=False)

    def get_int_score(self, date, student, msg):
        scores = Journal.objects.filter(event__lesson__int_control__isnull=False,
                                        student_id=student, module_student_group=msg).aggregate(score=Avg('score'))
        return scores['score']

    def get_lesson_score(self, date, student, msg):
        scores = Journal.objects.filter(event__lesson__lesson__isnull=False,
                                        student_id=student, module_student_group=msg).aggregate(score=Avg('score'))
        # TODO: FIX DATE FILTER
        return scores['score']

    def get_final_score(self, date, student, msg):
        scores = Journal.objects.filter(event__lesson__fin_control__isnull=False,
                                        student_id=student, module_student_group=msg).aggregate(Avg('score'))
        return scores['score__avg']

    def get_failed_student(self):
        student = Journal.objects.filter(event__lesson__lesson__isnull=True, score__lte=55, student_id=self.student_id)
        return student

    def list_of_nonlesson_events(self, module_student_group):
        event = Event.objects.filter(journal__module_student_group=module_student_group,
                                     lesson__lesson__lesson__isnull=True)
        return event

    def get_all_lessons_scores(self, student):
        scores = Journal.objects.filter(student=student, event__lesson__lesson__isnull=False).all()
        return scores

    def get_all_int_scores(self, student):
        scores = Journal.objects.filter(student=student, event__lesson__int_control__isnull=False).all()
        return scores


class Retake(Base):
    retake_event = models.ForeignKey(Journal, on_delete=models.CASCADE, null=True)
    score = models.PositiveSmallIntegerField(null=True, blank=True)
    passed = models.BooleanField(default=False)
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    description = models.TextField(null=True)

    def get_active_retake(self):
        retake = Retake.objects.filter(score__isnull=True, retake_event__student_id=self.id)
        return retake

    def get_inactive_retake(self):
        retake = Retake.objects.filter(score__isnull=True, retake_event__student_id=self.id)
        return retake


class QualitativeAssessment(Base):
    name = models.JSONField()
    min_score = models.PositiveSmallIntegerField()
    max_score = models.PositiveSmallIntegerField()

    def get_qualitative_assessment(self, score):
        return QualitativeAssessment.objects.filter(min_score__lte=score, max_score__gte=score)
