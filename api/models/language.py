from api.models.base import Base
from django.db import models


class Language(Base):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=5)

    class Meta:
        db_table = 'languages'