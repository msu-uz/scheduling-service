from django.db import models

from api.models.base import Base


class Department(Base):
    name = models.JSONField()

    def __str__(self) -> str:
        if isinstance(self.name, dict):
            return next(iter(self.name.values()))
        return super().__str__()
