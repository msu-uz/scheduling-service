from datetime import datetime
from typing import Union

from django.db.models import Sum, Avg
from django.db.models.fields.files import ImageFieldFile
from django.db.models.query_utils import Q
from api.models.schedule import LessonEvent
from django.db import models
from api.models.base import Base
from api.models import User, CourseType, Discipline
from django.contrib.auth.models import Group


class Student(Base):
    user = models.OneToOneField('User', on_delete=models.CASCADE)
    applicant = models.OneToOneField("Applicant", on_delete=models.CASCADE, null=True, blank=True)
    employee = models.OneToOneField("Employee", on_delete=models.CASCADE, null=True, blank=True)
    course_type = models.ForeignKey("CourseType", on_delete=models.CASCADE)
    is_student = models.BooleanField(default=True)

    class Meta:
        db_table = 'students'

    def get_address(self):
        if self.applicant:
            from api.models import Applicant
            return Applicant.objects.filter(id=self.applicant.id).values_list('living_address', flat=True)[0]
        elif self.employee:
            from api.models import Employee
            return Employee.objects.filter(id=self.employee.id).values_list('living_place__name', flat=True)[0]
        return ""

    def get_education_job(self):
        if self.applicant:
            from api.models import Applicant
            return Applicant.objects.filter(id=self.applicant.id).values_list('education_job', flat=True)[0]
        elif self.employee:
            from api.models import Employee
            return Employee.objects.filter(id=self.employee.id).values_list('position__name', flat=True)[0]
        return ""

    @property
    def attendance(self):
        from api.models import StudentGroup
        forming = self.studentgroupforming_set.last()
        if forming:
            group = forming.group
            group_uuid = group.uuid
        else:
            return 0
        group = StudentGroup.objects.filter(uuid=group_uuid, is_completed=False).last()
        lesson_event_count = LessonEvent.objects.filter(module_student_group_id__in=group.modulestudentgroup_set.all().values_list('id'),
                                                        date__lte=datetime.today().date()).count()
        absent_count = self.journal_set.filter(is_absent=True).count()
        if lesson_event_count != 0:
            return ((lesson_event_count - absent_count) / lesson_event_count) * 100
        else:
            return lesson_event_count

    def module_scores(self, msg):
        home_task = self.hometaskreceiver_set\
            .filter(hometaskanswer__hometaskanswerreview__mark__isnull=False,
                    home_task__module_course__modulestudentgroup=msg)\
            .values('hometaskanswer__hometaskanswerreview__mark')\
            .aggregate(mark=Avg('hometaskanswer__hometaskanswerreview__mark'))['mark'] or 0
        lesson = self.taskcollectionreceiver_set\
            .filter(taskanswercollection__taskanswerreviewcollection__total_mark__isnull=False,
                    task_collection__module_week_type__lesson__isnull=False,
                    event__module_student_group_id=msg)\
            .values('taskanswercollection__taskanswerreviewcollection__total_mark')\
            .aggregate(mark=Avg('taskanswercollection__taskanswerreviewcollection__total_mark'))['mark'] or 0
        intermediate = self.taskcollectionreceiver_set\
            .filter(taskanswercollection__taskanswerreviewcollection__total_mark__isnull=False,
                    task_collection__module_week_type__int_control__isnull=False,
                    event__module_student_group_id=msg)\
            .values('taskanswercollection__taskanswerreviewcollection__total_mark')\
            .aggregate(mark=Avg('taskanswercollection__taskanswerreviewcollection__total_mark'))['mark'] or 0
        final = self.taskcollectionreceiver_set \
            .filter(taskanswercollection__taskanswerreviewcollection__total_mark__isnull=False,
                    task_collection__module_week_type__fin_control__isnull=False,
                    event__module_student_group_id=msg) \
            .values('taskanswercollection__taskanswerreviewcollection__total_mark') \
            .aggregate(mark=Avg('taskanswercollection__taskanswerreviewcollection__total_mark'))['mark'] or 0
        attendance = self.attendance
        total_score = 0.05 * attendance + 0.15 * lesson + 0.20 * intermediate + 0.50 * final + 0.10 * home_task
        return total_score

    def __str__(self):
        name = self.user.username

        if employee := self.employee:
            name = str(employee)
        elif applicant := self.applicant:
            name = str(applicant)
        else:
            name = self.user.get_full_name()

        return name

    @property
    def image(self) -> Union[ImageFieldFile, str, None]:  # иногда может возвращать строку
        if self.employee and self.employee.image:
            return self.employee.image
        elif self.applicant and self.applicant.image:
            return self.applicant.image
        elif self.user.profile_image:
            return self.user.profile_image

    @property
    def student_groups(self):
        from api.models.group import StudentGroup
        group = StudentGroup.objects.filter(studentgroupforming__student=self).last()
        if group:
            groups = StudentGroup.objects.filter(uuid=group.uuid)
        else:
            groups = StudentGroup.objects.none()
        return groups

    @property
    def age(self):
        if self.applicant:
            if self.applicant.birth_date:
                return int((datetime.now().date() - self.applicant.birth_date).days / 365.25)

        elif self.employee:
            if self.employee.birth_date:
                return int((datetime.now().date() - self.employee.birth_date).days / 365.25)


class Teacher(Base):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    employee = models.OneToOneField('Employee', on_delete=models.CASCADE, null=True, blank=True)
    disciplines = models.ManyToManyField(Discipline, blank=True, related_name='disciplines')
    can_create_test = models.BooleanField(default=False)
    can_check_test = models.BooleanField(default=False)

    class Meta:
        db_table = 'teachers'

    @property
    def teaching_load(self):
        """
        Количество ивентов, которые ещё не пройдены этим преподавателем.
        """

        return LessonEvent.objects.filter(
            Q(module_student_group__teacher=self) &
            (Q(date__gt=datetime.today()) |
             Q(date=datetime.today(),
               interval__start_time__gte=datetime.now().time()))).count()

    def __str__(self):
        name = self.user.username

        if employee := self.employee:
            name = " ".join([employee.last_name, employee.first_name, employee.second_name])
        elif self.user.first_name and self.user.last_name:
            name = " ".join([self.user.last_name, self.user.first_name, self.user.middle_name])

        return name


class AdmissionBoardUnit(Base):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'admission_board_units'


class EducationOfficeUnit(Base):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'education_office_units'


class Librarian(Base):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'librarian'
