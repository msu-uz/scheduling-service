from api.models.language import Language
from api.models.notification import Notification, NotificationTemplate, PivotNotification
from api.models.region import City, District
from api.models.user import User
from api.models.catalog import Nationality, Organization, Position, TaskType, CourseType, LibraryElementType, \
    LibraryElementCategory, CatalogLanguage, Category
from api.models.department import Department
from api.models.discipline import Topic, Section, Discipline, Course, ModuleCourse, Week, \
    IntermediateControl, IntermediateControlTopics, ModuleWeekControlDay, ModuleWeekLessonDay, FinalControlTopics, \
    FinalControl, ModuleWeekType
from api.models.schedule import Event, EventType
from api.models.statement import Journal
from api.models.group import StudentGroup, StudentGroupForming, ModuleStudentGroup
from api.models.library import LibraryBook, TypeOfBook, VideoMaterial, LessonMaterial
from api.models.schedule import LessonEvent, LessonEventInterval
from api.models.task import *
from api.models.employee import *
from api.models.applicant import *
from api.models.resources import *

__all__ = [
    'Language', 'City', 'District', 'User', 'Nationality',
    'Organization', 'Position', 'TaskType', 'CourseType',
    'LibraryElementCategory', 'LibraryElementType', 'CatalogLanguage', 'Category',
    'Discipline', 'Section', 'Topic', 'Course', 'ModuleCourse', 'Week', 'IntermediateControl',
    'IntermediateControlTopics', 'ModuleWeekLessonDay', 'ModuleWeekControlDay', 'ModuleWeekType',
    'Applicant', 'StudentGroup', 'StudentGroupForming', 'LibraryBook', 'VideoMaterial', 'TypeOfBook', 'Department',
    'ModuleWeekControlDay', 'HomeTaskReceiver', 'HomeTaskAnswerFiles', 'HomeTaskFiles', 'HomeTaskAnswerReview',
    'Applicant', 'StudentGroup', 'StudentGroupForming', 'LibraryBook', 'VideoMaterial', 'TypeOfBook',
    'Notification', 'PivotNotification', 'NotificationTemplate',
    'Type', 'Difficulty', 'Task', 'Answer', 'Teacher', 'TaskAnswer', 'TaskAnswerReview', 'HomeTask', 'HomeTaskAnswer',
    'LessonEvent', 'TaskCollection', 'ModuleStudentGroup', 'Employee', 'EmployeeRelative', 'Journal', 'Student',
    'Resource', 'LessonMaterial', 'TaskAnswerReviewCollection'
]
