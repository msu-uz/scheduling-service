from django.db import models

from api.models.user import User
from api.models.base import Base


class Notification(Base):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender', null=True)
    title = models.CharField(max_length=255)
    body = models.TextField(null=False)


class PivotNotification(Base):
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, default=None, null=True, blank=True,
                                 related_name='receiver_real')
    notification = models.ForeignKey(Notification, on_delete=models.CASCADE, default=None, null=True, blank=True)
    is_read = models.BooleanField(default=False)


class NotificationTemplate(Base):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender_template', null=True)
    title = models.CharField(max_length=255)
    body = models.TextField(null=False)
    receiver = models.ManyToManyField(User, blank=True)
    group = models.ManyToManyField('api.StudentGroup', blank=True)


class NotificationComment(Base, models.Model):
    comment = models.TextField()
    notification = models.ForeignKey(PivotNotification, on_delete=models.CASCADE)
    writer = models.ForeignKey(User, on_delete=models.CASCADE)

