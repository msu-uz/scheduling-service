FROM python:3.8.5

RUN apt-get update
RUN apt-get install -y netcat

RUN pip install poetry

RUN poetry config virtualenvs.create false

WORKDIR /code

COPY pyproject.toml .
COPY poetry.lock .

RUN poetry install

COPY . .

CMD bash entrypoint.sh
