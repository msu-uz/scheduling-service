#!/bin/sh

set -e

python3 manage.py collectstatic --noinput
python3 manage.py migrate

i=0
while ! nc -w 1 -z db 5432; do
  sleep 0.1;
  if ["$i" -gt 10]; then
    break;
  fi

  ((i++))
done;

while :;
  do exec gunicorn dist_learning.wsgi --bind=0.0.0.0:8000;
done;
