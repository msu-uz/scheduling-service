from django.apps import AppConfig


class PeriodicTaskConfig(AppConfig):
    name = 'periodic_task'
