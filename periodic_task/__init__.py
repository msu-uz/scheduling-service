default_app_config = 'periodic_task.apps.PeriodicTaskConfig'
from .celery import app as celery_app
#
__all__ = ('celery_app',)