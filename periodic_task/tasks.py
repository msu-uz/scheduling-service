from datetime import date
from datetime import datetime

from celery import shared_task

from api.models import TaskCollectionReceiver
from api.models.schedule import LessonEvent
from api.v1.student.task.receivers_generator import generator


@shared_task
def student_task_generator():
    for lesson_event in LessonEvent.objects.filter(date=date.today(), interval__end_time__lte=datetime.now().time()):
        generator(lesson_event=lesson_event)


@shared_task
def task_collection_receiver_deactivate(tcr_id: int) -> None:
    TaskCollectionReceiver.objects.filter(id=tcr_id).update(is_active=False)
