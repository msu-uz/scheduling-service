# Distance learning API
Restful API of distance learning system created by MSU students

## Getting started
Install modules
```
poetry install
```

Fill in `.env` file. For reference take a look at `example.env`
```
DEBUG=false
...
```

Activate the shell
```
poetry shell
```

Run migrations
```
python manage.py migrate
```

Run the server
```
python manage.py runserver
```

## Run in docker

```
export TAG=local
docker-compose build && docker-compose up
```
