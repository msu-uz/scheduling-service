# Distance learning API


## Schedule
### schedule/studentgroup/list?course='int:id'&search='string'  -GET
response:
```json
{
    "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
          "id": 2,
          "student_group": 2,
          "course": 1
        },
        {
          "id": 3,
          "student_group": 3,
          "course": 1
        },
        {
          "id": 6,
          "student_group": 4,
          "course": 1
        }
    ]
}
```
### studentgroup/<<int:id>>/update - PUT PATCH
request:
```json
{
    "course": "course_id",
    "start_date": "date"
}
```
response:

```json
{
    "name": "string",
    "course": "string",
    "course_type": "string"
}
```

### studentgroup/<<int:id>>/event/list?start='datetime'&end='datetime' - GET
расписание для группы studentgroup(множ-во LessonEvent)  в промежутке start - end <br>

response:

```json
{
    "results": [
        {
          "id": 2,
          "title": "Метематика",
          "start": "2020-11-19T17:20:22",
          "end": "2020-11-19T17:20:22",
          "teacher": "Иванов И.И",
          "room": "24 аудитория"
          
        },
        {
          "id": 15,
          "title": "Метематика",
          "start": "2020-11-19T17:20:22",
          "end": "2020-11-19T17:20:22",
          "teacher": "Иванов И.И",
          "room": "24 лабаратория"
          
        }
    ]
}
```

### /event/interval/list - GET
response:

```json
{
    "results": [
        {
          "id": 1,
          "number": "1",
          "start": "17:20:00",
          "end": "17:20:00"
          
        },
        {
          "id": 2,
          "number": "2",
          "start": "17:20:00",
          "end": "17:20:00"
          
        }
    ]
}
```

### /event/module/list - GET
response:

```json
{
    "results": [
        {
          "id": 1,
          "number": "1",
          "start": "17:20:00",
          "end": "17:20:00"
          
        },
        {
          "id": 2,
          "number": "2",
          "start": "17:20:00",
          "end": "17:20:00"
          
        }
    ]
}

```


### studentgroup/<<int:id>>/event/create - POST
расписание для группы studentgroup(множ-во LessonEvent)  в промежутке start - end <br>

request:

```json
{
      "id": 2,
      "title": "Метематика",
      "start": "2020-11-19T17:20:22",
      "end": "2020-11-19T17:20:22",
      "teacher": "Иванов И.И",
      "room": "24 аудитория"

}
```

response:

```json
{
    "results": [
        {
          "id": 2,
          "title": "Метематика",
          "start": "2020-11-19T17:20:22",
          "end": "2020-11-19T17:20:22",
          "teacher": "Иванов И.И",
          "room": "24 аудитория"
          
        },
        {
          "id": 15,
          "title": "Метематика",
          "start": "2020-11-19T17:20:22",
          "end": "2020-11-19T17:20:22",
          "teacher": "Иванов И.И",
          "room": "24 лабаратория"
          
        }
    ]
}
```